--  ||---------------------------------------------------------------------------- 
--  || Filename: XXFA_ASSET_ADD_STAGING.ctl
--  ||
--  || Description : SQL*Loader Control File for FA Load
--  ||
--  || Modification History
--  ||   Author               Date            Description
--  ||   -------------------- --------------- ------------------------------------
--  ||   K Nasskau            16-Dec-2019     Initial version
--  ||
--  ||---------------------------------------------------------------------------- 

LOAD DATA
INFILE '*'
APPEND
INTO TABLE XXFA_ASSET_ADD_STAGING
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
( 
   book_type_code
 , asset_type
 , description                     
 , category_1                      
 , category_2                      
 , category_3                      
 , units                           
 , cost
 , original_cost
 , location_1                      
 , location_2                      
 , location_3   
 , asset_key
 , cat_attribute1
 , cat_attribute2
 , cat_attribute3
 , cat_attribute4 char(4000)
 , cat_attribute5
 , cat_attribute7
 , cat_attribute8
 , exp_gl_segment1                 
 , exp_gl_segment2                 
 , exp_gl_segment3                 
 , exp_gl_segment4                 
 , exp_gl_segment5                 
 , clr_gl_segment1                 
 , clr_gl_segment2                 
 , clr_gl_segment3                 
 , clr_gl_segment4                 
 , clr_gl_segment5                 
 , date_placed_in_service          DATE "DD/MM/YYYY"
 , asset_number                    
 , asset_status                    
 , tag_number                      
 , serial_number                   
 , manufacturer_name               
 , model_number                    
 , employee_number                 
 , source_line_project_number
 , transaction_type_code
 , in_use                          
 , in_physical_inventory
 , ownership                       
 , bought                          
 , warranty_number                 
 , property_type                   
 , property_class                  
 , depreciation_reserve            
 , ytd_depreciation                
 , amortization_start_date         DATE "DD/MM/YYYY"
 , amortize_nbv                    
 , bonus_ytd_depreciation          
 , bonus_depreciation_reserve      
 , invoice_number                  
 , parent_asset_number             
 , distribution_set                
 , short_fiscal_year_flag          
 , conversion_date                 DATE "DD/MM/YYYY"
 , original_depr_start_date        DATE "DD/MM/YYYY"
 , group_asset                     
 , lease_number                    
 , depreciation_method             
 , life_in_months                  
 , basic_rate                      
 , adjusted_rate                   
 , prorate_convention              
 , bonus_rule                      
 , depreciate_flag
 , depreciation_limit_type         
 , depreciation_limit_amount       
 , depreciation_limit_percent      
 , salvage_type                    
 , salvage_value                   
 , salvage_value_percent           
 , unit_of_measure                 
 , production_capacity             
 , revaluation_amortization_basis  
 , revaluation_reserve    
 , unique_reference_number
 , load_sequence_id                "XXFA_ASSET_ADD_STAGING_S.nextval"
 , load_datetime                   SYSDATE
              
)