  /*
  ||----------------------------------------------------------------------------
  || Filename: assign_XXFA_NEW_ASSET_BK_LOAD.sql
  ||
  || Description : This assigns concurrent program to responsibility
  ||
  || Modification History
  ||   Author               Date            Description
  ||   -------------------- --------------- ------------------------------------
  ||   K Nasskau            Dec-2019        Initial version
  ||
  ||----------------------------------------------------------------------------
  */

  declare
  lar_responsibilities    xxfnd_installer.responsibility_table;
begin
  lar_responsibilities(1) := 'PW Assets Manager';
  --
  xxfnd_installer.assign_program(
    pv_program_sn        => 'XXFA_NEW_ASSET_BK_LOAD',
    pv_program_asn       => 'XXFA',
    pav_responsibilities => lar_responsibilities);
  --
end;
/
