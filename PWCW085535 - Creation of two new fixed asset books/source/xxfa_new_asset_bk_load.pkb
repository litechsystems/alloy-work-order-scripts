create or replace package body xxfa_new_asset_bk_load as
  /*
  || Package: xxfa_new_asset_bk_load
  ||
  || Description: This package body contains utility procedures and functions
  ||              for the Loading Assets
  ||
  || Modification History
  ||   Author               Date            Description
  ||   -------------------- --------------- -------------------------------------
  ||   K Nasskau            Dec-2019        Initial version
  ||
  */

  gcv_pkg_name        constant varchar2(30) := 'xxfa_new_asset_bk_load';  
  
  gcv_error           constant varchar2(01) := 'E';
  gcv_validated       constant varchar2(01) := 'V';
  gcv_new             constant varchar2(01) := 'N';
  gcv_loaded          constant varchar2(01) := 'L';
  
  gcv_ret_success     constant varchar2(01) := '0'; 
  gcv_ret_warning     constant varchar2(01) := '1'; 
  gcv_ret_error       constant varchar2(01) := '2'; 

  gv_retcode          varchar2(01) := gcv_ret_success; -- Default to Success - Completed
 
  /*
  || ---------------------------------------------------------------------------
  || Procedure:   log_msg
  || Description: This procedure uses the generic logging routines to write to
  ||   the concurrent request log (and custom table in non production 
  ||   environments
  || ---------------------------------------------------------------------------
  */
  procedure log_msg(
    pv_procedure varchar2,
    pv_message   varchar2)
  as
  begin
--    xxfnd_log.log_msg(
--      pv_package         => '', --gcv_pkg_name,
--      pv_procedure       => '', --pv_procedure,
--      pv_message         => pv_message);
    fnd_file.put_line(fnd_file.LOG, pv_message);  
  end log_msg;
  
  --
  --
  --
  function check_lookup (
      pv_lookup_type varchar2
    , pv_lookup_code varchar2
    ) return boolean
  is
     lv_procedure varchar2(30) := 'check_lookup';
     lv_exists    varchar2(01);
  begin
    select 'Y'
    into   lv_exists
    from   fa_lookups_b
    where  lookup_type = pv_lookup_type
    and    upper(lookup_code) = upper(pv_lookup_code);
--    log_msg(lv_procedure, 'Lookup Type ['||pv_lookup_type||'] Lookup Code ['||pv_lookup_code||']');
    return(TRUE);
  exception
    when no_data_found then
       return(FALSE);
  end check_lookup;

  --
  --
  --

  function append_comments (
    pv_comments varchar2
  , pv_text     varchar2
  ) return varchar2
  is
    lv_return_comments varchar2(4000) := '';
  begin
     case pv_comments
       when null then lv_return_comments := pv_text;
       when ''   then lv_return_comments := pv_text;
       else lv_return_comments := pv_comments ||', '|| pv_text;
     end case;  
     return(lv_return_comments);
  end append_comments;

  --
  --
  --

  procedure process_assets (
    pv_errbuf     out varchar2
  , pv_retcode    out varchar2
  , pv_book_type_code  in  varchar2
  , pv_action          in  varchar2
  )
  as
  begin
     if pv_action = 'VALIDATE' then
        validate_assets(pv_book_type_code);
     elsif pv_action = 'LOAD' then
        load_assets(pv_book_type_code);
     elsif pv_action = 'DELETE' then
        delete_staging_records(pv_book_type_code);
     end if;
     pv_retcode := gv_retcode;
  exception
     when others then
       pv_retcode := gcv_ret_error;
       pv_errbuf  := SQLERRM;
  end process_assets;
  
  --
  --
  --

  procedure delete_staging_records (
    pv_book_type_code  in  varchar2
  )
  as
    lv_procedure         varchar2(30) := 'delete_staging_records';
  begin
    log_msg(lv_procedure, 'Start');
    log_msg(lv_procedure, 'pv_book_type_code ['||pv_book_type_code||']');
    
    delete
    from xxfa_asset_add_staging
    where book_type_code = pv_book_type_code;
    
    log_msg(lv_procedure, 'Deleted ['|| sql%rowcount|| '] records.');
  end delete_staging_records;
  
  --
  --
  --
  
  procedure validate_assets (
    pv_book_type_code  in  varchar2
  )
  as
    
    lv_procedure         varchar2(30) := 'validate_assets';
    ln_location_ccid     number;
    ln_exp_gl_ccid       number;
    ln_clr_gl_ccid       number;
    ln_assigned_id       number;
    ln_warranty_id       number;
    ln_group_asset_id    number;
    ln_category_ccid     number;
    lv_deprn_name        fa_methods.name%type;
    lv_bonus_rule_desc   fa_bonus_rules.description%type;
    ln_asset_id          number;
    ln_asset_key_ccid    number;
    ln_project_id        number;
    lv_book_class        fa_book_controls.book_class%type;
    lv_dist_source_book  fa_book_controls.distribution_source_book%type;
    
    lb_error           boolean;
    lv_import_comments varchar2(4000);
    ln_error_count     number := 0;
    ln_record_count    number := 0;
    lv_import_status   varchar2(01);
    lv_dummy           varchar2(01);
    
     -- All records that E(rror) or N(ew)  
     cursor c_assets 
     is
       select *
       from   xxfa_asset_add_staging
       where  nvl(import_status,gcv_new) in (gcv_new, gcv_error)
       and    book_type_code = pv_book_type_code
       for update;
       
  begin
  
    lb_error := FALSE; -- Will be changed to TRUE if any validation fails
  
    log_msg(lv_procedure, 'lv_procedure      ['||lv_procedure||']');
    log_msg(lv_procedure, 'pv_book_type_code ['||pv_book_type_code||']');
    
    -- Reset load - unless there are loaded records
    update xxfa_asset_add_staging
    set    import_comments    = null
         , import_status      = gcv_new
         , import_msg_data    = null
         , import_msg_count   = 0
    where  book_type_code = pv_book_type_code
    and not exists (select 'x'
                    from xxfa_asset_add_staging
                    where nvl(import_status,gcv_new) in (gcv_loaded));

    -- Determine the book classification for the book type.  
    -- Expect CORPORATE or TAX, if not, then error
    begin
        select book_class
             , distribution_source_book
        into   lv_book_class
             , lv_dist_source_book
        from   fa_book_controls bc
        where  bc.book_type_code = pv_book_type_code;    
        
        log_msg(lv_procedure, 'lv_book_class ['||lv_book_class||']');
        log_msg(lv_procedure, 'lv_dist_source_book ['||lv_dist_source_book||']');
        
        if lv_book_class not in ('CORPORATE','TAX') then
            lb_error := TRUE;
            lv_import_comments   := 'Error: Book Type Code ['||pv_book_type_code||'] has book class ['||lv_book_class||'].  Must be CORPORATE or TAX classification.';
        end if;
    exception
      when no_data_found then
        lb_error := TRUE;
        lv_import_comments   := 'Error: Could not determine Book Class for Book Type Code ['||pv_book_type_code||']';    -- Cumulative comments as errors detected during validation  
    end;
  
  if not lb_error then
  
    for datarec in c_assets loop
    
      -- Initialise loop variables
      lb_error             := FALSE; -- Will be changed to TRUE if any validation fails
      lv_import_comments   := '';    -- Cumulative comments as errors detected during validation  
      lv_import_status     := '';
      ln_location_ccid     := null;
      ln_exp_gl_ccid       := null;
      ln_clr_gl_ccid       := null;
      ln_assigned_id       := null;
      ln_warranty_id       := null;
      ln_group_asset_id    := null;
      ln_category_ccid     := null;
      ln_asset_key_ccid    := null;
      
      ln_record_count      := ln_record_count + 1;

      -- Validate that mandatory fields have been supplied
      -- asset_type_rec_type	asset_type
      -- asset_desc_rec_type	description
      -- asset_desc_rec_type	current_units
      
-- If asset number isn't supplied, then should default to asset_id
--      if datarec.asset_number is null then
--         lb_error := TRUE;
--         lv_import_comments := append_comments(lv_import_comments, 'Missing Asset Number');
--      end if;

      if datarec.asset_type is null then
         lb_error := TRUE;
         lv_import_comments := append_comments(lv_import_comments, 'Missing Asset Type');
      end if;
      if datarec.description is null then
         lb_error := TRUE;
         lv_import_comments := append_comments(lv_import_comments, 'Missing Description ');
      end if;
      if datarec.units is null then
         lb_error := TRUE;
         lv_import_comments := append_comments(lv_import_comments, 'Missing Units');
      end if;
      if datarec.unique_reference_number is null then
         lb_error := TRUE;
         lv_import_comments := append_comments(lv_import_comments, 'Missing Unique Reference Number');
      end if;
      
      -- Check that either Serial Number of Cat_Attribute8 exists, if not then flag as an error .
      if  datarec.serial_number is null
      and datarec.cat_attribute8 is null 
      then
         lb_error := TRUE;
         lv_import_comments := append_comments(lv_import_comments, 'Missing both Serial Number and Cat_attribute8 - at least one is required to uniquely identify the Asset');
      end if;
      
      -- Check Asset Number doesn't already exist
      if datarec.asset_number is not null then
        begin
          ln_asset_id := null;
          select asset_id
          into   ln_asset_id
          from   fa_additions_b
          where  asset_number = datarec.asset_number;

          if ln_asset_id is not null then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments, 'Asset Number ['||datarec.asset_number||'] already exists! ');
          end if;
        exception
          when no_data_found then
             null;
        end;
      end if;
      
      -- If book type book classification is TAX, then the asset should already exist in the corresponding CORPORATE book
      -- which is the distribution_source_book in fa_books
      if lv_book_class = 'TAX' then
        begin
          ln_asset_id := null;

          -- Get the unique reference number from the staging table for the CORPORATE book 
          -- If the CORPORATE book hasn't been loaded then this won't be found and an error
          -- should be raised.
          select loaded_asset_id
          into   ln_asset_id
          from   xxfa_asset_add_staging s
          where  s.unique_reference_number = datarec.unique_reference_number
          and    s.book_type_code = lv_dist_source_book; --in (z
--                     select book_type_code
--                     from   fa_book_controls bc
--                     where  bc.book_type_code = s.book_type_code
--                     and    bc.book_class = 'CORPORATE'
--                     );
                               
          log_msg(lv_procedure, 'datarec.unique_reference_number ['||datarec.unique_reference_number||'] asset_id ['||ln_asset_id||'] in lv_dist_source_book ['||lv_dist_source_book||']');

          if ln_asset_id is null then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments, 'Asset Id not yet assigned to asset record in staging table source book ['||lv_dist_source_book||'] for unique reference ['||datarec.unique_reference_number||'].  Please process source book first.');
          end if;
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments, 'Asset not found in distribution source book ['||lv_dist_source_book||'] with unique reference ['||datarec.unique_reference_number||']');
        end;
      end if;

      -- Validate location  (Optional) - 3 segments configured
      -- Check any supplied, if one missed then won't be found and exception will be raised.
      if datarec.location_1 is not null
      or datarec.location_2 is not null
      or datarec.location_3 is not null
      then
        begin
          select location_id
          into   ln_location_ccid
          from   fa_locations
          where  segment1 = datarec.location_2
          and    segment2 = datarec.location_3
          and    segment3 = datarec.location_1;
          --log_msg(lv_procedure, 'ln_location_ccid ['||ln_location_ccid||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments, 'Location not found: '||datarec.location_1||'.'||datarec.location_2||'.'||datarec.location_3||']');
        end;
      end if;
      
      -- Validate Asset Category DFF (Against Asset in FA_ADDITIONS_B)
      -- Note, only attribute6 is validated against pre-defined values, other attributes
      -- are free text.
--      if datarec.cat_attribute6 is not null then
--        declare
--          lv_dummy varchar2(01);
--        begin
--          select 'x'
--          into   lv_dummy
--          from   fnd_flex_value_sets ffvs
--               , fnd_flex_values ffv
--          where  ffvs.flex_value_set_name = 'XXPW_FA_PA_QTY_VS'
--          and    ffvs.flex_value_set_id = ffv.flex_value_set_id
--          and    ffv.flex_value = datarec.cat_attribute6;
--        exception
--          when no_data_found then
--             lb_error := TRUE;
--             lv_import_comments := append_comments(lv_import_comments, 'Unrecognised value for cat_attribute6: '||datarec.cat_attribute6||']');
--        end;
--      end if;

      -- Validate category (Optional) 
      -- category_1, category_2, category_3
      -- Check any supplied, if one missed then won't be found and exception will be raised.
      if datarec.category_1 is not null
      or datarec.category_2 is not null
      or datarec.category_3 is not null
      then
        begin
          select category_id 
          into   ln_category_ccid
          from   fa_categories_b
          where  segment1 = datarec.category_1
          and    segment2 = datarec.category_2
          and    segment3 = datarec.category_3;
          --log_msg(lv_procedure, 'ln_category_ccid ['||ln_category_ccid||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments, 'Category not found [ '||datarec.category_1||'.'||datarec.category_2||'.'||datarec.category_3||']');
        end;
       end if;
       
      -- Validate expense gl segments
      -- Check any supplied, if one missed then won't be found and exception will be raised.
      if datarec.exp_gl_segment1 is not null
      or datarec.exp_gl_segment2 is not null
      or datarec.exp_gl_segment3 is not null
      or datarec.exp_gl_segment4 is not null
      or datarec.exp_gl_segment5 is not null
      then
        begin
          select code_combination_id
          into   ln_exp_gl_ccid
          from   gl_code_combinations 
          where  segment1 = datarec.exp_gl_segment1
          and    segment2 = datarec.exp_gl_segment2
          and    segment3 = datarec.exp_gl_segment3
          and    segment4 = datarec.exp_gl_segment4
          and    segment5 = datarec.exp_gl_segment5;
          --log_msg(lv_procedure, 'ln_exp_gl_ccid ['||ln_exp_gl_ccid||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Expense CCID not found [ '||datarec.exp_gl_segment1
                                                                                              ||'.'||datarec.exp_gl_segment2
                                                                                              ||'.'||datarec.exp_gl_segment3
                                                                                              ||'.'||datarec.exp_gl_segment4
                                                                                              ||'.'||datarec.exp_gl_segment5||']');
        end;
      end if;
      
      -- Validate clearing gl segments
      if datarec.clr_gl_segment1 is not null
      or datarec.clr_gl_segment2 is not null
      or datarec.clr_gl_segment3 is not null
      or datarec.clr_gl_segment4 is not null
      or datarec.clr_gl_segment5 is not null
      then
        begin
          select code_combination_id
          into   ln_clr_gl_ccid
          from   gl_code_combinations 
          where  segment1 = datarec.clr_gl_segment1
          and    segment2 = datarec.clr_gl_segment2
          and    segment3 = datarec.clr_gl_segment3
          and    segment4 = datarec.clr_gl_segment4
          and    segment5 = datarec.clr_gl_segment5;
          --log_msg(lv_procedure, 'ln_clr_gl_ccid ['||ln_clr_gl_ccid||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Clearing CCID not found [ '||datarec.clr_gl_segment1
                                                                                               ||'.'||datarec.clr_gl_segment2
                                                                                               ||'.'||datarec.clr_gl_segment3
                                                                                               ||'.'||datarec.clr_gl_segment4
                                                                                               ||'.'||datarec.clr_gl_segment5||']');
        end;
      end if;
        
      -- Validate source_line_project_number
      if datarec.source_line_project_number is not null then
        begin
          select project_id
          into   ln_project_id
          from   pa_projects_all
          where  segment1 = datarec.source_line_project_number;
          --log_msg(lv_procedure, 'ln_project_id ['||ln_project_id||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Source Line Project Number not found [ '||datarec.source_line_project_number||']');
        end;
      end if;
      
      -- Validate employee_number
      if datarec.employee_number is not null then
        begin
          select employee_id
          into   ln_assigned_id
          from   fa_employees
          where  employee_number = datarec.employee_number;
          --log_msg(lv_procedure, 'ln_assigned_id ['||ln_assigned_id||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Employee Number not found [ '||datarec.employee_number||']');
        end;
      end if;

      -- Validate in_use
      if datarec.in_use is not null then
        if upper(datarec.in_use) not in ('YES','NO') then
           lb_error := TRUE;
           lv_import_comments := append_comments(lv_import_comments,  'In Use Flag not recognisedi [ '||datarec.in_use||']');
        end if;
      end if;

      -- Validate in_physical_inventory
      if datarec.in_physical_inventory is not null then
        if upper(datarec.in_physical_inventory) not in ('YES','NO') then
           lb_error := TRUE;
           lv_import_comments := append_comments(lv_import_comments,  'In Physical Inventory not recognisedi [ '||datarec.in_physical_inventory||']');
        end if;
      end if;

      -- Validate depreciate_flag
      if datarec.depreciate_flag is not null then
        if upper(datarec.depreciate_flag) not in ('YES','NO') then
           lb_error := TRUE;
           lv_import_comments := append_comments(lv_import_comments,  'Depreciate Flag not recognisedi [ '||datarec.depreciate_flag||']');
        end if;
      end if;

      -- Validate transaction_type_code -  TRANSACTION SUBTYPE
      if datarec.ownership is not null then
        if NOT check_lookup('TRANSACTION SUBTYPE',datarec.transaction_type_code) then
          lb_error := TRUE;
          lv_import_comments := append_comments(lv_import_comments,  'Transaction Type Code not found [ '||datarec.transaction_type_code||']');
        end if;
      end if;
      
      -- Validate ownership -  OWN or LEASED
      if datarec.ownership is not null then
        if NOT check_lookup('OWNLEASE',datarec.ownership) then
          lb_error := TRUE;
          lv_import_comments := append_comments(lv_import_comments,  'Ownership not found [ '||datarec.ownership||']');
        end if;
      end if;
      
      -- Validate bought - NEW or USED - is there a lookup for this?????
      if datarec.bought is not null then
        if NOT check_lookup('NEWUSE',datarec.bought) then
          lb_error := TRUE;
          lv_import_comments := append_comments(lv_import_comments,  'Bought flag not recognised [ '||datarec.bought||']');
        end if;
      end if;
      

      -- Validate Asset Key
--      if datarec.bought is not null then
--        begin
--          select code_combination_id
--          into   ln_asset_key_ccid
--          from   fa_asset_keywords
--          where  segment1 = upper(datarec.bought);
--          --log_msg(lv_procedure, 'ln_asset_key_ccid ['||ln_asset_key_ccid||']');
--        exception
--          when no_data_found then
--             lb_error := TRUE;
--             lv_import_comments := append_comments(lv_import_comments,  'Asset Key (bought) not found [ '||upper(datarec.bought)||']');
--        end;
--      end if;
      if datarec.asset_key is not null then
        begin
          select code_combination_id
          into   ln_asset_key_ccid
          from   fa_asset_keywords
          where  segment1 = upper(datarec.asset_key);
          --log_msg(lv_procedure, 'ln_asset_key_ccid ['||ln_asset_key_ccid||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Asset Key not found [ '||upper(datarec.asset_key)||']');
        end;
      end if;
      

      -- Validate asset_type - MANDATORY
      if NOT check_lookup('ASSET TYPE',datarec.asset_type) then
        lb_error := TRUE;
        lv_import_comments := append_comments(lv_import_comments,  'Asset Type not recognised [ '||datarec.asset_type||']');
      end if;

      -- Validate warranty_number
      if datarec.warranty_number is not null then
        begin
          select warranty_id
          into   ln_warranty_id
          from   fa_warranties
          where  warranty_number = datarec.warranty_number;
          --log_msg(lv_procedure, 'ln_warranty_id ['||ln_warranty_id||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Warranty Number not found [ '||datarec.warranty_number||']');
        end;
      end if;

      -- Validate property_type - PERSONAL, REAL
      if datarec.property_type is not null then
        if NOT check_lookup('PROPERTY TYPE',datarec.property_type) then
          lb_error := TRUE;
          lv_import_comments := append_comments(lv_import_comments,  'Property Type not recognised [ '||datarec.property_type||']');
        end if;
      end if;
      
      -- Validate group_asset --- ???? What will be provided in extract?
      if datarec.group_asset is not null then
        begin
          select group_asset_id
          into   ln_group_asset_id
          from   fa_group_assets
          where  segment1 = datarec.group_asset;
          --log_msg(lv_procedure, 'ln_group_asset_id ['||ln_group_asset_id||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Group Asset not found [ '||datarec.group_asset||']');
        end;
      end if;

      if (datarec.depreciation_method is not null and datarec.life_in_months is     null)
      or (datarec.depreciation_method is     null and datarec.life_in_months is not null)
      then
         lb_error := TRUE;
         lv_import_comments := append_comments(lv_import_comments,  ' Depreciation Method Code / Life In Months must be supplied together ['||datarec.depreciation_method||' / '||datarec.life_in_months||']');
      end if;
      
      -- Validate depreciation_method
      if datarec.depreciation_method is not null and
         datarec.life_in_months is not null
      then
        begin
          select name
          into   lv_deprn_name
          from   fa_methods
          where  method_code    = datarec.depreciation_method
          and    life_in_months = datarec.life_in_months;
          --log_msg(lv_procedure, 'lv_deprn_name ['||lv_deprn_name||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Depreciation Method Code / Life In '||datarec.depreciation_method||' / '||datarec.life_in_months||']');
        end;
      end if;
      
      -- Validate bonus_rule
      if datarec.bonus_rule is not null then
        begin
          select description
          into   lv_bonus_rule_desc
          from   fa_bonus_rules
          where  bonus_rule = datarec.bonus_rule;
          --log_msg(lv_procedure, 'lv_bonus_rule_desc ['||lv_bonus_rule_desc||']');
        exception
          when no_data_found then
             lb_error := TRUE;
             lv_import_comments := append_comments(lv_import_comments,  'Bonus Rule not recognised [ '||datarec.bonus_rule||']');
        end;
      end if;
       
      -- Validate asset_status
      if datarec.asset_status is not null then
        if NOT check_lookup('ASSET STATUS',datarec.asset_status) then
          lb_error := TRUE;
          lv_import_comments := append_comments(lv_import_comments,  'Asset Status not recognised [ '||datarec.asset_status||']');
        end if;
      end if;
      
      -- Validate depreciation_limit_type
      if datarec.depreciation_limit_type is not null then
        if NOT check_lookup('SALVAGE_DEPRN_LIMIT_TYPE',datarec.depreciation_limit_type) then
          lb_error := TRUE;
          lv_import_comments := append_comments(lv_import_comments,  'Depreciation Limit Type not recognised [ '||datarec.depreciation_limit_type||']');
        end if;
      end if;
      
      -- Tally the errors detected
      if lb_error then
        ln_error_count := ln_error_count + 1;
      end if;
      
      lv_import_status := case lb_error
                                  when TRUE then gcv_error
                                  else           gcv_validated
                          end;
                          
      -- Update the validated records with the result
      update xxfa_asset_add_staging
      set    import_status   = lv_import_status
           , import_comments = lv_import_comments
           , location_ccid   = ln_location_ccid 
           , exp_gl_ccid     = ln_exp_gl_ccid   
           , clr_gl_ccid     = ln_clr_gl_ccid   
           , assigned_id     = ln_assigned_id   
           , warranty_id     = ln_warranty_id   
           , group_asset_id  = ln_group_asset_id
           , category_ccid   = ln_category_ccid    
           , asset_key_ccid  = ln_asset_key_ccid
           , project_id      = ln_project_id
           , loaded_asset_id = ln_asset_id
      where current of c_assets;

      if lv_import_comments is not null then
        log_msg(lv_procedure, rpad('>LoadSeqId ['||datarec.load_sequence_id||']',30,' ')
                           || rpad('>Asset No ['||datarec.asset_number||']',25,' ')
                           || lv_import_comments);
      end if;
      
    end loop;

  end if;  
  
    log_msg(lv_procedure, ' ');
    log_msg(lv_procedure, '-------------------------------------------------------');
    log_msg(lv_procedure, 'Validate Assets - Completed - Results:');
    log_msg(lv_procedure, 'Number of records read:     ['||ln_record_count||']');
    log_msg(lv_procedure, 'Number of records in error: ['||ln_error_count||']');
    if ln_error_count = 0 then
      log_msg(lv_procedure, 'Validation Successful - Ready to Load');
      gv_retcode := gcv_ret_success;
    else
      log_msg(lv_procedure, 'Validation Failed - Please review the Log!');
      gv_retcode := gcv_ret_warning;
    end if;
    log_msg(lv_procedure, '-------------------------------------------------------');
    

  end validate_assets;

  ---
  ---
  ---
  
  procedure load_assets (
    pv_book_type_code  in  varchar2
  )
  as
       -- All records that are V(alidated) and where no E(rror) or N(ew) records exist
     cursor c_assets 
     is
       select *
       from   xxfa_asset_add_staging
       where  nvl(import_status,gcv_new) = gcv_validated
       and    book_type_code = pv_book_type_code
       and    not exists (select 'X' 
                          from   xxfa_asset_add_staging 
                          where  nvl(import_status,gcv_new) in (gcv_error, gcv_new)
                          and    book_type_code = pv_book_type_code
                          )
       for update;

-- , import_status                   VARCHAR2(01) - V - Validated, N - New, E - Error
-- , import_msg_data                 VARCHAR2(4000)
-- , import_msg_count                NUMBER
  
   l_trans_rec                FA_API_TYPES.trans_rec_type;
   l_dist_trans_rec           FA_API_TYPES.trans_rec_type;
   l_asset_hdr_rec            FA_API_TYPES.asset_hdr_rec_type;
   l_asset_desc_rec           FA_API_TYPES.asset_desc_rec_type;
   l_asset_cat_rec            FA_API_TYPES.asset_cat_rec_type;
   l_asset_type_rec           FA_API_TYPES.asset_type_rec_type;
   l_asset_hierarchy_rec      FA_API_TYPES.asset_hierarchy_rec_type;
   l_asset_fin_rec            FA_API_TYPES.asset_fin_rec_type;
   l_asset_deprn_rec          FA_API_TYPES.asset_deprn_rec_type;
   l_asset_dist_rec           FA_API_TYPES.asset_dist_rec_type;
   l_asset_dist_tbl           FA_API_TYPES.asset_dist_tbl_type;
   l_inv_tbl                  FA_API_TYPES.inv_tbl_type;
   l_inv_rate_tbl             FA_API_TYPES.inv_rate_tbl_type;
   l_asset_inv_type_rec       FA_API_TYPES.inv_rec_type;
   l_lease_details_rec        FA_API_TYPES.lease_details_rec_type;
   
   lv_procedure               varchar2(30) := 'load_assets';
   lv_import_comments         varchar2(4000);
   lv_import_status           varchar2(01);
   l_return_status            varchar2(1);     
   l_mesg_count               number;
   l_mesg                     varchar2(4000);
   ln_error_count             number := 0;
   ln_record_count            number := 0;

  begin
  
    for datarec in c_assets loop

      ln_record_count    := ln_record_count + 1;
      lv_import_comments := '';
      lv_import_status   := '';
    
      -- Assign values to records....
      
      ---- Assign Category
      l_asset_cat_rec.category_id                         := datarec.category_ccid;
      l_asset_cat_rec.desc_flex.attribute1                := substr(datarec.cat_attribute1,1,150);
      l_asset_cat_rec.desc_flex.attribute2                := substr(datarec.cat_attribute2,1,150);
      l_asset_cat_rec.desc_flex.attribute3                := substr(datarec.cat_attribute3,1,150);
      l_asset_cat_rec.desc_flex.attribute4                := substr(datarec.cat_attribute4,1,150);
      l_asset_cat_rec.desc_flex.attribute5                := substr(datarec.cat_attribute5,1,150);
      l_asset_cat_rec.desc_flex.attribute7                := substr(datarec.cat_attribute7,1,150);
      l_asset_cat_rec.desc_flex.attribute8                := substr(datarec.cat_attribute8,1,150);
      
      -- Assign Asset Type Details
      l_asset_type_rec.asset_type                         := upper(datarec.asset_type);
      
      -- Assign Depreciation Details
      l_asset_deprn_rec.bonus_deprn_reserve          := datarec.bonus_depreciation_reserve;
      l_asset_deprn_rec.bonus_ytd_deprn              := datarec.bonus_ytd_depreciation;
      l_asset_deprn_rec.deprn_reserve                := datarec.depreciation_reserve;
      l_asset_deprn_rec.reval_amortization_basis     := datarec.revaluation_amortization_basis;
      l_asset_deprn_rec.reval_deprn_reserve          := datarec.revaluation_reserve;
      l_asset_deprn_rec.ytd_deprn                    := datarec.ytd_depreciation;
      
      -- Asset Information
      l_asset_desc_rec.asset_number                  := datarec.asset_number;
      l_asset_desc_rec.current_units                 := datarec.units;
      l_asset_desc_rec.description                   := datarec.description;
      l_asset_desc_rec.in_use_flag                   := upper(datarec.in_use);
      l_asset_desc_rec.inventorial                   := upper(datarec.in_physical_inventory);
      l_asset_desc_rec.manufacturer_name             := datarec.manufacturer_name;
      l_asset_desc_rec.model_number                  := datarec.model_number;
      l_asset_desc_rec.new_used                      := upper(datarec.bought);
      l_asset_desc_rec.owned_leased                  := upper(datarec.ownership);
      l_asset_desc_rec.parent_asset_id               := datarec.parent_asset_number; --????
      l_asset_desc_rec.property_type_code            := upper(datarec.property_type);
      l_asset_desc_rec.serial_number                 := datarec.serial_number;
      --l_asset_desc_rec.tag_number                    := datarec.serial_number;
      l_asset_desc_rec.warranty_id                   := datarec.warranty_id;
      l_asset_desc_rec.asset_key_ccid                := datarec.asset_key_ccid;
      
      -- Asset Status - not passed (yet), where to pass in API?  asset_desc_rec_type.status?  CRL assets only apparently (see doco)
      
      -- Distribution Information
      l_asset_dist_rec.assigned_to                   := datarec.assigned_id;
      l_asset_dist_rec.location_ccid                 := datarec.location_ccid;
      l_asset_dist_rec.expense_ccid                  := datarec.exp_gl_ccid;
      l_asset_dist_rec.units_assigned                := datarec.units; -- otherwise fa_addition_pvt.initialize sets units to 0
      l_asset_dist_tbl(1)                            := l_asset_dist_rec;
      
      --  Asset Financial Information
      l_asset_fin_rec.adjusted_rate                  := datarec.adjusted_rate;
      l_asset_fin_rec.depreciate_flag                := datarec.depreciate_flag;
      l_asset_fin_rec.allowed_deprn_limit            := datarec.depreciation_limit_percent; --???
      l_asset_fin_rec.allowed_deprn_limit_amount     := datarec.depreciation_limit_amount;
      l_asset_fin_rec.basic_rate                     := datarec.basic_rate;
      l_asset_fin_rec.bonus_rule                     := datarec.bonus_rule;
      l_asset_fin_rec.conversion_date                := datarec.conversion_date;
      l_asset_fin_rec.cost                           := datarec.cost;
      l_asset_fin_rec.original_cost                  := datarec.original_cost;
      l_asset_fin_rec.date_placed_in_service         := datarec.date_placed_in_service;
      l_asset_fin_rec.deprn_limit_type               := upper(datarec.depreciation_limit_type);
      l_asset_fin_rec.deprn_method_code              := datarec.depreciation_method;
      l_asset_fin_rec.deprn_start_date               := datarec.original_depr_start_date;
      l_asset_fin_rec.group_asset_id                 := datarec.group_asset_id;
      l_asset_fin_rec.life_in_months                 := datarec.life_in_months;
      l_asset_fin_rec.percent_salvage_value          := datarec.salvage_value_percent;
      l_asset_fin_rec.production_capacity            := datarec.production_capacity;
      l_asset_fin_rec.prorate_convention_code        := datarec.prorate_convention;
      --l_asset_fin_rec.salvage_type                   := datarec.salvage_type; --Not passed yet values - AMT, PCT, SUM (see doco)
      l_asset_fin_rec.salvage_value                  := datarec.salvage_value;
      l_asset_fin_rec.short_fiscal_year_flag         := datarec.short_fiscal_year_flag;
      l_asset_fin_rec.unit_of_measure                := datarec.unit_of_measure;
      
      l_asset_hdr_rec.book_type_code                 := pv_book_type_code; 
      l_asset_hdr_rec.asset_id                       := datarec.loaded_asset_id; --  Will have been set if TAX book (not CORPORATE)
      
      l_asset_inv_type_rec.payables_code_combination_id := datarec.clr_gl_ccid;
      l_asset_inv_type_rec.project_id                   := datarec.project_id;      
      l_asset_inv_type_rec.invoice_number               := datarec.invoice_number; -- Treating as text
      
      l_lease_details_rec.lease_number               := datarec.lease_number;
      
      l_trans_rec.amortization_start_date            := datarec.amortization_start_date;
      l_trans_rec.transaction_date_entered           := null; --datarec.date_placed_in_service; ---- check see Oracle Support community.oracle.com/thread/3537924
      l_trans_rec.transaction_subtype                := datarec.transaction_type_code;
          
      -- Call the api 
      fa_addition_pub.do_addition(
             -- Standard parameters
             p_api_version             => 1.0,
             p_init_msg_list           => FND_API.G_FALSE,
             p_commit                  => FND_API.G_FALSE,
             p_validation_level        => FND_API.G_VALID_LEVEL_FULL,
             p_calling_fn              => null,
             x_return_status           => l_return_status,
             x_msg_count               => l_mesg_count,
             x_msg_data                => l_mesg,
             -- API parameters
             px_trans_rec              => l_trans_rec,
             px_dist_trans_rec         => l_dist_trans_rec,
             px_asset_hdr_rec          => l_asset_hdr_rec,
             px_asset_desc_rec         => l_asset_desc_rec,
             px_asset_type_rec         => l_asset_type_rec,
             px_asset_cat_rec          => l_asset_cat_rec,
             px_asset_hierarchy_rec    => l_asset_hierarchy_rec,
             px_asset_fin_rec          => l_asset_fin_rec,
             px_asset_deprn_rec        => l_asset_deprn_rec,
             px_asset_dist_tbl         => l_asset_dist_tbl,
             px_inv_tbl                => l_inv_tbl
            );
  
      -- Dump messages
      l_mesg_count := fnd_msg_pub.count_msg;
  
      if l_mesg_count > 0 then
         l_mesg := chr(10) || substr(fnd_msg_pub.get(fnd_msg_pub.G_FIRST, fnd_api.G_FALSE), 1, 250);
         --dbms_output.put_line(l_mesg);
         lv_import_comments := lv_import_comments || ' ' || l_mesg;
         for i in 1..(l_mesg_count - 1) loop
            l_mesg := substr(fnd_msg_pub.get(fnd_msg_pub.G_NEXT,fnd_api.G_FALSE), 1, 250);
            --dbms_output.put_line(l_mesg);
            lv_import_comments := lv_import_comments || ' ' || l_mesg;
         end loop;
         fnd_msg_pub.delete_msg();
         lv_import_status := gcv_error;
         ln_error_count := ln_error_count + 1;
      else
         lv_import_status := gcv_loaded;
         lv_import_comments := '';      
      end if;
      log_msg(lv_procedure, 'Asset Number ['||datarec.asset_number||'] unique_reference_number ['||datarec.unique_reference_number||'] load_sequence_id ['||datarec.load_sequence_id||'] '||lv_import_comments);

      -- Update the validated records with the result
      update xxfa_asset_add_staging
      set    import_status   = lv_import_status
           , import_comments = lv_import_comments
           , loaded_asset_id = l_asset_hdr_rec.asset_id
      where current of c_assets;

    end loop;
    
    if ln_error_count > 0 then
      gv_retcode := gcv_ret_warning;
    end if;
    
    log_msg(lv_procedure, ' ');
    log_msg(lv_procedure, '-------------------------------------------------------');
    log_msg(lv_procedure, 'Load Assets - Completed - Results:');
    log_msg(lv_procedure, 'Number of records read:     ['||ln_record_count||']');
    log_msg(lv_procedure, 'Number of records in error: ['||ln_error_count||']');
    log_msg(lv_procedure, '-------------------------------------------------------');

  end load_assets;
   
end xxfa_new_asset_bk_load;
/
