create or replace package xxfa_new_asset_bk_load as
  /*
  || Package: create or replace package xxfa_new_asset_bk_load as

  ||
  || Description: This package spec contains utility procedures and functions
  ||              for the Loading of Assets
  ||
  || Modification History
  ||   Author               Date            Description
  ||   -------------------- --------------- -------------------------------------
  ||   K Nasskau            Dec-2019        Initial version
  ||
  */

  procedure validate_assets (
    pv_book_type_code  in  varchar2
  );
  
  procedure load_assets (
    pv_book_type_code  in  varchar2
  );

  procedure delete_staging_records (
    pv_book_type_code  in  varchar2
  );

  procedure process_assets (
    pv_errbuf     out varchar2
  , pv_retcode    out varchar2
  , pv_book_type_code  in  varchar2
  , pv_action          in  varchar2
  );

end xxfa_new_asset_bk_load;
/
