  /*
  ||----------------------------------------------------------------------------
  || Filename: create_XXFA_ASSET_ADD_STAGING.sql
  ||
  || Description : This script creates objects:
  ||                - table XXFA_ASSET_ADD_STAGING
  ||                - sequence XXFA_ASSET_ADD_STAGING_S
  ||
  || Modification History
  ||   Author               Date            Description
  ||   -------------------- --------------- ------------------------------------
  ||   K Nasskau            Dec-2019        Initial version
  ||
  ||----------------------------------------------------------------------------
  */

set serveroutput on size 1000000
set linesize 254

declare
  lr_obj  xxfnd_installer.object_create_record;
  lv_error varchar2(4000);
begin
  dbms_output.enable(null);
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_ASSET_ADD_STAGING';
  lr_obj.object_type  := 'TABLE';
  lr_obj.create_ddl   := q'[
create table XXFA_ASSET_ADD_STAGING
(
   book_type_code                  VARCHAR2(30)
 , asset_type                      VARCHAR2(30)
 , description                     VARCHAR2(80)
 , category_1                      VARCHAR2(30)
 , category_2                      VARCHAR2(30)
 , category_3                      VARCHAR2(30)
 , units                           NUMBER
 , cost                            NUMBER
 , original_cost                   NUMBER
 , location_1                      VARCHAR2(30)
 , location_2                      VARCHAR2(30)
 , location_3                      VARCHAR2(30)
 , asset_key                       VARCHAR2(30)
 , cat_attribute1                  VARCHAR2(4000)
 , cat_attribute2                  VARCHAR2(4000)
 , cat_attribute3                  VARCHAR2(4000)
 , cat_attribute4                  VARCHAR2(4000)
 , cat_attribute5                  VARCHAR2(4000)
 , cat_attribute7                  VARCHAR2(4000)
 , cat_attribute8                  VARCHAR2(4000)
 , exp_gl_segment1                 VARCHAR2(25)
 , exp_gl_segment2                 VARCHAR2(25)
 , exp_gl_segment3                 VARCHAR2(25)
 , exp_gl_segment4                 VARCHAR2(25)
 , exp_gl_segment5                 VARCHAR2(25)
 , clr_gl_segment1                 VARCHAR2(25)
 , clr_gl_segment2                 VARCHAR2(25)
 , clr_gl_segment3                 VARCHAR2(25)
 , clr_gl_segment4                 VARCHAR2(25)
 , clr_gl_segment5                 VARCHAR2(25)
 , date_placed_in_service          DATE
 , asset_number                    VARCHAR2(15)
 , asset_status                    VARCHAR2(30)
 , tag_number                      VARCHAR2(15)
 , serial_number                   VARCHAR2(35)
 , manufacturer_name               VARCHAR2(360)
 , model_number                    VARCHAR2(40)
 , employee_number                 VARCHAR2(30)
 , source_line_project_number      VARCHAR2(30)
 , transaction_type_code           VARCHAR2(30)
 , in_use                          VARCHAR2(03)
 , in_physical_inventory           VARCHAR2(03)
 , ownership                       VARCHAR2(15)
 , bought                          VARCHAR2(15)
 , warranty_number                 VARCHAR2(15)
 , property_type                   VARCHAR2(10)
 , property_class                  VARCHAR2(100)
 , depreciation_reserve            NUMBER
 , ytd_depreciation                NUMBER
 , amortization_start_date         DATE
 , amortize_nbv                    NUMBER
 , bonus_ytd_depreciation          NUMBER
 , bonus_depreciation_reserve      NUMBER
 , invoice_number                  VARCHAR2(50)
 , parent_asset_number             VARCHAR2(15)
 , distribution_set                VARCHAR2(100)
 , short_fiscal_year_flag          VARCHAR2(03)
 , conversion_date                 DATE
 , original_depr_start_date        DATE
 , group_asset                     VARCHAR2(100)
 , lease_number                    VARCHAR2(15)
 , depreciation_method             VARCHAR2(12)
 , life_in_months                  NUMBER
 , basic_rate                      NUMBER
 , adjusted_rate                   NUMBER
 , prorate_convention              VARCHAR2(10)
 , bonus_rule                      VARCHAR2(30)
 , depreciate_flag                 VARCHAR2(03)
 , depreciation_limit_type         VARCHAR2(30)
 , depreciation_limit_amount       NUMBER
 , depreciation_limit_percent      NUMBER
 , salvage_type                    VARCHAR2(30)
 , salvage_value                   NUMBER
 , salvage_value_percent           NUMBER
 , unit_of_measure                 VARCHAR2(25)
 , production_capacity             NUMBER
 , revaluation_amortization_basis  NUMBER
 , revaluation_reserve             VARCHAR2(30)
 , unique_reference_number         NUMBER
 , import_status                   VARCHAR2(01)
 , import_comments                 VARCHAR2(4000)
 , import_msg_data                 VARCHAR2(4000)
 , import_msg_count                NUMBER
 , load_sequence_id                NUMBER
 , load_datetime                   DATE
 , location_ccid                   NUMBER
 , exp_gl_ccid                     NUMBER
 , clr_gl_ccid                     NUMBER
 , assigned_id                     NUMBER
 , warranty_id                     NUMBER
 , group_asset_id                  NUMBER
 , category_ccid                   NUMBER
 , asset_key_ccid                  NUMBER
 , loaded_asset_id                 NUMBER
 , project_id                      NUMBER
) 
]';
  XXFND_INSTALLER.CREATE_ALWAYS(LR_OBJ,lv_error);
--   constraint XXFA_ASSET_ADD_STAGING_U1 primary key (column_id???)

  --
  -- ----------------------------------------------------------------------
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_ASSET_ADD_STAGING_S';
  lr_obj.object_type  := 'SEQUENCE';
  lr_obj.create_ddl   := q'[
create sequence XXFA_ASSET_ADD_STAGING_S nocache
]';
  XXFND_INSTALLER.CREATE_IF_REQUIRED(LR_OBJ,lv_error);
  --
  --
  -- ----------------------------------------------------------------------
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_ASSET_ADD_STAGING_BI';
  lr_obj.object_type  := 'TRIGGER';
  lr_obj.create_ddl   := q'[
create or replace trigger XXFA_ASSET_ADD_STAGING_BI 
before INSERT on XXFA_ASSET_ADD_STAGING for each row
declare
begin
   :new.import_status    := 'N';
   :new.import_msg_count := 0;
end;
]';
  XXFND_INSTALLER.CREATE_IF_REQUIRED(LR_OBJ,lv_error);
--
  --
  -- ----------------------------------------------------------------------
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_ASSET_ADD_STG_BKS_V';
  lr_obj.object_type  := 'VIEW';
  lr_obj.create_ddl   := q'[
create or replace view XXFA_ASSET_ADD_STG_BKS_V
as 
select distinct book_type_code
from XXFA_ASSET_ADD_STAGING
]';
  XXFND_INSTALLER.CREATE_IF_REQUIRED(LR_OBJ,lv_error);

  -- ----------------------------------------------------------------------

  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_ASSET_ADD_STAGING_N1';
  lr_obj.object_type  := 'INDEX';
  lr_obj.create_ddl   := q'[
create index APPS.XXFA_ASSET_ADD_STAGING_N1 on APPS.XXFA_ASSET_ADD_STAGING
(
    book_type_code
  , unique_reference_number  
)
]';
  XXFND_INSTALLER.CREATE_ALWAYS(LR_OBJ,lv_error);

  -- ----------------------------------------------------------------------

  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_ASSET_ADD_STAGING_N2';
  lr_obj.object_type  := 'INDEX';
  lr_obj.create_ddl   := q'[
create index APPS.XXFA_ASSET_ADD_STAGING_N2 on APPS.XXFA_ASSET_ADD_STAGING
(
    book_type_code
  , import_status  
)
]';
  XXFND_INSTALLER.CREATE_ALWAYS(LR_OBJ,lv_error);

end;
/

-- ----------------------------------------------------------------------
-- Grants to PWC_APPS_RO_ROLE
-- ----------------------------------------------------------------------

grant select on APPS.XXFA_ASSET_ADD_STAGING   to PWC_APPS_RO_ROLE
/
grant select on APPS.XXFA_ASSET_ADD_STG_BKS_V to PWC_APPS_RO_ROLE
/
grant select on APPS.XXFA_ASSET_ADD_STAGING_S to PWC_APPS_RO_ROLE
/

