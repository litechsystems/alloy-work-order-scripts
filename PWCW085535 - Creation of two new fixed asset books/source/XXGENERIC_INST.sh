#!/usr/bin/env bash
# **************************************************************************************************
# * $Id:$*                                                                                         *
# * Program Name : XXGENERIC_INST.sh                                                               *
# *                                                                                                *
# **************************************************************************************************
# *                                                                                                *
# * Description  : Generic installation script for APPS customisations and enhancements            *
# *                                                                                                *
# **************************************************************************************************
# *                                                                                                *
# * Version Date Issued Author               Description                                           *
# * ======= =========== ===================  ======================================================*
# *                                                                                                *
# * ============================================================================================== *
# * Below are the options and calling instructions for the script                                  *
# * This is also returned as part of the usage information if script is called incorrectly         *
# *                                                                                                *
# * Optional Parameters   : File List                                                              *
# *                                                                                                *
# * Usage : XXGENERIC_INST [FileList]                                                              *
# * ============================================================================================== *
# *                                                                                                *
# * Below is the minimum required information to be placed at the start of all functions:          *
# *                                                                                                *
# * #-------------------------------------------------------------------------------               *
# *  Overview:					(Short description of functionality)                               *
# *                                                                                                *
# *  Function:					(Function name)                                                    *
# *                                                                                                *
# *  Parameters:        NA      (List of all passed in parameter names excluding leading 'lp_'     *
# *                              and identifying if the parameter is 'required' or if it has a     *
# *	                             'default' value)                                                  *
# *  Return:			NA		(Information on any returned data                                  *
# * #-------------------------------------------------------------------------------               *
# *                                                                                                *
# **************************************************************************************************

# Uncomment below for debuging
# set -xv

# -------------------------------------------------
#	Global setting
# -------------------------------------------------
shopt -s nullglob

# -------------------------------------------------
#	Global variables
# -------------------------------------------------
declare gv_additional_msg
declare gb_voyeur_on
declare g_verbose
declare -a ga_upwd

gb_voyeur_on=true
#-------------------------------------------------------------------------------
#	Overview: Display message with appropriate colouring
#
# Function:		print_msg
#
# Parameters: colour	(default: NORM)
#							msg			(required)
#							where		(default:ERR-8,else 7)
#
# Return:			required message
#-------------------------------------------------------------------------------
print_msg()
{
	local lp_colour lp_msg
	local lc_nl ln_where
	local ERR MSG NORM SUCC WARN

	 ERR='\e[1;31m'	#red
	 MSG='\e[1;36m'	#cyan
	NORM='\e[0;37m'	#normal
	SUCC='\e[1;32m'	#green
	WARN='\e[1;33m'	#yellow

	case $# in
		1) lp_msg=$1;;
	2|3) lp_colour=$1
			 lp_msg=$2;;
		*) print_msg ERR "ERROR: Incorrect number of arguments passed to ${FUNCNAME[0]}!"
			 exit 1;;
	esac

	ln_where=2

	$gb_voyeur_on || ln_where=8

	if [[ "$lp_colour" != MSG ]]
	then
		lc_nl='\n'
	fi

	echo -ne "\\n${!lp_colour:-$NORM}$lp_msg$NORM$lc_nl" >&"$ln_where"

	# Make sure when not watching the output on screen that the log is populated
	$gb_voyeur_on || echo -ne "\\n${!lp_colour:-$NORM}$lp_msg$NORM$lc_nl" >&2

} #print_msg

#-------------------------------------------------------------------------------
#	Overview:		Tests that functions have been passed the correct number of
#							parameters
#
# Function:		func_test
#
# Parameters: func_name	(required)
#							num_args	(required)
#							min				(required)
#							max				(optional)
#
# Return:			Error message (if required)
#-------------------------------------------------------------------------------
func_test()
{
	local lp_func_name lp_num_args lp_min lp_max

	lp_func_name="$1"
	lp_num_args="$2"
	lp_min="$3"
	lp_max="${4:-$3}"

	if (( lp_num_args < lp_min || lp_num_args > lp_max ))
	then
		print_msg ERR "ERROR: Incorrect number of arguments passed to $lp_func_name!"
		exit 1
	fi
} #func_test

#-------------------------------------------------------------------------------
#	Overview:		Process parameters from command line
#
# Function:		parse_params
#
# Parameters: switches	(required)
#							params		(required)
#
# Return:			Values stored in passed in variables
#-------------------------------------------------------------------------------
parse_params()
{
	local lp_switches lp_params
	local l_temp_name l_arg l_temp_arg

	lp_switches="$1"
	lp_params="$2"
	shift 2

	for l_arg
	do
		l_temp_arg=

		if [[ ${l_arg:0:1} == '-' ]]
		then
			l_temp_name=$lp_switches

			if [[ ${l_arg:1:1} == '-' ]]
			then
				l_temp_arg="$l_arg"
			else
				for (( i = 1; i < ${#l_arg}; i++ ))
				do
					if [[ -n "$l_temp_arg" ]]
					then
						l_temp_arg="$l_temp_arg -${l_arg:i:1}"
					else
						l_temp_arg="-${l_arg:i:1}"
					fi
				done
			fi
		else
			l_temp_name=$lp_params
			l_temp_arg="$l_arg"
		fi

		if [[ -n "${!l_temp_name}" ]]
		then
			read -r "$l_temp_name" <<< "${!l_temp_name} $l_temp_arg"
		else
			read -r "$l_temp_name" <<< "$l_temp_arg"
		fi
	done
} #parse_params

#-------------------------------------------------------------------------------
#	Overview: Provide usage information for script
#
# Function:		run_instructions
#
# Parameters: where	(optional)
#
# Return:			usage message
#-------------------------------------------------------------------------------
run_instructions()
{
	cat<<-USAGE

		Usage: $0 [switch] [file|type]
	
		Switch:
		    -d, --defaults          User can use default values
		    -i, --info [type]       Request field information on type
		    -t, --template [name]   Create template install file
		    -v, --verbose           Allows system commands to show verbose output, eg. chmod -v
		    -h, --help              Display this help message
	
		File:
		    file              Name of File List to use in script
		    type              Type to be used in input file
		    name              Name to be used for input file creation
	
		$(print_msg WARN "You will be requested to enter the Universal Application Schema name password")
	
		Aborted : $(date)

	USAGE

	exit 1
} #run_instructions

#-------------------------------------------------------------------------------
#	Overview: Return answers for Yes / No based questions
#
# Function:		yn
#
# Parameters: prompt	(required)
#
# Return:			yes / no
#-------------------------------------------------------------------------------
yn()
{
	local lp_prompt
	local l_answer l_ret_val l_regex

	func_test "${FUNCNAME[0]}" $# 1

	lp_prompt="$1"
	l_regex='yes|no'

	until [[ $l_ret_val =~ $l_regex ]]
	do
		print_msg MSG "$lp_prompt"
		read -r -n1 l_answer

		case "$l_answer" in
			y|Y) l_ret_val=yes;;
			n|N) l_ret_val=no;;
			  *) print_msg WARN "Please try again as acceptable responses are y or n!";;
		esac
	done

	echo "$l_ret_val"
} #yn

#-------------------------------------------------------------------------------
#	Overview: Open / Close the log file
#
# Function:		open_close_log
#
# Parameters: state			(default: close)
#							log_file
#
# Return:			NA
#-------------------------------------------------------------------------------
open_close_log()
{
	local lp_state lp_log_file
	local la_log_files

	func_test "${FUNCNAME[0]}" $# 0 2

	lp_state="${1:-close}"
	lp_log_file="$2"

	if [[ "$lp_state" == open ]]
	then
		exec 7>&1 8>&2

		# Check for and ask if log files should be deleted
		la_log_files=( "${0%.*}"*.log )
		(( ${#la_log_files[*]} > 0 )) && [[ $(yn "Previous log files exist. Do you want them removed? [y/n] ") == yes ]] && rm "${la_log_files[@]}"

		if [[ "$(yn "\\nDo you wish to see the output? [y/n] ")"  == yes ]]
		then
			exec &> >(exec tee "$lp_log_file")
		else
			exec &> "$lp_log_file"

			gb_voyeur_on=false
		fi

		echo >&7
	else
		exec >&7 2>&8 7>&- 8>&-
		gb_voyeur_on=true
	fi

} #open_close_log

#-------------------------------------------------------------------------------
#	Overview: Centralised place for any major TYPE arrays which may change or need
#						to be added to
#
# Function:		get_types
#
# Parameters: switch			(required)
#							array_name	(required)
#
# Return:			array
#-------------------------------------------------------------------------------
get_types()
{
	local lp_switch
	local -a la_all_types la_all_descriptions  
	local -a la_fnd_types la_lct_paths
	local -a la_xdo_types la_xdo_lob_types la_xdo_file_types

	func_test "${FUNCNAME[0]}" $# 1

	lp_switch="$1"

	# Add items to the following arrays in the same order:
	#		la_all_types and la_all_descriptions

	# All types in alphabetic order
	la_all_types=(	ADFF AEXC AEXL AKLD ALRT ALTD AMAC AMAG AMAS AMAT AMAU
									AMCD AMRL ATTC ATYP BC4J BCNT BCMP BEXC BLAY BMAP BNE 
									BNEI BPRM CPRG CPY  DFF  DMSG DTD  FAUG FCUS FFCT FFRM
									FMB  FTOL FUNC ILDT JAVA JPX  LKUP MDS  MDSO MDSR MENU
									PDRV PERL PINF PLL  PREP PROF PSTL REQG ROLE SHLL SQL  
									TLAY URSP VSET WFE  WFT  WFX  XBRT XDOT XDF  XDTP XMAP 
									XRTF XSMP XTXT XXML XXSL
							 )

	# All descriptions in type alphabetic order
	la_all_descriptions=(	'FNDLOAD: AD_FILES - Register Flagged Files entries'
                        'ASCII Executable' 'ASCII Executable + fndcpesr link'
												'AK Loader' 'FNDLOAD: ALERTS'
												'FNDLOAD: HXC Alternate Name Definition' 'FNDLOAD: AME Approval Group Config'
												'FNDLOAD: AME Groups' 'FNDLOAD: AME Action Usages' 'FNDLOAD: AME Attributes'
												'FNDLOAD: AME Attribute Usages' 'FNDLOAD: AME Conditions'
												'FNDLOAD: AME Rules' 'FNDLOAD: Attachment Seed Data' 'FNDLOAD: HXC Alias Types'
												'Java Binary' 'WebADI Contents (deprecated)' 
												'WebADI Component (deprecated)' 'Binary Executable'
												'WebADI Layout (deprecated)' 'WebADI Map (deprecated)'
												'WebADI Integrator' 'WebADI Integrator (deprecated)' 
												'WebADI Parameter List (deprecated)' 'FNDLOAD: Concurrent Programs'
												'Copy File' 'FNDLOAD: Descriptive Flex Field'
												'FNDLOAD: Messages' 'XML Gateway DTD'
												'FNDLOAD: Audit Groups'
												'FNDLOAD: FND Forms Customizations' 'FNDLOAD: Fast Formula Functions'
												'FNDLOAD: Fast Formula'	'Forms 6 form'
												'FNDLOAD: Folders'
												'FNDLOAD: FORM, FUNCTION, MENU, ENTRY, OBJECT, OBJECT_INSTANCE_SET, GRANT'
												'FNDLOAD: iLDT SOA Integration Repository Loader'
												'Java ASCII' 'JPX Import'
												'FNDLOAD: Lookup Types and Lookup Values' 'MDS Import'
												'MDS Import - Organisation Specific' 'MDS Import - Responsibility Specific'
												'FNDLOAD: Menu' 'FNDLOAD: Printer Drivers'
												'Perl Script'	'FNDLOAD: Printer Information'
												'Forms 6 plsql library' 'Pre-Parse Shell Scripts'
												'FNDLOAD: Profile Options' 'FNDLOAD: Printer Styles'
												'FNDLOAD: Request Groups'	'FNDLOAD: Roles'
												'Shell Script' 'SQl Script'
												'FNDLOAD:  HXC Configurable Self-Service UI'
												'FNDLOAD: USERS, RESPONSIBILITIES, SECURITY GROUPS, APPLICATIONS, USER RESPONSIBILITIES'
												'FNDLOAD: Value Sets and Value Set Values' 'Workflow XML Loader - Events'
												'WFT Loader' 'Workflow XML Loader - Subscriptions'
												'XML Publisher - Bursting Control File'	'FNDLOAD: XDO Template Manager'
												'XDF Object Definition' 'XML Publisher - Data Template' 
												'XML Gateway Map' 'XML Publisher - RTF File' 
												'XML Publisher - Sample Data' 'XML Publisher - XSL-TEXT File'
												'XML Publisher - XSL-XML File' 'XML Publisher - XSL File'
											)

	# Add items to the following arrays in the same order:
	#		la_fnd_types and la_lct_paths

	# All FNDLOAD types ordered by lct path locations below
	la_fnd_types=(	ATTC FAUG CPRG DMSG FCUS FUNC
									LKUP MENU PDRV PINF PROF PSTL
									REQG ROLE URSP VSET DFF	 FTOL
									ILDT ALTD ATYP TLAY XDOT ALRT 
									BNE  BLAY BPRM BCNT BCMP BMAP 
									BNEI FFCT FFRM AMAT AMAU AMCD 
									AMAG AMRL AMAC AMAS ADFF
							 )

	# ALL FNDLOAD lct paths
  # Note: The adff.lct file is a custom lct at DAFWA.
	la_lct_paths=(	"$FND_TOP"/patch/115/import/af{attach,faudit,cpprog,mdmsg,frmcus,sload,lvmlu,sload,cppinf,cppinf,scprof,cppinf,cpreqg,role,scursp,ffload,ffload}.lct
									"$FND_TOP"/patch/115/import/fndfold.lct
									"$FND_TOP"/patch/115/import/wfirep.lct                  
									"$HXC_TOP"/patch/115/import/hxc{al{dfaliasdefn,tpaliastype},laytlayoutsld}.lct
									"$XDO_TOP"/patch/115/import/xdotmpl.lct
									"$ALR_TOP"/patch/115/import/alr.lct
									"$BNE_TOP"/patch/115/import/bne{integrator,lay,paramlist,cont,comp,map}.lct 
									"$BNE_TOP"/admin/import/bneint.lct 
									"$XXPAY_TOP"/install/fndload/xxff{function,rmula}.lct
									"$AME_TOP"/patch/115/import/ames{matt,matr,conk,appg,rulk,aagc,actu}.lct
                  "$XXDAF_TOP"/patch/xxadff.lct
							 )

	# Arrays for calling xdoinformation
	# All 3 arrays MUST be in the same order
	la_xdo_types=( XBRT XDTP XSMP XRTF
								 XXSL XXML XTXT
							 )

	la_xdo_lob_types=(	BURSTING_FILE
											DATA_TEMPLATE
											XML_SAMPLE
											TEMPLATE_SOURCE
											TEMPLATE
											TEMPLATE
											TEMPLATE
									 )

	la_xdo_file_types=(	XML-BURSTING-FILE
											XML-DATA-TEMPLATE
											XML
											RTF
											XSL-FO
											XSL-XML
											XSL-TEXT
										)

	case "$lp_switch" in
		 -a) la_return=( "${la_all_types[@]}" );;
		 -d) la_return=( "${la_all_descriptions[@]}" );;
		 -f) la_return=( "${la_fnd_types[@]}" );;
		 -p) la_return=( "${la_lct_paths[@]}" );;
		 -x) la_return=( "${la_xdo_types[@]}" );;
		-xf) la_return=( "${la_xdo_file_types[@]}" );;
		-xl) la_return=( "${la_xdo_lob_types[@]}" );;
		  *) print_msg ERR "ERROR: Invalid switch passed to ${FUNCNAME[0]} :- $lp_switch"
				exit 1;;
	esac

	echo "${la_return[@]}"
} #get_types

#-------------------------------------------------------------------------------
#	Overview: Validates the input file used prior to any actions being performed
#						by the installer
#
# Function:		validate_file_list
#
# Parameters: file_list	(required)
#
# Return:			message (success/fail)
#-------------------------------------------------------------------------------
validate_file_list()
{
	local lp_file_list
	local lwa_line la_temp
	local ln_limit ln_errors ln_line_cnt ln_cnt
	local ln_have_digit ln_no_digit

	func_test "${FUNCNAME[0]}" $# 1

	lp_file_list="$1"
	ln_errors=0
	ln_line_cnt=0

	# Remove any lines starting with # or blank lines and create replacement
	grep -Eq '^(\s+|#.*)$' "$lp_file_list" && sed -r -i.orig '/^(#.*|\s*)$/d' "$lp_file_list"

	# Check for leading digits for install sorting, if required
	ln_have_digit=$(grep -c -m1 '^[0-9]' "$lp_file_list")
	ln_no_digit=$(grep -c -m1 '^[^0-9]' "$lp_file_list")

	if (( ln_have_digit + ln_no_digit == 2 ))
	then
		print_msg ERR 'ERROR: mixed numbering files cannot be processed'
		exit 1
	elif (( ln_have_digit == 1 ))
	then
		mv "$lp_file_list" "${lp_file_list}.wd"

		IFS=,
		while read -ra la_line
		do
			if (( la_temp[la_line[0]]++ == 0 ))
			then
				sed 's/[[:cntrl:]]//g' <<<"${la_line[*]:1}" >> "$lp_file_list"
			else
				print_msg ERR "ERROR: one or more lines using the same sort order numnber ${la_line[0]}"
				exit 1
			fi
		done< <(sort -n "${lp_file_list}.wd")
		unset IFS
	fi

	while IFS=, read -ra lwa_line
	do
		ln_limit=4
		(( ln_line_cnt++ ))

		case "${lwa_line[2]}" in
			AEXL|AKLD|ALRT|ALTD|AMAC|AMAS|AMAT|AMAU|AMCD|AMAG|\
			AMRL|ATTC|ATYP|BCNT|BCMP|BLAY|BMAP|BNE|BNEI|BPRM|CPRG|\
			DFF|DMSG|FAUG|FCUS|FFCT|FFRM|FTOL|FUNC|ILDT|LKUP|\
			PDRV|PERL|PINF|PREP|PROF|PSTL|SHLL|SQL|\
			TLAY|VSET|WFT|XDOT|XMAP|ADFF)
				        :;;
			AEXC|BEXC|CPY|FMB|PLL|MENU|REQG|URSP|ROLE)
			          [[ -n "${lwa_line[4]}" || -n "${lwa_line[5]}" ]] && ln_limit=6;;
			BC4J|JAVA|JPX|XDF|WFE|WFX)
								ln_limit=5;;
			DTD|MDS|MDSR|XBRT|XDTP|XRTF|XSMP|XTXT|XXML|XXSL)
			          ln_limit=6;;
			MDSO)     ln_limit=7;;
			*)	print_msg ERR "ERROR: ${lwa_line[2]} is not a known type!!!"
								(( ln_errors++ ))
								continue;;
		esac

		for (( ln_cnt = 0; ln_cnt < ln_limit; ln_cnt++ ))
		do
			if [[ -z "${lwa_line[ln_cnt]}" ]]
			then
				(( ln_errors++ ))

				print_msg ERR "ERROR: Mandatory field $(( ln_cnt + 1 )) is missing for type ${lwa_line[2]} on line $ln_line_cnt"
			fi
		done

	done<"$lp_file_list"

	if (( ln_errors > 0 ))
	then
		print_msg ERR "ERROR: $ln_errors error(s) found in $lp_file_list. Please correct and retry installer"
		exit 1
	fi
} #validate_file_list

#-------------------------------------------------------------------------------
#	Overview: Function to check login is valid
#
# Function:		chk_login
#
# Parameters: sqlplus_connect	(required)
#
# Return:			true / false
#-------------------------------------------------------------------------------
chk_login()
{
	local lp_sqlplus_connect

	func_test "${FUNCNAME[0]}" $# 1

	lp_sqlplus_connect="$1"

	if sqlplus -s /nolog <<-EOF &>/dev/null
			WHENEVER SQLERROR EXIT 1;
			connect $lp_sqlplus_connect
			COLUMN tbl NEW_VALUE tbl_name NOPRINT;
			SELECT table_name tbl
		  FROM   all_tables
			WHERE  table_name = 'DUAL';
			EXIT;
		EOF
	then
		true
	else
		false
	fi
} #chk_login

#-------------------------------------------------------------------------------
#	Overview:		Tests a username/password connect string combination f
#
# Function:		login_test
#
# Parameters: user_name							(required)
#							password							(required)
#							jdbc connect string		(optional)
#
# Return:			Error message (if required)
#-------------------------------------------------------------------------------
login_test()
{
  local lp_user_name lp_password lp_jdbc_connect 
  local l_connect_string
  
  func_test "${FUNCNAME[0]}" $# 3
  
  lp_user_name="$1"
  lp_password="$2"
  lp_jdbc_connect="$3"
  
	# Create connection string for sql queries
	l_connect_string="$lp_user_name/$lp_password@$lp_jdbc_connect"

	if ! chk_login "$l_connect_string"
	then
		print_msg ERR "$(cat<<-CHK
			-----------------------------------------ERROR-----------------------------------------
			  $lp_user_name Schema password may not be correct or JDBC Connect String is invalid.
			     UserName: $lp_user_name
			     Password: $lp_password
			  JDBC String: $lp_jdbc_connect
			---------------------------------------------------------------------------------------
		CHK
		)"

		exit 1
	fi  
} #login_test

#-------------------------------------------------------------------------------
#	Overview: Check if all files for the installation exist in 
#						the current directory
#
# Function:		check_files_missing
#
# Parameters: install_from	(required)
#							file_list			(required)
#
# Return:			ret_status		(true/false)
#-------------------------------------------------------------------------------
check_files_missing()
{
	local lp_install_from lp_file_list
	local lw_file_name lw_source_dir
	local l_ret_status 

	func_test "${FUNCNAME[0]}" $# 2

	lp_install_from="$1"
	lp_file_list="$2"  # currently no check to test if this is a valid file

	l_ret_status=false

	cat<<-CHECK_FILES
		Checking all installation files are present in $lp_install_from.
		If there are no 'ERROR: Cannot Find <file>' messages below, then it is ok to continue with the install

	CHECK_FILES

	# Check for missing files
	#
	# List of columns used in file and what associated data is being used:
	#
	# lw_file_name	:- file name to check
	# lw_source_dir	:- directory where file should be located

	while IFS=, read -r lw_file_name _ _ lw_source_dir _
	do
		[[ -n "$lw_file_name" ]] || continue

		if [[ ! -f "$lp_install_from/$lw_source_dir/$lw_file_name" ]]
		then
			print_msg ERR "ERROR: Cannot Find '$lp_install_from/$lw_source_dir/$lw_file_name'"
			l_ret_status=true
		fi
	done<"$lp_file_list"

	$l_ret_status
} #check_files_missing

#-------------------------------------------------------------------------------
#	Overview: Emulates the functionality of the dos2unix command.
#
# Function:		dostounix
#
# Parameters: conv_file	(required)
#
# Return:			NA
#-------------------------------------------------------------------------------
dostounix()
{ # Using this rather than "dos2unix" as it is not available on the db tiers
	local lp_conv_file

	func_test "${FUNCNAME[0]}" $# 1

	lp_conv_file="$1"

	print_msg "Converting $lp_conv_file to unix format\\n"
	#	s/\r//		        :- Remove windows line endings
	#	s/[[:cntrl:]]//g	:- Remove unknown characters
	#	$a\				        :- Append new line ending to final line in file
	sed -i '
					s/\r//
					s/[[:cntrl:]]//g
					$a\
				 ' "$lp_conv_file"
} #dostounix

#-------------------------------------------------------------------------------
#	Overview: Check for any preparse shell scripts.
#
# Function:		preparse
#
# Parameters: file_list		(required)
#							apps_un			(required)
#							apps_pw			(requierd)
#
# Return:			Success / Fail message
#-------------------------------------------------------------------------------
preparse()
{
	local lp_file_list lp_apps_un lp_apps_pw
	local lw_file_name lw_type lw_add_script_params

	func_test "${FUNCNAME[0]}" $# 3

	lp_file_list="$1"
	lp_apps_un="$2"
	lp_apps_pw="$3"

	# Check for preparse scripts and run them
	#
	# List of columns used in file and what associated data is being used:
	#
	# lw_file_name	:- script file name
	# lw_type				:- identifies if script requires parsing
	# lw_add_script_params		:- additional script parameters

	while IFS=, read -r lw_file_name _ lw_type _ lw_add_script_params _ _ _
	do
		[[ -n "$lw_file_name" ]] || continue

		if [[ "$lw_type" == "PREP" ]]
		then
			cat<<-PREPARSE
				$([[ -z "$lw_add_script_params" ]] && print_msg WARN "Note: No parameters provided for preparse shell script")

				-- load_preparse: Converting to Unix format
				$(dostounix "$lw_file_name")
				-- load_preparse: Changing permissions
				$(chmod $g_verbose a+rx "$lw_file_name")
				-- load_preparse: Running shell script '$lw_file_name' $lw_add_script_params

			PREPARSE

			if "./$lw_file_name" "$lp_apps_un/$lp_apps_pw" "$lw_add_script_params"
			then
				print_msg SUCC "-- Running Preparse Shell Script '$lw_file_name' successful"
			else
				print_msg ERR "-- ERROR: Running Preparse Shell Script '$lw_file_name' failed"
				exit 1
			fi
		fi
	done<"$lp_file_list"
} #preparse

#-------------------------------------------------------------------------------
#	Overview: Check for any XDF import scripts.
#
# Function:		xdf_check
#
# Parameters: file_list		(required)
#
# Return:			Success / Fail message
#-------------------------------------------------------------------------------
xdf_check()
{
	local lp_file_list lp_jdbc_connect
	local lw_file_name lw_type lw_schema_name 
  local l_pwd

	func_test "${FUNCNAME[0]}" $# 2

	lp_file_list="$1"
	lp_jdbc_connect="$2"

	# Check for xdf scripts and get the schema passwords
	#
	# List of columns used in file and what associated data is being used:
	#
	# lw_file_name	:- script file name
	# lw_type				:- identifies if script requires parsing
	# lw_schema_name		:- schema to load the xdf file into

	while IFS=, read -r lw_file_name _ lw_type _ lw_schema_name _ _ _
	do
		[[ -n "$lw_file_name" ]] || continue

		if [[ "$lw_type" == "XDF" ]]
		then
			if ! [[ "${ga_upwd[*]}" =~ $lw_schema_name ]] 
			then
				print_msg MSG "Please enter the password for $lw_schema_name: "
				read -r -s l_pwd < /dev/tty
				login_test "$lw_schema_name" "$l_pwd" "$lp_jdbc_connect"
				ga_upwd+=( "$lw_schema_name" "$l_pwd" )
			fi
		fi
	done<"$lp_file_list"
} #xdf_check


#-------------------------------------------------------------------------------
#	Overview: Get corresponding ID as required
#
# Function:		get_id
#
# Parameters: result_name			(required)
#							org_or_file			(required)
#							sqlplus_connect	(required)
#							is_org					(default: false)
#
# Return:			result_name			(resp/org id)
#-------------------------------------------------------------------------------
get_id()
{
	local lp_result_name lp_org_or_file lp_sqlplus_connect lp_is_org
	local l_respkey l_respapp l_idx l_type

	func_test "${FUNCNAME[0]}" $# 3 4

	lp_result_name="$1"
	lp_org_or_file="$2"
	lp_sqlplus_connect="$3"
	lp_is_org="${4:-false}"

	if $lp_is_org
	then
		l_type=organization

		l_idx=($(sqlplus -s /nolog <<-EOF
							set heading off
							set feedback off
							set pau off
							set linesize 120
							set pages 32767
							connect $lp_sqlplus_connect
							select organization_id
							from   hr_all_organization_units hou
							where  hou.name = '$lp_org_or_file';
							exit
						EOF
						))

		print_msg "  deriving organization_id for org name: $lp_org_or_file."
	else
		l_type=responsibility

		l_respkey=$(grep responsibilityKey "$lp_org_or_file")
		l_respkey=${l_respkey##*responsibilityKey=\"}
		l_respkey=${l_respkey%%\"*}

		l_respapp=$(grep responsibilityAppShortName "$lp_org_or_file")
		l_respapp=${l_respapp##*responsibilityAppShortName=\"}
		l_respapp=${l_respapp%%\"*}

		l_idx=($(sqlplus -s /nolog <<-EOF
							set heading off
							set feedback off
							set pau off
							set linesize 120
							set pages 32767
							connect $lp_sqlplus_connect
							select fr.responsibility_id
							from   fnd_responsibility fr,
									   fnd_application    fa
							where  upper(fr.responsibility_key) = upper('$l_respkey')
							and    fr.application_id = fa.application_id
							and    upper(fa.application_short_name) = upper('$l_respapp');
							exit
						EOF
						))

#check where xml_file comes from
		cat<<-RESPID
			Extracting responsibility key and app short name from XML file.
			[$lp_org_or_file]

			Responsibility Key: $l_respkey
			Responsibility App: $l_respapp

		RESPID
	fi

	if [[ -z "${l_idx[1]}" ]]
	then
		print_msg ERR "  ERROR: unable to derive $l_type id"
		exit 1
	else
		read -r "$lp_result_name" <<< "${l_idx[1]}"
	fi
} #get_id

#-------------------------------------------------------------------------------
#	Overview: Create any symbolic links required
#
# Function:		create_softlink
#
# Parameters: target_dir			(required)
#							target_file			(required)
#							source_dir
#							source_file
#
# Return:			success / fail message
#-------------------------------------------------------------------------------
create_softlink()
{
	local lp_target_dir lp_target_file lp_source_dir lp_source_file
	local la_target la_source
	local l_path_to_target_file l_path_to_source_file
	local l_target_linked_to l_create_link l_switch
	local l_rel_sym_path l_diff_count l_sym_path_to_source

	# Set target directories
	lp_target_dir="$1"
	lp_target_file="$2"

	if (( $# == 4 ))
	then
		lp_source_dir="$3"
		lp_source_file="$4"
	elif (( $# == 2 ))
	then
		lp_source_dir="$FND_TOP/bin"
		lp_source_file="fndcpesr"

		if [[ "${lp_target_file##*.}" == prog ]]
		then
			lp_target_file=${lp_target_file%.prog}
		else
			print_msg ERR "ERROR: Script file names must end in '.prog' and '$lp_target_file' does not"
			exit 1
		fi
	else
		print_msg ERR "ERROR: Incorrect number of arguments passed to ${FUNCNAME[0]}!"
		exit 1
	fi

	# Set default values
	l_create_link=true

	# Create relative links for all softlinks so clones do not get compromised
	la_target=( ${lp_target_dir//\// } )
	la_source=( ${lp_source_dir//\// } )

	l_rel_sym_path=""
	l_diff_count=0

	for l_index in ${!la_target[*]}
	do
		[[ "${la_source[l_index]}" != "${la_target[l_index]}" || -n "$l_rel_sym_path" ]] && l_rel_sym_path+='../'
		[[ "${la_source[l_index]}" != "${la_target[l_index]}" ]] && (( l_diff_count == 0 )) && l_diff_count=$l_index
	done

	l_rel_sym_path+="${la_source[*]:l_diff_count}"

	l_path_to_source_file="${lp_source_dir//\/\///}$lp_source_file"
	l_path_to_target_file="${lp_target_dir//\/\///}$lp_target_file"
	l_sym_path_to_source="${l_rel_sym_path// //}/$lp_source_file"

	cat<<-SLINK

		-- softlink: Creating softlink '$l_path_to_target_file' pointing to '$l_sym_path_to_source'..."
	SLINK

	# Need to test if file or sym link already exist as link may be broken
	if [[ -e "$l_path_to_target_file" || -h "$l_path_to_target_file" ]]
	then
		if [[ -h "$l_path_to_target_file" ]]
		then
		# If target is already a sym link check what it is linked to 
		# and if we need to change it
			l_target_linked_to=$(readlink -e "$l_path_to_target_file")

			if [[ "$l_target_linked_to" == "$l_path_to_source_file" ]]
			then
				if [[ "$(readlink "$l_path_to_target_file")" == "$l_sym_path_to_source" ]]
				then
					print_msg "  -- Softlink '$l_path_to_target_file' already exists and is valid.."
					l_create_link=false
				else
					print_msg WARN "  -- Softlink 'Updating to relative symlink :- $l_sym_path_to_source\\n"
					l_switch='-f'
				fi
			else
#				[[ $(yn "Do you wish to replace this link? [y/n] ") == yes ]] && l_switch='-f' \
#																																			|| l_create_link=false
				if [[ -n  "$l_target_linked_to" ]]
				then
					# TODO : Need to find an alternative where the user can be asked what to do
					print_msg ERR "  -- ERROR: '$l_path_to_target_file' exists and is linked to $l_target_linked_to incorrectly.  Aborting."
					exit 1
				else
					print_msg WARN "$l_path_to_target_file is currently a broken link"
					l_switch='-f'
				fi
			fi
		elif [[ -f "$l_path_to_target_file" ]]
		then
		# If tagrget is a regular file, advise user it will be backed up and replaced by link
			print_msg WARN "  -- '$l_path_to_target_file' exists - creating softlink with backup parameter"
			l_switch='--backup'
		else
		# Target is not of a type that we should link to
			print_msg ERR "  -- ERROR: '$l_path_to_target_file' exists and is not a softlink or regular file.  Aborting."
			exit 1
		fi
	elif ! [[ -d "$lp_target_dir" ]]
	then
		mkdir -p "$lp_target_dir"
	fi

	# Test and create link as necessary
	if $l_create_link
	then
		if ln $g_verbose "$l_switch" -s "$l_sym_path_to_source" "$l_path_to_target_file"
		then
			print_msg SUCC "-- Creation of softlink for '$l_path_to_target_file' successful"
		else
			print_msg ERR "-- ERROR: Creation of softlink for '$l_path_to_target_file' failed"
			exit 1
		fi
	fi
} #create_softlink

#-------------------------------------------------------------------------------
#	Overview: Make directory if required
#
# Function:		make_directory
#
# Parameters: directory_path	(required)
#
# Return:			NA
#-------------------------------------------------------------------------------
make_directory()
{
	# Used by the java installation code to create the required subdirectories in the 
	# module top or P_CUSTOM_TOP top prior to copying (if the directories do not exist)
	local lp_directory_path

	func_test "${FUNCNAME[0]}" $# 1

	lp_directory_path="$1"

	if [[ ! -d $lp_directory_path ]]
	then
		print_msg WARN "'$lp_directory_path' doesn't exist - creating it"
		mkdir $g_verbose -p "$lp_directory_path"
	fi
} #make_directory

#-------------------------------------------------------------------------------
#	Overview: Copy file as required
#
# Function:   copy_file_basic
#
# Parameters: file_with_path    (required)
#             target_dest       (required)
#
# Return:     success / fail message
#-------------------------------------------------------------------------------
copy_file_basic()
{
	local lp_file_with_path lp_target_dest

	func_test "${FUNCNAME[0]}" $# 2

	lp_file_with_path="$1"
	lp_target_dest="$2"

	print_msg "  - copying '$lp_file_with_path' to '$lp_target_dest'."
	make_directory "$lp_target_dest"

	if cp $g_verbose -pf --backup=t "$lp_file_with_path" "$lp_target_dest"
	then
		print_msg SUCC "    copy of '$lp_file_with_path' successful"
	else
		print_msg ERR  "    ERROR: copy of '$lp_file_with_path' failed"
		exit 1
	fi
} #copy_file_basic

#-------------------------------------------------------------------------------
#	Overview: Test if file needs to be copied and also
#						set any permissions required
#
# Function:		copy_files
#
# Parameters: file_list       (required)
#             install_from    (required)
#             java_dir        (required)
#             sqlplus_connect (required)
#
# Return:			NA
#-------------------------------------------------------------------------------
copy_files()
{
	local lp_file_list lp_install_from lp_java_dir lp_sqlplus_connect
	local lw_file_name lw_top_dir lw_type lw_source_dir lw_optional_1
	local lw_org_name
	local l_source_file l_id l_is_text l_is_exe l_is_oaf l_regex

	func_test "${FUNCNAME[0]}" $# 4

	lp_file_list="$1"
	lp_install_from="$2"
	lp_java_dir="$3"
	lp_sqlplus_connect="$4"

	# Tests / regexes
	l_is_text='^text'
	l_is_exe='[AB]EX[CL]|SHLL'
	l_is_oaf='BC4J|JPX|JAVA|MDS[RO]?'
	l_regex='^\$([^/]*)'

	# List of columns used in file and what associated data is being used:
	#
	# Fields: lw_file_name  :- file to work on
	#         lw_top_dir    :- target directory
	#         lw_type       :- all but type PREP to be worked on
	#         lw_source_dir :- original location of file under lp_install_from
	#         lw_optional_1 :- if required, sub directory location
	#         lw_org_name   :- org name if required for sql query

	while IFS=, read -r lw_file_name lw_top_dir lw_type lw_source_dir lw_optional_1 _ lw_org_name _
	do
		[[ -n "$lw_file_name" ]] || continue

		[[ "$lw_top_dir" =~ $l_regex ]] && lw_top_dir=${!BASH_REMATCH[1]}/${lw_top_dir#*/}

		[[ "$lw_optional_1" == . || "$lw_optional_1" == ./ ]] && unset lw_optional_1
		lw_optional_1="${lw_optional_1:+${lw_optional_1%/}/}"

		if [[ "$lw_type" == PREP ]]
		then
			print_msg "Skipping file copy for $lw_file_name"
		else
			cat<<-COPY_START

				+------------------------------------------------------------------------------+
				Copying File: $lw_file_name
				      (Type = $lw_type)
			COPY_START

			l_source_file="$lp_install_from/$lw_source_dir/$lw_file_name"

			# dostounix the file, except if its a binary
			if [[ $(file -bi "$l_source_file") =~ $l_is_text ]]
			then
				dostounix "$l_source_file"
			else
				print_msg "  Skipping dos2unix for '$lw_file_name'"
			fi

			# If the file is an ascii executable, or a binary executable ensure
			# the unix permissions are set correctly.
			if [[ "$lw_type" =~ $l_is_exe ]]
			then
				chmod $g_verbose a+rx "$l_source_file"
				print_msg "  Ensuring '$l_source_file' is readable and executable"
			else
				chmod $g_verbose a+r "$l_source_file"
				print_msg "  Ensuring '$l_source_file' is readable"
			fi

			print_msg "  Copying '$l_source_file'..."

			if [[ "$lw_type" =~ $l_is_oaf ]]
			then
				l_id=

				if [[ "$lw_type" == MDSR ]]
				then
					get_id l_id "$l_source_file" "$lp_sqlplus_connect"
				elif [[ "$lw_type" == MDSO ]]
				then
					get_id l_id "$lw_org_name" "$lp_sqlplus_connect" true
				fi

				copy_file_basic "$l_source_file" "${lw_top_dir%/}/${lw_optional_1#/}$l_id"

				if [[ "$lp_java_dir" != "$lw_top_dir" ]]
				then
					copy_file_basic "$l_source_file" "$lp_java_dir/${lw_optional_1#/}$l_id"
				fi

				if [[ "$lw_type" == BC4J && $(hostname) =~ agric ]]
				then
					create_softlink "$OA_JAVA/$lw_optional_1" "$lw_file_name" \
													"$lp_java_dir/$lw_optional_1" "$lw_file_name"
				fi
			else
				copy_file_basic "$l_source_file" "$lw_top_dir"
			fi 
		fi
	done < "$lp_file_list"
} #copy_files

#-------------------------------------------------------------------------------
#	Overview: Create sql files for cleaning menus, request_groups,
#						responsibilities and wf_roles
#
# Function:		create_sql
#
# Parameters: clean_type	(required)
#
# Return:			NA
#-------------------------------------------------------------------------------
create_sql()
{
	local lp_clean_type
	local l_file_name

	func_test "${FUNCNAME[0]}" $# 1

	lp_clean_type="$1"

	l_file_name="xxclean_$lp_clean_type.sql"

	# make sure the script to clear off child records from a menu is in the current directory
	if [[ ! -f "$l_file_name" ]]
	then
		cat<<-CLEAN

			-- Creating $l_file_name
		CLEAN

		cat> "$l_file_name" <<-SQL
			set verify off
			set serveroutput on size 1000000
			set feedback off
		SQL

		case "$lp_clean_type" in
			menu) cat>> "$l_file_name" <<-SQL
							DECLARE
							  lv_menu_name   varchar2(30) := '&1';
							  CURSOR menu_entries IS
							  SELECT fme.entry_sequence, 
						  		     fme.prompt, 
						  		     sm.menu_name sub_menu_name, 
						  		     ff.function_name
						  	FROM   fnd_menu_entries_vl fme,
						  		     fnd_form_functions  ff,
						  		     fnd_menus           fm,
						  		     fnd_menus           sm
						  	WHERE  fm.menu_name    = lv_menu_name
						  	AND    fm.menu_id      = fme.menu_id
						  	AND    fme.function_id = ff.function_id(+)
						  	AND    fme.sub_menu_id = sm.menu_id(+);
		
							BEGIN
								dbms_output.ENABLE(NULL);
							  dbms_output.put_line('Removing child records for menu ['||lv_menu_name||']');
							  FOR datarec IN menu_entries LOOP
							    dbms_output.put_line(substr('Removing menu entry: ['||datarec.entry_sequence||'] '||datarec.prompt,1,254));
							    fnd_function_security.MENU_ENTRY (
							                                      menu_name       => lv_menu_name,
							                                      entry_sequence  => datarec.entry_sequence,
							                                      PROMPT          => datarec.prompt,
							                                      sub_menu_name   => datarec.sub_menu_name,
							                                      function_name   => datarec.function_name,
							                                      delete_flag     => 'Y'
							                                     );
							  END LOOP;
						SQL
						;;
			reqg) cat>> "$l_file_name" <<-SQL
							DECLARE
							  lv_rg_name   VARCHAR2(30) := '&1';
							  lv_asn       VARCHAR2(50) := '&2';
							  CURSOR rg_cp_units IS
							    SELECT fcp.concurrent_program_name program_sn,
							           cpa.application_short_name  program_asn,
							           frg.request_group_name,
							           rga.application_short_name  request_group_asn
							    FROM   fnd_request_groups      frg,
							           fnd_request_group_units frgu,
							           fnd_concurrent_programs fcp,
							           fnd_application         cpa,
							           fnd_application         rga
							    WHERE  frg.request_group_name     = lv_rg_name
									AND    rga.application_short_name = lv_asn
							    AND    frgu.request_group_id      = frg.request_group_id
							    AND    frgu.application_id        = frg.application_id
							    AND    rga.application_id         = frg.application_id
							    AND    fcp.concurrent_program_id  = frgu.request_unit_id
							    AND    fcp.application_id         = frgu.unit_application_id
							    AND    cpa.application_id         = fcp.application_id
							    AND    frgu.request_unit_type     = 'P';
							  CURSOR rg_rs_units IS
							    SELECT frg.request_group_name     request_group_name,
							           rga.application_short_name request_group_asn,
							           frs.request_set_name,
							           rsa.application_short_name request_set_asn
							    FROM   fnd_request_groups      frg,
							           fnd_request_group_units frgu,
							           fnd_request_sets        frs,
							           fnd_application         rsa,
							           fnd_application         rga
							    WHERE  frg.request_group_name     = lv_rg_name
									AND    rga.application_short_name = lv_asn
							    AND    frgu.request_group_id      = frg.request_group_id
							    AND    frgu.application_id        = frg.application_id
							    AND    rga.application_id         = frg.application_id
							    AND    frs.request_set_id         = frgu.request_unit_id
							    AND    frs.application_id         = frgu.unit_application_id
							    AND    rsa.application_id         = frs.application_id
							    AND    frgu.request_unit_type     = 'S';
							  CURSOR rg_ap_units IS
							    SELECT frg.request_group_name,
							           rga.application_short_name request_group_asn,
							           app.application_short_name,
							           app.application_name
							    FROM   fnd_request_groups      frg,
							           fnd_request_group_units frgu,
							           fnd_application_vl      app,
							           fnd_application         rga
							    WHERE  frg.request_group_name     = lv_rg_name
									AND    rga.application_short_name = lv_asn
							    AND    frgu.request_group_id      = frg.request_group_id
							    AND    frgu.application_id        = frg.application_id
							    AND    rga.application_id         = frg.application_id
							    AND    app.application_id         = frgu.unit_application_id
							    AND    frgu.request_unit_type     = 'A';
						BEGIN
					    dbms_output.ENABLE(NULL);
					    dbms_output.put_line('Removing child concurrent programs from request group ['||lv_rg_name||']');
					    FOR datarec IN rg_cp_units LOOP
					      dbms_output.put_line(substr('Removing CP from group: ['||datarec.program_asn||'] '||datarec.program_sn,1,254));
					      fnd_program.remove_from_group(
					                                    program_short_name   => datarec.program_sn,
					                                    program_application  => datarec.program_asn,
					                                    request_group        => datarec.request_group_name,
					                                    group_application    => datarec.request_group_asn
						                                 );
					    END LOOP;
					    dbms_output.put_line('Removing child request sets from request group ['||lv_rg_name||']');
					    FOR datarec IN rg_rs_units LOOP
					      dbms_output.put_line(substr('Removing RS from group: ['||datarec.request_set_asn||'] '||datarec.request_set_name,1,254));
					      fnd_set.remove_set_from_group(
					                                    request_set         => datarec.request_set_name,
					                                    set_application     => datarec.request_set_asn,
					                                    request_group       => datarec.request_group_name,
					                                    group_application   => datarec.request_group_asn
						                                 );
					    END LOOP;
					    dbms_output.put_line('Removing child applications from request group ['||lv_rg_name||']');
					    FOR datarec IN rg_ap_units LOOP
					      dbms_output.put_line(substr('Removing AP from group: ['||datarec.application_short_name||'] '||datarec.application_name,1,254));
					      fnd_program.remove_application_from_group(
					                                                application_name    => datarec.application_short_name,
					                                                request_group       => datarec.request_group_name,
					                                                group_application   => datarec.request_group_asn
						                                             );
					    END LOOP;
					SQL
					;;
			resp) cat>> "$l_file_name" <<-SQL
							DECLARE
							  lv_resp_key    varchar2(30) := '&1';
							  lv_asn         varchar2(50) := '&2';
							  CURSOR fnd_resp_func is
							    SELECT fr.responsibility_key, 
							           frf.rule_type,
							           DECODE(frf.rule_type,'F',ff.function_name,
							                                'M',fm.menu_name) rule_name,
							           'Y' delete_flag
							    FROM   fnd_responsibility fr,
							           fnd_application    fa,
							           fnd_resp_functions frf,
							           fnd_menus          fm,
							           fnd_form_functions ff
							    WHERE  fr.responsibility_key     = lv_resp_key
							    AND    fa.application_short_name = lv_asn
							    AND    fr.application_id         = fa.application_id
							    AND    frf.responsibility_id     = fr.responsibility_id
							    AND    frf.application_id        = fr.application_id
							    AND    ff.function_id(+)         = frf.action_id
							    AND    fm.menu_id(+)             = frf.action_id;
							  CURSOR fnd_resp_exclude IS
							    SELECT aei.responsibility_id,
							           aei.resp_application_id application_id,
							           aei.attribute_code,
							           aei.attribute_application_id attribute_appl_id
							    FROM   ak_excluded_items  aei,
							           fnd_responsibility fr,
							           fnd_application    fa
							    WHERE  fr.responsibility_key     = lv_resp_key
							    AND    fa.application_short_name = lv_asn
							    AND    fr.application_id         = fa.application_id
							    AND    aei.responsibility_id     = fr.responsibility_id;
							  CURSOR fnd_resp_sec_attr IS
							    SELECT aei.responsibility_id,
							           aei.resp_application_id application_id,
							           aei.attribute_code,
							           aei.attribute_application_id attribute_appl_id
							    FROM   ak_resp_security_attributes aei,
							           fnd_responsibility fr,
							           fnd_application fa
							    WHERE  fr.responsibility_key     = lv_resp_key
							    AND    fa.application_short_name = lv_asn
							    AND    fr.application_id         = fa.application_id
							    AND    aei.resp_application_id   = fr.application_id
							    AND    aei.responsibility_id     = fr.responsibility_id;
							BEGIN
							  dbms_output.ENABLE(NULL);
							  dbms_output.put_line('Removing child records for responsibility ['||lv_asn||'] '||lv_resp_key);
							  FOR datarec IN fnd_resp_func LOOP
							    dbms_output.put_line(substr('Removing security rule: ['||datarec.rule_type||'] '||datarec.rule_name,1,254));
							    fnd_function_security.SECURITY_RULE (
							                                         responsibility_key => datarec.responsibility_key,
							                                         rule_type          => datarec.rule_type,
							                                         rule_name          => datarec.rule_name,
							                                         delete_flag        => datarec.delete_flag);
							  END LOOP;
							  --
							  DECLARE
							    lv_status    VARCHAR2(10);
							    ln_msg_count NUMBER;
							    lv_msg_data  varchar2(2000);
							  BEGIN
							    FOR datarec IN fnd_resp_exclude LOOP
							      dbms_output.put_line(substr('Removing resp exclusion: '||datarec.attribute_code,1,254));
							      icx_resp_excl_attr_pub.Delete_Resp_Excl_Attr(
							                                                   p_api_version_number  => 1.0,
							                                                   p_init_msg_list       => FND_API.G_FALSE,
							                                                   p_simulate            => FND_API.G_FALSE,
							                                                   p_commit              => FND_API.G_TRUE,
							                                                   p_validation_level    => FND_API.G_VALID_LEVEL_FULL,
							                                                   p_return_status       => lv_status,
							                                                   p_msg_count           => ln_msg_count,
							                                                   p_msg_data            => lv_msg_data,
							                                                   p_responsibility_id   => datarec.responsibility_id,
							                                                   p_application_id      => datarec.application_id,
							                                                   p_attribute_code      => datarec.attribute_code,
							                                                   p_attribute_appl_id   => datarec.attribute_appl_id
							                                                  );
							    END LOOP;
							
							    FOR datarec IN fnd_resp_sec_attr LOOP
							      dbms_output.put_line(substr('Removing resp securing attribute: '||datarec.attribute_code,1,254));
							      ICX_RESP_SEC_ATTR_PUB.Delete_Resp_Sec_Attr(
							                                                 p_api_version_number  => 1.0,
							                                                 p_init_msg_list       => FND_API.G_FALSE,
							                                                 p_simulate            => FND_API.G_FALSE,
							                                                 p_commit              => FND_API.G_TRUE,
							                                                 p_validation_level    => FND_API.G_VALID_LEVEL_FULL,
							                                                 p_return_status       => lv_status,
							                                                 p_msg_count           => ln_msg_count,
							                                                 p_msg_data            => lv_msg_data,
							                                                 p_responsibility_id   => datarec.responsibility_id,
							                                                 p_application_id      => datarec.application_id,
							                                                 p_attribute_code      => datarec.attribute_code,
							                                                 p_attribute_appl_id   => datarec.attribute_appl_id
							                                                );
							    END LOOP;
                END;
							SQL
							;;
			role) cat>> "$l_file_name" <<-SQL
							DECLARE
							  lv_role_name   VARCHAR2(320) := '&1';
							  CURSOR role_children IS
							    SELECT wrh.relationship_id,
							           wrh.super_name
							    FROM   wf_role_hierarchies wrh
							    WHERE  wrh.sub_name = lv_role_name;
							BEGIN
							  dbms_output.ENABLE(NULL);
							  dbms_output.put_line(substr('Removing child roles from role ['||lv_role_name||']',1,254));
							  FOR datarec IN role_children LOOP
							    dbms_output.put_line(substr('Removing child role: ['||datarec.super_name||']',1,254));
							    wf_role_hierarchy.removeRelationship(
							                                         p_relationshipID => datarec.relationship_id,
							                                         p_forceRemove    => TRUE
							                                        );
							  END LOOP;
							SQL
							;;
		esac

		cat>> "$l_file_name" <<-SQL
			  dbms_output.put_line('Finished');
			END;
			/
			exit
		SQL
	fi
} # create_sql

#-------------------------------------------------------------------------------
#	Overview: Check if any supplied packages are in the
#						correct format and contain required data
#
# Function:		check_package
#
# Parameters: path_to_file		(required)
#							sqlplus_connect	(required)
#
# Return:			if packagae exists to be checked, success of failure of check
#							returned
#-------------------------------------------------------------------------------
check_package()
{
	local lp_path_to_file lp_sqlplus_connect
	local la_create_details
	local l_create_regex l_item
	local l_object_name l_object_type

	func_test "${FUNCNAME[0]}" $# 2

	lp_path_to_file="$1"
	lp_sqlplus_connect="$2"

	l_create_regex='create( or replace)? (\<(function|package|procedure|trigger|type)\>( body)?) ([^ ]*)'

# make sure the script to check for errors is in the current directory
	if [[ ! -f xxcheck_plsql_is_valid.sql ]]
	then
		cat<<-CP

			-- Creating xxcheck_plsql_is_valid.sql
		CP

		cat > xxcheck_plsql_is_valid.sql <<-SQL
			set verify off
			set serveroutput on size 1000000
			set feedback off
		  whenever sqlerror exit failure
			DECLARE
			  lv_object_exists  varchar2(15);
			  lv_object_owner   varchar2(30)  := upper('&1');
			  lv_object_name    varchar2(128) := upper('&2');
			  lv_object_type    varchar2(19)  := upper('&3');
			  lb_header_printed boolean       := FALSE;
			  lb_object_found   boolean       := FALSE;
			  CURSOR object_type_exists is
			    SELECT 'object exists'
			    FROM   dba_objects
			    WHERE  object_name = lv_object_name
			    AND    object_type = lv_object_type
					AND    owner       = lv_object_owner;
			  CURSOR errors_exist(pv_owner       in varchar2,
			                      pv_type        in varchar2,
			                      pv_object_name in varchar2) is
			    SELECT substr('Line.Position ('||line||'.'||position||') '||text,1,254) error_detail
			    FROM   dba_errors
			    WHERE  attribute = 'ERROR'
			    AND    owner     = pv_owner
			    AND    type      = pv_type
			    AND    name      = pv_object_name
			    ORDER BY sequence;
			BEGIN
			  open object_type_exists;
			  fetch object_type_exists into lv_object_exists;
			  IF object_type_exists%notfound THEN
			    close object_type_exists;
			    dbms_output.put_line('Errors:- Unable to find owner ('||lv_object_owner||') type ('||lv_object_type||') name ('||lv_object_name||') in dba_objects!?');
			  ELSE
			    close object_type_exists;
			    FOR errors in errors_exist(lv_object_owner, lv_object_type, lv_object_name) LOOP
			      IF not lb_header_printed THEN
			        dbms_output.put_line('Errors found in: '||lv_object_type||' '||lv_object_owner||'.'||lv_object_name);
			        lb_header_printed := TRUE;
			      END IF;
			      dbms_output.put_line(errors.error_detail);
			    END LOOP;
			  END IF;
			END;
			/
			exit
		SQL
	fi

	IFS="%"
	la_create_details=($(awk 'BEGIN{IGNORECASE=1;ORS=" "}/^create/{x=1}x{print tolower($0)}/\<[ai]s\>/{x=0;print "%"}' "$lp_path_to_file"))
	unset IFS

	if [[ -n "${la_create_details[0]}" ]]
	then
		# Loop through and find our objects
		for l_item in "${la_create_details[@]}"
		do
			if [[ "$l_item" =~ $l_create_regex ]]
			then
				l_object_type="${BASH_REMATCH[2]}"
				l_object_name="${BASH_REMATCH[5]#*.}"
				[[ "${BASH_REMATCH[5]}" == "$l_object_name" ]] || l_object_owner="${BASH_REMATCH[5]%.*}"

				la_errors=($(	sqlplus -s /nolog <<-SQL
											connect $lp_sqlplus_connect
											@xxcheck_plsql_is_valid.sql ${l_object_owner:-APPS} $l_object_name '$l_object_type'
										SQL
										))

				if [[ ! "${la_errors[*]}" =~ Errors ]]
				then
					print_msg SUCC "  -- No errors found - successfully installed $l_object_type ${l_object_owner:-APPS}.$l_object_name} "
				else
					print_msg ERR "$(cat<<-ERR
						-------------------ERROR--------------------
						  Installation not successful
							object name :- $l_object_name
							object type :- $l_object_type
							${la_errors[@]}
							Please check and rerun
							Aborted.
						--------------------------------------------
					ERR
					)"
					exit 1
				fi
			fi
		done
	else
		cat<<-NOOBJ
			-- SQL Script did not contain a package, package body, procedure, type or function
			-- Assuming successful install
		NOOBJ
	fi
} #check_package

#-------------------------------------------------------------------------------
#	Overview: Formats sql accordingly and executes
#
# Function:		install_sql
#
# Parameters: top_dir					(required)
#							source_file			(required)
#							apps_un					(required)
#							sqlplus_connect	(required)
#
# Return:			success / fail message
#-------------------------------------------------------------------------------
install_sql()
{
	local lp_top_dir lp_source_file lp_apps_un lp_sqlplus_connect
	local la_check_defines
	local l_path_to_file l_counter

	func_test "${FUNCNAME[0]}" $# 4

	lp_source_file="$1"
	lp_top_dir="$2"
	lp_apps_un="$3"
	lp_sqlplus_connect="$4"

	l_path_to_file=$lp_top_dir/$lp_source_file

	OLD=$IFS
	IFS=$'\n'
	la_check_defines=($(awk 'match($0,/(&|^[[:space:]]*set define .*)/,f){if(f[1] || f[2])print (f[1]?f[1]:f[2])}' "$l_path_to_file") )
	IFS=$OLD

	l_counter=0
	for l_item in "${la_check_defines[@]}"
	do
		case "$l_item" in
			'set define off')
					 (( l_counter == 0 )) && (( l_counter-- ));;
			'&') (( l_counter >= 0 )) && (( l_counter++ ));;
				*) print_msg ERR "$(cat<<-ERR
						----------------------------ERROR----------------------------
						  File contains $l_item.  Please replace with corresponding
							chr(num) before including in a code pack for this
							installer.
							Aborted.
						-------------------------------------------------------------
						ERR
						)"
				 exit 1;;
		esac
	done

	(( l_counter > 0 )) && sed -i '1 i set define off' "$l_path_to_file"

	# add a trailing / and an exit to the end of the file if they're not already there
	# add whenever sqlerror exit failure to the start of the file if its not already there
	cat<<-FILE_START
		Stripping trailing blank lines from '$l_path_to_file'
		$(sed -i ':a /^[[:space:]]*$/{$d;N;ba}' "$l_path_to_file")
		Appending trailing / and EXIT if required to '$l_path_to_file'
		$(sed -i -e '$,/./{/^end/I s|$|\n/\nexit|}' -e '${/^\// s|$|\nexit|}' "$l_path_to_file")
		Checking for WHENEVER SQLERROR in $l_path_to_file
		$(sed -i '1{/whenever sqlerror/I! i\
					WHENEVER SQLERROR EXIT FAILURE
					}' "$l_path_to_file")

		-- install_sql: Executing SQL Script '$lp_source_file' as $lp_apps_un...
	FILE_START

	if sqlplus -s /nolog <<-EOF
			connect $lp_sqlplus_connect
			@$l_path_to_file
		EOF
	then
		print_msg SUCC "  -- SQLPlus returned success for '$lp_source_file'"
		check_package "$l_path_to_file" "$lp_sqlplus_connect"
	else
		print_msg ERR "$(cat<<-ERR
			----------------------------ERROR----------------------------
			  Installation not successful for $l_path_to_file
				Please check and rerun
				Aborted.
			-------------------------------------------------------------
		ERR
		)"
		exit 1
	fi
} #install_sql

#-------------------------------------------------------------------------------
#	Overview: Check if the stupid xdfcmp tool actually created the object it said it did
#
# Function:		check_object
#
# Parameters: path_to_file		(required)
#							sqlplus_connect	(required)
#
# Return:			success or failure of object exists check
#-------------------------------------------------------------------------------
check_object()
{
	local lp_path_to_file lp_sqlplus_connect
	local la_errors

	func_test "${FUNCNAME[0]}" $# 2

	lp_path_to_file="$1"
	lp_sqlplus_connect="$2"

# make sure the xslt is in the current directory
	if [[ ! -f xxextract_objects.xslt ]]
	then
		cat<<-CP

			-- Creating xxextract_objects.xslt
		CP

		cat > xxextract_objects.xslt <<-XSLT
			<?xml version="1.0"?>
			<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
				<xsl:output method="text"/>
					<xsl:template match="/">
						<xsl:for-each select="/ROWSET/ROW/*/SCHEMA_OBJ">
						<xsl:copy-of select="OWNER_NAME" />
						<xsl:text>|</xsl:text>
						<xsl:copy-of select="NAME" />
						<xsl:text>|</xsl:text>
						<xsl:copy-of select="TYPE_NAME" />
						<xsl:text>&#10;</xsl:text>
					</xsl:for-each>
				</xsl:template>
			</xsl:stylesheet>
		XSLT
	fi

# make sure the script to check for objects is in the current directory
	if [[ ! -f xxcheck_object_exists.sql ]]
	then
		cat<<-CP

			-- Creating xxcheck_object_exists.sql
		CP

		cat > xxcheck_object_exists.sql <<-SQL
			set verify off
			set serveroutput on size 1000000
			set feedback off
		  whenever sqlerror exit failure
			DECLARE
			  lv_object_exists  varchar2(15);
			  lv_object_owner   varchar2(30)  := upper('&1');
			  lv_object_name    varchar2(128) := upper('&2');
			  lv_object_type    varchar2(19)  := upper('&3');
			  lb_header_printed boolean       := FALSE;
			  lb_object_found   boolean       := FALSE;
			  CURSOR object_type_exists is
			    SELECT 'object exists'
			    FROM   dba_objects
			    WHERE  object_name = lv_object_name
			    AND    object_type = lv_object_type
					AND    owner       = lv_object_owner;
			BEGIN
			  open object_type_exists;
			  fetch object_type_exists into lv_object_exists;
			  IF object_type_exists%notfound THEN
			    close object_type_exists;
			    dbms_output.put_line('Errors:- Unable to find owner ('||lv_object_owner||') type ('||lv_object_type||') name ('||lv_object_name||') in dba_objects!?');
			  ELSE
			    close object_type_exists;
			  END IF;
			END;
			/
			exit
		SQL
	fi

	for object in $(xsltproc xxextract_objects.xslt "$lp_path_to_file")
	do
		params=(${object//|/ })
		la_errors=($(	sqlplus -s /nolog <<-SQL
										connect $lp_sqlplus_connect
										@xxcheck_object_exists.sql ${params[0]} ${params[1]} ${params[2]}
										SQL
							))
	
		if [[ ! "${la_errors[*]}" =~ Errors ]]
		then
			print_msg SUCC "  -- No errors found - successfully installed ${params[2]} ${params[0]}.${params[1]} "
		else
			print_msg ERR "$(cat<<-ERR
				--------------------------ERROR--------------------------
				  XDF Installation not successful
					object name :- ${params[1]}
					object type :- ${params[2]}
					${la_errors[@]}
					Please check and rerun
					Aborted.
				---------------------------------------------------------
			ERR
			)"
			exit 1
		fi
	done
} #check_object

#-------------------------------------------------------------------------------
#	Overview: Uses xdfcmp.pl to install an xdf file, then calls check_object to 
#           see if it actually installed it!
#
# Function:		install_xdf
#
# Parameters: top_dir					(required)
#							source_file			(required)
#							apps_un					(required)
#							sqlplus_connect	(required)
#
# Return:			success / fail message
#-------------------------------------------------------------------------------
install_xdf()
{
	local lp_source_file lp_top_dir lp_schema_un lp_apps_un lp_apps_pw lp_sqlplus_connect
	local l_path_to_file ls_pwd

	func_test "${FUNCNAME[0]}" $# 6

	lp_source_file="$1"
	lp_top_dir="$2"
  lp_schema_un="$3"
	lp_apps_un="$4"
  lp_apps_pw="$5"
	lp_sqlplus_connect="$6"

	l_path_to_file=$lp_top_dir/$lp_source_file

	for ln_idx in "${!ga_upwd[@]}"
	do
		if [[ "${ga_upwd[ln_idx]}" == "$lp_schema_un" ]]
		then
			ls_pwd="${ga_upwd[ln_idx+1]}"
			break
		fi
	done

	#run the xdf utility to import the xdf file.
	perl "$FND_TOP/bin/xdfcmp.pl" "$lp_schema_un/$ls_pwd" "$l_path_to_file" "$lp_apps_un/$lp_apps_pw"

	print_msg SUCC "  -- xdfcmp returned success for '$lp_source_file'"
	check_object "$l_path_to_file" "$lp_sqlplus_connect"

} #install_xdf

#-------------------------------------------------------------------------------
#	Overview:		Check created_by user_id is greater than 2
#
# Function:		check_created_by
#
# Parameters: file_name					(required)
#							sqlplus_connect		(required)
#             asn               (required)
#             lob_code          (required)
#
# Return:			NA
#-------------------------------------------------------------------------------
check_created_by()
{
	# passed in variables
	local lp_sqlplus_connect
	local lp_asn lp_lob_code
	local l_lob_code_field l_table_name l_object

	lp_type="$1"
	lp_sqlplus_connect="$2"
  lp_asn="$3"
  lp_lob_code="$4"

	print_msg "-- Checking created_by in XDO tables is greater than 2.."
  
  case $lp_type  in
  XRTF|XTXT|XXML|XXSL)
    l_lob_code_field="template_code"
    l_table_name="xdo_templates_b"
    l_object="template"
    ;;
	XDTP)
    l_lob_code_field="data_source_code"
    l_table_name="xdo_ds_definitions_b"
    l_object="data definition"
    ;;
  XBRT)
    l_lob_code_field="lob_code"
    l_table_name="xdo_lobs"
    l_object="bursting control file"
    ;;
  XSMP)
    # ignore
    :;;
  esac
  
  
  # Set created_by to 'CONCURRENT MANAGER' user_id
	if [[ -n "$l_lob_code_field" ]] && sqlplus -s /nolog <<-SQL &>/dev/null
		    WHENEVER SQLERROR EXIT 1
		    connect $lp_sqlplus_connect
		    update $l_table_name
		    set    created_by = 4
		    where  $l_lob_code_field       = '$lp_lob_code'
		    and    application_short_name  = '$lp_asn'
		    and    created_by              < 3;
		    exit
			SQL
	then
		print_msg SUCC "Created by details corrected for $l_object"
	else
		print_msg ERR "ERROR: Failed to update created by details for $l_object in $l_table_name for $lp_asn:$lp_lob_code"
		exit 1
	fi
} #check_created_by

#-------------------------------------------------------------------------------
#	Overview: Display type sepcific information as required in input file
#
# Function:		create_template
#
# Parameters: NA
#
# Return:			NA
#-------------------------------------------------------------------------------
create_template()
{
	local l_file_name

	(( $# == 1 )) && l_file_name="$1" || l_file_name="${0##*/}"

	# Set file extension
	l_file_name="${l_file_name%.*}.files"

	echo '# FILE_NAME, TOP_DIR, TYPE, SOURCE_DIR, OPTIONAL_1, OPTIONAL_2, ORG_NAME' > "$l_file_name" && \
	print_msg SUCC "Template file $l_file_name created successfully"

	exit 0
} #create_template

#-------------------------------------------------------------------------------
#	Overview: Display type specific information as required in input file
#
# Function:		get_info
#
# Parameters: type (required)
#
# Return:			outputs type info
#-------------------------------------------------------------------------------
get_info()
{
	local la_current_types la_current_descriptions
	local lf_type
	local l_output l_upper

	la_current_types=($(get_types -a ))
	la_current_types=( TYPE '----' "${la_current_types[@]}" )

	la_current_descriptions=($(get_types -d ))
	la_current_descriptions=(	DESCRIPTION '-----------' "${la_current_descriptions[@]}" )

	if (( $# == 0 ))
	then
		for t in ${!la_current_types[*]}
		do
			printf "%s|%s\\n" "${la_current_types[t]}" "${la_current_descriptions[t]}"
		done | column -t -s'|' | less -RF
	else
		print_msg "$(cat<<-MSG
		Currently there are 7 fields used in the input file.
		Below is a listing of the fields and their associated names:

		Field  Name
		  1    file_name
		  2    top_dir
		  3    type
		  4    source_dir
		  5    optional_1
		  6    optional_2
		  7    org_name

		The first 4 fields are mandatory for all types
		------------------------------------------------------------
		MSG
		)"

		for lf_type
		do
			unset l_output
			l_upper="$(tr '[:lower:]' '[:upper:]' <<< "$lf_type")"

			case "$l_upper" in
				AEXL|AKLD|ALRT|ALTD|AMAC|AMAS|AMAT|AMAU|AMCD|AMAG|\
				AMRL|ATTC|ATYP|BC4J|BCNT|BCMP|BLAY|BMAP|BNE|BNEI|\
				BPRM|CPRG|DFF|DMSG|FAUG|FCUS|FFCT|FFRM|FTOL|FUNC|\
				ILDT|JAVA|JPX|LKUP|PDRV|PINF|PREP|PROF|PSTL|\
				SQL|TLAY|VSET|XDOT|XMAP|ADFF)
									 l_output=("Mandatory fields only");;
				URSP)      l_output=("Optional :- additional text in field 5 (to purge responsibility first)");;
				ROLE)      l_output=("Optional :- additional text in field 5 (to purge role first)");;
				REQG)      l_output=("Optional :- additional text in field 5 (to purge request group first)");;
				MENU)      l_output=("Optional :- additional MENU_NAME in field 5 (to purge it first)");;
				WFT)       l_output=("Optional :- mode in field 5 (default :- UPLOAD)");;
				PERL|SHLL) l_output=("Optional :- additional script parameters in field 5");;
				AEXC|BEXC|CPY|FMB|PLL)
									 l_output=( "Both or neither:"
															"Optional :- target directory for soft link in field 5"
															"         :- target file for soft link in field 6" );;
				WFE|WFX)	 l_output=("Mandatory :- event / subscription key in field 5");;
				DTD)			 l_output=( "Mandatory :- root element in field 5"
															"          :- dtd version in field 6" );;
				MDS|MDSO|MDSR)
									 l_output=( "Mandatory :- MDS sub-directory in field 5"
															"          :- root package directory in field 6" )
				[[ "$l_upper" == MDSO ]] && l_output+=( "          :- ORG name in field 7" );;
				XBRT|XDTP|XRTF|XSMP|XTXT|XXML|XXSL)
									 l_output=( "Mandatory :- LOB code in field 5"
															"          :- ASN in field 6" );;
				XDF)
									 l_output=( "Mandatory :- DB Schema Name in field 5" );;
				*)	print_msg ERR "ERROR: $lf_type is not a known type!!!"
						continue;;
			esac

			print_msg MSG "-- $l_upper :- "
			printf "\\n%s" "${l_output[@]}"
		done

		print_msg '------------------------------------------------------------'
	fi
	exit 0
} #get_info

# -------------------------------------------------------------------
# Processes each file in the file list and
# performs the necessary actions.
# -------------------------------------------------------------------

#-------------------------------------------------------------------------------
#	Overview: Load database files as required
#
# Function:		load_db_files
#
# Parameters: file_list					(required)
#							apps_un						(required)
#							apps_pw						(required)
#							jdbc_connect			(required)
#							xml_jdbc_connect	(required)
#							sqlplus_connect		(required)
#							install_from			(required)
#							java_dir					(required)
#
# Return:			success / fail message as required
#-------------------------------------------------------------------------------
load_db_files()
{
	# passed in variables
	local lp_java_dir lp_file_list lp_apps_un lp_apps_pw
	local lp_jdbc_connect lp_xml_jdbc_connect lp_sqlplus_connect
	local lp_install_from
	# for loop variables
	local lf_name lf_xdo_type
	# while loop variables
	local lw_file_name lw_top_dir lw_type lw_source_dir
	local lw_optional_1 lw_optional_2 lw_org_name #lw_inc_port
	# local array variables
	local -a la_names la_xdo_type la_xdo_file_type
	local -a la_xdo_lob_type la_start_msg la_command la_territory 
	local -a la_sql_params la_forms_gen_command la_lct_path
	# local variables
	local l_cnt l_file_exists l_object l_msg_name l_id_name l_id l_ildt_params
	local l_x_file l_module_type l_result_msg l_need_to_cd l_need_to_update
	local l_create_sql_group l_fnd_force l_expand_var_regex l_dir_ending_regex

	func_test "${FUNCNAME[0]}" $# 8
	#
	# Set passed in variables
	#
	lp_file_list="$1"
	lp_apps_un="$2"
	lp_apps_pw="$3"
	lp_jdbc_connect="$4"
	lp_xml_jdbc_connect="$5"
	lp_sqlplus_connect="$6"
	lp_install_from="$7"
	lp_java_dir="$8"

	#
	# Set local variables
	#
	l_need_to_update='XBRT|XDTP|XRTF|XSMP|XTXT|XXML|XXSL' # list of types that need check_created_by run
  l_need_to_cd='PERL|SHLL|ZIP'							# list of types that 'cd' somewhere
	l_create_sql_group='MENU|REQG|ROLE|URSP'	# list of types that call create_sql
	l_expand_var_regex='^\$([^/]*)'						# Used to expand variables passed in from file
	l_dir_ending_regex='AEXC|BEXC|CPY|FMB|PLL|MDS|MDSO|MDSR' # list of types that require sanitising of dir path

	# Retrieve necessary array details
	la_names=($(get_types -f ))
#	la_lct=($(get_types -p))
	la_xdo_type=($(get_types -x ))
	la_xdo_lob_type=($(get_types -xl ))
	la_xdo_file_type=($(get_types -xf ))

	#
	#	Assign types to an integer value
	#
	l_cnt=0
	for lf_name in "${la_names[@]}"
	do
		(( "$lf_name" = l_cnt++ ))
	done

	l_cnt=0
	for lf_xdo_type in "${la_xdo_type[@]}"
	do
		(( "$lf_xdo_type" = l_cnt++ ))
	done
	#
	#	Process file list to call appropriate function
	#
	# List of columns used in file and what associated data is being used:
	#
	#	Fields: lw_file_name		:- file to be reviewed
	#					lw_top_dir			:- where file is located or target directory
	#					lw_type					:- defines what tasks to perform
	#					lw_source_dir		:- source location
	#					lw_optional_1		:- optional details for some types
	#					lw_optional_2		:- optional details for some types
	#					lw_org_name			:- org name to be used in sql query

	while IFS=, read -r lw_file_name lw_top_dir lw_type lw_source_dir lw_optional_1 lw_optional_2 lw_org_name _
	do
		[[ -n "$lw_file_name" ]] || continue

		if [[ "$lw_type" =~ $l_dir_ending_regex ]]
		then
			[[ "$lw_optional_1" == . || "$lw_optional_1" == ./ ]] && unset lw_optional_1
			lw_optional_1="${lw_optional_1:+${lw_optional_1%/}/}"
			lw_optional_1="${lw_optional_1#/}"
		fi

		# Currently this is only working on a single field.
		# Should we identify that other fields may be affected we may need to loop over them
		[[ "$lw_top_dir" =~ $l_expand_var_regex ]] && lw_top_dir=${!BASH_REMATCH[1]}/${lw_top_dir#*/}
		lw_top_dir=${lw_top_dir%/}

		cat<<-LOAD_START


		In Load_db_file for: $lw_file_name, type: $lw_type ...
		LOAD_START

		unset la_start_msg
		unset la_command
		l_result_msg=
		l_file_exists=.

		#
		# Each action section which calls a system command
		# will require as a minimum the following 3 items:
		#
		# la_start_msg	-> Output to identify that this section has started
		# l_result_msg	-> Output to proceed either success or error message
		# la_command		-> The actual command to be executed (this is assigned
		#									 in pieces to an array)
		#
		case "$lw_type" in
			#
			# Types that don't need to be "loaded", simply copied
			# Handler for PREP scripts (that we've already run, just so we don't report 
			# unhandled file type errors
			#
			BC4J|PREP) print_msg "  --Type is $lw_type, copy only, loading not applicable";;
			#
			# Most likely, all these will require nothing further (they're all diffent sorts
			# of COPY ONLY options).  But there is a possibility that we may want to softlink 
			# the copied file elsewhere.  Eg. CPYB a report RDF file into the custom top's 
			# reports directory, then softlink it into the standard application top reports 
			# directory.  So check if we need to
			#
			AEXC|AEXL|BEXC|CPY)
						if [[ -n "$lw_optional_1" && -n "$lw_optional_2" || "$lw_type" == AEXL ]]
						then
							create_softlink "$lw_optional_1" "$lw_optional_2" "$lw_top_dir" "$lw_file_name"
						elif [[ -z "$lw_optional_1" && -z "$lw_optional_2" ]]
						then
							print_msg "--Type is $lw_type, copy only, loading not applicable"
						fi
						;;

			#
			# AK Loader
			#
			AKLD) la_start_msg="load_ak: AK Load  $lw_top_dir/$lw_file_name..."
						l_result_msg="AK Load of '$lw_file_name'"
						la_command=( java oracle.apps.ak.akload
												 "$lp_apps_un"
												 "$lp_apps_pw"
												 THIN
												 "$lp_jdbc_connect"
												 UPLOAD
												 "$lw_top_dir/$lw_file_name"
												 UPDATE AMERICAN_AMERICA.WE8ISO8859P1
											 )
						;;

			#
			# FND Loader
			#
				
			ALTD|AMAC|AMAS|AMAT|AMAU|AMCD|AMAG|AMRL|ATYP|ALRT|\
			ATTC|BCNT|BCMP|BLAY|BMAP|BNE|BNEI|BPRM|CPRG|DMSG|DFF|\
			FAUG|FCUS|FFCT|FFRM|FTOL|FUNC|ILDT|LKUP|MENU|PROF|PINF|\
			PSTL|PDRV|REQG|ROLE|TLAY|URSP|VSET|XDOT|ADFF)
						# Use the following to get the lct path to use
						la_lct_path=($(sed -nr '/FNDLOAD/s%[^@]*@([^:]+):([^ ]+).*%\1_TOP /\2%p' "$lw_top_dir/$lw_file_name"))

						[[ "$lw_type" == LKUP ]] && l_fnd_force= || l_fnd_force=FORCE
						[[ "$lw_type" == ILDT ]] && l_ildt_params="- WARNING=YES UPLOAD_MODE=REPLACE" || l_ildt_params= 
			
						#[[ "$lw_type" == XDOT ]] && check_created_by "$lw_top_dir/$lw_file_name" "$lp_sqlplus_connect"
			
						if [[ "$lw_type" =~  $l_create_sql_group && -z "$lw_optional_1" ]]
						then
							l_fnd_force=
			
							case "$lw_type" in
								MENU) l_type=menu
											la_sql_params=$lw_optional_1;;
								REQG) l_type=reqg
											IFS='|'
											la_sql_params=($(awk -F\" '/BEGIN REQUEST_GROUP /{print $2"|"$4}' "$lw_top_dir/$lw_file_name"))
											unset IFS;;
								ROLE) l_type=role
											la_sql_params=($(awk -F\" '/BEGIN WF_ROLE "[^Z]/{print $2}' "$lw_top_dir/$lw_file_name"));;
								URSP) l_type=resp
											la_sql_params=($(awk -F\" '/BEGIN.*RESPO/{print $2,$4}' "$lw_top_dir/$lw_file_name"));;
							esac
			
							print_msg "-- load_$l_type: Installing LDT '$lw_top_dir/$lw_file_name'..."
			
							create_sql "$l_type"
			
							print_msg " Running pre-clean script for [${la_sql_params[*]}]..."
			
							sqlplus -s /nolog <<-SQL
								connect $lp_sqlplus_connect
								@xxclean_$l_type.sql '${la_sql_params[0]}' ${la_sql_params[1]:+'${la_sql_params[1]}'}
							SQL
						else
							print_msg " NOT Running pre-clean script for $lw_type... Populate NAME in optional_1 column 5 in $lp_file_list if you want to"						
						fi
			
						la_start_msg="load_fnd: Installing LDT '$lw_top_dir/$lw_file_name'..."
						l_result_msg="FNDLOAD of '$lw_file_name'"
						# "${la_lct[${!lw_type}]}" previous version of retrieving path
						la_command=( FNDLOAD
												 "$lp_apps_un/$lp_apps_pw@${ORACLE_SID:-$TWO_TASK}"
												 0 Y UPLOAD
												 "${!la_lct_path[0]}${la_lct_path[1]}"
												 "$lw_top_dir/$lw_file_name"
												 "$l_ildt_params"
												 CUSTOM_MODE="$l_fnd_force"
											 )
						;;
            
			#
			# XML Importer and JPX Importer for Java CEMLI code
			#
			 JPX)	la_start_msg="load_jpx: JPXImport '$lp_java_dir/$lw_optional_1$lw_file_name'..."
					  l_result_msg="Import '$lw_file_name'"
					  la_command=( java oracle.jrad.tools.xml.importer.JPXImporter
												 "$lp_java_dir/$lw_optional_1$lw_file_name"
												 -username "$lp_apps_un"
												 -password "$lp_apps_pw"
												 -dbconnection "$lp_jdbc_connect"
											 )
						;;

			MDS|MDSO|MDSR)
						l_msg_name=
						l_id_name=
						l_id=
			
						if [[ "$lw_type" == MDSO ]]
						then
							get_id l_id "$lw_org_name" "$lp_sqlplus_connect" true
							l_msg_name=org
							l_id_name=Organisation
						elif [[ "$lw_type" == MDSR ]]
						then
							get_id l_id "$lp_install_from/$lw_source_dir/$lw_file_name" "$lp_sqlplus_connect"
							l_msg_name=resp
							l_id_name=Responsibility
						fi
			
						la_start_msg=( "load${l_msg_name:+_$l_msg_name}_xml: XMLImport  '$lw_top_dir/$lw_optional_1${l_id:+$l_id/}$lw_file_name'..." )
						[[ -n "$l_id" ]] && la_start_msg+=( "  $l_id_name ID : $l_id" )
						l_result_msg="Import $lw_file_name"
						la_command=( java oracle.jrad.tools.xml.importer.XMLImporter
												 "$lw_top_dir/$lw_optional_1${l_id:+$l_id/}$lw_file_name"
												 -username     "$lp_apps_un"
												 -password     "$lp_apps_pw"
												 -dbconnection "$lp_jdbc_connect"
												 -rootdir      "$lw_top_dir/$lw_optional_1${l_id:+$l_id/}"
												 -rootpackage  "$lw_optional_2${l_id:+$l_id/}"
											 )
						;;

			JAVA)	la_start_msg=("load_lava: javac '$lw_top_dir/$lw_optional_1$lw_file_name'...")
						l_result_msg="Compile of '$lw_file_name'"
						la_command=( javac
												 "$lw_top_dir/$lw_optional_1$lw_file_name"
											 )
						;;

			#
			# Workflow related imports.
			#
			WFE|WFX)
						l_object=$([[ "$lw_type" == WFE ]] && echo EVENTS || echo SUBSCRIPTIONS)
						la_start_msg=("load_wfx: Upload $l_object '$lw_top_dir/$lw_file_name'...")
						l_result_msg="WFXUpload of '$lw_file_name'"
						la_command=( java oracle.apps.fnd.wf.WFXLoad
												 -u "$lp_apps_un"
												 "$lp_apps_pw"
												 "$lp_jdbc_connect"
												 THIN US
												 "$lw_file_name"
												 "$l_object"
												 "$lw_optional_1"
											 )
						;;

			WFT)  la_start_msg=("load_wft: Upload WFT '$lw_top_dir/$lw_file_name' - Mode: ${lw_optional_1:-UPLOAD}...")
					  l_result_msg="WFLOAD of '$lw_file_name'"
					  la_command=( WFLOAD
												 "$lp_sqlplus_connect"
												 0 Y "${lw_optional_1:-UPLOAD}"
												 "$lw_top_dir/$lw_file_name"
											 )
						;;

			#
			# Forms FMB and PLL files.
			#
			FMB|PLL) 
						l_x_file="${lw_file_name%.*}.$([[ $lw_type == PLL ]] && echo plx || echo fmx)"
						l_module_type="$([[ $lw_type == PLL ]] && echo LIBRARY || echo FORM)"
						l_file_exists="$lw_top_dir/$l_x_file"
			
						if [[ "$lw_type" == FMB ]]
						then
							if [[ -z "$lw_optional_1" && -z "$lw_optional_2" ]]
							then
								print_msg WARN "No soflink specified for this fmx in $lp_file_list"
							fi
							if [[ -n "$lw_optional_2" ]]
							then
								create_softlink "$lw_optional_1" "$lw_optional_2" "$lw_top_dir" "$l_x_file"
							fi
						else
							if [[ -n "$lw_optional_1" ]]
							then
								# Requested a softlink into $AU_TOP/resource.
								# Create_softlink will backup the existing files if required.
								create_softlink "$AU_TOP/resource/" "$l_x_file" "$lw_top_dir" "$l_x_file"
								create_softlink "$AU_TOP/resource/" "$l_x_file" "$lw_top_dir" "$l_x_file"
							fi
						fi
			
						if [[ -f "$l_file_exists" ]]
						then
							print_msg "  -- Removing existing version of '$l_file_exists'"
							rm $g_verbose -f "$l_file_exists"
						fi
			
						# Use the e-business version to determine which forms compiler to use.
						la_forms_gen_command=($(sqlplus -s /nolog <<-EOF
																			set termout off
																			connect $lp_sqlplus_connect
																			set heading off
																			set feedback off
																			set pagesize 32767
																			set linesize 120
																			SELECT CASE substr(release_name,1,2)
																						 WHEN '11' THEN
																							'f60gen'
																						 WHEN '12' THEN
																							'frmcmp_batch'
																						 ELSE
																							 'UNKNOWN_APPS_RELEASE'
																						 END
																			FROM   apps.fnd_product_groups;
																			exit
																		EOF
																		))
			
						la_start_msg=("load_$(tr '[:upper:]' '[:lower:]' <<<"$lw_type"): Generate '$lw_top_dir/$lw_file_name' to '$l_x_file'...")
						l_result_msg="Generate '$l_x_file'"
						la_command=( ${la_forms_gen_command[1]}
												 module="$lw_top_dir/$lw_file_name"
												 userid="$lp_apps_un/$lp_apps_pw"
												 module_type="$l_module_type"
												 compile_all=YES
												 output_file="$l_file_exists"
											 )
						;;

			#
			# XML Publisher 
      #   - XDTP: Data Template.
      #   - XSMP: Sample Data.
      #   - XXSL: XSL-FO File
      #   - XXML: XSL-XML File
      #   - XTXT: XSL-TEXT File
      #   - XRTF: RTF file.
      #   - XBRT: Bursting Control file.
      #
			XBRT|XDTP|XRTF|XSMP|XTXT|XXML|XXSL)     
						la_territory=($(sqlplus -s /nolog <<-SQL
															set heading off
															set feedback off
															set linesize 100
															set termout off
															connect $lp_sqlplus_connect
															SELECT default_territory
															FROM   xdo_templates_b
															WHERE  template_code          = '$lw_optional_1'
															AND    application_short_name = '$lw_optional_2'
															/
															exit
														SQL
						))

						gv_additional_msg="XDOLoader does not return success or failure, so please check log"
			
						la_start_msg=( "  Loading into territory ${la_territory[1]:-00}"
													 ""
													 "load_xdo: Loading ${la_xdo_lob_type[${!lw_type}]} - '$lw_top_dir/$lw_file_name'"
												 )
						l_result_msg="XDO Load of '$lw_file_name'"
						la_command=( java oracle.apps.xdo.oa.util.XDOLoader UPLOAD
												 -DB_USERNAME     "$lp_apps_un"
												 -DB_PASSWORD     "$lp_apps_pw"
												 -JDBC_CONNECTION "$lp_jdbc_connect"
												 -LOB_TYPE        "${la_xdo_lob_type[${!lw_type}]}"
												 -APPS_SHORT_NAME "$lw_optional_2"
												 -LOB_CODE        "$lw_optional_1"
												 -LANGUAGE        en
												 -TERRITORY       "${la_territory[1]:-00}"
												 -XDO_FILE_TYPE   "${la_xdo_file_type[${!lw_type}]}"
												 -FILE_NAME       "$lw_top_dir/$lw_file_name"
												 -CUSTOM_MODE     FORCE
												 -NLS_LANG        "$NLS_LANG"
											 )
            
						;;

			# 
			# XML Gateway
			#
			DTD)  la_start_msg=("load_dtd: LoadDTDToClob '$lw_file_name' into $lp_xml_jdbc_connect...")
						l_result_msg="Import '$lw_file_name'"
						la_command=( java oracle.apps.ecx.loader.LoadDTDToClob
												 "$lp_apps_un"
												 "$lp_apps_pw"
												 "$lp_xml_jdbc_connect"
												 "$lw_top_dir/$lw_file_name"
												 "$lw_optional_1"
												 "$lw_optional_2"
											 )
						;;

			XMAP) la_start_msg=("load_dtd: LoadMap '$lw_file_name' into $lp_xml_jdbc_connect ...")
						l_result_msg="Import '$lw_file_name'"
						la_command=( java oracle.apps.ecx.loader.LoadMap
												 "$lp_apps_un"
												 "$lp_apps_pw"
												 "$lp_xml_jdbc_connect"
												 "$lw_top_dir/$lw_file_name"
											 )
						;;

			#
			# Others.
			#
			PERL) [[ -z "$lw_optional_1" ]] && print_msg WARN "Note: No Parameters specified for Perl script"
	
						cd "$lw_top_dir" || exit
	
						la_start_msg=("load_perl: Running perl script $lw_file_name with parameters :- $lw_optional_1")
						l_result_msg="Running PERL Script '$lw_file_name'"
						la_command=( perl
												 "$lw_file_name"
												 "$lp_apps_un/$lp_apps_pw"
												 "$lw_optional_1"
											 )
						;;
	
			SHLL) [[ -z "$lw_optional_1" ]] && print_msg WARN "Note: No Parameters specified for shell script"
	
						cd "$lw_top_dir" || exit
	
						la_start_msg=("load_shell: Running shell script '$lw_file_name' with parameters :- $lw_optional_1")
						l_result_msg="Running Shell Script '$lw_file_name'"
						la_command=( "./$lw_file_name"
												 "$lp_apps_un/$lp_apps_pw"
												 "$lw_optional_1"
											 )
						;;
			#
			# XDF File Importer
			#
			 XDF)	install_xdf   "$lw_file_name" "$lw_top_dir" "$lw_optional_1" "$lp_apps_un" "$lp_apps_pw" "$lp_sqlplus_connect";;
            
			 SQL)	install_sql   "$lw_file_name" "$lw_top_dir" "$lp_apps_un" "$lp_sqlplus_connect";;
#			 ZIP)	load_zip      "$lw_top_dir" "$lw_file_name" "$lw_org_name" $lw_inc_port ;; #--Currently not used
				 *)	print_msg ERR "  --ERROR: load_db_file does not cater for $lw_type type !!!"
						exit 1;;
		esac

		if [[ -n "${la_command[0]}" ]]
		then
			cat<<-START_MSG
	
				$(printf -- "-- %s\\n" "${la_start_msg[@]}")
			START_MSG
	
			if "${la_command[@]}" && [[ -e "$l_file_exists" ]]
			then
				print_msg SUCC "  -- $l_result_msg successful"
			else
				print_msg ERR "  -- ERROR: $l_result_msg failed"
				exit 1
			fi
	
			if [[ "$lw_type" =~ $l_need_to_cd ]]
			then
				cd - || exit
			fi
 			[[ "$lw_type" =~ $l_need_to_update ]] && check_created_by "$lw_type" "$lp_sqlplus_connect" "$lw_optional_2" "$lw_optional_1"
			[[ "$lw_type" == JAVA ]] && create_softlink "$OA_JAVA/$lw_optional_1" "${lw_file_name%.*}.class" \
																	"$lp_java_dir/$lw_optional_1" "${lw_file_name%.*}.class"
		fi
	done <"$lp_file_list"
} #load_db_files

#-------------------------------------------------------------------------------
# Overview:		Check custom / java top directory values are accessible
#
# Function:		top_test
#
# Parameters:	top_name	(required)
#							top_path	(required)
#							java			(optional)
#
# Return:			Path to TOP dir
#-------------------------------------------------------------------------------
top_test()
{
	local lp_top_name lp_top_path lp_java

	func_test "${FUNCNAME[0]}" $# 2 3

	lp_top_name="$1"
	lp_java="$3"

	case "${2:0:1}" in
		[[:upper:]]) lp_top_path="${!2}$lp_java";;
		$) lp_top_path="${2:1}"
			 lp_top_path="${!lp_top_path}$lp_java";;
		/) lp_top_path="$2$lp_java";;
		*) print_msg ERR "ERROR: $2 is not a valid entry"
			 exit 1;;
	esac

	if [[ -d "$lp_top_path" ]]
	then
		print_msg "$lp_top_name is set to $lp_top_path"
		read -r "$lp_top_name" <<< "$lp_top_path"
	else
		print_msg ERR "$(cat<<-ERR
			--------------------------ERROR--------------------------
			  $lp_top_name is set to $lp_top_path - unable to access
				this directory.
			  Please check your entered value and restart script.
			---------------------------------------------------------
		ERR
		)"

		exit 1
	fi
} #top_test

main()
{
	local lp_file_list lp_use_defaults
	local la_rac_env la_connect_strings
	local l_install_from l_apps_pw l_log_file l_custom_top
	local l_java_top l_resource_dir l_jdbc_connect l_apps_un
	local l_xml_jdbc_connect l_sqlplus_connect l_cowner
	local l_first_top
  

	[[ "$1" == '-d' ]] && lp_use_defaults=false || lp_use_defaults=true
	shift

	[[ -z "$1" ]] && lp_file_list="${0%.*}.files" || lp_file_list="$1"

	if [[ ! -r "$lp_file_list" ]]
	then
		print_msg ERR "$(cat<<-ERR
			--------------------------ERROR--------------------------
			  $lp_file_list is not accessible by $USER
				Either check spelling of file name or enter file name
				at the command line if default (${0%.*}.files) has
				been used and does not exist.
			---------------------------------------------------------
		ERR
		)" 2

		exit 1
	fi

	dostounix "$lp_file_list"
	validate_file_list "$lp_file_list"
  
	l_cstm_top=($(awk '/PROD_TOP/ && /XX/{print substr($1,2);exit}' "$CONTEXT_FILE"))
  l_xbol_top=($(awk '/PROD_TOP/ && /XBOL/{print substr($1,2);exit}' "$CONTEXT_FILE"))
  
  l_first_top=${l_xbol_top[0]:-${l_cstm_top[0]}}

	l_install_from="$PWD"
	l_log_file="$l_install_from/${0%.*}_${TWO_TASK:-$ORACLE_SID}_$(date +%F-%H-%M-%S).log"

	open_close_log 'open' "$l_log_file"

	# Get default settings from user, if required
	if $lp_use_defaults
	then
		print_msg MSG "Please enter the user name for database access [apps]: "
		read -r l_apps_un

		print_msg WARN "$(cat<<-'INFO'
		When entering options for the next questions you have 3 choices:
		  1. Env variable name => XX_TOP
		  2. Env variable      => $XX_TOP
		  3. Full path         => /u01/../../etc
		INFO
		)"
		print_msg MSG "Please enter custom top [$l_first_top]: "
		read -r l_custom_top

    if [[ "$(yn "\\nDo you wish to use the OA_JAVA directory for java files? [y/n] ")"  == yes ]]
		then    
      l_java_top=${OA_JAVA}
      l_java_append=""
    else
		  print_msg MSG "Please enter java top, (/java will be appended) [${l_custom_top:-$l_first_top}]: "
		  read -r l_java_top
      l_java_append="/java"
    fi
  else
    if [[ -d $l_first_top/java ]] 
    then
      l_java_append="/java"
    else
      l_java_top="${OA_JAVA}"
      l_java_append=""
    fi
	fi

	[[ -z "$l_custom_top" ]] && l_custom_top="$l_first_top"
	top_test l_custom_top "$l_custom_top"

	[[ -z "$l_java_top" ]] && l_java_top="$l_custom_top"
	top_test l_java_top "$l_java_top" $l_java_append

	# required to allow previous information to show before asking for password
	sleep 1s

	l_resource_dir="$l_custom_top/resource"

	[[ -d "$l_resource_dir" ]] || mkdir -p "$l_resource_dir"

	l_apps_un="${l_apps_un:-apps}"

	print_msg MSG "Please enter the password for $l_apps_un: "
	read -r -s l_apps_pw

	#load the apps schema/password combination into ga_upwd
	ga_upwd=( "$l_apps_un" "$l_apps_pw" )

	#load any passwords for XDF files
	xdf_check "$lp_file_list" "$l_jdbc_connect"  
  
	cat<<-START

	-------------------
	Installation script
	-------------------

	Version 1.0

	Started : $(date)

	---------------------------

	START

	if [[ -z "$CONTEXT_FILE" || ! -f "$CONTEXT_FILE" ]]
	then
		print_msg ERR "ERROR: The CONTEXT_FILE shell variable must exist and contain the full path and name of the e-business environment context file."
		exit 1
	else
		l_cowner="$(stat -c %U "$CONTEXT_FILE")"

		if [[ "$(stat -c %U "$PWD")" != "$l_cowner" ]]
		then
			print_msg ERR "ERROR: Installation files must all be owned by $l_cowner"
			exit 1
		fi
	fi

	cat<<-TNSPING
	Using tnsping to ping service: ${TWO_TASK:-$ORACLE_SID}

	---------------------------

	TNSPING

	if ! tnsping "${TWO_TASK:-$ORACLE_SID}" > /dev/null
	then
		print_msg ERR "ERROR: The TWO_TASK or ORACLE_SID variables must point to an available instance"
		exit 1
	fi

	# Get the JDBC Connect String from CONTEXT_FILE
	l_jdbc_connect=$(sed -rn '/\/jdbc_url>/s%(.*@|</.*)%%gp' "$CONTEXT_FILE")

	# Create connection string for sql queries
	l_sqlplus_connect="$l_apps_un/$l_apps_pw@$l_jdbc_connect"

	login_test "$l_apps_un" "$l_apps_pw" "$l_jdbc_connect"
  
	# Determine if this is RAC or not 
	print_msg "Connecting to ${TWO_TASK:-$ORACLE_SID} to determine if this is RAC"

	la_rac_env=($(sqlplus -s /nolog <<-EOF
							set termout off
							connect $l_sqlplus_connect
							set heading off
							set feedback off
							set pagesize 32767
							set linesize 120
							SELECT CASE
											WHEN vi.instance_name = vd.name then
												'N'
											ELSE
												'Y'
											END RAC_ENV
							FROM   v\$instance vi,
										 v\$database vd;
							exit
						EOF
						))

		# Case will assign the following to connect_strings array
		# [0] Host
		# [1] Port
		# [2] SID/ServiceName
		# This information is required by the XML Gateway Java Class Loaders ONLY at this stage

		case "${la_rac_env[1]}" in
			Y)	cat<<-RAC
				This is a RAC environment
				Attempting to get an instance, host and port for the XML Gateway Routines
				RAC

				# Index 1 is SID (instance_name)
				# Index 2 is host
				la_connect_strings=($(sqlplus -s /nolog <<-EOF
														set termout off
														connect $l_sqlplus_connect
														set heading off
														set feedback off
														set pagesize 32767
														set linesize 120
														SELECT instance_name||' '||host_name
														FROM   v\$instance vi;
														exit
													EOF
													))

					print_msg "Attempting to get the sqlnet port for ${la_connect_strings[1]} on ${la_connect_strings[2]}"

					# Break up the returned results from tnsping
					la_connect_strings=($(tnsping "${la_connect_strings[1]}" | awk -vhost="${la_connect_strings[2]%%.*}" -F= '/HOST|PORT|SID|SERVER_NAME/{a[++i]=$2;print}$0 ~ h{x=i}END{if($0 ~ /OK/)print a[x],a[x+1],a[x+2]}' RS="[()\n]+"))
					;;
				N)	print_msg "This is NOT a RAC environment"
					# Break up the JDBC Connect String
					la_connect_strings=($(awk -F= '/HOST|SID|PORT|SERVER_NAME/{print $2}' RS="[()\n]+" <<<"$l_jdbc_connect"))
					;;
				*)	print_msg ERR "ERROR: RAC environment query failed."
					exit 1
					;;
			esac

			# Assign connection string values, delimited by a colon (:)
			l_xml_jdbc_connect=$(IFS=:;echo "${la_connect_strings[*]}")

			if check_files_missing "$l_install_from" "$lp_file_list"
			then
				print_msg ERR "ERROR: Not all required files are present"
				exit 1
			fi

			cat<<-PARAMS

			------------------------------------------------------------------------
			Parameter List
			------------------------------------------------------------------------

			Universal Application Schema name           : $l_apps_un
			File List                                   : $lp_file_list
			Install From                                : $l_install_from
			Connect String                              : $l_jdbc_connect
			SID Specific Connect String (RAC)           : $l_xml_jdbc_connect
			Custom Top                                  : $l_custom_top
			Custom Java Top                             : $l_java_top

			------------------------------------------------------------------------

			PARAMS

			if [[ $(yn "Continue with install? [y/n] ") == no ]]
			then
				print_msg WARN "Aborted : $(date)"
				exit 1
			fi

			echo >&7

			preparse "$lp_file_list" "$l_apps_un" "$l_apps_pw"

			copy_files "$lp_file_list" "$l_install_from" "$l_java_top" "$l_sqlplus_connect"

			load_db_files "$lp_file_list" "$l_apps_un" "$l_apps_pw" "$l_jdbc_connect" \
				"$l_xml_jdbc_connect" "$l_sqlplus_connect" "$l_install_from" "$l_java_top"


			cat<<-FIN

			------------------------------------------------------------------------

			Installation Completed : $(date)

			------------------------------------------------------------------------

			Please refer to the Log file $l_log_file for success/failure of this Installation
			FIN

			open_close_log
} #main

# -------------------------------------------------
# Starting point of the Installation script
# -------------------------------------------------

# Test if env file has been sourced
if [[ -z $APPL_TOP ]]
then
	print_msg ERR "ERROR: You have not sourced your env file.\\nPlease do so and restart script"
	exit 2
fi

# Check not more than 3 parameters passed to script
if (( $# > 3 )) && ! [[ "$*" =~ -i ]]
then
	print_msg ERR "ERROR: Invalid Number of parameters specified ($#)"

	run_instructions
fi

parse_params switches params "$@"

for sw in $switches
do
	case $sw in
		-d|--defaults) default='-d';;
		-t|--template) create_template "${@:2}";;
		-v|--verbose ) g_verbose='-v';;
		-i|--info    ) get_info "${@:2}";;
		-h|--help|*  ) run_instructions;;
	esac
done

main "$default" "$params"

print_msg SUCC "Script completed successfully :)"

[[ -n "$gv_additional_msg" ]] && print_msg WARN "$gv_additional_msg"

exit 0