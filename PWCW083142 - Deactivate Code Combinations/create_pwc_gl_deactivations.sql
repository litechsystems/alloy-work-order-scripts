set serveroutput on

declare
  lr_obj  xxfnd_installer.object_create_record;
  lv_error varchar2(4000);
begin
    dbms_output.enable(null);
    --
    lr_obj.object_owner := 'APPS';
    lr_obj.object_name  := 'PWC_GL_DEACTIVATIONS';
    lr_obj.object_type  := 'TABLE';
    lr_obj.create_ddl   := q'[
create table pwc_gl_deactivations(
    concatenated_segments varchar2(250)
  , entity                varchar2(25)
  , bu                    varchar2(25)
  , location              varchar2(25)
  , activity              varchar2(25)
  , type                  varchar2(25)
  , gl_status             varchar2(1)  -- disable program statuses ('N'ot Exists, 'A'ctive, 'D'isabled before processing, 'C'omplete processing, 'X' Unknown FMS status, 'E'rror occurred )
                                       -- reanable program statuses (i'G'nore, 'R'eanable, 'F'ixed)
  , code_combination_id   number
  , status_msg            varchar2(250)
)]';
    --
    xxfnd_installer.create_if_required(lr_obj,lv_error);
end;
/
