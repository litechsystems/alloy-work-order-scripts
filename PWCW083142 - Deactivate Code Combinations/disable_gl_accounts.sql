set serveroutput on

declare
    ln_total number := 0;
    lt_start timestamp := systimestamp;
    lt_end   timestamp;
    --
    gl_rec gl_code_combinations%rowtype;
    cursor deac_records is
        select pg.code_combination_id
             , gcc.rowid x_rowid
        from   pwc_gl_deactivations pg
             , gl_code_combinations gcc
        where  pg.gl_status = 'A'
        and    pg.code_combination_id = gcc.code_combination_id
--        and    rownum < 10
        for update of gl_status;
    --
    procedure set_status(
        pv_status in varchar2
      , pv_message in varchar2
    ) is
    begin
        update pwc_gl_deactivations
        set    gl_status = pv_status
             , status_msg = substr(pv_message,1,250)
        where current of deac_records;
    end set_status;
begin
    dbms_output.enable(null);
    dbms_output.put_line(to_char(lt_start,'DD-MON-YYYY HH24:MI:SS.FFFF')||': Program Start');
    -- split up the concatenated segments into individual values
    update pwc_gl_deactivations
    set entity = substr(concatenated_segments,1,2)
      , bu = substr(concatenated_segments,4,3)
      , location = substr(concatenated_segments,8,2)
      , activity = substr(concatenated_segments,11,2)
      , type = substr(concatenated_segments,14,3)
    where instr(concatenated_segments,'.',1,3) = 10;
    dbms_output.put_line(to_char(lt_start,'DD-MON-YYYY HH24:MI:SS.FFFF')||': Split '||SQL%ROWCOUNT||' concatenated segments in PWC_DEACTIVATIONS into individual segments');

    update pwc_gl_deactivations
    set entity = substr(concatenated_segments,1,2)
      , bu = substr(concatenated_segments,4,3)
      , location = substr(concatenated_segments,8,6)
      , activity = null
      , type = null
    where instr(concatenated_segments,'.',1,3) = 14;
    dbms_output.put_line(to_char(lt_start,'DD-MON-YYYY HH24:MI:SS.FFFF')||': Split '||SQL%ROWCOUNT||' concatenated segments in PWC_DEACTIVATIONS into individual segments (COA 50368)');

    -- find accounts to process
    update pwc_gl_deactivations pcc
    set    gl_status = nvl( (
                        select case gcc.enabled_flag 
                               when 'Y' then 'A'
                               when 'N' then 'D'
                               else 'X'
                               end
                        from   gl_code_combinations gcc
                        where  pcc.entity = gcc.segment1
                        and    pcc.bu = gcc.segment2
                        and    pcc.location = gcc.segment3
                        and    coalesce(pcc.activity,'X') = coalesce(gcc.segment4,'X')
                        and    coalesce(pcc.type,'X') = coalesce(gcc.segment5,'X')), 'N')
         , code_combination_id = 
                       (select gcc.code_combination_id
                        from   gl_code_combinations gcc
                        where  pcc.entity = gcc.segment1
                        and    pcc.bu = gcc.segment2
                        and    pcc.location = gcc.segment3
                        and    coalesce(pcc.activity,'X') = coalesce(gcc.segment4,'X')
                        and    coalesce(pcc.type,'X') = coalesce(gcc.segment5,'X'))
    where  nvl(gl_status,'X') not in ('C','E');
    dbms_output.put_line(to_char(lt_start,'DD-MON-YYYY HH24:MI:SS.FFFF')||': Checked status of '||SQL%ROWCOUNT||' accounts in PWC_DEACTIVATIONS against FMS');
    commit;
    --
    for datarec in deac_records loop
        begin
            gl_rec := null;
            gl_rec.code_combination_id := datarec.code_combination_id;
            gl_code_combinations_pkg.select_row(gl_rec);
            gl_code_combinations_pkg.lock_row(
                x_rowid                     => datarec.x_rowid                      
              , x_code_combination_id       => gl_rec.code_combination_id           
              , x_alt_code_combination_id   => gl_rec.alternate_code_combination_id 
              , x_chart_of_accounts_id      => gl_rec.chart_of_accounts_id          
              , x_detail_posting_f          => gl_rec.detail_posting_allowed_flag   
              , x_detail_budgeting_f        => gl_rec.detail_budgeting_allowed_flag 
              , x_balanced_budgetf          => gl_rec.igi_balanced_budget_flag      
              , x_account_type              => gl_rec.account_type                  
              , x_enabled_flag              => gl_rec.enabled_flag                  
              , x_summary_flag              => gl_rec.summary_flag                  
              , x_segment1                  => gl_rec.segment1                      
              , x_segment2                  => gl_rec.segment2                      
              , x_segment3                  => gl_rec.segment3                      
              , x_segment4                  => gl_rec.segment4                      
              , x_segment5                  => gl_rec.segment5                      
              , x_segment6                  => gl_rec.segment6                      
              , x_segment7                  => gl_rec.segment7                      
              , x_segment8                  => gl_rec.segment8                      
              , x_segment9                  => gl_rec.segment9                      
              , x_segment10                 => gl_rec.segment10                     
              , x_segment11                 => gl_rec.segment11                     
              , x_segment12                 => gl_rec.segment12                     
              , x_segment13                 => gl_rec.segment13                     
              , x_segment14                 => gl_rec.segment14                     
              , x_segment15                 => gl_rec.segment15                     
              , x_segment16                 => gl_rec.segment16                     
              , x_segment17                 => gl_rec.segment17                     
              , x_segment18                 => gl_rec.segment18                     
              , x_segment19                 => gl_rec.segment19                     
              , x_segment20                 => gl_rec.segment20                     
              , x_segment21                 => gl_rec.segment21                     
              , x_segment22                 => gl_rec.segment22                     
              , x_segment23                 => gl_rec.segment23                     
              , x_segment24                 => gl_rec.segment24                     
              , x_segment25                 => gl_rec.segment25                     
              , x_segment26                 => gl_rec.segment26                     
              , x_segment27                 => gl_rec.segment27                     
              , x_segment28                 => gl_rec.segment28                     
              , x_segment29                 => gl_rec.segment29                     
              , x_segment30                 => gl_rec.segment30                     
              , x_description               => gl_rec.description                   
              , x_template_id               => gl_rec.template_id                   
              , x_start_date_active         => gl_rec.start_date_active             
              , x_end_date_active           => gl_rec.end_date_active               
              , x_attribute1                => gl_rec.attribute1                    
              , x_attribute2                => gl_rec.attribute2                    
              , x_attribute3                => gl_rec.attribute3                    
              , x_attribute4                => gl_rec.attribute4                    
              , x_attribute5                => gl_rec.attribute5                    
              , x_attribute6                => gl_rec.attribute6                    
              , x_attribute7                => gl_rec.attribute7                    
              , x_attribute8                => gl_rec.attribute8                    
              , x_attribute9                => gl_rec.attribute9                    
              , x_attribute10               => gl_rec.attribute10                   
              , x_context                   => gl_rec.context                       
              , x_segment_attribute1        => gl_rec.segment_attribute1            
              , x_segment_attribute2        => gl_rec.segment_attribute2            
              , x_segment_attribute3        => gl_rec.segment_attribute3            
              , x_segment_attribute4        => gl_rec.segment_attribute4            
              , x_segment_attribute5        => gl_rec.segment_attribute5            
              , x_segment_attribute6        => gl_rec.segment_attribute6            
              , x_segment_attribute7        => gl_rec.segment_attribute7            
              , x_segment_attribute8        => gl_rec.segment_attribute8            
              , x_segment_attribute9        => gl_rec.segment_attribute9            
              , x_segment_attribute10       => gl_rec.segment_attribute10           
              , x_segment_attribute11       => gl_rec.segment_attribute11           
              , x_segment_attribute12       => gl_rec.segment_attribute12           
              , x_segment_attribute13       => gl_rec.segment_attribute13           
              , x_segment_attribute14       => gl_rec.segment_attribute14           
              , x_segment_attribute15       => gl_rec.segment_attribute15           
              , x_segment_attribute16       => gl_rec.segment_attribute16           
              , x_segment_attribute17       => gl_rec.segment_attribute17           
              , x_segment_attribute18       => gl_rec.segment_attribute18           
              , x_segment_attribute19       => gl_rec.segment_attribute19           
              , x_segment_attribute20       => gl_rec.segment_attribute20           
              , x_segment_attribute21       => gl_rec.segment_attribute21           
              , x_segment_attribute22       => gl_rec.segment_attribute22           
              , x_segment_attribute23       => gl_rec.segment_attribute23           
              , x_segment_attribute24       => gl_rec.segment_attribute24           
              , x_segment_attribute25       => gl_rec.segment_attribute25           
              , x_segment_attribute26       => gl_rec.segment_attribute26           
              , x_segment_attribute27       => gl_rec.segment_attribute27           
              , x_segment_attribute28       => gl_rec.segment_attribute28           
              , x_segment_attribute29       => gl_rec.segment_attribute29           
              , x_segment_attribute30       => gl_rec.segment_attribute30           
              , x_segment_attribute31       => gl_rec.segment_attribute31           
              , x_segment_attribute32       => gl_rec.segment_attribute32           
              , x_segment_attribute33       => gl_rec.segment_attribute33           
              , x_segment_attribute34       => gl_rec.segment_attribute34           
              , x_segment_attribute35       => gl_rec.segment_attribute35           
              , x_segment_attribute36       => gl_rec.segment_attribute36           
              , x_segment_attribute37       => gl_rec.segment_attribute37           
              , x_segment_attribute38       => gl_rec.segment_attribute38           
              , x_segment_attribute39       => gl_rec.segment_attribute39           
              , x_segment_attribute40       => gl_rec.segment_attribute40           
              , x_segment_attribute41       => gl_rec.segment_attribute41           
              , x_segment_attribute42       => gl_rec.segment_attribute42           
              , x_jgzz_recon_context        => gl_rec.jgzz_recon_context            
              , x_jgzz_recon_flag           => gl_rec.jgzz_recon_flag               
              , x_reference1                => gl_rec.reference1                    
              , x_reference2                => gl_rec.reference2                    
              , x_reference3                => gl_rec.reference3                    
              , x_reference4                => gl_rec.reference4                    
              , x_reference5                => gl_rec.reference5                    
              , x_preserve_flag             => gl_rec.preserve_flag                 
              , x_refresh_flag              => gl_rec.refresh_flag                  
            );
            --
            gl_rec.enabled_flag := 'N';
            --
            gl_code_combinations_pkg.update_row(
                x_rowid                    => datarec.x_rowid                
              , x_code_combination_id      => gl_rec.code_combination_id     
              , x_alt_code_combination_id  => gl_rec.alternate_code_combination_id
              , x_last_update_date         => gl_rec.last_update_date        
              , x_last_updated_by          => gl_rec.last_updated_by         
              , x_chart_of_accounts_id     => gl_rec.chart_of_accounts_id    
              , x_detail_posting_f         => gl_rec.detail_posting_allowed_flag  
              , x_detail_budgeting_f       => gl_rec.detail_budgeting_allowed_flag
              , x_balanced_budgetf         => gl_rec.igi_balanced_budget_flag     
              , x_account_type             => gl_rec.account_type            
              , x_enabled_flag             => gl_rec.enabled_flag            
              , x_summary_flag             => gl_rec.summary_flag            
              , x_segment1                 => gl_rec.segment1                
              , x_segment2                 => gl_rec.segment2                
              , x_segment3                 => gl_rec.segment3                
              , x_segment4                 => gl_rec.segment4                
              , x_segment5                 => gl_rec.segment5                
              , x_segment6                 => gl_rec.segment6                
              , x_segment7                 => gl_rec.segment7                
              , x_segment8                 => gl_rec.segment8                
              , x_segment9                 => gl_rec.segment9                
              , x_segment10                => gl_rec.segment10               
              , x_segment11                => gl_rec.segment11               
              , x_segment12                => gl_rec.segment12               
              , x_segment13                => gl_rec.segment13               
              , x_segment14                => gl_rec.segment14               
              , x_segment15                => gl_rec.segment15               
              , x_segment16                => gl_rec.segment16               
              , x_segment17                => gl_rec.segment17               
              , x_segment18                => gl_rec.segment18               
              , x_segment19                => gl_rec.segment19               
              , x_segment20                => gl_rec.segment20               
              , x_segment21                => gl_rec.segment21               
              , x_segment22                => gl_rec.segment22               
              , x_segment23                => gl_rec.segment23               
              , x_segment24                => gl_rec.segment24               
              , x_segment25                => gl_rec.segment25               
              , x_segment26                => gl_rec.segment26               
              , x_segment27                => gl_rec.segment27               
              , x_segment28                => gl_rec.segment28               
              , x_segment29                => gl_rec.segment29               
              , x_segment30                => gl_rec.segment30               
              , x_description              => gl_rec.description             
              , x_template_id              => gl_rec.template_id             
              , x_start_date_active        => gl_rec.start_date_active       
              , x_end_date_active          => gl_rec.end_date_active         
              , x_attribute1               => gl_rec.attribute1              
              , x_attribute2               => gl_rec.attribute2              
              , x_attribute3               => gl_rec.attribute3              
              , x_attribute4               => gl_rec.attribute4              
              , x_attribute5               => gl_rec.attribute5              
              , x_attribute6               => gl_rec.attribute6              
              , x_attribute7               => gl_rec.attribute7              
              , x_attribute8               => gl_rec.attribute8              
              , x_attribute9               => gl_rec.attribute9              
              , x_attribute10              => gl_rec.attribute10             
              , x_context                  => gl_rec.context                 
              , x_segment_attribute1       => gl_rec.segment_attribute1      
              , x_segment_attribute2       => gl_rec.segment_attribute2      
              , x_segment_attribute3       => gl_rec.segment_attribute3      
              , x_segment_attribute4       => gl_rec.segment_attribute4      
              , x_segment_attribute5       => gl_rec.segment_attribute5      
              , x_segment_attribute6       => gl_rec.segment_attribute6      
              , x_segment_attribute7       => gl_rec.segment_attribute7      
              , x_segment_attribute8       => gl_rec.segment_attribute8      
              , x_segment_attribute9       => gl_rec.segment_attribute9      
              , x_segment_attribute10      => gl_rec.segment_attribute10     
              , x_segment_attribute11      => gl_rec.segment_attribute11     
              , x_segment_attribute12      => gl_rec.segment_attribute12     
              , x_segment_attribute13      => gl_rec.segment_attribute13     
              , x_segment_attribute14      => gl_rec.segment_attribute14     
              , x_segment_attribute15      => gl_rec.segment_attribute15     
              , x_segment_attribute16      => gl_rec.segment_attribute16     
              , x_segment_attribute17      => gl_rec.segment_attribute17     
              , x_segment_attribute18      => gl_rec.segment_attribute18     
              , x_segment_attribute19      => gl_rec.segment_attribute19     
              , x_segment_attribute20      => gl_rec.segment_attribute20     
              , x_segment_attribute21      => gl_rec.segment_attribute21     
              , x_segment_attribute22      => gl_rec.segment_attribute22     
              , x_segment_attribute23      => gl_rec.segment_attribute23     
              , x_segment_attribute24      => gl_rec.segment_attribute24     
              , x_segment_attribute25      => gl_rec.segment_attribute25     
              , x_segment_attribute26      => gl_rec.segment_attribute26     
              , x_segment_attribute27      => gl_rec.segment_attribute27     
              , x_segment_attribute28      => gl_rec.segment_attribute28     
              , x_segment_attribute29      => gl_rec.segment_attribute29     
              , x_segment_attribute30      => gl_rec.segment_attribute30     
              , x_segment_attribute31      => gl_rec.segment_attribute31     
              , x_segment_attribute32      => gl_rec.segment_attribute32     
              , x_segment_attribute33      => gl_rec.segment_attribute33     
              , x_segment_attribute34      => gl_rec.segment_attribute34     
              , x_segment_attribute35      => gl_rec.segment_attribute35     
              , x_segment_attribute36      => gl_rec.segment_attribute36     
              , x_segment_attribute37      => gl_rec.segment_attribute37     
              , x_segment_attribute38      => gl_rec.segment_attribute38     
              , x_segment_attribute39      => gl_rec.segment_attribute39     
              , x_segment_attribute40      => gl_rec.segment_attribute40     
              , x_segment_attribute41      => gl_rec.segment_attribute41     
              , x_segment_attribute42      => gl_rec.segment_attribute42     
              , x_jgzz_recon_context       => gl_rec.jgzz_recon_context      
              , x_jgzz_recon_flag          => gl_rec.jgzz_recon_flag         
              , x_reference1               => gl_rec.reference1              
              , x_reference2               => gl_rec.reference2              
              , x_reference3               => gl_rec.reference3              
              , x_reference4               => gl_rec.reference4              
              , x_reference5               => gl_rec.reference5              
              , x_preserve_flag            => gl_rec.preserve_flag           
              , x_refresh_flag             => gl_rec.refresh_flag            
            );
            set_status('C',SQLERRM);
        exception
            when others then 
              set_status('E',SQLERRM);
        end;
    end loop;
    dbms_output.put_line(to_char(systimestamp,'DD-MON-YYYY HH24:MI:SS.FFFF')||': Finished processing PWC_DEACTIVATIONS');
    lt_end := systimestamp;
    dbms_output.put_line('Processing completed - program runtime was ('||to_char(lt_end-lt_start)||')');
    --
    -- Report Results 
    --
    dbms_output.put_line('');
    dbms_output.put_line(rpad('Status',35)||' '||lpad('Record Count',15));
    dbms_output.put_line(rpad('-',35,'-')||' '||lpad('-',15,'-'));
    for datarec in (
        select case gl_status
               when 'D' then 'Disabled in FMS already'
               when 'C' then 'Account Successfully Disabled'
               when 'A' then 'Account is Active'
               when 'N' then 'Account Doesn''t Exist In FMS'
               when 'X' then 'Unknown enabled_flag in FMS'
               when 'E' then 'Error occurred processing account'
               else 'Unknown status'
               end status
             , count(*) record_count
        from   pwc_gl_deactivations 
        group by case gl_status
               when 'D' then 'Disabled in FMS already'
               when 'C' then 'Account Successfully Disabled'
               when 'A' then 'Account is Active'
               when 'N' then 'Account Doesn''t Exist In FMS'
               when 'X' then 'Unknown enabled_flag in FMS'
               when 'E' then 'Error occurred processing account'
               else 'Unknown status'
               end
    ) loop
        dbms_output.put_line(rpad(datarec.status,35)||' '||lpad(to_char(datarec.record_count,'99,990'),15));
        ln_total := ln_total + datarec.record_count;
    end loop;
    dbms_output.put_line(rpad('-',35,'-')||' '||lpad('-',15,'-'));
    dbms_output.put_line(rpad('Total Accounts',35)||' '||lpad(to_char(ln_total,'99,990'),15));
end;
/
