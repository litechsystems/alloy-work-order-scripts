/*
|| Create a table to hold the cdishdates (only if it doesn't already exist)
*/
declare
    table_exists exception;
    pragma exception_init(table_exists,-955);
begin
    begin
        execute immediate q'[
            create table pwc_consumer_cdishdates (
                consumer_no number(14,0)
              , seq         integer
              , cdish_date  date
            )]';
    exception
        when table_exists then
            null;
    end;
end;
/

declare
    /*
    || Consumer to extract the cdishdate values from
    */
    ln_consumer_no   number := 8811329710;
    /*
    || Dates newer than this date will be kept in the consumer record.
    */
    ld_cutoff_date   date := add_months(sysdate,-12*7);
    /*
    || Number of dates to keep in the cdishdates field (starting from most recent).  
    || Dates may be older than the cutoff date
    */
    li_dates_to_keep integer := 5;
    /*
    || Local variables
    */
    pav_dates        pwc_vc_array_typ;
    lv_cdishdate     genprod.consumer.cdishdate%type;
    li_idx           integer;
    lb_load_temp     boolean := true;
begin
    /*
    || Query cdishdate into local variable
    */
    dbms_output.put_line('Querying previously loaded pwc_consumer_cdishdates');
    lv_cdishdate := null;
    for datarec in (
        select cdish_date
        from   pwc_consumer_cdishdates
        where  consumer_no = ln_consumer_no
        order  by seq
    ) loop
        if lv_cdishdate = null then
            lv_cdishdate := idate(datarec.cdish_date);
        else
            lv_cdishdate := lv_cdishdate || chr(253) || idate(datarec.cdish_date);
        end if;
    end loop;
    /*
    || If no records were found, query the cdishdate values from rms.
    */
    if lv_cdishdate is null then
        dbms_output.put_line('Populating pwc_consumer_cdishdates');
        /*
        || Query cdishdate into local variable
        */
        select cdishdate 
        into   lv_cdishdate
        from   genprod.consumer
        where  consumerno = ln_consumer_no;
    else 
        lb_load_temp := false;
    end if;
    /*
    || Split the cdishdate value up into dates
    */
    pav_dates := pwc_rms_data_source.split_string(
        pv_field  => 'cdishdate'
      , pv_string => lv_cdishdate
      , pv_odate  => 'Y'
    );
    if lb_load_temp then
        /*
        || Insert the individual dates into the pwc_consumer_cdishdates table
        */
        li_idx := pav_dates.first;
        while li_idx is not null loop
            insert into pwc_consumer_cdishdates (
                consumer_no
              , seq
              , cdish_date
            ) values (
                ln_consumer_no
              , li_idx
              , pwc_integration_utils.char_to_date(pav_dates(li_idx))
            );
            li_idx := pav_dates.next(li_idx);
        end loop;
        /*
        || Commit the dates into pwc_consumer_cdishdates
        */
        commit;
    end if;
    /*
    || Build a new cdishdate string for the consumer
    */
    dbms_output.put_line('Creating new cdishdate string..');
    lv_cdishdate := null;
    li_idx := pav_dates.first;
    while li_idx is not null and (li_idx <= li_dates_to_keep or pwc_integration_utils.char_to_date(pav_dates(li_idx)) > ld_cutoff_date) loop
        if lv_cdishdate = null then
            lv_cdishdate := idate(pwc_integration_utils.char_to_date(pav_dates(li_idx)));
        else
            lv_cdishdate := lv_cdishdate || chr(253) || idate(pwc_integration_utils.char_to_date(pav_dates(li_idx)));
        end if;
        li_idx := pav_dates.next(li_idx);
    end loop;
    /*
    || Replace the cdishdate value for this consumer
    */
    dbms_output.put_line('Update consumer table..');
    update genprod.consumer
    set    cdishdate = lv_cdishdate
    where  consumerno = ln_consumer_no;
    /*
    || Commit the change
    */
    commit;
end;
/
