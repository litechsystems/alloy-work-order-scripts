/*
|| Restore the consumer record from the dates in the pwc_consumer_cdishdates field
*/
declare
    lv_cdishdate     varchar2(4000);
    ln_consumer_no   number := 8811329710;
begin
    for datarec in (
        select cdish_date
        from   pwc_consumer_cdishdates
        where  consumer_no = ln_consumer_no
        order  by seq
    ) loop
        if lv_cdishdate = null then
            lv_cdishdate := idate(datarec.cdish_date);
        else
            lv_cdishdate := lv_cdishdate || chr(253) || idate(datarec.cdish_date);
        end if;
    end loop;
    
    update genprod.consumer
    set    cdishdate = lv_cdishdate
    where  consumerno = ln_consumer_no;
end;
/
