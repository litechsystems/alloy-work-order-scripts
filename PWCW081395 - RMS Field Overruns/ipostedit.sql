/*
|| Create a table to hold the edits (only if it doesn't already exist)
*/
declare
    table_exists exception;
    pragma exception_init(table_exists,-955);
begin
    begin
        execute immediate q'[
            create table pwc_install_ipostedit (
                install_no  number(12,0)
              , seq         integer
              , ipost_edit  varchar2(100)
            )]';
    exception
        when table_exists then
            null;
    end;
end;
/

declare
    /*
    || Install to extract the ipostedit values from
    */
    ln_install_no    number := 1039843;
    /*
    || Dates newer than this date will be kept in the install record.
    */
    ld_cutoff_date   date := add_months(sysdate,-12*7);
    /*
    || Number of ipostedit values to keep in the ipostedit field (starting from most recent)
    */
    li_edits_to_keep integer := 10;
    /*
    || Local variables
    */
    pav_edits        pwc_vc_array_typ;
    lv_ipostedit     genprod.pwc_ipostedit.ipostedit%type;
    li_idx           integer;
    lb_load_temp     boolean := true;
begin
    /*
    || Query the ipostedit values from our custom table
    */
    dbms_output.put_line('Querying previously loaded pwc_install_ipostedit');
    lv_ipostedit := null;
    for datarec in (
        select ipost_edit
        from   pwc_install_ipostedit
        where  install_no = ln_install_no
        order  by seq
    ) loop
        if lv_ipostedit = null then
            lv_ipostedit := datarec.ipost_edit;
        else
            lv_ipostedit := lv_ipostedit || chr(253) || datarec.ipost_edit;
        end if;
    end loop;
    /*
    || If no records were found, query the ipostedit values from rms.
    */
    if lv_ipostedit is null then
        dbms_output.put_line('Populating pwc_install_ipostedit');
        /*
        || Query ipostedit into local variable
        */
        select ipostedit 
        into   lv_ipostedit
        from   genprod.pwc_ipostedit
        where  install = ln_install_no;
    else 
        lb_load_temp := false;
    end if;
    /*
    || Split the lv_ipostedit value up into strings
    */
    pav_edits := pwc_rms_data_source.split_string(
        pv_field  => 'ipostedit'
      , pv_string => lv_ipostedit
    );
    if lb_load_temp then
        /*
        || Insert the individual edits into the pwc_install_ipostedit table
        */
        li_idx := pav_edits.first;
        while li_idx is not null loop
            insert into pwc_install_ipostedit (
                install_no
              , seq
              , ipost_edit
            ) values (
                ln_install_no
              , li_idx
              , pav_edits(li_idx)
            );
            li_idx := pav_edits.next(li_idx);
        end loop;
        /*
        || Commit the edits into pwc_install_ipostedit
        */
        commit;
    end if;
    /*
    || Build a new ipostedit string for the install
    */
    dbms_output.put_line('Creating new ipostedit string..');
    lv_ipostedit := null;
    li_idx := pav_edits.first;
    while li_idx is not null and (li_idx <= li_edits_to_keep or to_date(substr(pav_edits(li_idx),1,10),'DD/MM/YYYY') > ld_cutoff_date) loop 
        if lv_ipostedit = null then
            lv_ipostedit := pav_edits(li_idx);
        else
            lv_ipostedit := lv_ipostedit || chr(253) || pav_edits(li_idx);
        end if;
        li_idx := pav_edits.next(li_idx);
    end loop;
    /*
    || Replace the ipostedit value for this install
    */
    dbms_output.put_line('Update pwc_ipostedit table..');
    update genprod.pwc_ipostedit
    set    ipostedit = lv_ipostedit
    where  install = ln_install_no;
    /*
    || Commit the change
    */
    commit;
end;
/
