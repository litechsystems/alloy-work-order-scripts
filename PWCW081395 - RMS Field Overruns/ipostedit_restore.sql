/*
|| Restore the install record from the edits in the pwc_install_ipostedit field
*/
declare
    lv_ipostedit     varchar2(4000);
    ln_install_no    number := 1039843;
begin
    for datarec in (
        select ipost_edit
        from   pwc_install_ipostedit
        where  install_no = ln_install_no
        order  by seq
    ) loop
        if lv_ipostedit = null then
            lv_ipostedit := datarec.ipost_edit;
        else
            lv_ipostedit := lv_ipostedit || chr(253) || datarec.ipost_edit;
        end if;
    end loop;
    
    update genprod.pwc_ipostedit
    set    ipostedit = lv_ipostedit
    where  install = ln_install_no;
end;
/
