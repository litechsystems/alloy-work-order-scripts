create or replace PACKAGE BODY         PCS IS
    ----------------------------------------------------------------------------------
    --  CS 10/01/07  Changes for Variable Concession Rates
    --  CS 30/06/07  Revert to Rate/Year Months factoring for MVR
    --  CS 05/07/07  Fix Ownership bug in new code 
    ----------------------------------------------------------------------------------

    --
    -- Logger object and log levels
    --
    gci_unexpected  constant integer := genprod.log_pkg.gci_unexpected;
    gci_error       constant integer := genprod.log_pkg.gci_error;
    gci_exception   constant integer := genprod.log_pkg.gci_exception;
    gci_event       constant integer := genprod.log_pkg.gci_event;
    gci_procedure   constant integer := genprod.log_pkg.gci_procedure;
    gci_statement   constant integer := genprod.log_pkg.gci_statement;
    log genprod.logger_obj := new genprod.logger_obj(
        pv_package_name => 'pcs'
    );
    
    gcd_max_date    constant date := to_date('01-JAN-2111','DD-MON-YYYY');
    gcv_rate        constant varchar2(4) := 'RATE';
    gcv_cap         constant varchar2(3) := 'CAP';

    TYPE date_range_typ IS RECORD (
        start_date date
      , end_date   date
    );

    procedure insert_pcs_logs(
        pv_logs     varchar2
      , pd_log_date date
    ) is
        pragma autonomous_transaction;
    begin
        insert into pension.pcs_logs(
            logs
          , log_date
        ) values (
            substr(pv_logs,1,50)
          , pd_log_date
        );
        commit;
    end insert_pcs_logs;

    -----------------------------------------------------------------------------
    FUNCTION Part_Payments(
        p_conc_subtype_id IN NUMBER
    ) RETURN BOOLEAN
    IS
        v_part_payments_allowed   INTPAWA_CONC_TYPE.part_payments_allowed%TYPE;
    BEGIN
        log.set_method('part_payments');
        log.log_number('p_conc_subtype_id',p_conc_subtype_id);
        
        SELECT part_payments_allowed
        INTO  v_part_payments_allowed
        FROM  pension.intpawa_conc_type
        WHERE conc_type_id = p_conc_subtype_id;
        
        log.log_char('v_part_payments_allowed',v_part_payments_allowed);
        log.end_method;
        RETURN(v_part_payments_allowed = 'YES');
    EXCEPTION
        WHEN OTHERS THEN
            log.log_number('p_conc_subtype_id',p_conc_subtype_id, gci_unexpected);
            log.log_error(
                pv_message               => 'Unhandled exception.'
              , pv_sqlerrm               => SQLERRM
            );
            log.end_method;
            RETURN FALSE;
    END Part_Payments;
    
    -----------------------------------------------------------------------------
    FUNCTION Ownership_Applies(
        p_person_id IN NUMBER
      , p_conc_amount IN NUMBER
      , p_message     OUT NUMBER
    ) RETURN NUMBER
    IS
        v_ownership      NUMBER(4);
        Final_conc_amt   NUMBER(8,3) :=0;
    BEGIN
        log.set_method('ownership_applies');
        log.log_number('p_person_id',p_person_id);
        log.log_number('p_conc_amount',p_conc_amount);
        SELECT ownership
        INTO   v_ownership
        FROM   pension.intpawa_client
        WHERE  person_id = p_person_id;
        
        log.log_number('v_ownership',v_ownership);
        IF v_ownership = 0 THEN
            p_message := 07;
        ELSE
            final_conc_amt := (p_conc_amount) * (v_ownership/100) ;
            p_message := 00;
        END IF;
        
        log.log_number('p_message',p_message);
        log.log_number('final_conc_amt',final_conc_amt);
        log.end_method;
        RETURN final_conc_amt;
    EXCEPTION
        WHEN OTHERS THEN
            log.log_number('p_person_id',p_person_id, gci_unexpected);
            log.log_number('p_conc_amount',p_conc_amount, gci_unexpected);
            log.log_error(
                pv_message               => 'Unhandled exception.'
              , pv_sqlerrm               => SQLERRM
            );
            log.end_method;
            -- SA 14/05 Surely we shouldn't just "continue happily" at this point!?
    END Ownership_Applies;
    -----------------------------------------------------------------------------
    FUNCTION Display_Err(
        p_msg_no IN NUMBER
    ) RETURN VARCHAR2
    IS
        v_err_msg  ERR_MESSAGES.msg_text%TYPE;
    BEGIN
        log.set_method('display_err');
        log.log_number('p_msg_no',p_msg_no);

        SELECT msg_text
        INTO   v_err_msg
        FROM   PENSION.ERR_MESSAGES
        WHERE  msg_no = p_msg_no;

        log.log_char('v_err_msg',v_err_msg);
        log.end_method;

        RETURN v_err_msg;
    EXCEPTION
        WHEN OTHERS THEN
            log.log_number('p_msg_no',p_msg_no, gci_unexpected);
            log.log_error(
                pv_message               =>'Unhandled exception.'
              , pv_sqlerrm               => SQLERRM
            );
            log.end_method;
            RETURN 'Error generating Error Message';
    END Display_Err;
    
    procedure get_rate_date_range(
        pr_cal_range        in out nocopy date_range_typ
      , pr_effective_range  in out nocopy date_range_typ
      , pr_rate_range       in out nocopy date_range_typ
    ) is
    begin
        log.set_method('get_rate_date_range');
        log.log_date('pr_cal_range.start_date',pr_cal_range.start_date);
        log.log_date('pr_cal_range.end_date',pr_cal_range.end_date);
        log.log_date('pr_effective_range.start_date',pr_effective_range.start_date);
        log.log_date('pr_effective_range.end_date',pr_effective_range.end_date);
        
        -- Extract the rate segment data start and end dates
        if pr_effective_range.start_date < pr_cal_range.start_date then
            pr_rate_range.start_date := pr_cal_range.start_date;
        else
            pr_rate_range.start_date := pr_effective_range.start_date;
        end if;
        if nvl(pr_effective_range.end_date, gcd_max_date) >= pr_cal_range.end_date THEN   
            pr_rate_range.end_date := pr_cal_range.end_date;
        else
            pr_rate_range.end_date := pr_effective_range.end_date;
        end if;
        
        log.log_date('pr_rate_range.start_date',pr_rate_range.start_date);
        log.log_date('pr_rate_range.end_date',pr_rate_range.end_date);
        log.end_method;
    end get_rate_date_range;
    
    procedure add_chunk_amount(
        pv_chunk_type          in varchar2
      , pn_data_type_id        in number
      , pr_service_range       in out nocopy date_range_typ
      , pr_cal_range           in out nocopy date_range_typ
      , pn_rate_value          in number
      , pn_service_amount      in number
      , pn_conc_segment_amount in out nocopy number
      , pr_rate_range          in out nocopy date_range_typ
      
    ) is
        ln_calc_number_of_years number(7,2);
        ln_conc_chunk_amount    number(6,3) := 0;
        ln_perc_of_total_claim  number(6,3) := 0;
    begin
        log.set_method('add_chunk_amount');
        log.log_char('pv_chunk_type',pv_chunk_type);
        log.log_number('pn_data_type_id',pn_data_type_id);
        log.log_date('pr_service_range.start_date',pr_service_range.start_date);
        log.log_date('pr_service_range.end_date',pr_service_range.end_date);
        log.log_date('pr_cal_range.start_date',pr_cal_range.start_date);
        log.log_date('pr_cal_range.end_date',pr_cal_range.end_date);
        log.log_number('pn_rate_value',pn_rate_value);
        log.log_number('pn_service_amount',pn_service_amount);
        log.log_number('pn_conc_segment_amount',pn_conc_segment_amount);
        log.log_date('pr_rate_range.start_date',pr_rate_range.start_date);
        log.log_date('pr_rate_range.end_date',pr_rate_range.end_date);

        IF (pn_data_type_id = 1 and pv_chunk_type = gcv_rate) or
           (pn_data_type_id = 3 and pv_chunk_type = gcv_cap) 
        THEN      -- Annual Flat rate concession
            ln_calc_number_of_years := months_between ( pr_cal_range.end_date,pr_cal_range.start_date)/12 ;
            ln_conc_chunk_amount    := pn_rate_value * ln_calc_number_of_years;
        ELSIF (pn_data_type_id = 2 and pv_chunk_type = gcv_rate) or
              (pn_data_type_id = 1 and pv_chunk_type = gcv_cap) 
        THEN      -- % of service charge
            ln_perc_of_total_claim := (pr_rate_range.end_date + 1 - pr_rate_range.start_date) / (pr_service_range.end_date + 1 - pr_service_range.start_date);
            ln_conc_chunk_amount   := pn_service_amount * (pn_rate_value/100) * ln_perc_of_total_claim;
            pn_conc_segment_amount := pn_conc_segment_amount + ln_conc_chunk_amount;
        ELSIF (pn_data_type_id = 3 and pv_chunk_type = gcv_rate) or
              (pn_data_type_id = 2 and pv_chunk_type = gcv_cap) 
        THEN      -- daily rate concession
            ln_conc_chunk_amount   := pn_rate_value * (pr_rate_range.end_date +1 - pr_rate_range.start_date);
            pn_conc_segment_amount := pn_conc_segment_amount + ln_conc_chunk_amount;    
        ELSE
            NULL;
        END IF;

        if ln_conc_chunk_amount != 0 and pv_chunk_type = gcv_rate then
            -- original code had number(6,2) for rate and number(6,3) for cap.
            ln_conc_chunk_amount := round(ln_conc_chunk_amount,2);
        end if;
        pn_conc_segment_amount  := pn_conc_segment_amount + ln_conc_chunk_amount;
        
        log.log_number('ln_conc_chunk_amount',ln_conc_chunk_amount);
        log.log_number('pn_conc_segment_amount',pn_conc_segment_amount);
        log.end_method;
        
    end add_chunk_amount;
    
    -------------------------------------------------------------------------
    FUNCTION calculate_common (
        pv_source              in varchar2
      , p_conc_subtype_id      in number
      , pr_service_range       in out nocopy date_range_typ
      , pr_cal_range           in out nocopy date_range_typ
      , p_service_amount       in number
      , p_debug_flag           in boolean := true     -- cs 10/01/07  
    ) RETURN NUMBER
    IS
        CURSOR c_rate IS  -- extract all valid rate/cap records for the claim segment period
            SELECT  A.conc_type_id
                  , case pv_source
                    when gcv_rate then A.rate_data_type_id
                    when gcv_cap then NVL(A.cap_data_type_id,DECODE(A.RATE_DATA_TYPE_ID,1,3,2,1,3,2,0)) 
                    end data_type_id
                  , case pv_source
                    when gcv_rate then A.rate_value
                    when gcv_cap then NVL(A.cap_value,A.rate_value)
                    end rate_value
                  , A.effective_start_date
                  , A.effective_end_date
            FROM    PENSION.intpawa_conc_type A    
            WHERE   A.conc_type_id = p_conc_subtype_id
            AND     A.effective_start_date <= pr_cal_range.end_date  
            AND     NVL(A.effective_end_date, gcd_max_date)  >= pr_cal_range.start_date
            ORDER BY A.effective_start_date;   

        ln_conc_segment_amount  NUMBER(6,2) := 0;
        li_loop                 INTEGER := 0;

        
        lr_rate_range          date_range_typ;
        lr_effective_range     date_range_typ;
    BEGIN
        log.set_method('calculate_common');
        log.log_char('pv_source',pv_source);
        log.log_number('p_conc_subtype_id',p_conc_subtype_id);
        log.log_date('pr_service_range.start_date',pr_service_range.start_date);
        log.log_date('pr_service_range.end_date',pr_service_range.end_date);
        log.log_date('pr_cal_range.start_date',pr_cal_range.start_date);
        log.log_date('pr_cal_range.end_date',pr_cal_range.end_date);
        log.log_number('p_service_amount',p_service_amount);
        log.log_boolean('p_debug_flag',p_debug_flag);
        
        FOR r_rate IN c_rate
        LOOP
        
            li_loop := li_loop + 1;

            lr_effective_range.start_date := r_rate.effective_start_date;
            lr_effective_range.end_date   := r_rate.effective_end_date;
            
            -- Extract the rate segment data start and end dates
            get_rate_date_range(
                pr_cal_range        => pr_cal_range
              , pr_effective_range  => lr_effective_range
              , pr_rate_range       => lr_rate_range
            );

            add_chunk_amount(
                pv_chunk_type          => pv_source
              , pn_data_type_id        => r_rate.data_type_id
              , pr_service_range       => pr_service_range
              , pr_cal_range           => pr_cal_range
              , pn_rate_value          => r_rate.rate_value
              , pn_service_amount      => p_service_amount
              , pn_conc_segment_amount => ln_conc_segment_amount
              , pr_rate_range     => lr_rate_range
            );
        END LOOP;
        
        IF li_loop = 0  THEN 
            log.log_message('No valid rate / cap records for the claim segment period',gci_unexpected);
            ln_conc_segment_amount := 0;
        END IF;
        
        log.end_method;
        RETURN ln_conc_segment_amount;
        
    EXCEPTION
        WHEN OTHERS THEN
            log.log_char('pv_source',pv_source,gci_unexpected);
            log.log_number('p_conc_subtype_id',p_conc_subtype_id,gci_unexpected);
            log.log_date('pr_service_range.start_date',pr_service_range.start_date,gci_unexpected);
            log.log_date('pr_service_range.end_date',pr_service_range.end_date,gci_unexpected);
            log.log_date('pr_cal_range.start_date',pr_cal_range.start_date,gci_unexpected);
            log.log_date('pr_cal_range.end_date',pr_cal_range.end_date,gci_unexpected);
            log.log_number('p_service_amount',p_service_amount,gci_unexpected);
            log.log_boolean('p_debug_flag',p_debug_flag,gci_unexpected);
            log.log_error(
                pv_message               =>'Unhandled exception.'
              , pv_sqlerrm               => SQLERRM
            );
            log.end_method;

            -- SA 14/05 Surely we shouldn't just "continue happily" at this point!?
            RETURN NULL;
    END calculate_common;    
    
    
    -------------------------------------------------------------------------
    FUNCTION Calculate_Concession (
        p_conc_subtype_id      in number
      , p_service_start_date   in date
      , p_service_end_date     in date
      , p_cal_start_date       in date
      , p_cal_end_date         in date
      , p_service_amount       in number
      , p_debug_flag           in boolean := true     -- cs 10/01/07  
    ) RETURN NUMBER
    IS
        lr_service_range     date_range_typ;
        lr_cal_range         date_range_typ;
    BEGIN
        
        lr_service_range.start_date := p_service_start_date;
        lr_service_range.end_date   := p_service_end_date;

        lr_cal_range.start_date := p_cal_start_date;
        lr_cal_range.end_date   := p_cal_end_date;
        
        return calculate_common (
            pv_source              => gcv_rate
          , p_conc_subtype_id      => p_conc_subtype_id
          , pr_service_range       => lr_service_range
          , pr_cal_range           => lr_cal_range
          , p_service_amount       => p_service_amount
          , p_debug_flag           => p_debug_flag
        );
    END Calculate_Concession;
    
    ---------------------------
    
    FUNCTION Calculate_Threshold(
        p_conc_subtype_id      in number
      , p_service_start_date   in date
      , p_service_end_date     in date
      , p_cal_start_date       in date
      , p_cal_end_date         in date
      , p_service_amount       in number
      , p_debug_flag           in boolean := true  -- cs 10/01/07  
    ) RETURN NUMBER
    IS
        lr_service_range     date_range_typ;
        lr_cal_range         date_range_typ;
    BEGIN
        
        lr_service_range.start_date := p_service_start_date;
        lr_service_range.end_date   := p_service_end_date;

        lr_cal_range.start_date := p_cal_start_date;
        lr_cal_range.end_date   := p_cal_end_date;

        return calculate_common (
            pv_source              => gcv_rate
          , p_conc_subtype_id      => p_conc_subtype_id
          , pr_service_range       => lr_service_range
          , pr_cal_range           => lr_cal_range
          , p_service_amount       => p_service_amount
          , p_debug_flag           => p_debug_flag
        );
    END Calculate_Threshold;
    -----------------------------------------------------------------------------
    
    PROCEDURE add_conc (
        p_pcs_id             in number
      , p_concession_type    in number
      , p_conc_id            in varchar
      , p_service_start_date in date
      , p_service_end_date   in date
      , p_service_amount     in number
      , p_conc_value         in number
      , p_end_date           in date
    ) IS
        v_final_date DATE;
        -- CS 10/01/07   Possible bug.. see below
    
    BEGIN
        log.set_method('add_conc');
        log.log_number('p_pcs_id',p_pcs_id);
        log.log_number('p_concession_type',p_concession_type);
        log.log_char('p_conc_id',p_conc_id);
        log.log_date('p_service_start_date',p_service_start_date);
        log.log_date('p_service_end_date',p_service_end_date);
        log.log_number('p_service_amount',p_service_amount);
        log.log_number('p_conc_value',p_conc_value);
        log.log_date('p_end_date',p_end_date);

        INSERT INTO PENSION.INTPAWA_CONC_TRANS (
            person_id
          , conc_type_id
          , trans_type_id
          , agents_trans_no
          , trans_date
          , serv_start_date
          , serv_end_date
          , serv_value
          , conc_value
          , serv_prov_agent_id
        ) VALUES (
            p_pcs_id
          , p_concession_type
          , 1
          , p_conc_id
          , sysdate
          , p_service_start_date
          , p_service_end_date
          , p_service_amount
          , p_conc_value
          , 1
        );
    
        v_final_date := (p_end_date + 1);
    
        /* update concession eligibility to reflect the new claim */
        UPDATE pension.intpawa_conc_elig
        SET    elig_start_date = v_final_date
        WHERE  person_id = p_pcs_id
        AND    conc_type_id = p_concession_type
        AND    elig_end_date IS NULL;
        -- CS 10/01/07   I see a problem here if the pensioner has an Elig End date.. Wont update
        --               so then multiple concessions could be given!!!
        
        COMMIT;
        
        log.end_method;
    
    EXCEPTION
        WHEN OTHERS THEN
            log.log_number('p_pcs_id',p_pcs_id,gci_unexpected);
            log.log_number('p_concession_type',p_concession_type,gci_unexpected);
            log.log_char('p_conc_id',p_conc_id,gci_unexpected);
            log.log_date('p_service_start_date',p_service_start_date,gci_unexpected);
            log.log_date('p_service_end_date',p_service_end_date,gci_unexpected);
            log.log_number('p_service_amount',p_service_amount,gci_unexpected);
            log.log_number('p_conc_value',p_conc_value,gci_unexpected);
            log.log_date('p_end_date',p_end_date,gci_unexpected);
            log.log_error(
                pv_message               => 'Unhandled exception.'
              , pv_sqlerrm               => SQLERRM
            );
            log.end_method;
            -- SA 14/05 Surely we shouldn't just "continue happily" at this point!?
    END add_conc;
    
    -----------------------------------------------------------------------------
    
    FUNCTION concession_common (
        pv_source          in varchar2
      , concession_type    in number
      , pr_service_range   in out nocopy date_range_typ
      , service_amount     in number
      , pcs_id             in number
      , name               in varchar2
      , date_of_birth      in date
      , street             in varchar2
      , lis_key            in varchar2
      , concession_amount  out number
      , concession_id      out varchar2
      , return_code        out number
      , error_message      out varchar2
    ) RETURN BOOLEAN
    IS
        -- CS 10/01/07   Replicate PENSION.PCS   PCS_CONCESSION_PROCESSING Package functionality.
        --               New Cursor.  New code to use Variable Rates over Eligible period.
        -- CS 05/07/07   Fix Ownership bug in new code
        --               
        CURSOR c_select IS  -- CS 10/01/07  Only interested in Eligibilities at this stage.  Rates not needed.
            SELECT C.name
                 , C.birth_date
                 , C.ADDRESS
                 , C.suburb
                 , C.POST_CODE
                 , C.lis_key
                 , C.ownership
                 , T.conc_type_id
                 --T.rate_data_type_id,                 -- May need these to debug 
                 --T.EFFECTIVE_START_DATE RATE_S_DATE,
                 --T.EFFECTIVE_END_DATE   RATE_E_DATE,
                 --T.rate_value, 
                 --T.cap_data_type_id,
                 --T.cap_value, 
                 , TRUNC(E.elig_start_date) ELIG_START_DATE
                 , TRUNC(E.elig_end_date) ELIG_END_DATE
                 , T.birth_date_required
                 , T.address_required
                 , T.part_payments_allowed
                 , T.Ownership_Applies
            FROM   PENSION.INTPAWA_CLIENT      C
                 , PENSION.INTPAWA_CONC_ELIG   E
                 , PENSION.INTPAWA_CONC_TYPE   T
            WHERE  C.person_id    = PCS_ID 
            AND    C.person_id    = E.person_id
            AND    T.conc_type_id = CONCESSION_TYPE
            AND    T.conc_type_id = E.conc_type_id
            AND    TRUNC(E.elig_start_date)                   <= pr_service_range.END_DATE
            AND    NVL(TRUNC(E.elig_end_date),gcd_max_date)  >= pr_service_range.START_DATE
            AND    NVL(T.EFFECTIVE_END_DATE,gcd_max_date)    = (                -- Isolate single row from Variable Rate rows
                       SELECT MAX(NVL(T2.EFFECTIVE_END_DATE,gcd_max_date) )
                       FROM  PENSION.INTPAWA_CONC_TYPE   T2
                       where T2.CONC_TYPE_ID = T.CONC_TYPE_ID
                   )
            ORDER BY E.elig_start_date;
        
        /* Variables */
        v_return_flag                        NUMBER(2);
        v_thresh_return_flag                 NUMBER(2);
        v_validate_conc                      NUMBER (6,2) ;
        v_validate_threshold                 NUMBER(6,2) ;
        v_ownership_message                  NUMBER(3);
        v_ownership                          NUMBER(8,2);
        v_own_first                          NUMBER(8,2);   -- CS 05/07/07
        v_own_app_first                      VARCHAR2(4);   -- CS 05/07/07
        v_part_payments                      BOOLEAN ;
        v_conc_start_date                    DATE;
        v_conc_end_date                      DATE;
        v_thresh_start_date                  DATE;
        v_thresh_end_date                    DATE;
        v_final_start_date                   DATE;
        v_final_end_date                     DATE;
        v_cal_start_date                     DATE;
        v_cal_end_date                       DATE;
        v_address                            BOOLEAN;
        v_addr1                              VARCHAR2(30);
        v_addr2                              VARCHAR2(30);
        v_addr3                              VARCHAR2(30);
        v_addr4                              VARCHAR2(30);
        v_addr5                              VARCHAR2(30);
        
        v_debug                              BOOLEAN := TRUE;     -- CS 10/01/07  
        v_count                              BINARY_INTEGER := 0;
        v_final_conc                         NUMBER := 0;
        v_final_cap                          NUMBER := 0;
        
        r_select                             c_select%ROWTYPE;
        
        grant_conc                           EXCEPTION;
        e_validate_data                      EXCEPTION;
        
    BEGIN
        log.set_method('concession_common');
        log.log_char('pv_source',pv_source);
        log.log_number('concession_type',concession_type);
        log.log_date('pr_service_range.start_date',pr_service_range.start_date);
        log.log_date('pr_service_range.end_date',pr_service_range.end_date);
        log.log_number('service_amount',service_amount);
        log.log_number('pcs_id',pcs_id);
        log.log_char('name',name);
        log.log_date('date_of_birth',date_of_birth);
        log.log_char('street',street);
        log.log_char('lis_key',lis_key);
        
        /* Basic parameter edits */
        IF  CONCESSION_TYPE IS NULL OR
            pr_service_range.START_DATE IS NULL OR
            pr_service_range.END_DATE IS NULL OR
            SERVICE_AMOUNT IS NULL OR
            PCS_ID IS NULL
        THEN
            /* insufficient info passed to module */
            return_code :=99 ;
            RAISE e_validate_data;
        END IF;
        /*Check if the client exists and has an eligibility record for the concession type being claimed 
        based on the dates of the service being provided */
        FOR r_select IN c_select
        LOOP
            v_count := v_count +1;
            IF v_count = 1  THEN
                
                /* validate the client */
                IF r_select.name  <> NAME
                THEN   /*No concession eligibility*/
                    return_code :=03;
                    RAISE e_validate_data;
                END IF;
                IF r_select.birth_date_required = 'YES' THEN
                    IF        r_select.birth_date <> DATE_OF_BIRTH
                    THEN
                        /*No concession eligibility*/
                        return_code :=04;
                        RAISE e_validate_data;
                    END IF;
                END IF;
                /* Now check the address, if required */
                IF  r_select.address_required = 'YES'
                THEN
                    --v_address := Pcs.fixpawa_address(r_select.address,v_addr1, v_addr2, v_addr3, v_addr4, v_addr5);
                    v_address := Pcs.fixpawa_address(r_select.address, v_addr1);
                    
                    IF ( SUBSTR(v_addr1,1,3) = STREET
                        OR SUBSTR(v_addr2,1,3) = STREET
                        OR SUBSTR(v_addr3,1,3) = STREET
                        OR SUBSTR(v_addr4,1,3) = STREET
                        OR SUBSTR(v_addr5,1,3) = STREET
                        OR r_select.LIS_key = LIS_KEY)
                    THEN
                        NULL;  -- GOTO Eligibility;
                    ELSE
                        /*No concession eligibility */
                        return_code :=05;
                        RAISE e_validate_data;
                    END IF;
                END IF;  -- r_select.address_required = 'YES'
                
                -- CS 05/07/07 Save these for processing after accumulating elig concessions   
                v_own_first     := r_select.ownership;          -- CS 05/07/07
                v_own_app_first := r_select.Ownership_Applies;  -- CS 05/07/07
                
            END IF;  --  IF v_count = 1  
            --
            -- CS 10/01/07 Determine Working Eligibility Segment dates
            --             Rework original code   
            
            -- debug('Eligibility start '||r_select.elig_start_date||' Elig. end '||r_select.elig_end_date, v_debug); 
            IF  r_select.elig_start_date > pr_service_range.START_DATE  THEN
                IF r_select.part_payments_allowed = 'NO' THEN  -- no concession eligibility 
                    return_code := 06;
                    RAISE e_validate_data ;
                ELSE
                    v_cal_start_date := r_select.elig_start_date ;
                    return_code :=01;
                END IF;
            ELSE
                v_cal_start_date := pr_service_range.START_DATE;
            END IF;
            IF (NVL(r_select.elig_end_date,gcd_max_date) >= pr_service_range.END_DATE)    THEN
                v_cal_end_date := pr_service_range.END_DATE;
            ELSE
                IF  r_select.part_payments_allowed = 'NO'  THEN
                    Return_code := 06 ;
                    RAISE e_validate_data;
                ELSE
                    v_cal_end_date := r_select.elig_end_date;
                    return_code :=01;
                END IF;
            END IF;
            
            -- debug('Elig. Seg. start '||v_cal_start_date||' segment end '||v_cal_end_date, v_debug);
            v_validate_conc := Pcs.Calculate_Concession (concession_type,pr_service_range.start_date,pr_service_range.end_date,
                    v_cal_start_date,v_cal_end_date,service_amount, v_debug);
            
            v_validate_threshold := Pcs.Calculate_Threshold (concession_type, pr_service_range.start_date,pr_service_range.end_date,
                       v_cal_start_date,v_cal_end_date,service_amount, v_debug);
            --debug('Validate Threshold: '||v_validate_threshold, v_debug);
            v_final_conc := v_final_conc + v_validate_conc;
            --debug('Final Conc : '||v_final_conc, v_debug);
            v_final_cap  := v_final_cap  + v_validate_threshold;
            --debug('Final Cap : '||v_final_cap, v_debug);
            
        END LOOP; 
        
        --  Adjust Final Conc by Cap, Ownership etc.          
        IF  v_final_cap < v_final_conc  THEN
            v_final_conc := v_final_cap;
            --debug('Final Conc : '||v_final_conc, v_debug);
        END IF;
        
        IF  v_own_app_first = 'YES' -- CS 05/07/07
        THEN
            v_ownership := Pcs.Ownership_Applies(pcs_id, v_final_conc, v_ownership_message);
            IF v_ownership_message = 07
            THEN
                RETURN_CODE := 07;
                RAISE e_validate_data;
            ELSE
                v_final_conc := v_ownership;
            END IF;
        END IF;
        
        IF v_final_conc > 0
        THEN
            IF v_own_first < 100 AND v_own_first > 0                   -- CS 05/07/07  
            THEN
                return_code :=01;
                RAISE grant_conc;
            ELSE
                return_code :=00;
                RAISE grant_conc;
            END IF;
        ELSE
            return_code :=02;
            RAISE e_validate_data;
        END IF;
        
        IF v_count = 0 THEN  -- If no Eligibilities found..
            return_code :=02;
            RAISE e_validate_data;
        END IF;
        
        log.end_method;
        
    EXCEPTION
        WHEN e_validate_data  THEN
            error_message := Display_Err(return_code);
            concession_amount :=0;
            if (pv_source='getconcession') then concession_id := 0; end if;
            log.log_message(error_message,gci_unexpected);
            log.end_method;
            RETURN FALSE;
        WHEN grant_conc THEN
            error_message := Display_Err(return_code);
            concession_amount :=v_final_conc;
            log.log_message(error_message,gci_unexpected);
            log.log_number('concession_amount',concession_amount,gci_unexpected);
            
            if (pv_source='getconcession') then
                SELECT pension.conc_seq.nextval ||':'|| TO_CHAR(SYSDATE,'dd-mm-yyyy:HH24:MI:SS') 
                INTO   concession_id 
                FROM   dual;
                
                pcs.add_conc(
                    pcs_id
                  , concession_type
                  , concession_id
                  , pr_service_range.start_date
                  , pr_service_range.end_date
                  , service_amount
                  , concession_amount
                  , v_cal_end_date
                );
            end if;
    
            log.end_method;
            
            RETURN TRUE;
        WHEN OTHERS THEN
            concession_amount :=0;
            Return_code := 99;
            if (pv_source='getconcession') then concession_id := 0; end if;
            error_message := Display_Err(return_code);

            log.log_char('pv_source',pv_source,gci_unexpected);
            log.log_number('concession_type',concession_type,gci_unexpected);
            log.log_date('pr_service_range.start_date',pr_service_range.start_date,gci_unexpected);
            log.log_date('pr_service_range.end_date',pr_service_range.end_date,gci_unexpected);
            log.log_number('service_amount',service_amount,gci_unexpected);
            log.log_number('pcs_id',pcs_id,gci_unexpected);
            log.log_char('name',name,gci_unexpected);
            log.log_date('date_of_birth',date_of_birth,gci_unexpected);
            log.log_char('street',street,gci_unexpected);
            log.log_char('lis_key',lis_key,gci_unexpected);
            log.log_error(
                pv_message               => error_message
              , pv_sqlerrm               => SQLERRM
            );
            log.end_method;
            
            RETURN FALSE;
    END concession_common;

    -----------------------------------------------------------------------------
    
    FUNCTION Concession (
        concession_type    IN NUMBER
      , service_start_date IN DATE
      , service_end_date   IN DATE
      , service_amount     IN NUMBER
      , pcs_id             IN NUMBER
      , name               IN VARCHAR2
      , date_of_birth      IN DATE
      , street             IN VARCHAR2
      , lis_key            IN VARCHAR2
      , concession_Amount  OUT NUMBER
      , concession_id      OUT NUMBER
      , return_code        OUT NUMBER
      , error_message      OUT VARCHAR2
    ) RETURN BOOLEAN
    IS
        lv_concession_id varchar2(100);
        lb_result        boolean;
        lr_service_range     date_range_typ;
        
    BEGIN
        lr_service_range.start_date := service_start_date;
        lr_service_range.end_date   := service_end_date;

        lb_result := concession_common (
            pv_source          => 'concession'
          , concession_type    => concession_type
          , pr_service_range   => lr_service_range
          , service_amount     => service_amount
          , pcs_id             => pcs_id
          , name               => name
          , date_of_birth      => date_of_birth
          , street             => street
          , lis_key            => lis_key
          , concession_amount  => concession_amount
          , concession_id      => lv_concession_id
          , return_code        => return_code
          , error_message      => error_message
        );
        begin
            concession_id := to_number(lv_concession_id);
        exception
            when others then
                concession_id := -1;
        end;
        return lb_result;
    END concession;

    -----------------------------------------------------------------------------

    FUNCTION Getconcession(
        concession_type    IN NUMBER
      , service_start_date IN DATE
      , service_end_date   IN DATE
      , service_amount     IN NUMBER
      , pcs_id             IN NUMBER
      , name               IN VARCHAR2
      , date_of_birth      IN DATE
      , street             IN VARCHAR2
      , lis_key            IN VARCHAR2
      , concession_Amount  OUT NUMBER
      , concession_id      OUT VARCHAR2
      , return_code        OUT NUMBER
      , error_message      OUT VARCHAR2
    )
    RETURN BOOLEAN
    IS
        lr_service_range     date_range_typ;
    BEGIN
        lr_service_range.start_date := service_start_date;
        lr_service_range.end_date   := service_end_date;

        return concession_common (
            pv_source          => 'getconcession'
          , concession_type    => concession_type
          , pr_service_range   => lr_service_range
          , service_amount     => service_amount
          , pcs_id             => pcs_id
          , name               => name
          , date_of_birth      => date_of_birth
          , street             => street
          , lis_key            => lis_key
          , concession_amount  => concession_amount
          , concession_id      => concession_id
          , return_code        => return_code
          , error_message      => error_message
        );    
    END getconcession;
    
    -----------------------------------------------------------------------------
    FUNCTION Reverseconcession (
        Concession_type          IN  NUMBER
      , service_start_date       IN  DATE
      , service_end_date         IN  DATE
      , service_amount           IN  NUMBER
      , agent_id                 IN  NUMBER
      , pcs_id                   IN  NUMBER
      , Name                     IN  VARCHAR2
      , date_of_birth            IN  DATE
      , Street                   IN  VARCHAR2
      , Lis_key                  IN  VARCHAR2
      , Concession_id            IN  VARCHAR2
      , Return_code              OUT NUMBER
      , Return_message           OUT VARCHAR2
    ) RETURN BOOLEAN
    IS
        -- CS 10/01/07   No Concession Calc is performed, but Eligibilities need to be processed
        --               to perform the validation.
        --                 
        
        CURSOR c_select IS  --  CS 10/01/07  Only interested in Eligibilities.  Rates for debug only
            SELECT C.name
                 , C.birth_date
                 , C.ADDRESS
                 , C.suburb
                 , C.POST_CODE
                 , C.lis_key
                 , C.ownership
                 , T.conc_type_id
                 --T.rate_data_type_id,                 -- May need these to debug 
                 --T.EFFECTIVE_START_DATE RATE_S_DATE,
                 --T.EFFECTIVE_END_DATE   RATE_E_DATE,
                 --T.rate_value, 
                 --T.cap_data_type_id,
                 --T.cap_value, 
                 , TRUNC(E.elig_start_date) ELIG_START_DATE
                 , TRUNC(E.elig_end_date) ELIG_END_DATE
                 , T.birth_date_required
                 , T.address_required
                 , T.part_payments_allowed
                 , T.Ownership_Applies
        FROM       intpawa_client C
                 , intpawa_conc_elig E
                 , intpawa_conc_type T
        WHERE     C.person_id    = PCS_ID 
        AND       C.person_id    = E.person_id 
        AND       T.conc_type_id = concession_type
        AND       T.conc_type_id = E.conc_type_id
        AND       NVL(T.EFFECTIVE_END_DATE,gcd_max_date)    = (     -- Isolate single Eligibility row from Variable Rate rows
                      SELECT MAX(NVL(T2.EFFECTIVE_END_DATE,gcd_max_date) )
                      FROM   PENSION.INTPAWA_CONC_TYPE   T2
                      WHERE  T2.CONC_TYPE_ID = T.CONC_TYPE_ID
                  )
        AND       E.elig_start_date    = (    -- Isolate single Eligibility row from Variable Rate rows
                      SELECT MAX(E2.elig_start_date)
                      FROM   INTPAWA_CONC_ELIG  E2
                      where  E2.person_id  = E.person_id
                      and E2.conc_type_id = E.conc_type_id
                  );  
        
        -- This is the old Shonky
        --       WHERE
        --         Client.person_id = PCS_ID AND
        --         Elig.person_id   = PCS_ID AND
        --         TYPE.conc_type_id = CONCESSION_TYPE AND
        --         elig.conc_type_id = CONCESSION_TYPE  AND
        --         Client.person_id = elig.person_id; 
        
        
        e_validate_data       EXCEPTION;
        e_no_part_payments    EXCEPTION;
        v_cal_start_date      DATE;
        v_cal_end_date        DATE;
        v_conc_id             VARCHAR2(40);
        v_address             BOOLEAN;
        v_addr1               VARCHAR2(30);
        v_addr2               VARCHAR2(30);
        v_addr3               VARCHAR2(30);
        v_addr4               VARCHAR2(30);
        v_addr5               VARCHAR2(30);
        
        v_debug               BOOLEAN := TRUE;
        v_count               BINARY_INTEGER := 0;
        
        r_select              c_select%ROWTYPE;
        
        grant_conc            EXCEPTION;
        
    BEGIN
        log.set_method('Reverseconcession');
        log.log_number('Concession_type',Concession_type);
        log.log_date('service_start_date',service_start_date);
        log.log_date('service_end_date',service_end_date);
        log.log_number('service_amount',service_amount);
        log.log_number('agent_id',agent_id);
        log.log_number('pcs_id',pcs_id);
        log.log_char('name',name);
        log.log_date('date_of_birth',date_of_birth);
        log.log_char('Street',Street);
        log.log_char('Lis_key',Lis_key);
        log.log_char('Concession_id',Concession_id);
        
        IF  concession_type IS NULL OR
            service_start_date IS NULL OR
            service_end_date IS NULL OR
            service_amount IS NULL OR
            Agent_id IS NULL OR
            PCS_id IS NULL OR
            name IS NULL OR
            concession_id IS NULL
        THEN
            return_code := 99;
            RAISE e_validate_data;
        END IF;
        
        /*Check if the client exists and has an eligibility record for the concession type being claimed */
        --
        -- CS 10/01/07  Not sure why all this complication is necessary.  Why not just use the Concession_Id 
        --              to find the original concession??   ...and then why the validation again???
        --              I think there is also a bug here in that reversals will not work when a Pensioner has
        --              terminated during a previous S/Period.  Cursor wont find a record to reverse against since the 
        --              eligibility will no longer exist.  Users need to confirm this and maybe a redesign is necessary.
        --              Anyway....  Just need a single fetch here.
        --  
        
        OPEN c_select;
        FETCH c_select INTO r_select;
        IF c_select%NOTFOUND  THEN
            CLOSE c_select;
            return_code :=98;
            RAISE e_validate_data;
        END IF;
        CLOSE c_select;
        
        log.log_message('c_select query successful',gci_statement);
        
        /* validate the client */
        IF r_select.name  <> NAME
        THEN   /*No concession eligibility*/
            return_code :=03;
            RAISE e_validate_data;
        END IF;

        IF r_select.birth_date_required = 'YES' THEN
            IF r_select.birth_date <> DATE_OF_BIRTH
            THEN
                /*No concession eligibility*/
                return_code :=04;
                RAISE e_validate_data;
            END IF;
        END IF;
        
        /* Now check the address, if required */
        IF  r_select.address_required = 'YES'
        THEN
            --v_address := Pcs.fixpawa_address (r_select.ADDRESS,v_addr1,v_addr2, v_addr3,v_addr4, v_addr5);
            v_address := Pcs.fixpawa_address(r_select.address,v_addr1);
            
            IF NOT (SUBSTR(v_addr1,1,3) = STREET
            OR SUBSTR(v_addr2,1,3) = STREET
            OR SUBSTR(v_addr3,1,3) = STREET
            OR SUBSTR(v_addr4,1,3) = STREET
            OR SUBSTR(v_addr5,1,3) = STREET
            OR r_select.LIS_key <> LIS_KEY)
            THEN
                /*No concession eligibility */
                return_code :=05;
                RAISE e_validate_data;
            END IF;
        END IF;  -- r_select.address_required = 'YES'
        --
        -- CS 10/01/07 Determine Working Eligibility Segment dates
        --             Copy from GetConcession to simply get final End Elig date... All a bit much.   
        
        -- debug('Eligibility start '||r_select.elig_start_date||' Elig. end '||r_select.elig_end_date, v_debug); 
        IF  r_select.elig_start_date > SERVICE_START_DATE  THEN
            IF r_select.part_payments_allowed = 'NO' THEN  -- no concession eligibility 
                return_code := 06;
                RAISE e_validate_data ;
            ELSE
                v_cal_start_date := r_select.elig_start_date ;
                return_code :=01;
            END IF;
        ELSE
            v_cal_start_date := SERVICE_START_DATE;
        END IF;
        IF (NVL(r_select.elig_end_date,gcd_max_date) >= SERVICE_END_DATE)    THEN
            v_cal_end_date := SERVICE_END_DATE;
        ELSE
            IF  r_select.part_payments_allowed = 'NO'  THEN
                Return_code := 06 ;
                RAISE e_validate_data;
            ELSE
                v_cal_end_date := r_select.elig_end_date;
                return_code :=01;
            END IF;
        END IF;
        
        log.log_message('validation successful',gci_statement);
        
        BEGIN
            INSERT INTO PENSION.INTPAWA_CONC_TRANS( 
                PERSON_ID
              , CONC_TYPE_ID
              , TRANS_TYPE_ID
              , AGENTS_TRANS_NO
              , TRANS_DATE
              , SERV_START_DATE
              , SERV_END_DATE
              , SERV_VALUE
              , CONC_VALUE
              , SERV_PROV_AGENT_ID
            ) VALUES (
                PCS_ID
              , concession_type
              , 2
              , concession_id
              , SYSDATE
              , service_start_date
              , service_end_date
              , service_amount
              , 0
              , agent_id
            );
            
            /* update concession eligibility to reflect the new .... */
            UPDATE PENSION.INTPAWA_CONC_ELIG
            SET    elig_start_date  = v_cal_start_date 
            WHERE  person_id     = PCS_ID
            AND    conc_type_id  = CONCESSION_TYPE
            AND    elig_end_date IS NULL;
            -- CS 10/01/07   I see a problem here if the pensioner has an Elig End date.. Wont update
            --               so then rebilling will fail!!!
            
            -- debug('Ins. Trans: '||PCS_ID||', '||concession_type||', '||2||', '||concession_id||', '||SYSDATE||', '||
            --     service_start_date||', '||service_end_date||', '||service_amount||', '||0||', '||agent_id, v_debug);
            
            COMMIT;
        
        END;
        
        Return_code := 00;
        return_message := Display_Err(return_code);
        
        log.end_method;
        RETURN TRUE;
    
    EXCEPTION
        WHEN e_validate_data   THEN
            return_message :=Display_Err(return_code);
            log.log_message('e_validate_data: '||return_message,gci_error);
            log.end_method;
            RETURN FALSE;
        WHEN OTHERS THEN
            log.log_number('Concession_type',Concession_type,gci_unexpected);
            log.log_date('service_start_date',service_start_date,gci_unexpected);
            log.log_date('service_end_date',service_end_date,gci_unexpected);
            log.log_number('service_amount',service_amount,gci_unexpected);
            log.log_number('agent_id',agent_id,gci_unexpected);
            log.log_number('pcs_id',pcs_id,gci_unexpected);
            log.log_char('name',name,gci_unexpected);
            log.log_date('date_of_birth',date_of_birth,gci_unexpected);
            log.log_char('Street',Street,gci_unexpected);
            log.log_char('Lis_key',Lis_key,gci_unexpected);
            log.log_char('Concession_id',Concession_id,gci_unexpected);
            log.log_error(
                pv_message => 'Unhandled exception.'
              , pv_sqlerrm => SQLERRM
            );
            log.end_method;
            RETURN FALSE;
    END Reverseconcession;
    
    -----------------------------------------------------------------------------
    PROCEDURE transfer_trans_to_pcs
    IS
        v_count   NUMBER(7);
        my_count  NUMBER(7);
        pcs_count NUMBER(7);
    BEGIN
        log.set_method('transfer_trans_to_pcs');
        -- 
        -- Count the number of records in the temp table
        --
        SELECT COUNT (*) 
        INTO   v_count 
        FROM   pension_temp; 
        log.log_number('v_count',v_count);
        --
        -- Record this in the pension_temp_count log table
        --
        INSERT INTO PENSION_TEMP_COUNT(
            r_count
          , log_date
        ) values (
            v_count
          , sysdate
        );
        --
        -- Transfer the records from intpawa_conc_trans into pension_temp
        --
        INSERT INTO pension_temp
        (SELECT * 
         FROM   PENSION.INTPAWA_CONC_TRANS);
        my_count := sql%rowcount;
        log.log_number('my_count',my_count);
        --
        -- Count the total records in pension_temp
        --
        SELECT COUNT(*) 
        INTO   pcs_count 
        FROM   pension_temp;
        log.log_number('pcs_count',pcs_count);
        --
        -- Check the totals match
        --
        IF my_count + v_count = pcs_count
        THEN
            insert_pcs_logs('Data Transfered to PCS',SYSDATE);
            INSERT INTO PENSION.INTPAWA_CONC_TRANS_BKP 
            SELECT * FROM PENSION.INTPAWA_CONC_TRANS;
            log.log_number('INTPAWA_CONC_TRANS_BKP insert count',sql%rowcount);

            DELETE FROM PENSION.INTPAWA_CONC_TRANS;
            log.log_number('INTPAWA_CONC_TRANS delete count',sql%rowcount);
            COMMIT;
        ELSE
            insert_pcs_logs('Data has not been transfered to PCS-Error found',SYSDATE);
            ROLLBACK;
        END IF;
        log.end_method;
    EXCEPTION
        WHEN OTHERS THEN
            log.log_error(
                pv_message => 'Unhandled exception.'
              , pv_sqlerrm => SQLERRM
            );
            insert_pcs_logs('Unidentified Error found-File not transfered ',SYSDATE);
            log.end_method;
            ROLLBACK;
    END transfer_trans_to_pcs;
    
    -----------------------------------------------------------------------------
    FUNCTION fixpawa_address(
        p_address IN VARCHAR2
      , p_1 OUT VARCHAR2
    ) RETURN BOOLEAN IS
        result varchar2(60);
        list   intpawa_client.address%type;
        rest   varchar2(20);
    BEGIN
        log.set_method('fixpawa_address');
        log.log_char('p_address',p_address);

        list := ltrim (rtrim (p_address));
        rest := substr (list,instr(list, ' ') + 1,8);
        result:=replace(rest,' ','');
        if length(result)>3
        then
            p_1:=substr(result,1,3);
        end if;
        
        log.log_char('p_1',p_1);
        log.end_method;
        return (true);
    EXCEPTION
        WHEN OTHERS THEN
            log.log_char('p_address',p_address,gci_unexpected);
            log.log_error(
                pv_message =>'Unhandled exception.'
              , pv_sqlerrm => SQLERRM
            );
            log.end_method;
            return(false);
    END fixpawa_address;
END pcs; 
