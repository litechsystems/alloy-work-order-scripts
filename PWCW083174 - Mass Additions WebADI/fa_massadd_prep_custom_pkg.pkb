create or replace PACKAGE BODY fa_massadd_prep_custom_pkg AS
    /* $Header: FAMAPREPCB.pls 120.2.12010000.2 2009/07/19 09:44:21 glchen ship $ */
    -- +============================================================================+
    -- | Change Record:
    -- | ===============
    -- |
    -- | Version  Date         Author           Comments
    -- | =======  ===========  ===============  ====================================+
    -- | 1.00     19-Dec-2016  Paul Job         Initial Version.
    -- | 1.10     16-May-2018  Stephen Anderson Refactored to fix various issues
    -- +============================================================================+
    
    CURSOR c_asset_serial_lookup (
        cp_asset_serialno        IN VARCHAR2
      , cp_source_book_type_code IN VARCHAR2) IS
        SELECT asset_id
             , asset_number
             , corpbook_retired
             , taxbook_retired
        FROM   (
                SELECT fab.asset_id
                     , fab.asset_number
                     , fab.creation_date
                     , fa_massadd_prep_custom_pkg.f_asset_retired (
                           p_asset_id              => fab.asset_id
                         , p_source_book_type_code => cp_source_book_type_code
                         , p_target_book_class     => 'CORPORATE'
                       ) corpbook_retired
                     , fa_massadd_prep_custom_pkg.f_asset_retired (
                           p_asset_id              => fab.asset_id
                         , p_source_book_type_code => cp_source_book_type_code
                         , p_target_book_class     => 'TAX'
                       ) taxbook_retired
                FROM   fa_additions_b fab
                WHERE  fab.serial_number = cp_asset_serialno
                AND    exists (
                           select 'in source book'
                           from   fa_books fb 
                           where  fb.book_type_code = cp_source_book_type_code 
                           and    fb.asset_id = fab.asset_id
                       )
               )
        ORDER BY 
            CASE
            WHEN corpbook_retired = 'N' and taxbook_retired = 'N' THEN
              1
            WHEN corpbook_retired = 'N' and taxbook_retired = 'Y' THEN
              2
            WHEN corpbook_retired = 'Y' and taxbook_retired = 'N' THEN
              3
            WHEN corpbook_retired = 'Y' and taxbook_retired = 'Y' THEN
              4
            END
          , creation_date desc;

    -- Private type declarations
    SUBTYPE c_asset_serial_lookup_typ IS c_asset_serial_lookup%ROWTYPE;
    
    -- Private constant declarations
    gx_package     CONSTANT VARCHAR2(30) := 'fa_massadd_prep_custom_pkg';
    
    -- Private variable declarations
    
    -- Function and procedure implementations
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    PROCEDURE p_log(
        p_level  NUMBER
      , p_msg    VARCHAR2
    ) AS
    BEGIN
    
        IF (p_level >= fnd_log.g_current_runtime_level) THEN
            fnd_log.string(p_level,'FA.PLSQL.'||gx_package, p_msg);
        END IF;
        
        IF NVL(fnd_profile.value('AFLOG_ENABLED'),'N') = 'Y' AND
            fnd_global.conc_request_id > 0 THEN
            fnd_file.put_line(fnd_file.log,p_msg);
        END IF;
        
        -- comment the above and write dbms_output if running from back end
        --dbms_output.put_line(p_msg);
    
    END p_log;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_asset_category_match (
        p_asset_id          IN NUMBER
      , p_asset_category_id IN VARCHAR2
    ) RETURN BOOLEAN 
    IS
        
        -- Constants
        x_function CONSTANT VARCHAR2(30) := 'f_asset_category_match';
        
        -- Variables
        v_return   BOOLEAN := FALSE;
        v_cnt      NUMBER  := 0;
        
    BEGIN
    
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_id:          '||p_asset_id);
        p_log(fnd_log.level_statement, 'p_asset_category_id: '||p_asset_category_id);
        
        SELECT count(*)
        INTO   v_cnt
        FROM   fa_additions_b
        WHERE  asset_id          = p_asset_id
        AND    asset_category_id = p_asset_category_id;
        
        IF v_cnt > 0 THEN
            v_return := TRUE;
        END IF;
        
        p_log(fnd_log.level_statement, 'v_cnt: '||v_cnt);
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN v_return;
    
    END f_asset_category_match;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_asset_location_match (
        p_asset_id              IN NUMBER
      , p_mass_addition_id      IN NUMBER
      , p_source_book_type_code IN VARCHAR2
    ) RETURN BOOLEAN 
    IS
        
        -- Constants
        x_function CONSTANT VARCHAR2(30) := 'f_asset_location_match';
        
        -- Variables
        v_return   BOOLEAN := FALSE;
        v_cnt      NUMBER  := 0;
    
    BEGIN
    
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_id:    '||p_asset_id);
        p_log(fnd_log.level_statement, 'p_mass_addition_id:      '||p_mass_addition_id);
        p_log(fnd_log.level_statement, 'p_source_book_type_code: '||p_source_book_type_code);
        
        SELECT count(*)
        INTO   v_cnt
        FROM   fa_distribution_history fdh
        WHERE  fdh.asset_id          = p_asset_id
        AND    fdh.location_id       IN (
                   SELECT fmd.location_id
                   FROM   fa_mass_additions        fa
                        , fa_massadd_distributions fmd
                   WHERE  fa.mass_addition_id = p_mass_addition_id
                   AND    fa.mass_addition_id = fmd.mass_addition_id)
        AND    fdh.date_ineffective          IS NULL
        AND    fdh.transaction_header_id_out IS NULL
        AND    fdh.book_type_code IN (
                   SELECT fbc.book_type_code
                   FROM   fa_book_controls fbc
                   WHERE  fbc.distribution_source_book = p_source_book_type_code
        );
        
        IF v_cnt > 0 THEN
            v_return := TRUE;
        END IF;
        
        p_log(fnd_log.level_statement, 'v_cnt: '||v_cnt);
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN v_return;
    
    END f_asset_location_match;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_asset_expcode_match (
        p_asset_id              IN NUMBER
      , p_mass_addition_id      IN NUMBER
      , p_source_book_type_code IN VARCHAR2
    ) RETURN BOOLEAN 
    IS
        
        -- Constants
        x_function CONSTANT VARCHAR2(30) := 'f_asset_expcode_match';
        
        
        -- Variables
        v_return   BOOLEAN := FALSE;
        v_cnt      NUMBER  := 0;
        
    BEGIN
    
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_id:              '||p_asset_id);
        p_log(fnd_log.level_statement, 'p_mass_addition_id:      '||p_mass_addition_id);
        p_log(fnd_log.level_statement, 'p_source_book_type_code: '||p_source_book_type_code);
        
        SELECT count(*)
        INTO   v_cnt
        FROM   fa_distribution_history fdh
        WHERE  fdh.asset_id            = p_asset_id
        AND    fdh.code_combination_id IN (
                   SELECT fmd.deprn_expense_ccid
                   FROM   fa_mass_additions        fa
                        , fa_massadd_distributions fmd
                   WHERE  fa.mass_addition_id = p_mass_addition_id
                   AND    fa.mass_addition_id = fmd.mass_addition_id
               )
        AND    fdh.date_ineffective          IS NULL
        AND    fdh.transaction_header_id_out IS NULL
        AND    fdh.book_type_code IN (
                   SELECT fbc.book_type_code
                   FROM   fa_book_controls fbc
                   WHERE  fbc.distribution_source_book = p_source_book_type_code
               );
        
        IF v_cnt > 0 THEN
            v_return := TRUE;
        END IF;
        
        p_log(fnd_log.level_statement, 'v_cnt: '||v_cnt);
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN v_return;
    
    END f_asset_expcode_match;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_asset_retired (
        p_asset_id              IN NUMBER
      , p_source_book_type_code IN VARCHAR2
      , p_target_book_class     IN VARCHAR2
    ) RETURN VARCHAR2 
    IS
    
        -- Constants
        x_function CONSTANT VARCHAR2(20) := 'f_asset_retired';
    
        -- Variables
        v_return   BOOLEAN := FALSE;
        lv_retired VARCHAR2(1) := 'N';
    
        cursor c_latest_retirement is
            select case 
                   when fr.status = 'PROCESSED' and 
                        fr.transaction_header_id_out is null and
                        fth.transaction_type_code in ('FULL RETIREMENT','PARTIAL RETIREMENT') 
                   then
                       'Y'
                   else
                       'N'
                   end
            from   fa_retirements fr
                 , fa_transaction_headers fth
            where  fr.asset_id = p_asset_id
            and    fr.transaction_header_id_in = fth.transaction_header_id
            AND    fr.book_type_code IN (
                       SELECT fbc.book_type_code
                       FROM   fa_book_controls fbc
                       WHERE  fbc.distribution_source_book = p_source_book_type_code
                       AND    fbc.book_class               = p_target_book_class
                   )
            order by fr.book_type_code, fr.date_effective desc;
    BEGIN
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_id:              '||p_asset_id);
        p_log(fnd_log.level_statement, 'p_source_book_type_code: '||p_source_book_type_code);
        p_log(fnd_log.level_statement, 'p_target_book_class:     '||p_target_book_class);
        
        IF p_asset_id IS NOT NULL THEN
            OPEN c_latest_retirement;
            FETCH c_latest_retirement INTO lv_retired;
            IF c_latest_retirement%NOTFOUND THEN
                lv_retired := 'N';
            END IF;
            CLOSE c_latest_retirement;
        END IF;
        
        p_log(fnd_log.level_statement, 'lv_retired: '||lv_retired);
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        
        RETURN coalesce(lv_retired,'N');
    END f_asset_retired;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_get_asset_key_name(
        p_asset_key_ccid IN NUMBER
    ) RETURN fa_asset_keywords.segment1%TYPE 
    IS
        
        -- Constants
        x_function  CONSTANT VARCHAR2(20) := 'f_get_asset_key_name';
        
        -- Variables
        v_asset_key fa_asset_keywords.segment1%TYPE;
        
        -- CURSORS
        CURSOR c_asset_key (cp_asset_key_ccid IN NUMBER) IS
            SELECT fak.segment1
            FROM   fa_asset_keywords fak
            WHERE  fak.code_combination_id = cp_asset_key_ccid
            AND    fak.enabled_flag        = 'Y'
            AND    TRUNC(SYSDATE) BETWEEN NVL(start_date_active, TRUNC(SYSDATE))
                                      AND NVL(end_date_active,   TRUNC(SYSDATE));
    
    BEGIN
    
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_key_ccid: '||p_asset_key_ccid);
        
        OPEN  c_asset_key (cp_asset_key_ccid => p_asset_key_ccid);
        FETCH c_asset_key INTO v_asset_key;
        IF c_asset_key%NOTFOUND THEN
            p_log(fnd_log.level_exception, 'Asset Key not found for asset_key_ccid: '||p_asset_key_ccid);
            RAISE_APPLICATION_ERROR(-20100, 'Asset Key not found for asset_key_ccid: '||p_asset_key_ccid);
        END IF;
        CLOSE c_asset_key;
        
        p_log(fnd_log.level_statement, 'v_asset_key: '||v_asset_key);
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN v_asset_key;
    
    END f_get_asset_key_name;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_get_asset_info (
        p_asset_serialno        IN VARCHAR2
      , p_source_book_type_code IN VARCHAR2
    ) RETURN c_asset_serial_lookup_typ
        RESULT_CACHE
        RELIES_ON(fa_additions_b, fa_retirements, fa_transaction_headers, fa_book_controls)
    IS
    
        -- Constants
        x_function CONSTANT VARCHAR2(25) := 'f_get_asset_info';
        
        -- Variables
        lr_asset   c_asset_serial_lookup_typ;
    BEGIN
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_serialno: '||p_asset_serialno);
        p_log(fnd_log.level_statement, 'p_source_book_type_code: '||p_source_book_type_code);
        
        OPEN  c_asset_serial_lookup (
            cp_asset_serialno        => p_asset_serialno
          , cp_source_book_type_code => p_source_book_type_code
        );
        FETCH c_asset_serial_lookup INTO lr_asset;
        IF c_asset_serial_lookup%notfound then
            lr_asset.asset_id         := NULL;
            lr_asset.asset_number     := NULL;
            lr_asset.corpbook_retired := 'N';
            lr_asset.taxbook_retired  := 'N';
        END IF;
        CLOSE c_asset_serial_lookup;
        
        fnd_file.put_line(fnd_file.log, 'lr_asset.asset_id         = '||lr_asset.asset_id);
        fnd_file.put_line(fnd_file.log, 'lr_asset.asset_number     = '||lr_asset.asset_number);
        fnd_file.put_line(fnd_file.log, 'lr_asset.corpbook_retired = '||lr_asset.corpbook_retired);
        fnd_file.put_line(fnd_file.log, 'lr_asset.taxbook_retired  = '||lr_asset.taxbook_retired);
        
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN lr_asset;
    END f_get_asset_info;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_compare_to_asset (
        p_asset_serialno        IN VARCHAR2
      , p_source_book_type_code IN VARCHAR2
    ) RETURN NUMBER 
    IS
    
        -- Constants
        x_function CONSTANT VARCHAR2(25) := 'f_compare_to_asset';
        
        -- Variables
        lr_asset   c_asset_serial_lookup_typ;
    BEGIN
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_serialno: '||p_asset_serialno);
        
        lr_asset := f_get_asset_info (
            p_asset_serialno        => p_asset_serialno
          , p_source_book_type_code => p_source_book_type_code
        );
        
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN lr_asset.asset_number;
    END f_compare_to_asset;
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION f_compare_to_asset_id (
        p_asset_serialno        IN VARCHAR2
      , p_source_book_type_code IN VARCHAR2
    ) RETURN NUMBER 
    IS
    
        -- Constants
        x_function CONSTANT VARCHAR2(25) := 'f_compare_to_asset_id';
        
        -- Variables
        lr_asset   c_asset_serial_lookup_typ;
    BEGIN
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'p_asset_serialno: '||p_asset_serialno);
        
        lr_asset := f_get_asset_info (
            p_asset_serialno        => p_asset_serialno
          , p_source_book_type_code => p_source_book_type_code
        );
        
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);
        RETURN lr_asset.asset_id;
    END f_compare_to_asset_id;    
    ---------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------
    FUNCTION prepare_attributes(
        px_mass_add_rec IN OUT NOCOPY fa_massadd_prepare_pkg.mass_add_rec
      , p_log_level_rec IN            fa_api_types.log_level_rec_type DEFAULT NULL
    ) RETURN BOOLEAN 
    IS
        
        -- Constants
        x_function CONSTANT VARCHAR2(20) := 'prepare_attributes';
        
        -- Variables
        v_asset_key        fa_asset_keywords.segment1%TYPE;
        v_asset_id         NUMBER;
        v_corpbook_retired BOOLEAN := FALSE;
        v_taxbook_retired  BOOLEAN := FALSE;
        lr_asset           c_asset_serial_lookup_typ;
    
    BEGIN
        p_log(fnd_log.level_procedure, 'Entering :: '||gx_package||'.'||x_function);
        p_log(fnd_log.level_statement, 'px_mass_add_rec.mass_addition_id: '||px_mass_add_rec.mass_addition_id);
        
        IF COALESCE(px_mass_add_rec.attribute15,'N') = 'N' THEN
            -- Get the asset_key as this is the rule driver
            v_asset_key        := f_get_asset_key_name   (p_asset_key_ccid => px_mass_add_rec.asset_key_ccid);
            lr_asset := f_get_asset_info (
                p_asset_serialno        => px_mass_add_rec.serial_number
              , p_source_book_type_code => px_mass_add_rec.book_type_code
            );
            
            v_asset_id         := lr_asset.asset_id;
            v_corpbook_retired := (lr_asset.corpbook_retired='Y');
            v_taxbook_retired  := (lr_asset.taxbook_retired='Y');
            
            p_log(fnd_log.level_statement, 'v_asset_key:        '||v_asset_key);
            p_log(fnd_log.level_statement, 'v_asset_id:         '||v_asset_id);
            
            IF v_corpbook_retired THEN
                p_log(fnd_log.level_statement, 'v_corpbook_retired: TRUE');
            END IF;
            IF v_taxbook_retired THEN
                p_log(fnd_log.level_statement, 'v_taxbook_retired: TRUE ');
            END IF;
            
            -- RULE REPLACE
            IF v_asset_key = 'REPLACEMENT' THEN
                p_log(fnd_log.level_statement, 'REPLACEMENT');
                IF v_asset_id IS NULL THEN
                    px_mass_add_rec.queue_name     := 'KEYR';
                    px_mass_add_rec.posting_status := 'KEYR';
                ELSE /* Asset_ID NOT NULL */
                    IF NOT v_corpbook_retired AND NOT v_taxbook_retired THEN
                        px_mass_add_rec.queue_name     := 'REPLACE';
                        px_mass_add_rec.posting_status := 'REPLACE';
                    ELSIF NOT v_corpbook_retired AND v_taxbook_retired THEN
                        px_mass_add_rec.queue_name     := 'TAXRETR';
                        px_mass_add_rec.posting_status := 'TAXRETR';
                    ELSIF v_corpbook_retired AND NOT v_taxbook_retired THEN
                        px_mass_add_rec.queue_name     := 'RETTAX';
                        px_mass_add_rec.posting_status := 'RETTAX';
                    ELSE
                        px_mass_add_rec.queue_name     := 'POST';
                        px_mass_add_rec.posting_status := 'POST';
                    END IF;
                END IF;
            ELSIF v_asset_key = 'UPGRADE' THEN  -- UPGRADE RULE
                p_log(fnd_log.level_statement, 'UPGRADE');
                IF v_asset_id IS NULL THEN
                    px_mass_add_rec.queue_name     := 'KEYU';
                    px_mass_add_rec.posting_status := 'KEYU';
                ELSE /* Asset_ID NOT NULL */
                    -- Only add to asset if px_mass_add_rec.add_to_asset_id is null
                    IF px_mass_add_rec.add_to_asset_id IS NULL THEN
                        px_mass_add_rec.add_to_asset_id := v_asset_id;
                        px_mass_add_rec.amortize_flag   := 'YES';
                    END IF;
            
                    IF NOT v_corpbook_retired AND v_taxbook_retired THEN
                        px_mass_add_rec.queue_name     := 'TAXRETU';
                        px_mass_add_rec.posting_status := 'TAXRETU';
                    ELSIF v_corpbook_retired THEN
                        px_mass_add_rec.queue_name     := 'UPGRADE';
                        px_mass_add_rec.posting_status := 'UPGRADE';
                    ELSIF NOT v_corpbook_retired AND NOT v_taxbook_retired THEN
                        IF NOT f_asset_category_match (
                            p_asset_id          => v_asset_id,
                            p_asset_category_id => px_mass_add_rec.asset_category_id) THEN
                            px_mass_add_rec.queue_name     := 'VACU';
                            px_mass_add_rec.posting_status := 'VACU';
                        ELSIF NOT f_asset_location_match (
                            p_asset_id              => v_asset_id,
                            p_mass_addition_id      => px_mass_add_rec.mass_addition_id,
                            p_source_book_type_code => px_mass_add_rec.book_type_code) THEN
                            px_mass_add_rec.queue_name     := 'VALU';
                            px_mass_add_rec.posting_status := 'VALU';
                        ELSIF NOT f_asset_expcode_match (
                            p_asset_id              => v_asset_id,
                            p_mass_addition_id      => px_mass_add_rec.mass_addition_id,
                            p_source_book_type_code => px_mass_add_rec.book_type_code) THEN
                            px_mass_add_rec.queue_name     := 'VEXU';
                            px_mass_add_rec.posting_status := 'VEXU';
                        ELSE
                            px_mass_add_rec.queue_name     := 'POST';
                            px_mass_add_rec.posting_status := 'POST';
                        END IF;
                    END IF;
                END IF;
            ELSE -- ALL OTHERS
                p_log(fnd_log.level_statement, 'OTHER');
                IF v_asset_id IS NULL THEN
                    px_mass_add_rec.queue_name     := 'POST';
                    px_mass_add_rec.posting_status := 'POST';
                ELSE /* Asset_ID NOT NULL */
                    IF NOT v_corpbook_retired AND v_taxbook_retired THEN
                        px_mass_add_rec.queue_name     := 'TAXRETKEY';
                        px_mass_add_rec.posting_status := 'TAXRETKEY';
                    ELSIF NOT v_corpbook_retired AND NOT v_taxbook_retired THEN
                        px_mass_add_rec.queue_name     := 'SERIAL';
                        px_mass_add_rec.posting_status := 'SERIAL';
                    END IF;
                END IF;
            END IF;
        END IF;
        
        p_log(fnd_log.level_procedure, 'Exiting :: '||gx_package||'.'||x_function);

        RETURN TRUE;
    
    EXCEPTION
        WHEN OTHERS THEN
            p_log(fnd_log.level_exception, 'px_mass_add_rec.mass_addition_id: '||px_mass_add_rec.mass_addition_id);
            p_log(fnd_log.level_exception, 'Exception: '||SQLERRM);
            RAISE;
    
    END;

END fa_massadd_prep_custom_pkg;
