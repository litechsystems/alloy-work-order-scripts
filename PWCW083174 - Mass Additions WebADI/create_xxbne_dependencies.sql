declare
  lr_obj  xxfnd_installer.object_create_record;
  lv_error varchar2(4000);
begin
  lr_obj.object_owner := 'XXPWC';
  lr_obj.object_name  := 'XXBNE_PARAM_LIST_S';
  lr_obj.object_type  := 'SEQUENCE';
  lr_obj.create_ddl   := q'[
create sequence xxpwc.xxbne_param_list_s
start with 1000
nocache]';
  XXFND_INSTALLER.CREATE_IF_REQUIRED(LR_OBJ,lv_error);
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXBNE_PARAM_LIST_S';
  lr_obj.object_type  := 'SYNONYM';
  lr_obj.create_ddl   := 'create synonym apps.xxbne_param_list_s for xxpwc.xxbne_param_list_s';
  xxfnd_installer.create_if_required(lr_obj,lv_error);  
  --
  --
  lr_obj.object_owner := 'XXPWC';
  lr_obj.object_name  := 'XXBNE_ATTRIBUTE_S';
  lr_obj.object_type  := 'SEQUENCE';
  lr_obj.create_ddl   := q'[
create sequence xxpwc.xxbne_attribute_s
start with 1000
nocache]';
  xxfnd_installer.create_if_required(lr_obj,lv_error);
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXBNE_ATTRIBUTE_S';
  lr_obj.object_type  := 'SYNONYM';
  lr_obj.create_ddl   := 'create synonym apps.xxbne_attribute_s for xxpwc.xxbne_attribute_s';
  xxfnd_installer.create_if_required(lr_obj,lv_error);
end;
/

CREATE OR REPLACE VIEW xxbne_webadi_flex_values_v AS
/*
|| WebADI has problems with fields prefixed with a table alias.  Simplifies
|| flex value usage in adi.
*/
SELECT fv.flex_value,
       ft.description,
       fvs.flex_value_set_name
FROM   fnd_flex_value_sets fvs,
       fnd_flex_values fv,
       fnd_flex_values_tl ft
WHERE  fv.flex_value_set_id      = fvs.flex_value_set_id
AND    fv.flex_value_id          = ft.flex_value_id
AND    ft.language               = 'US'
AND    fv.enabled_flag           = 'Y'
AND    SYSDATE BETWEEN nvl(fv.start_date_active,SYSDATE-1)
                   and nvl(fv.end_date_active,sysdate+1);
/

CREATE OR REPLACE VIEW xxbne_webadi_lookup_values_v AS
/*
|| WebADI has problems with fields prefixed with a table alias.  Simplifies
|| lookup value usage in adi.
*/
select fv.lookup_type,
       fv.lookup_code,
       fv.meaning,
       fv.description,
       fv.view_application_id
FROM   fnd_lookup_values_vl fv
WHERE  fv.enabled_flag           = 'Y'
and    sysdate between nvl(fv.start_date_active,sysdate-1)
                   and nvl(fv.end_date_active,sysdate+1)
/

CREATE OR REPLACE VIEW xxbne_webadi_lookups_v AS
/*
|| WebADI has problems with fields prefixed with a table alias.  Simplifies
|| lookup value usage in adi.
*/
select fv.lookup_type,
       fv.lookup_code,
       fv.meaning,
       fv.description
FROM   fnd_lookups fv
WHERE  fv.enabled_flag           = 'Y'
and    sysdate between nvl(fv.start_date_active,sysdate-1)
                   and nvl(fv.end_date_active,sysdate+1)
/

CREATE OR REPLACE VIEW xxbne_fa_lookups_v AS
select fl.lookup_type,
       fl.lookup_code,
       fl.meaning,
       fl.description
FROM   fa_lookups fl
WHERE  fl.enabled_flag           = 'Y'
and    sysdate between nvl(fl.start_date_active,sysdate-1)
                   and nvl(fl.end_date_active,sysdate+1)
/

declare
  LN_APPL_ID number;
  LCV_LUD    varchar2(20) := TO_CHAR(sysdate,'YYYY/MM/DD');
  procedure CREATE_MSG(PV_MSG_NAME in varchar2,
                       PV_MSG_TEXT in varchar2) is
  begin
    fnd_new_messages_pkg.load_row(
      X_APPLICATION_ID   => LN_APPL_ID,
      X_MESSAGE_NAME     => PV_MSG_NAME,
      X_MESSAGE_NUMBER   => null,
      X_MESSAGE_TEXT     => PV_MSG_TEXT,
      X_DESCRIPTION      => NULL,
      X_TYPE             => '30_PCT_EXPANSION_PROMPT',
      X_MAX_LENGTH       => NULL,
      X_CATEGORY         => NULL,
      X_SEVERITY         => NULL,
      X_FND_LOG_SEVERITY => NULL,
      X_OWNER            => 'ORACLE',
      X_CUSTOM_MODE      => 'FORCE',
      X_LAST_UPDATE_DATE => LCV_LUD);
  end create_msg;
                       
begin
  select application_id
  into   ln_appl_id
  from   fnd_application
  where  APPLICATION_SHORT_NAME = 'BNE';

  create_msg('XXBNE_OBJECT_EXC','Exception raised validating '||chr(38)||'OBJECT: '||chr(38)||'EXCEPTION');
end;
/