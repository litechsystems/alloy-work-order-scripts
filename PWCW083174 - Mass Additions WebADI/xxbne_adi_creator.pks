CREATE OR REPLACE PACKAGE xxbne_adi_creator IS
-- +===================================================================================+
-- |   NAME:       XXBNE_ADI_CREATOR                                                   |
-- |                                                                                   |
-- |   PURPOSE:                                                                        |
-- |   Bypass the flakey GUI wizard and create ADIs.                                   |
-- |                                                                                   |
-- |   REVISIONS:                                                                      |
-- |                                                                                   |
-- |   Version   Date          Author           Description                            |
-- |   --------  ------------  ---------------  ------------------------------------   |
-- |   1.0                     S Anderson       Initial Version                        |
-- |   1.1       20-Sep-12     W Soo            Amended to include required hint for   |
-- |                                            mandatory parameters.                  |
-- |   1.2       25-Jan-13     S Anderson       Added intg_prefix as an attribute for  |
-- |                                            integrator_rec                         |
-- |   1.3       05-Dec-13     S Anderson       Added ability to pass parameters for   |
-- |                                            content sql                            |
-- |   1.4       08-Jan-14     S Anderson       Added attributes for function params   |
-- +===================================================================================+
    gcv_assign_default_null  constant varchar2(10) := '#$%NULL#$%';
    
    
    TYPE param_defn_rec IS RECORD(
        appl_id                       bne_param_defns_vl.application_id%type
      , param_defn_code               bne_param_defns_vl.param_defn_code%type
      , object_version_number         bne_param_defns_vl.object_version_number%type
      , param_name                    bne_param_defns_vl.param_name%type
      , param_source                  bne_param_defns_vl.param_source%type
      , param_category                bne_param_defns_vl.param_category%type
      , user_name                     bne_param_defns_vl.user_name%type
      , datatype                      bne_param_defns_vl.datatype%type
      , attribute_app_id              bne_param_defns_vl.attribute_app_id%type
      , attribute_code                bne_param_defns_vl.attribute_code%type
      , param_resolver                bne_param_defns_vl.param_resolver%type
      , default_required_flag         bne_param_defns_vl.default_required_flag%type
      , default_visible_flag          bne_param_defns_vl.default_visible_flag%type
      , default_user_modifyable_flag  bne_param_defns_vl.default_user_modifyable_flag%type
      , default_string                bne_param_defns_vl.default_string%type
      , default_String_trans_flag     bne_param_defns_vl.default_String_trans_flag%type
      , default_date                  bne_param_defns_vl.default_date%type
      , default_number                bne_param_defns_vl.default_number%type
      , default_boolean_flag          bne_param_defns_vl.default_boolean_flag%type
      , default_formula               bne_param_defns_vl.default_formula%type
      , val_type                      bne_param_defns_vl.val_type%type
      , val_value                     bne_param_defns_vl.val_value%type
      , max_size                      bne_param_defns_vl.max_size%type
      , display_type                  bne_param_defns_vl.display_type%type
      , display_style                 bne_param_defns_vl.display_style%type
      , display_size                  bne_param_defns_vl.display_size%type
      , default_desc                  bne_param_defns_vl.default_desc%type
      , prompt_left                   bne_param_defns_vl.prompt_left%type
      , prompt_above                  bne_param_defns_vl.prompt_above%type
      , user_tip                      bne_param_defns_vl.user_tip%type
      , help_url                      bne_param_defns_vl.help_url%type
      , format_mask                   bne_param_defns_vl.format_mask%type
      , access_key                    bne_param_defns_vl.access_key%type
      , oa_flex_application_id        bne_param_defns_vl.oa_flex_application_id%type
      , oa_flex_code                  bne_param_defns_vl.oa_flex_code%type
      , oa_flex_num                   bne_param_defns_vl.oa_flex_num%type
    );
    
    TYPE param_list_item_rec IS RECORD(
--        appl_id               bne_param_list_items.application_id%type
--      , param_list_code       bne_param_list_items.param_list_code%type
        sequence_num          bne_param_list_items.sequence_num%type
      , object_version_number bne_param_list_items.object_version_number%type
      , param_defn            param_defn_rec
      , param_name            bne_param_list_items.param_name%type
      , attribute_app_id      bne_param_list_items.attribute_app_id%type
      , attribute_code        bne_param_list_items.attribute_code%type
      , string_value          bne_param_list_items.string_value%TYPE
      , date_value            bne_param_list_items.date_value%TYPE
      , number_value          bne_param_list_items.number_value%TYPE
      , boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
      , formula_value         bne_param_list_items.formula_value%TYPE
      , desc_value            bne_param_list_items.desc_value%TYPE
    );
    TYPE list_items_table IS TABLE OF param_list_item_rec;

    TYPE param_group_rec IS RECORD (
--        appl_id               bne_param_groups_vl.application_id%type
--      , param_list_code       bne_param_groups_vl.param_list_code%type
        sequence_num          bne_param_groups_vl.sequence_num%type
      , object_version_number bne_param_groups_vl.object_version_number%type
      , attribute_app_id      bne_param_groups_vl.attribute_app_id%type
      , attribute_code        bne_param_groups_vl.attribute_code%type
      , group_resolver        bne_param_groups_vl.group_resolver%type
      , user_name             bne_param_groups_vl.user_name%type
    );
    TYPE list_groups_table IS TABLE OF param_group_rec;
    
    TYPE param_list_rec IS RECORD(
        appl_id               bne_param_lists_vl.application_id%type
      , param_list_code       bne_param_lists_vl.param_list_code%type
      , object_version_number bne_param_lists_vl.object_version_number%type
      , persistent_flag       bne_param_lists_vl.persistent_flag%type
      , comments              bne_param_lists_vl.comments%type
      , attribute_app_id      bne_param_lists_vl.attribute_app_id%type
      , attribute_code        bne_param_lists_vl.attribute_code%type
      , list_resolver         bne_param_lists_vl.list_resolver%type
      , user_tip              bne_param_lists_vl.user_tip%type
      , prompt_left           bne_param_lists_vl.prompt_left%type
      , prompt_above          bne_param_lists_vl.prompt_above%type
      , user_name             bne_param_lists_vl.user_name%type
      , list_items            list_items_table
      , list_groups           list_groups_table
    );
    /*
    || Define type and table to store parameter names for content sql
    */
    subtype simple_query_rec is bne_simple_query%rowtype;
    type content_sql_param_rec is record(
        param_name             bne_param_list_items.param_name%type
      , user_name              bne_param_defns_tl.user_name%TYPE
      , prompt_left            bne_param_defns_tl.prompt_left%TYPE
      , prompt_above           bne_param_defns_tl.prompt_above%TYPE
      , user_tip               bne_param_defns_tl.user_tip%TYPE
      , datatype               varchar2(1)  -- (V)archar2/(D)ate/(N)umber
      , format_mask            bne_param_defns_b.format_mask%TYPE
      , max_size               bne_param_defns_b.max_size%TYPE
      , display_size           bne_param_defns_b.display_size%type
      , display_style          bne_param_defns_b.display_style%type default 1 --??
      , display_type           bne_param_defns_b.display_type%TYPE  -- 3 list box, 4 text field
      , validation_type        varchar2(1) -- (N)one, (V)alue Sets, (F)nd Lookups, (B)ne Query
      , validation_value       bne_param_defns_b.val_value%TYPE  --null, value_set_name, ??, app_id:query_code
      , validation_query       simple_query_rec
      , validation_query_name  varchar2(240)
      , default_required_flag  varchar2(1)
      , default_visible_flag   varchar2(1) default 'Y'
      , default_user_mod_flag  varchar2(1) default 'Y'
      , default_string         bne_param_defns_tl.default_string%type
      , default_desc           bne_param_defns_tl.default_desc%type
      , default_date           bne_param_defns_b.default_date%type
      , default_number         bne_param_defns_b.default_number%type
      , default_boolean        bne_param_defns_b.default_boolean_flag%type
      , default_formula        bne_param_defns_b.default_formula%type
      , reuse_existing_flag    boolean default false   -- set to true if the content sql param is created by a 
    );                                                 -- prior content sql, and you wish to reuse it for this
                                                       -- content sql too.
    type content_sql_param_table is table of content_sql_param_rec index by binary_integer;
    /*
    || Mapping record for content sql
    */
    TYPE update_map_rec IS record(
        mapping_code       bne_mappings_b.mapping_code%TYPE -- update map
      , mapping_name       bne_mappings_tl.user_name%TYPE   -- update map user name
      , content_code       bne_contents_b.content_code%type -- populated by the API.
      , sql_content_code   bne_contents_b.content_code%TYPE -- populated by the API.
      , content_name       bne_contents_tl.user_name%type
      , query_name         bne_queries_tl.user_name%type
      , force_content      boolean default false
      , content_sql        bne_stored_sql.query%type
      , content_where      varchar2(500)
      , param_list         param_list_rec
      , content_sql_params content_sql_param_table
      , content_cols       varchar2(4000)                   -- populated by the API.
    );  
    /*
    || Record type to define the layout attributes
    */
    TYPE layout_attr_rec IS RECORD(
        layout_code         bne_layouts_b.layout_code%TYPE -- layout code (optional)
      , layout_name         bne_layouts_tl.user_name%TYPE  -- layout user name
      , number_of_rows      number default 10
      , update_map          update_map_rec                 -- if this is an updateable layout, populate this
      , updateable_layout   boolean                        -- populated by API based on update_map.content_sql
      , create_header_block boolean                        -- populated by API based on layout columns
                                                           -- (can be manually set)
      , max_display_seq     NUMBER
    );
    TYPE layout_attr_table IS TABLE OF layout_attr_rec INDEX BY BINARY_INTEGER;
    /*
    || Record type to define the integrator
    */
    TYPE integrator_rec IS record(
        created_by_user        fnd_user.user_name%type
      , asn                    fnd_application.application_short_name%type
        -- intg_prefix can be used while developing.  If there is only 1 integrator with this prefix
        -- when the create_integrator routine is called, it will be deleted, an incremental number will
        -- be added to this prefix, and the new use created with that.  This saves the need to bounce
        -- apache (although you'll still need to bounce it for java changes)
      , intg_prefix            varchar2(25)
      , intg_code              bne_integrators_b.integrator_code%type        -- this will have "_INTG" appended to it
      , intg_name              bne_integrators_tl.user_name%type
      , viewer                 varchar2(50)  default '231:EXCEL2010'
      , layouts                layout_attr_table                             -- optional
      , update_map             update_map_rec                                -- optional
      , no_empty_content       boolean default FALSE                         -- set to true if no empty content should be available
      , empty_content_name     varchar2(240)                                 -- only applies to integrators with updateable maps
      , integrator_code        bne_integrators_b.integrator_code%TYPE        -- this is generated by the API
      , create_doc_list        param_list_rec                                -- optional, can be used to copy a create_doc from an existing one
      , appl_id                fnd_application.application_id%type           -- can be left null, api will load from asn
      , user_id                fnd_user.user_id%type                         -- can be left null, api will load from created_by_user
      , force_replace          boolean default false
    );
    /*
    || Record type to define the interface for an integrator.  API Type's do work
    || to create integrators in the tables, but unfortunately they're pretty 
    || flakey in actual use. (created by this or the screens :-) )
    */    
    TYPE interface_rec IS record(
        interface_table_name  bne_attributes.attribute2%TYPE  -- fill in either the 
      , interface_table_owner bne_attributes.attribute2%TYPE  -- table name and owner
      , interface_table_asn   bne_attributes.attribute2%TYPE  -- OR the api details.
      , api_package_name      bne_attributes.attribute2%TYPE  
      , api_procedure_name    bne_attributes.attribute2%TYPE
      , interface_user_name   bne_interfaces_tl.user_name%TYPE
      , api_type              bne_attributes.attribute1%TYPE
      , api_return_type       bne_attributes.attribute3%TYPE
      , interface_code        bne_interfaces_b.interface_code%TYPE
      , appl_id               fnd_application.application_id%TYPE -- can be left null, api will load from integrator_rec.asn
      , user_id               fnd_user.user_id%TYPE             -- can be left null, api will load from integrator_rec.created_by_user
    );
    /*
    || Table of new prompts to display to the user, index by the
    || bne_interface_cols_b.interface_col_name they are for.
    */  
    subtype interface_col_name IS bne_interface_cols_b.interface_col_name%TYPE;
    
    /*
    || Record type to define the download layout attributes for a column
    */
    TYPE layout_col_attr IS RECORD(
        include_in_layout   varchar2(1)
      , read_only_flag      bne_layout_cols.read_only_flag%type default 'N' -- optional
      , default_type        bne_layout_cols.default_type%type               -- optional  
      , default_value       bne_layout_cols.default_value%type              -- optional  
      , display_width       bne_layout_cols.display_width%type              -- optional
        -- set header_field to true if you want the field to appear in the header instead
      , header_field        BOOLEAN default FALSE
      , sql_column          VARCHAR2(50) -- optional 
                                         -- assumed to match interface column name
                                         -- only used for update_layout
      , display_meaning     varchar2(1)  -- optional.  Set to Y if the update_map
                                         -- sql will return the ID but the spreadsheet
                                         -- displays the meaning.
      , internal_block_id   bne_layout_cols.block_id%type  -- handled by the api, do not populate
      , display_seq_num     NUMBER                         -- handled by the api, do not populate
    );
    TYPE layout_col_attr_table IS TABLE OF layout_col_attr INDEX BY BINARY_INTEGER;
    /*
    || Record type and Table of values to update the interface column records with
    || (use to make display only, add groupings for flexfields, etc
    */
    type interface_col_rec is record(
        prompt               bne_interface_cols_tl.prompt_above%TYPE
      , field_size           bne_interface_cols_b.field_size%TYPE
      , display_flag         bne_interface_cols_b.display_flag%TYPE
      , rename_to            bne_interface_cols_b.interface_col_name%TYPE
      , group_name           bne_interface_cols_b.group_name%TYPE
      , oa_concat_flex       bne_interface_cols_b.oa_concat_flex%TYPE
      , val_type             bne_interface_cols_b.val_type%TYPE
      , interface_col_type   bne_interface_cols_b.interface_col_type%TYPE
      , required_flag        bne_interface_cols_b.required_flag%type
      , default_type         bne_interface_cols_b.default_type%TYPE
      , default_value        bne_interface_cols_b.default_value%TYPE
      , user_hint            bne_interface_cols_tl.user_hint%type
      , upload_read_only     boolean default false                  -- Set to TRUE if you need this field uploaded even though it is read only.
      , dummy_column         boolean default FALSE
      , layout_attr          layout_col_attr_table
    );
    
    TYPE interface_col_table IS TABLE OF interface_col_rec INDEX BY interface_col_name;
    
    /*
    || TABLE LOV's are defined here - using the column name as the array index
    */
    TYPE table_lov_rec IS record(
        id_col                bne_interface_cols_b.val_id_col%TYPE
      , mean_col              bne_interface_cols_b.val_mean_col%TYPE
      , desc_col              bne_interface_cols_b.val_desc_col%TYPE
      , table_name            bne_interface_cols_b.val_obj_name%TYPE
      , addl_w_c              bne_interface_cols_b.val_addl_w_c%TYPE
      , window_caption        bne_param_defns_b.default_string%TYPE
      , window_width          bne_param_list_items.string_value%TYPE
      , window_height         bne_param_list_items.string_value%TYPE
      , table_block_size      bne_param_list_items.string_value%TYPE
      , table_sort_order      bne_param_list_items.string_value%type
      , table_column_alias    bne_param_list_items.string_value%type
      , poplist_flag          bne_interface_cols_b.lov_type%TYPE
    );
    TYPE table_lov_table IS TABLE OF table_lov_rec INDEX BY interface_col_name;
    /*
    || JAVA LOV's are defined here - using the column name as the array index
    */
    TYPE java_lov_rec IS record(
        component_class       bne_components_b.component_java_class%TYPE
      , validator_class       bne_interface_cols_b.val_obj_name%TYPE
      , long_list             boolean DEFAULT FALSE
      , window_caption        bne_param_defns_b.default_string%TYPE
      , window_width          bne_param_list_items.string_value%TYPE
      , window_height         bne_param_list_items.string_value%TYPE
      , table_block_size      bne_param_list_items.string_value%TYPE
      , table_columns         bne_param_list_items.string_value%TYPE
      , table_select_columns  bne_param_list_items.string_value%TYPE
      , table_hidden_columns  bne_param_list_items.string_value%TYPE
      , table_column_alias    bne_param_list_items.string_value%TYPE
      , table_headers         bne_param_list_items.string_value%TYPE
      , table_sort_order      bne_param_list_items.string_value%TYPE
      , val_component_app_id  bne_interface_cols_b.val_component_app_id%TYPE
      , val_component_code    bne_interface_cols_b.val_component_code%TYPE
    );
    TYPE java_lov_table IS TABLE OF java_lov_rec INDEX BY interface_col_name;
    /*
    || KFF LOV's are defined here - using the column name as the array index
    */
    TYPE kff_lov_rec IS record(
        group_name              bne_interface_cols_b.group_name%TYPE
      , oa_flex_code            bne_interface_cols_b.oa_flex_code%TYPE
      , oa_flex_application_id  bne_interface_cols_b.oa_flex_application_id%TYPE
      , oa_flex_num             bne_interface_cols_b.oa_flex_num%TYPE
      , column_prefix           varchar2(10)
    );
    TYPE kff_lov_table IS TABLE OF kff_lov_rec INDEX BY interface_col_name;
    /*
    || DFF LOV's are defined here - using the column name as the array index
    */
    TYPE dff_lov_rec IS record(
        flex_seg_col_name_prefix  VARCHAR2(30)
      , group_name                VARCHAR2(100)
      , flex_application_id       NUMBER
      , flex_code                 VARCHAR2(30)
      , context_col_name          VARCHAR2(30)
      , required_flag             VARCHAR2(30)
      , vrule                     VARCHAR2(200)
      , effective_date_col        varchar2(30)
      , prompt_above              VARCHAR2(240)
      , prompt_left               VARCHAR2(240)
      , user_hint                 VARCHAR2(240)
    );
    TYPE dff_lov_table IS TABLE OF dff_lov_rec INDEX BY binary_integer;
    /*
    || Calendar LOV's headings - using the column name as the array index
    */
    TYPE calendar_headings_table IS TABLE OF bne_param_defns_b.default_string%TYPE INDEX BY interface_col_name;
    /*
    || Dummy Columns (for use with Flexfields) - using the column name as the array index
    */
    TYPE dummy_cols_table IS TABLE OF bne_interface_cols_vl%rowtype INDEX BY binary_integer;
    /*
    || Display Sequence for ADI Columns. Add columns into the array in the order 
    || you want them to appear.
    */  
    TYPE display_seq_table IS TABLE OF bne_interface_cols_b.interface_col_name%TYPE INDEX BY binary_integer;
    /*
    || Default values for concurrent program parameters
    */
    TYPE conc_prog_defaults_rec IS record(
        default_value           bne_attributes.attribute5%TYPE  -- do not include enclosing quotes.
      , import_column           bne_attributes.attribute8%TYPE  -- can be created by pre-import rule.
    );
    TYPE conc_param_dflts_table IS TABLE OF conc_prog_defaults_rec INDEX BY binary_integer;
    /*
    || Columns Queried and Bind Variables used all need a record in the 
    || sql_statement_col_table (for the pre import rule)
    */
    TYPE sql_statement_col_rec IS record(
        sql_col_name            bne_param_list_items.param_name%TYPE
      , sql_col_type            bne_attributes.attribute1%TYPE      -- BIND/RETURN
      , adi_reference_name      bne_attributes.attribute2%TYPE      
      , import_source           bne_attributes.attribute4%TYPE      -- (For BIND)
      , data_type               bne_attributes.attribute5%TYPE      -- (For BIND)
      , attribute_code          bne_attributes.attribute_code%TYPE  -- populated within the package
    );
    TYPE sql_statement_col_table IS TABLE OF sql_statement_col_rec INDEX BY binary_integer;
    /*
    || Pre-import sql statements 
    */
    TYPE pre_import_rule_rec IS record(
        rule_name               bne_attributes.attribute5%TYPE   -- do not include enclosing quotes.
      , rule_desc               bne_param_lists_vl.comments%TYPE  -- do not include enclosing quotes.
      , rule_type               bne_attributes.attribute8%TYPE
      , sql_statement           bne_param_list_items.string_value%TYPE
      , sql_statement_cols      sql_statement_col_table          -- array index needs to match column select list order  
    );
    TYPE pre_import_rule_table IS TABLE OF pre_import_rule_rec INDEX BY binary_integer;
    
    subtype group_field IS bne_param_list_items.param_name%TYPE;
    TYPE group_fields_table IS TABLE OF group_field INDEX BY binary_integer;
    
    TYPE conc_prog_rec IS record(
        appl_id                 fnd_application.application_id%TYPE         -- populated within the package
      , asn                     fnd_application.application_short_name%TYPE -- asn for param lists
      , program_asn             fnd_application.application_short_name%TYPE -- asn for conc program to submit
      , concurrent_program_name fnd_concurrent_programs.concurrent_program_name%TYPE
      , user_conc_prog_name     fnd_concurrent_programs_vl.user_concurrent_program_name%TYPE -- populated within the package
      , program_appl_id         fnd_concurrent_programs.application_id%TYPE -- populated within the package,
      , param_list_code         bne_param_lists_b.param_list_code%TYPE      -- name to call the main import parameter list
                                                                            -- created in code
      , group_fields            group_fields_table                          -- cannot be interface_col_type 2
        -- Conc Prog submitted for each set of records where this set of fields match.
      , pre_import_rules        pre_import_rule_table
      , conc_param_dflts        conc_param_dflts_table
    );
    
    TYPE param_rec IS record(
        param_name              bne_attributes.attribute1%TYPE
      , param_type              bne_attributes.attribute2%TYPE  -- NUMBER/VARCHAR/VARCHAR2/etc.
      , direction               bne_attributes.attribute3%TYPE  -- IN/OUT (api param), BIND/RETURN (sql param)
      , source_type             bne_attributes.attribute7%TYPE  -- IMPORT/??
      , source_field            bne_attributes.attribute8%TYPE  -- eg. for Import source, XXBG_MASS_TIME_ENTRY_INTERFACE.RUN_ID
    );
    TYPE param_table IS TABLE OF param_rec INDEX BY binary_integer;
    
    TYPE plsql_api_rec IS record(
        param_list_code         bne_param_lists_b.param_list_code%TYPE -- name to call the main import parameter list
                                                                        -- created in code
      , procedure_name          bne_param_list_items.param_name%TYPE -- full pl/sql package and procedure name
      , importer_name           bne_param_lists_tl.user_name%TYPE
      , group_fields            group_fields_table  -- cannot be interface_col_type 2
          -- Conc Prog submitted for each set of records where this set of fields match.
          -- Field name must include the interface name 
          -- (eg. XXBG_MASS_TIME_ENTRY_INTERFACE.RUN_ID)
      , unique_row_fields       group_fields_table
          -- cannot be interface_col_type 2
          -- unique key combination of fields in interface.
      , procedure_params        param_table
      , errored_rows_sql        bne_param_list_items.string_value%TYPE
          -- Eg. SELECT * 
               --FROM   XXBG_MASS_TIME_ENTRY_INTERFACE 
               --WHERE  RESULT_CODE = 'E' 
               --AND    RUN_ID = $PARAM$.RUN_ID_DEV_SEQ 
      , er_sql_params           param_table
         -- Eg. RUN_ID_DEV_SEQ, IMPORT, XXBG_MASS_TIME_ENTRY_INTERFACE.RUN_ID, NUMBER
      , error_lkup_sql          bne_param_list_items.string_value%TYPE
          -- Eg. SELECT $PARAM$.MESSAGE ERROR_MESSAGE
               --FROM   DUAL
      , el_sql_params           param_table
         -- Eg. RETURN  ERROR_MESSAGE                                                         VARCHAR2
              --BIND    MESSAGE       INTERFACE_TABLE XXBG_MASS_TIME_ENTRY_INTERFACE.MESSAGE  VARCHAR2
    );
    
    -- used internally
    function imp_param_and_imp_not_reqd(
        pv_param_name         in bne_param_defns_b.param_name%type
      , pv_conc_prog          in fnd_concurrent_programs.concurrent_program_name%type
      , pv_plsql_api          in bne_param_list_items.param_name%type
    ) return varchar2;
    
    PROCEDURE copy_dummy_cols_to_intf(
        par_intf_cols   IN out nocopy interface_col_table
      , par_dummy_cols  IN out nocopy dummy_cols_table
    );
    
    PROCEDURE create_integrator(
        pr_intg         IN out nocopy integrator_rec
      , pr_intf         IN out nocopy interface_rec
      , pr_conc_prog    IN out nocopy conc_prog_rec
      , pr_plsql_api    IN out nocopy plsql_api_rec
      , par_intf_cols   IN out nocopy interface_col_table
      , par_table_lovs  IN out nocopy table_lov_table
      , par_java_lovs   IN out nocopy java_lov_table
      , par_kff_lovs    IN out nocopy kff_lov_table
      , par_dff_lovs    IN out nocopy dff_lov_table
      , pav_clndr_cols  IN out nocopy calendar_headings_table
      , par_dummy_cols  IN out nocopy dummy_cols_table
      , par_display_seq IN out nocopy display_seq_table
    );
    
    PROCEDURE create_integrator(
        pr_intg         IN out nocopy integrator_rec
      , pr_intf         IN out nocopy interface_rec
      , pr_conc_prog    IN out nocopy conc_prog_rec
      , par_intf_cols   IN out nocopy interface_col_table
      , par_table_lovs  IN out nocopy table_lov_table
      , par_java_lovs   IN out nocopy java_lov_table
      , par_kff_lovs    IN out nocopy kff_lov_table
      , par_dff_lovs    IN out nocopy dff_lov_table
      , pav_clndr_cols  IN out nocopy calendar_headings_table
      , par_dummy_cols  IN out nocopy dummy_cols_table
      , par_display_seq IN out nocopy display_seq_table
    );
    
    PROCEDURE create_integrator(
        pr_intg         IN out nocopy integrator_rec
      , pr_intf         IN out nocopy interface_rec
      , par_intf_cols   IN out nocopy interface_col_table
      , par_table_lovs  IN out nocopy table_lov_table
      , par_java_lovs   IN out nocopy java_lov_table
      , par_kff_lovs    IN out nocopy kff_lov_table
      , par_dff_lovs    IN out nocopy dff_lov_table
      , pav_clndr_cols  IN out nocopy calendar_headings_table
      , par_dummy_cols  IN out nocopy dummy_cols_table
      , par_display_seq IN out nocopy display_seq_table
    );
    
    PROCEDURE delete_integrator(
        pr_intg IN out nocopy integrator_rec
    );
    
    TYPE int_menu_rec IS record(
        application_id   bne_integrators_b.application_id%TYPE
      , code             bne_integrators_b.integrator_code%TYPE
    );
    TYPE integrator_table IS TABLE OF int_menu_rec INDEX BY binary_integer;
    
    PROCEDURE create_adi_menu(
        pv_menu_name      IN VARCHAR2
      , pv_user_menu_name IN VARCHAR2
      , par_integrators   IN out nocopy integrator_table
    );
END xxbne_adi_creator;
/
