declare
    ln_ma_id number;
    cursor ma_records is
        select fa.*
        from   fa_mass_Additions_steve fa
             , fa_massadd_dist_steve fd
        where  fa.mass_addition_id = fd.mass_addition_id(+)
        and    (select count(*) from FA_MASSADD_DIST_STEVE fd2 where fd2.mass_addition_id = fa.mass_addition_id) <= 1;
    subtype ma_rec is fa_mass_additions%rowtype;
    type ma_tbl is table of ma_rec index by binary_integer;
    lar_ma ma_tbl;
    li_idx integer;
    
    function get_ma_id
    return number 
    is
        ln_return_val number;
    begin
        select fa_mass_additions_s.nextval
        into   ln_return_val
        from   dual;
        return(ln_return_val);
    end get_ma_id;
begin
    dbms_output.enable(null);
    open ma_records;
    fetch ma_records bulk collect into lar_ma;
    close ma_records;
    
    li_idx := lar_ma.first;
    while li_idx is not null loop
        -- Backup the prod mass addition id in the vendor number field
        lar_ma(li_idx).vendor_number := lar_ma(li_idx).mass_addition_id;
        
        -- put a new mass addition id in the recrod
        lar_ma(li_idx).mass_addition_id := get_ma_id;
        dbms_output.put_line('lar_ma('||li_idx||').vendor_number = '||lar_ma(li_idx).vendor_number);
        dbms_output.put_line('lar_ma('||li_idx||').mass_addition_id = '||lar_ma(li_idx).mass_addition_id);
        li_idx := lar_ma.next(li_idx);
    end loop;
    
    -- load the records into fa mass additions
    forall i in indices of lar_ma 
        insert into fa_mass_additions
        values lar_ma(i);
    
    -- fix the parent mass addition id values
    update fa_mass_additions ma
    set    ma.parent_mass_Addition_id = (
             select ma2.mass_Addition_id 
             from   fa_mass_Additions ma2 
             where  ma2.vendor_number = ma.parent_mass_addition_id
           )
         , ma.merge_parent_mass_Additions_id = (
             select ma2.mass_Addition_id 
             from   fa_mass_Additions ma2 
             where  ma2.vendor_number = ma.merge_parent_mass_Additions_id
           )
    where  ma.parent_mass_Addition_id is not null;
end;
/

declare
    cursor ma_Recs is
        select mass_addition_id, vendor_number old_ma_id
        from   fa_mass_additions
        where vendor_number is not null
        for update of vendor_number;
    subtype md_rec is fa_massadd_distributions%rowtype;
    lr_md md_rec;
begin
    -- loop through all the mass additions distributions records created by the insert above
    for datarec in ma_recs loop
        begin
            -- load the values from the production distribution record
            select *
            into   lr_md 
            from   FA_MASSADD_DIST_STEVE
            where  mass_addition_id = datarec.old_ma_id;
            
            -- Update the new distribution record with the values from production
            update fa_massadd_distributions 
            set    units = lr_md.units
                 , deprn_expense_ccid = lr_md.deprn_expense_ccid
                 , location_id = lr_md.location_id
                 , employee_id = lr_md.employee_id
            where  mass_addition_id = datarec.mass_addition_id;
            
            -- clear out the old mass additions id from the vendor number field.
            update fa_mass_additions
            set    vendor_number = null
            where current of ma_Recs;
        exception
            when no_data_found then
                --ignore
                null;
        end;
    end loop;
end;
/
