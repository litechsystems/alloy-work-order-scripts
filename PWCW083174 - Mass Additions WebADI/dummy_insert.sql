select * from fa_mass_additions
/
create table fa_mass_additions_steve as select * from fa_mass_additions where 1 = 2
/
create table FA_MASSADD_DIST_STEVE as select * from FA_MASSADD_DISTRIBUTIONS where 1 = 2
/

delete from fa_mass_additions
where (nvl(mass_addition_id,-1), nvl(serial_number,'X')) in (select mass_addition_id, serial_number from fa_mass_Additions_steve)
/

select * from fa_mass_Additions_Steve
where vendor_number is not null
/
declare
    ln_ma_id number;
    cursor ma_records is
        select fa.*
        from   fa_mass_Additions_steve fa
             , fa_massadd_dist_steve fd
        where  fa.mass_addition_id = fd.mass_addition_id(+)
        and    (select count(*) from FA_MASSADD_DIST_STEVE fd2 where fd2.mass_addition_id = fa.mass_addition_id) <= 1;
    subtype ma_rec is fa_mass_additions%rowtype;
    type ma_tbl is table of ma_rec index by binary_integer;
    lar_ma ma_tbl;
    li_idx integer;
    
    function get_ma_id
    return number 
    is
        ln_return_val number;
    begin
        select fa_mass_additions_s.nextval
        into   ln_return_val
        from   dual;
        return(ln_return_val);
    end get_ma_id;
begin
    dbms_output.enable(null);
    open ma_records;
    fetch ma_records bulk collect into lar_ma;
    close ma_records;
    
    li_idx := lar_ma.first;
    while li_idx is not null loop
        lar_ma(li_idx).vendor_number := lar_ma(li_idx).mass_addition_id;
        lar_ma(li_idx).mass_addition_id := get_ma_id;
        dbms_output.put_line('lar_ma('||li_idx||').vendor_number = '||lar_ma(li_idx).vendor_number);
        dbms_output.put_line('lar_ma('||li_idx||').mass_addition_id = '||lar_ma(li_idx).mass_addition_id);
        li_idx := lar_ma.next(li_idx);
    end loop;
    
    forall i in indices of lar_ma 
        insert into fa_mass_additions
        values lar_ma(i);
    
    update fa_mass_additions ma
    set    ma.parent_mass_Addition_id = (
             select ma2.mass_Addition_id 
             from   fa_mass_Additions ma2 
             where  ma2.vendor_number = ma.parent_mass_addition_id
           )
         , ma.merge_parent_mass_Additions_id = (
             select ma2.mass_Addition_id 
             from   fa_mass_Additions ma2 
             where  ma2.vendor_number = ma.merge_parent_mass_Additions_id
           )
    where  ma.parent_mass_Addition_id is not null;
end;
/

declare
    cursor ma_Recs is
        select mass_addition_id, vendor_number old_ma_id
        from   fa_mass_additions
        where vendor_number is not null
        for update of vendor_number;
    subtype md_rec is fa_massadd_distributions%rowtype;
    lr_md md_rec;
begin
    for datarec in ma_recs loop
        begin
            select *
            into   lr_md 
            from   FA_MASSADD_DIST_STEVE
            where  mass_addition_id = datarec.old_ma_id;
            
            update fa_massadd_distributions 
            set    units = lr_md.units
                 , deprn_expense_ccid = lr_md.deprn_expense_ccid
                 , location_id = lr_md.location_id
                 , employee_id = lr_md.employee_id
            where  mass_addition_id = datarec.mass_addition_id;
            
            update fa_mass_additions
            set    vendor_number = null
            where current of ma_Recs;
        exception
            when no_data_found then
                --ignore
                null;
        end;
    end loop;
end;
/
    for datarec in (
        select fa.rowid, fd.rowid
        from   fa_mass_Additions_Steve fa
             , fa_massadd_dist_steve fd
        where  fa.mass_addition_id = fd.mass_addition_id(+)
        and    (select count(*) from FA_MASSADD_DIST_STEVE fd2 where fd2.mass_addition_id = fa.mass_addition_id) <= 1
    ) loop
        insert into fa_mass_additions
        (select *
    end loop;
end;
/
select fa_mass_Additions_s.nextval from dual
/

select * from fa_mass_additions where vendor_number is not null
/

insert into fa_massadd_distributions


select max(mass_addition_id) from fa_mass_additions
/

1053650

        select fa_mass_additions_s.nextval
        from   dual;
/
select *
from   fa_mass_additions
where mass_Addition_id > 1053650
/
select * from fa_massadd_distributions
where mass_addition_id = 1056241
/
select * from FA_MASSADD_DIST_STEVE
/
select * from fa_mass_additions where vendor_number = 1051923
/
select count(*) from fa_mass_additions where mass_Addition_id > 1053650
/
        select count(*)
        from   fa_mass_Additions_steve fa
             , fa_massadd_dist_steve fd
        where  fa.mass_addition_id = fd.mass_addition_id(+)
        and    (select count(*) from FA_MASSADD_DIST_STEVE fd2 where fd2.mass_addition_id = fa.mass_addition_id) <= 1
/
delete from fa_mass_additions
where mass_Addition_id > 1053650
/
delete from fa_massadd_distributions
where mass_Addition_id > 1053650
/
select *
from xxfa_mass_additions_adi_v
where serial_number = 'MX5073182'
/
select * from fa_mass_additions_Steve where serial_number = 'MX5073182'
/
select * from fa_mass_additions where serial_number = 'MX5073182'
