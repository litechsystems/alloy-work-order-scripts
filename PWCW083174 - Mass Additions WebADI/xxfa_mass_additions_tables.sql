SET serveroutput ON SIZE 1000000
set linesize 254

declare
  lr_obj  xxfnd_installer.object_create_record;
  lv_error varchar2(4000);
begin
  lr_obj.object_owner := 'XXPWC';
  lr_obj.object_name  := 'XXBNE_ASSET_ADI_RUNS_S';
  lr_obj.object_type  := 'SEQUENCE';
  lr_obj.create_ddl   := q'[
create sequence xxpwc.xxbne_asset_adi_runs_s
nocache]';
  XXFND_INSTALLER.CREATE_IF_REQUIRED(LR_OBJ,lv_error);
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXBNE_ASSET_ADI_RUNS_S';
  lr_obj.object_type  := 'SYNONYM';
  lr_obj.create_ddl   := 'create synonym apps.xxbne_asset_adi_runs_s for xxpwc.xxbne_asset_adi_runs_s';
  xxfnd_installer.create_if_required(lr_obj,lv_error);  
end;
/

create or replace view xxbne_fa_methods as
select method_id
     , method_code||' ('||life_in_months||' months)' method
     , name
from fa_methods fm
order by 2
/

create or replace view xxfa_mass_additions_adi_v as
select ma.book_type_code
     , ma.mass_addition_id
     , ma.serial_number
     , ma.tag_number
     , pp.segment1 project_number
     , pt.task_number
     , case 
       when ma.posting_status in ('SPLIT','MERGED') then ma.posting_status
       else ma.queue_name
       end queue_name
     , ma.posting_status
     , ma.description
     , ma.fixed_assets_units
     , case 
       when ma.parent_mass_addition_id is null 
       then
          nvl( 
             (select ma2.fixed_assets_cost
              from   fa_mass_additions ma2
              where  ma2.parent_mass_addition_id = ma.mass_addition_id
              and    rownum = 1)
            , ma.fixed_assets_cost
          )
       else
           ma.fixed_assets_cost
       end fixed_assets_cost
     , ma.asset_category_id
     , fc.segment1 asset_category_id_segment1
     , fc.segment2 asset_category_id_segment2
     , fc.segment3 asset_category_id_segment3
     , ma.feeder_system_name
     , fd.deprn_expense_ccid
     , gcc1.segment1 deprn_expense_ccid_segment1
     , gcc1.segment2 deprn_expense_ccid_segment2
     , gcc1.segment3 deprn_expense_ccid_segment3
     , gcc1.segment4 deprn_expense_ccid_segment4
     , gcc1.segment5 deprn_expense_ccid_segment5
     , ma.payables_code_combination_id payables_ccid
     , gcc2.segment1 payables_ccid_segment1
     , gcc2.segment2 payables_ccid_segment2
     , gcc2.segment3 payables_ccid_segment3
     , gcc2.segment4 payables_ccid_segment4
     , gcc2.segment5 payables_ccid_segment5
     , ma.date_placed_in_service
     , ma.asset_key_ccid
     , fk.segment1 asset_key_ccid_segment1
     , ma.asset_type
     , case 
       when ma.life_in_months is null and 
           ltrim(rtrim(translate(fc.segment3,'01234567890','          '))) is null 
       then
           to_number(fc.segment3)
       else
           floor(ma.life_in_months / 12)
       end life_in_years
     , case 
       when ma.life_in_months is null and 
           ltrim(rtrim(translate(fc.segment3,'01234567890','          '))) is null 
       then
           0
       else
           ma.life_in_months - (floor(ma.life_in_months / 12) * 12)
       end life_in_months
     , coalesce(fl.location_id, fl2.location_id) location_id
     , case when fl.location_id is not null then fl.segment1 else fl2.segment1 end location_id_segment1
     , case when fl.location_id is not null then fl.segment2 else fl2.segment2 end location_id_segment2
     , case when fl.location_id is not null then fl.segment3 else fl2.segment3 end location_id_segment3
     , ma.add_to_asset_id
     , CAST(
           fa_massadd_prep_custom_pkg.f_compare_to_asset (
               p_asset_serialno        => ma.serial_number
             , p_source_book_type_code => ma.book_type_code
           ) AS VARCHAR2(15)
       ) compare_to_asset
     , ma.attribute15 skip_custom_processing
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'MAXIMO_ASSET_NAME')       c_maximo_asset_name
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'MAXIMO_NUMBER')           c_maximo_number
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'ASSET_DESCRIPTION')       c_asset_description
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'ASSET_REGION')            c_asset_region
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'ASSET_SUBURB')            c_asset_suburb
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'SITE_LOCATION')           c_site_location
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'MAJOR_CATEGORY')          c_major_category
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'MINOR_CATEGORY')          c_minor_category
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'CATEGORY_LIFE')           c_category_life
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'QUANTITY')                c_quantity
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'UNIT_TYPE')               c_unit_type
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'EXPENSE_ACCOUNT')         c_expense_account
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'COST_ACCOUNT')            c_cost_account
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'GL_YEAR')                 c_gl_year
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'DATE_IN_SERVICE')         c_date_in_service
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'DATE_RETIRED')            c_date_retired
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'LIFE_IN_MONTHS')          c_life_in_months
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'LIFE_IN_YEARS')           c_life_in_years
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'REMAINING_LIFE')          c_remaining_life
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'PROJECT_NUMBER')          c_category_project_number
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'SRCLINE_PROJECT_NUMBERS') c_srcline_project_numbers
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'TOTAL_REVALUATION_RSRVE') c_revaluation_reserve
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'TOTAL_NET_BOOK_VALUE')    c_net_book_value
     , xxfas600_extras.get_adi_field(ma.book_type_code, fa_massadd_prep_custom_pkg.f_compare_to_asset_id(ma.serial_number, ma.book_type_code), 'RETIRED')                 c_retired
     , null run_id
     , 'N' status
     , null error_description
from   fa_mass_additions ma
     , fa_categories_b fc
     , fa_massadd_distributions fd
     , gl_code_combinations gcc1
     , gl_code_combinations gcc2
     , fa_asset_keywords fk
     , pa_projects_all pp
     , pa_tasks pt
     , fa_locations fl
     , fa_locations fl2
     , fa_book_controls fbc
where  ma.asset_category_id = fc.category_id(+)
and    ma.mass_addition_id = fd.mass_addition_id(+)
and    (select count(*) from fa_massadd_distributions fd2 where fd2.mass_addition_id = ma.mass_addition_id) <= 1
and    fd.deprn_expense_ccid = gcc1.code_combination_id(+)
and    ma.payables_code_combination_id = gcc2.code_combination_id(+)
and    ma.asset_key_ccid = fk.code_combination_id(+)
and    ma.project_id = pp.project_id(+)
and    ma.task_id = pt.task_id(+)
and    ma.location_id = fl.location_id(+)
and    fd.location_id = fl2.location_id(+)
and    ma.book_type_code = fbc.book_type_code
and    sysdate <= coalesce( fbc.date_ineffective,sysdate+1)
and    ma.parent_mass_addition_id is null
and    ma.queue_name not in ('POSTED','DELETED')
/

declare
  lr_obj  xxfnd_installer.object_create_record;
  lv_error varchar2(4000);
begin
  lr_obj.object_owner := 'XXPWC';
  lr_obj.object_name  := 'XXFA_MASS_ADDITIONS_ADI';
  lr_obj.object_type  := 'TABLE';
  lr_obj.create_ddl   := q'[
create table xxpwc.xxfa_mass_additions_adi (
  run_id                       number,
  status                       varchar2(2),
  error_description            varchar2(2000),
  book_type_code               varchar2(15),
  mass_addition_id             number,
  serial_number                varchar2(350),
  tag_number                   varchar2(15),
  project_number               varchar2(25),
  project_id                   number,
  task_number                  varchar2(25),
  task_id                      number,
  queue_name                   varchar2(15),
  posting_status               varchar2(15),
  description                  varchar2(80),
  fixed_assets_units           number,
  fixed_assets_cost            number,
  asset_category_id            number,
  deprn_expense_ccid           number,         -- expense account
  payables_ccid                number,
  date_placed_in_service       date,
  asset_key_ccid               number,
  location_id                  number,
  add_to_asset_id              number,
  compare_to_asset             varchar2(15),
  skip_custom_processing       varchar2(1)
)]';
  XXFND_INSTALLER.CREATE_ALWAYS(LR_OBJ,lv_error);
  --
  lr_obj.object_owner := 'APPS';
  lr_obj.object_name  := 'XXFA_MASS_ADDITIONS_ADI';
  lr_obj.object_type  := 'SYNONYM';
  lr_obj.create_ddl   := 'create synonym apps.xxfa_mass_additions_adi for xxpwc.xxfa_mass_additions_adi';
  xxfnd_installer.create_if_required(lr_obj,lv_error);  
end;
/