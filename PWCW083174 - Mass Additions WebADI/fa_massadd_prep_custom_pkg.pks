create or replace package FA_MASSADD_PREP_CUSTOM_PKG as
    /* $Header: FAMAPREPCS.pls 120.2.12010000.2 2009/07/19 09:44:58 glchen ship $ */
    -- +============================================================================+
    -- | Change Record:
    -- | ===============
    -- |
    -- | Version  Date         Author           Comments
    -- | =======  ===========  ===============  ====================================+
    -- | 1.00     03-Oct-2018  Stephen Anderson Created from original
    -- +============================================================================+



    -- Purpose :

    -- Public type declarations

    -- Public constant declarations

    -- Public variable declarations

    -- Public function and procedure declarations
    FUNCTION prepare_attributes(
        px_mass_add_rec IN OUT NOCOPY FA_MASSADD_PREPARE_PKG.mass_add_rec
      , p_log_level_rec IN     FA_API_TYPES.log_level_rec_type default null
    ) RETURN BOOLEAN;

    FUNCTION f_asset_retired (
        p_asset_id              IN NUMBER
      , p_source_book_type_code IN VARCHAR2
      , p_target_book_class     IN VARCHAR2
    ) RETURN VARCHAR2;

    FUNCTION f_compare_to_asset (
        p_asset_serialno        IN VARCHAR2
      , p_source_book_type_code IN VARCHAR2
    ) RETURN NUMBER;
    FUNCTION f_compare_to_asset_id(
        p_asset_serialno        IN VARCHAR2
      , p_source_book_type_code IN VARCHAR2
    ) RETURN NUMBER;
end FA_MASSADD_PREP_CUSTOM_PKG;
/
