select * from fa_mass_additions
where mass_addition_id is null
/
select * from fa_massadd_distributions
/
select * from FA_CATEGORIES_B_KFV
/
SELECT B.ROWID ROW_ID, B.CATEGORY_ID, B.SUMMARY_FLAG, B.ENABLED_FLAG, B.OWNED_LEASED, B.LAST_UPDATE_DATE, B.LAST_UPDATED_BY, B.CATEGORY_TYPE, B.CAPITALIZE_FLAG, T.DESCRIPTION, B.SEGMENT1, B.SEGMENT2, B.SEGMENT3, B.SEGMENT4, B.SEGMENT5, B.SEGMENT6, B.SEGMENT7, B.START_DATE_ACTIVE, B.END_DATE_ACTIVE, B.PROPERTY_TYPE_CODE, B.PROPERTY_1245_1250_CODE, B.DATE_INEFFECTIVE, B.CREATED_BY, B.CREATION_DATE, B.LAST_UPDATE_LOGIN, B.ATTRIBUTE1, B.ATTRIBUTE2, B.ATTRIBUTE3, B.ATTRIBUTE4, B.ATTRIBUTE5, B.ATTRIBUTE6, B.ATTRIBUTE7, B.ATTRIBUTE8, B.ATTRIBUTE9, B.ATTRIBUTE10, B.ATTRIBUTE11, B.ATTRIBUTE12, B.ATTRIBUTE13, B.ATTRIBUTE14, B.ATTRIBUTE15, B.ATTRIBUTE_CATEGORY_CODE, B.PRODUCTION_CAPACITY, B.GLOBAL_ATTRIBUTE1, B.GLOBAL_ATTRIBUTE2, B.GLOBAL_ATTRIBUTE3, B.GLOBAL_ATTRIBUTE4, B.GLOBAL_ATTRIBUTE5, B.GLOBAL_ATTRIBUTE6, B.GLOBAL_ATTRIBUTE7, B.GLOBAL_ATTRIBUTE8, B.GLOBAL_ATTRIBUTE9, B.GLOBAL_ATTRIBUTE10, B.GLOBAL_ATTRIBUTE11, B.GLOBAL_ATTRIBUTE12, B.GLOBAL_ATTRIBUTE13, B.GLOBAL_ATTRIBUTE14, B.GLOBAL_ATTRIBUTE15, B.GLOBAL_ATTRIBUTE16, B.GLOBAL_ATTRIBUTE17, B.GLOBAL_ATTRIBUTE18, B.GLOBAL_ATTRIBUTE19, B.GLOBAL_ATTRIBUTE20, B.GLOBAL_ATTRIBUTE_CATEGORY, B.INVENTORIAL from FA_CATEGORIES_TL T, FA_CATEGORIES_B B WHERE B.CATEGORY_ID = T.CATEGORY_ID and T.LANGUAGE = userenv('LANG')
/
select * from fnd_id_flex_segments
where id_flex_code = 'GL#'
/
select * from gl_ledgers
/
select * from fa_additions
/
select concatenated_segments, code_combination_id
from   gl_code_combinations_kfv
where  code_combination_id in (
            select payables_code_combination_id from fa_mass_additions
       )
/
select * from fnd_id_flexs
where application_table_name = 'FA_ASSET_KEYWORDS'
/
select distinct depreciate_flag from fa_mass_additions
/
select * from fnd_id_flexs
where application_id = 140
and id_flex_Code = 'KEY#'
/
select * from fnd_id_flex_structures
where application_id = 140
and id_flex_Code = 'KEY#'
/
select * from fnd_id_flex_segments
where application_id = 101
and id_flex_Code = 'GL#'
/
select * from fnd_application_vl where application_short_name like 'XX%'
/
select * from xxbne_webadi_lookup_values_v
where lookup_type like 'YES/NO'
and view_application_id = 101
/
select x.book_type_code, x.asset_number, x.serial_number, x.project_id, x.task_id, x.queue_name, x.description
, x.fixed_assets_units, x.fixed_assets_cost, x.asset_category_id, fc.segment1   asset_category_id_segment1
, fc.segment2 asset_category_id_segment2, fc.segment3 asset_category_id_segment3, x.feeder_system_name, fd.deprn_expense_ccid
, gcc1.segment1 deprn_expense_ccid_segment1, gcc1.segment2 deprn_expense_ccid_segment2
, gcc1.segment3 deprn_expense_ccid_segment3, gcc1.segment4 deprn_expense_ccid_segment4
, gcc1.segment5 deprn_expense_ccid_segment5, x.payables_code_combination_id payables_ccid, gcc2.segment1 payables_ccid_segment1
, gcc2.segment2  payables_ccid_segment2, gcc2.segment3 payables_ccid_segment3, gcc2.segment4 payables_ccid_segment4
, gcc2.segment5  payables_ccid_segment5, x.date_placed_in_service, x.asset_key_ccid, fk.segment1 asset_key_ccid_segment1
, x.asset_type, x.depreciate_flag, x.deprn_method_code, floor(x.life_in_months / 12) life_in_years
, x.life_in_months - (floor(x.life_in_months / 12) * 12) life_in_months, x.attribute15 skip_custom_processing
, null run_id, 'N' status, null error_description
from   fa_mass_additions x, fa_categories_b fc, fa_massadd_distributions fd, gl_code_combinations gcc1
, gl_code_combinations gcc2, fa_asset_keywords fk
where  x.asset_category_id = fc.category_id
and    x.mass_addition_id = fd.mass_addition_id
and    (select count(*) from fa_massadd_distributions fd2 where fd2.mass_addition_id = x.mass_addition_id) = 1
and    fd.deprn_expense_ccid = gcc1.code_combination_id
and    x.payables_code_combination_id = gcc2.code_combination_id
and    x.asset_key_ccid = fk.code_combination_id
and    x.book_type_code = 'IE FV'

/
select * from gl_ledgers
/
select * from fa_categories_b
/
select * 
from   fnd_descriptive_flexs df
     , fnd_descr_flex_column_usages dfu
where df.application_id = dfu.application_id
and   df.descriptive_flexField_name = dfu.descriptive_flexfield_name
and   df.application_table_name = 'FA_MASS_ADDITIONS'
/
select * from fa_methods
/
SELECT t.book_type_code AS book_type_code, t.book_type_name AS book_type_name FROM fa_book_controls where set_of_books_id = 2021 and sysdate between initial_date and nvl(date_ineffective,sysdate+1) ORDER BY book_type_code
/
select * from fa_asset_Keywords
/
select * from fa_book_Controls
where set_of_books_id = 2021
and   sysdate between initial_date and nvl(date_ineffective,sysdate+1)
/
select * from gl_ledgers
/
select * from FA_lookups
where lookup_code = 'CAPITALIZED'
/
select * from dba_tables where table_name like 'BNE%'--bne_integrator
/
select * from bne_integrators_tl where integrator_code = 'FA_MASS_ADDITIONS'
/
select * from bne_interfaces_b where interface_code = 'FA_MASS_ADD_INTERFACE'-- integrator_code = 'JOURNALS_120'
/
select * from BNE_INTERFACE_COLS_B where interface_code like 'FA_MASS_ADD_INTERFACE'
/
select * from BNE_INTERFACE_COLS_B where val_type like 'K%' and interface_code in ('FA_MASS_ADD_INTERFACE','_XXFAADD12_INTF')
and interface_col_name like 'EXP%'
/
select * from BNE_INTERFACE_COLS_B where val_obj_name like '%KFF%'
and interface_col_type = 1
order by interface_code desc
/
select * from BNE_INTERFACE_COLS_B
where val_obj_name is not null and interface_col_type = 1
and oa_flex_code = 'GL#'
--and  interface_col_name = 'CODE_COMBINATION_ID'
/
select * from bne_interface_cols_b where group_name in ('INITIATOR_ACCOUNT','RECIPIENT_ACCOUNT','EXPENSE_ACCOUNT','CLEARING_ACCOUNT')
and interface_col_name like '%SEG%T1'
/
select * from bne_interface_cols_b where val_type = 'KEYFLEXSEG'
order by interface_code desc
/
select fm.method_code
     , fm.life_in_months
     , fm.*
from   fa_methods fm
/
select * from fnd_lookup_values
where lookup_code= 'ORACLE PROJECTS'
/
select * from fnd_lookups
where lookup_code= 'ORACLE PROJECTS'
/
select * from dba_tables where table_name like 'BNE%PARAM%'
/
select * from bne_doc_creation_params
where param_name = 'PARAMETER-WINDOW-BOOK'
/
select * from bne_param_defns_vl where prompt_left = 'Corporate Asset Book'
/
select * from fa_book_controls
/
select * from BNE_PARAM_DEFNS_VL where param_defn_code  in ('MA_PARAM_WNDW_COAID','MA_PARAM_WNDW_BOOK','WEBADI:Parameter')
/
declare
    li_cnt integer;
begin
    for datarec in (
        select 'select count(*) from '||table_name||' where param_list_code = ''MA_PARAM_WNDW''' stmt 
        from dba_tab_columns 
        where table_name like 'BNE%' and column_name like 'PARAM_LIST_CODE'
    ) loop
        begin
            execute immediate datarec.stmt into li_cnt;
            if li_cnt > 0 then
                dbms_output.put_line(datarec.stmt);
            end if;
        exception
            when others then
                dbms_output.put_line('Failed for '||datarec.stmt||' with '||sqlerrm);
        end;
    end loop;
end;
/
select * from BNE_PARAM_GROUPS_VL where param_list_code = 'MA_PARAM_WNDW';
select * from BNE_PARAM_LISTS_VL where param_list_code = 'MA_PARAM_WNDW';
select * from BNE_PARAM_GROUP_ITEMS where param_list_code = 'MA_PARAM_WNDW';
select * from BNE_PARAM_LISTS_B where param_list_code = 'MA_PARAM_WNDW';
select * from BNE_PARAM_LISTS_TL where param_list_code = 'MA_PARAM_WNDW';
select * from BNE_PARAM_LIST_ITEMS where param_list_code = 'MA_PARAM_WNDW';

/

/
select * from BNE_PARAM_LISTS_b where param_list_code = 'MA_PARAM_WNDW'-- param_defn_code = 'MA_PARAM_WNDW_BOOK'
/
select * from BNE_PARAM_LIST_ITEMS where param_list_code = 'MA_PARAM_WNDW'-- param_defn_code = 'MA_PARAM_WNDW_BOOK'
/
select * from bne_param_list_items where param_list_code = 'WEBADI:Parameter'
/
select * from BNE_SIMPLE_QUERY where upper(object_name) like '%FA_BOOK_CONTROLS%'
/
select * from bne_param_defns_vl where param_defn_code = 'MA_PARAM_WNDW_BOOK'
/
select * from fnd_flex_value_sets where flex_value_set_name = 'FA_ADI_CORP_BOOK'
/
select * from fnd_flex_values where flex_value_set_id = 1009827
/
select * from fnd_flex_validation_tables where flex_value_set_id = 1009827
/
select * from BNE_PARAM_OVERRIDES
/
select * from BNE_PARAM_DEFNS_b where param_source = 'WEBADI:Parameter'
/
select * from bne_param_defns_vl where prompt_left = 'Asset Book'
/
select * from dba_tab_columns where table_name like 'BNE%' and column_name like '%INT%COL%TYPE%'
/
select * from BNE_INTERFACE_COLS_VL where prompt_left = 'Depreciation Method'
/
select * from bne_interface_cols_vl where interface_code in ('FA_MASS_ADD_INTERFACE','_XXFAADD7_INTF')
--and (prompt_left like '%rojec%' or prompt_left like '%ask%')
and (interface_col_name like '%CCID%')
order by interface_col_name
/
MA_PARAM_WNDW

select * from bne_interfaces_b
where interface_code = 'FA_MASS_ADD_INTERFACE'
/
select * from bne_integrators_b where (integrator_code = 'FA_MASS_ADDITIONS' or integrator_code like '_XXFA%')
/
select * from BNE_CONTENTS_B where (integrator_code = 'FA_MASS_ADDITIONS' or integrator_code like '_XXFA%')
/
select * from dba_source where name like 'BNE%' and upper(text) like '%CREATE_DOC_LIST_APP_ID%'
/
create_doc_list_app_id
create_doc_list_code
/
select * from bne_param_groups_vl
where param_list_code = 'MA_PARAM_WNDW'
/
    TYPE param_list_item_rec IS RECORD(
--        appl_id               bne_param_list_items.application_id%type
--      , param_list_code       bne_param_list_items.param_list_code%type
        sequence_num          bne_param_list_items.sequence_num%type
      , object_version_number bne_param_list_items.object_version_number%type
      , param_defn            param_defn_rec
      , param_name            bne_param_list_items.param_name%type
      , attribute_app_id      bne_param_list_items.attribute_app_id%type
      , attribute_code        bne_param_list_items.attribute_code%type
      , string_value          bne_param_list_items.string_value%TYPE
      , date_value            bne_param_list_items.date_value%TYPE
      , number_value          bne_param_list_items.number_value%TYPE
      , boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
      , formula_value         bne_param_list_items.formula_value%TYPE
      , desc_value            bne_param_list_items.desc_value%TYPE
    );
    TYPE list_items_table IS TABLE OF param_list_item_rec;

    TYPE param_group_rec IS RECORD (
--        appl_id               bne_param_groups_vl.application_id%type
--      , param_list_code       bne_param_groups_vl.param_list_code%type
        sequence_num          bne_param_groups_vl.sequence_num%type
      , object_version_number bne_param_groups_vl.object_version_number%type
      , attribute_app_id      bne_param_groups_vl.attribute_app_id%type
      , attribute_code        bne_param_groups_vl.attribute_code%type
      , group_resolver        bne_param_groups_vl.group_resolver%type
      , user_name             bne_param_groups_vl.user_name%type
    );
    TYPE list_groups_table IS TABLE OF param_group_rec;
/
declare
    lr_param_list   xxbne_adi_creator.param_list_rec;
    lr_list_item    xxbne_adi_creator.param_list_item_rec;
    lr_param_defn   xxbne_adi_creator.param_defn_rec;
    lr_list_group   xxbne_adi_creator.param_group_rec;
begin
    lr_param_list.appl_id               := 140;
    lr_param_list.param_list_code       := 'PWC_MA_PARAM_WNDW';
    --lr_param_list.object_version_number bne_param_lists_vl.object_version_number%type
    lr_param_list.persistent_flag       := 'Y';
    lr_param_list.comments              := 'Parameter used for Create Assets parameter window';
    --lr_param_list.attribute_app_id      bne_param_lists_vl.attribute_app_id%type
    --lr_param_list.attribute_code        bne_param_lists_vl.attribute_code%type
    --lr_param_list.list_resolver         bne_param_lists_vl.list_resolver%type
    lr_param_list.user_tip              := 'Choose a Corporate Book';
    lr_param_list.prompt_left           := 'Asset Book';
    lr_param_list.prompt_above          := 'Asset Book';
    lr_param_list.user_name             := 'Web ADI: PWC Mass Additions Parameter Window';
    lr_param_list.list_items  := xxbne_adi_creator.list_items_table(null);
    lr_param_list.list_groups := xxbne_adi_creator.list_groups_table(null);
    --
    -- Group
    --
    lr_list_group.sequence_num          := 1;
    --lr_list_group.object_version_number bne_param_groups_vl.object_version_number%type
    --lr_list_group.attribute_app_id      bne_param_groups_vl.attribute_app_id%type
    --lr_list_group.attribute_code        bne_param_groups_vl.attribute_code%type
    lr_list_group.group_resolver        := 'oracle.apps.fa.integrator.parameter.FACOAGroupProcessor';
    lr_list_group.user_name             := 'PWC_MA_PARAM_FLEXNUM_GROUP';
    
    lr_param_list.list_groups(1) := lr_list_group;
    --
    -- Parameter Definition
    --
    lr_param_defn.appl_id                       := 140;
    lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_BOOK';
    --lr_param_defn.object_version_number         bne_param_defns_vl.object_version_number%type
    lr_param_defn.param_name                    := 'BOOK_TYPE_CODE';
    lr_param_defn.param_source                  := 'WEBADI:Parameter';
    lr_param_defn.param_category                := 5;
    lr_param_defn.user_name                     := 'BOOK_TYPE_CODE';
    lr_param_defn.datatype                      := 1;
    --lr_param_defn.attribute_app_id              bne_param_defns_vl.attribute_app_id%type
    --lr_param_defn.attribute_code                bne_param_defns_vl.attribute_code%type
    --lr_param_defn.param_resolver                bne_param_defns_vl.param_resolver%type
    lr_param_defn.default_required_flag         := 'N';
    lr_param_defn.default_visible_flag          := 'Y';
    lr_param_defn.default_user_modifyable_flag  := 'Y';
    --lr_param_defn.default_string                bne_param_defns_vl.default_string%type
    lr_param_defn.default_String_trans_flag     := 'N';
    --lr_param_defn.default_date                  bne_param_defns_vl.default_date%type
    --lr_param_defn.default_number                bne_param_defns_vl.default_number%type
    --lr_param_defn.default_boolean_flag          bne_param_defns_vl.default_boolean_flag%type
    --lr_param_defn.default_formula               bne_param_defns_vl.default_formula%type
    lr_param_defn.val_type                      := 2;
    lr_param_defn.val_value                     := 'FA_ADI_CORP_BOOK';
    lr_param_defn.max_size                      := 15;
    lr_param_defn.display_type                  := 3;
    lr_param_defn.display_style                 := 1;
    lr_param_defn.display_size                  := 15;
    --lr_param_defn.default_desc                  bne_param_defns_vl.default_desc%type
    lr_param_defn.prompt_left                   := 'Asset Book';
    lr_param_defn.prompt_above                  := 'Asset Book';
    --lr_param_defn.user_tip                      bne_param_defns_vl.user_tip%type
    --lr_param_defn.help_url                      bne_param_defns_vl.help_url%type
    --lr_param_defn.format_mask                   bne_param_defns_vl.format_mask%type
    --lr_param_defn.access_key                    bne_param_defns_vl.access_key%type
    --lr_param_defn.oa_flex_application_id        bne_param_defns_vl.oa_flex_application_id%type
    --lr_param_defn.oa_flex_code                  bne_param_defns_vl.oa_flex_code%type
    --lr_param_defn.oa_flex_num                   bne_param_defns_vl.oa_flex_num%type
    --
    -- List Item
    --
    lr_list_item.sequence_num          := 1;
    --lr_list_item.object_version_number bne_param_list_items.object_version_number%type
    lr_list_item.param_defn            := lr_param_defn;
    lr_list_item.param_name            := 'ASSET_BOOK';
    --lr_list_item.attribute_app_id      bne_param_list_items.attribute_app_id%type
    --lr_list_item.attribute_code        bne_param_list_items.attribute_code%type
    --lr_list_item.string_value          bne_param_list_items.string_value%TYPE
    --lr_list_item.date_value            bne_param_list_items.date_value%TYPE
    --lr_list_item.number_value          bne_param_list_items.number_value%TYPE
    --lr_list_item.boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
    --lr_list_item.formula_value         bne_param_list_items.formula_value%TYPE
    --lr_list_item.desc_value            bne_param_list_items.desc_value%TYPE
    --lr_param_list.list_items.extend;
    lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
    --
    ------------------------------------------------------------------------------------------------------------------
    --
    --
    -- Parameter Definition
    --
    lr_param_defn := null;
    lr_param_defn.appl_id                       := 140;
    lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_COAID';
    --lr_param_defn.object_version_number         bne_param_defns_vl.object_version_number%type
    lr_param_defn.param_name                    := 'CHART_OF_ACCOUNTS_ID';
    lr_param_defn.param_source                  := 'WEBADI:Parameter';
    lr_param_defn.param_category                := 5;
    lr_param_defn.user_name                     := 'CHART_OF_ACCOUNTS_ID';
    lr_param_defn.datatype                      := 1;
    --lr_param_defn.attribute_app_id              bne_param_defns_vl.attribute_app_id%type
    --lr_param_defn.attribute_code                bne_param_defns_vl.attribute_code%type
    --lr_param_defn.param_resolver                bne_param_defns_vl.param_resolver%type
    lr_param_defn.default_required_flag         := 'N';
    lr_param_defn.default_visible_flag          := 'N';
    lr_param_defn.default_user_modifyable_flag  := 'N';
    --lr_param_defn.default_string                bne_param_defns_vl.default_string%type
    lr_param_defn.default_String_trans_flag     := 'N';
    --lr_param_defn.default_date                  bne_param_defns_vl.default_date%type
    --lr_param_defn.default_number                bne_param_defns_vl.default_number%type
    --lr_param_defn.default_boolean_flag          bne_param_defns_vl.default_boolean_flag%type
    --lr_param_defn.default_formula               bne_param_defns_vl.default_formula%type
    lr_param_defn.val_type                      := 1;
    --lr_param_defn.val_value                     := null;
    lr_param_defn.max_size                      := 100;
    lr_param_defn.display_type                  := 4;
    lr_param_defn.display_style                 := 1;
    lr_param_defn.display_size                  := 100;
    --lr_param_defn.default_desc                  bne_param_defns_vl.default_desc%type
    --lr_param_defn.prompt_left                   := null;
    --lr_param_defn.prompt_above                  := null;
    --lr_param_defn.user_tip                      bne_param_defns_vl.user_tip%type
    --lr_param_defn.help_url                      bne_param_defns_vl.help_url%type
    --lr_param_defn.format_mask                   bne_param_defns_vl.format_mask%type
    --lr_param_defn.access_key                    bne_param_defns_vl.access_key%type
    --lr_param_defn.oa_flex_application_id        bne_param_defns_vl.oa_flex_application_id%type
    --lr_param_defn.oa_flex_code                  bne_param_defns_vl.oa_flex_code%type
    --lr_param_defn.oa_flex_num                   bne_param_defns_vl.oa_flex_num%type
    --
    -- List Item
    --
    lr_list_item := null;
    lr_list_item.sequence_num          := 2;
    --lr_list_item.object_version_number bne_param_list_items.object_version_number%type
    lr_list_item.param_defn            := lr_param_defn;
    lr_list_item.param_name            := 'PWC_CHART_OF_ACCOUNTS_ID';
    --lr_list_item.attribute_app_id      bne_param_list_items.attribute_app_id%type
    --lr_list_item.attribute_code        bne_param_list_items.attribute_code%type
    --lr_list_item.string_value          bne_param_list_items.string_value%TYPE
    --lr_list_item.date_value            bne_param_list_items.date_value%TYPE
    --lr_list_item.number_value          bne_param_list_items.number_value%TYPE
    --lr_list_item.boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
    --lr_list_item.formula_value         bne_param_list_items.formula_value%TYPE
    --lr_list_item.desc_value            bne_param_list_items.desc_value%TYPE
    lr_param_list.list_items.extend;
    lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
    --
    ------------------------------------------------------------------------------------------------------------------
    --
    --
    -- Parameter Definition
    --
    lr_param_defn := null;
    lr_param_defn.appl_id                       := 140;
    lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_LOC';
    --lr_param_defn.object_version_number         bne_param_defns_vl.object_version_number%type
    lr_param_defn.param_name                    := 'LOCATION_FLEXNUM';
    lr_param_defn.param_source                  := 'WEBADI:Parameter';
    lr_param_defn.param_category                := 5;
    lr_param_defn.user_name                     := 'LOCATION_FLEXNUM';
    lr_param_defn.datatype                      := 1;
    --lr_param_defn.attribute_app_id              bne_param_defns_vl.attribute_app_id%type
    --lr_param_defn.attribute_code                bne_param_defns_vl.attribute_code%type
    --lr_param_defn.param_resolver                bne_param_defns_vl.param_resolver%type
    lr_param_defn.default_required_flag         := 'N';
    lr_param_defn.default_visible_flag          := 'N';
    lr_param_defn.default_user_modifyable_flag  := 'N';
    --lr_param_defn.default_string                bne_param_defns_vl.default_string%type
    lr_param_defn.default_String_trans_flag     := 'N';
    --lr_param_defn.default_date                  bne_param_defns_vl.default_date%type
    --lr_param_defn.default_number                bne_param_defns_vl.default_number%type
    --lr_param_defn.default_boolean_flag          bne_param_defns_vl.default_boolean_flag%type
    --lr_param_defn.default_formula               bne_param_defns_vl.default_formula%type
    lr_param_defn.val_type                      := 1;
    --lr_param_defn.val_value                     := null;
    lr_param_defn.max_size                      := 100;
    lr_param_defn.display_type                  := 4;
    lr_param_defn.display_style                 := 1;
    lr_param_defn.display_size                  := 100;
    --lr_param_defn.default_desc                  bne_param_defns_vl.default_desc%type
    --lr_param_defn.prompt_left                   := null;
    --lr_param_defn.prompt_above                  := null;
    --lr_param_defn.user_tip                      bne_param_defns_vl.user_tip%type
    --lr_param_defn.help_url                      bne_param_defns_vl.help_url%type
    --lr_param_defn.format_mask                   bne_param_defns_vl.format_mask%type
    --lr_param_defn.access_key                    bne_param_defns_vl.access_key%type
    --lr_param_defn.oa_flex_application_id        bne_param_defns_vl.oa_flex_application_id%type
    --lr_param_defn.oa_flex_code                  bne_param_defns_vl.oa_flex_code%type
    --lr_param_defn.oa_flex_num                   bne_param_defns_vl.oa_flex_num%type
    --
    -- List Item
    --
    lr_list_item := null;
    lr_list_item.sequence_num          := 3;
    --lr_list_item.object_version_number bne_param_list_items.object_version_number%type
    lr_list_item.param_defn            := lr_param_defn;
    lr_list_item.param_name            := 'PWC_LOCATION_FLEXNUM';
    --lr_list_item.attribute_app_id      bne_param_list_items.attribute_app_id%type
    --lr_list_item.attribute_code        bne_param_list_items.attribute_code%type
    --lr_list_item.string_value          bne_param_list_items.string_value%TYPE
    --lr_list_item.date_value            bne_param_list_items.date_value%TYPE
    --lr_list_item.number_value          bne_param_list_items.number_value%TYPE
    --lr_list_item.boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
    --lr_list_item.formula_value         bne_param_list_items.formula_value%TYPE
    --lr_list_item.desc_value            bne_param_list_items.desc_value%TYPE
    lr_param_list.list_items.extend;
    lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
    --
    ------------------------------------------------------------------------------------------------------------------
    --
    --
    -- Parameter Definition
    --
    lr_param_defn := null;
    lr_param_defn.appl_id                       := 140;
    lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_CAT';
    --lr_param_defn.object_version_number         bne_param_defns_vl.object_version_number%type
    lr_param_defn.param_name                    := 'CATEGORY_FLEXNUM';
    lr_param_defn.param_source                  := 'WEBADI:Parameter';
    lr_param_defn.param_category                := 5;
    lr_param_defn.user_name                     := 'CATEGORY_FLEXNUM';
    lr_param_defn.datatype                      := 1;
    --lr_param_defn.attribute_app_id              bne_param_defns_vl.attribute_app_id%type
    --lr_param_defn.attribute_code                bne_param_defns_vl.attribute_code%type
    --lr_param_defn.param_resolver                bne_param_defns_vl.param_resolver%type
    lr_param_defn.default_required_flag         := 'N';
    lr_param_defn.default_visible_flag          := 'N';
    lr_param_defn.default_user_modifyable_flag  := 'N';
    --lr_param_defn.default_string                bne_param_defns_vl.default_string%type
    lr_param_defn.default_String_trans_flag     := 'N';
    --lr_param_defn.default_date                  bne_param_defns_vl.default_date%type
    --lr_param_defn.default_number                bne_param_defns_vl.default_number%type
    --lr_param_defn.default_boolean_flag          bne_param_defns_vl.default_boolean_flag%type
    --lr_param_defn.default_formula               bne_param_defns_vl.default_formula%type
    lr_param_defn.val_type                      := 1;
    --lr_param_defn.val_value                     := null;
    lr_param_defn.max_size                      := 100;
    lr_param_defn.display_type                  := 4;
    lr_param_defn.display_style                 := 1;
    lr_param_defn.display_size                  := 100;
    --lr_param_defn.default_desc                  bne_param_defns_vl.default_desc%type
    --lr_param_defn.prompt_left                   := null;
    --lr_param_defn.prompt_above                  := null;
    --lr_param_defn.user_tip                      bne_param_defns_vl.user_tip%type
    --lr_param_defn.help_url                      bne_param_defns_vl.help_url%type
    --lr_param_defn.format_mask                   bne_param_defns_vl.format_mask%type
    --lr_param_defn.access_key                    bne_param_defns_vl.access_key%type
    --lr_param_defn.oa_flex_application_id        bne_param_defns_vl.oa_flex_application_id%type
    --lr_param_defn.oa_flex_code                  bne_param_defns_vl.oa_flex_code%type
    --lr_param_defn.oa_flex_num                   bne_param_defns_vl.oa_flex_num%type
    --
    -- List Item
    --
    lr_list_item := null;
    lr_list_item.sequence_num          := 4;
    --lr_list_item.object_version_number bne_param_list_items.object_version_number%type
    lr_list_item.param_defn            := lr_param_defn;
    lr_list_item.param_name            := 'PWC_CATEGORY_FLEXNUM';
    --lr_list_item.attribute_app_id      bne_param_list_items.attribute_app_id%type
    --lr_list_item.attribute_code        bne_param_list_items.attribute_code%type
    --lr_list_item.string_value          bne_param_list_items.string_value%TYPE
    --lr_list_item.date_value            bne_param_list_items.date_value%TYPE
    --lr_list_item.number_value          bne_param_list_items.number_value%TYPE
    --lr_list_item.boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
    --lr_list_item.formula_value         bne_param_list_items.formula_value%TYPE
    --lr_list_item.desc_value            bne_param_list_items.desc_value%TYPE
    lr_param_list.list_items.extend;
    lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
    --
    ------------------------------------------------------------------------------------------------------------------
    --
    --
    -- Parameter Definition
    --
    lr_param_defn := null;
    lr_param_defn.appl_id                       := 140;
    lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_KEY';
    --lr_param_defn.object_version_number         bne_param_defns_vl.object_version_number%type
    lr_param_defn.param_name                    := 'ASSETKEY_FLEXNUM';
    lr_param_defn.param_source                  := 'WEBADI:Parameter';
    lr_param_defn.param_category                := 5;
    lr_param_defn.user_name                     := 'ASSETKEY_FLEXNUM';
    lr_param_defn.datatype                      := 1;
    --lr_param_defn.attribute_app_id              bne_param_defns_vl.attribute_app_id%type
    --lr_param_defn.attribute_code                bne_param_defns_vl.attribute_code%type
    --lr_param_defn.param_resolver                bne_param_defns_vl.param_resolver%type
    lr_param_defn.default_required_flag         := 'N';
    lr_param_defn.default_visible_flag          := 'N';
    lr_param_defn.default_user_modifyable_flag  := 'N';
    --lr_param_defn.default_string                bne_param_defns_vl.default_string%type
    lr_param_defn.default_String_trans_flag     := 'N';
    --lr_param_defn.default_date                  bne_param_defns_vl.default_date%type
    --lr_param_defn.default_number                bne_param_defns_vl.default_number%type
    --lr_param_defn.default_boolean_flag          bne_param_defns_vl.default_boolean_flag%type
    --lr_param_defn.default_formula               bne_param_defns_vl.default_formula%type
    lr_param_defn.val_type                      := 1;
    --lr_param_defn.val_value                     := null;
    lr_param_defn.max_size                      := 100;
    lr_param_defn.display_type                  := 4;
    lr_param_defn.display_style                 := 1;
    lr_param_defn.display_size                  := 100;
    --lr_param_defn.default_desc                  bne_param_defns_vl.default_desc%type
    --lr_param_defn.prompt_left                   := null;
    --lr_param_defn.prompt_above                  := null;
    --lr_param_defn.user_tip                      bne_param_defns_vl.user_tip%type
    --lr_param_defn.help_url                      bne_param_defns_vl.help_url%type
    --lr_param_defn.format_mask                   bne_param_defns_vl.format_mask%type
    --lr_param_defn.access_key                    bne_param_defns_vl.access_key%type
    --lr_param_defn.oa_flex_application_id        bne_param_defns_vl.oa_flex_application_id%type
    --lr_param_defn.oa_flex_code                  bne_param_defns_vl.oa_flex_code%type
    --lr_param_defn.oa_flex_num                   bne_param_defns_vl.oa_flex_num%type
    --
    -- List Item
    --
    lr_list_item := null;
    lr_list_item.sequence_num          := 5;
    --lr_list_item.object_version_number bne_param_list_items.object_version_number%type
    lr_list_item.param_defn            := lr_param_defn;
    lr_list_item.param_name            := 'PWC_ASSETKEY_FLEXNUM';
    --lr_list_item.attribute_app_id      bne_param_list_items.attribute_app_id%type
    --lr_list_item.attribute_code        bne_param_list_items.attribute_code%type
    --lr_list_item.string_value          bne_param_list_items.string_value%TYPE
    --lr_list_item.date_value            bne_param_list_items.date_value%TYPE
    --lr_list_item.number_value          bne_param_list_items.number_value%TYPE
    --lr_list_item.boolean_value_flag    bne_param_list_items.boolean_value_flag%TYPE
    --lr_list_item.formula_value         bne_param_list_items.formula_value%TYPE
    --lr_list_item.desc_value            bne_param_list_items.desc_value%TYPE
    lr_param_list.list_items.extend;
    lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
    --
    -- Create
    --
    xxbne_adi_creator.create_param_list(
        pv_created_by_user => 'SANDERSON'
      , pr_param_list      => lr_param_list
    );
end;
/
select * from BNE_PARAM_LIST_ITEMS where param_list_code = 'PA_OF_CNT_PARAM'-- param_defn_code = 'MA_PARAM_WNDW'
/
select * from bne_param_defns_vl where param_defn_code in (select param_defn_code from bne_param_list_items where param_list_code = 'MA_PARAM_WNDW')
/
select * from BNE_PARAM_GROUPS_VL where param_list_code = 'MA_PARAM_WNDW';
/

select * from BNE_PARAM_LIST_ITEMS where param_list_code = 'PWC_MA_PARAM_WNDW'-- param_defn_code = 'MA_PARAM_WNDW_BOOK'
/
select * from bne_param_defns_vl where param_defn_code in (select param_defn_code from bne_param_list_items where param_list_code = 'PWC_MA_PARAM_WNDW')
/
select * from BNE_PARAM_GROUPS_VL where param_list_code in ('MA_PARAM_WNDW', 'PWC_MA_PARAM_WNDW');
/
select * from dba_tab_columns where table_name like 'BNE%' and column_name like '%SOURCE%'
/
select * from BNE_PARAM_GROUP_ITEMS where param_list_code in ( 'MA_PARAM_WNDW', 'PWC_MA_PARAM_WNDW')
/
select distinct param_source from bne_param_defns_b
/
select * from bne_param_lists_b
where application_id = 140
and   param_list_code = 'PWC_MA_PARAM_WNDW'
/
select * from BNE_CONTENTS_B
where trunc(creation_date) = trunc(sysdate)
/
update bne_contents_b
set param_list_app_id = 140
, param_list_code = 'PWC_MA_PARAM_WNDW'
where content_code = '_XXFAADD1_DL1_CNT'
/
update bne_param_groups_b
set object_version_number = 1 where param_list_code = 'PWC_MA_PARAM_WNDW'
/
delete from BNE_PARAM_LIST_ITEMS where param_list_code = 'PWC_MA_PARAM_WNDW'
/
delete from BNE_PARAM_LISTS_B where param_list_code = 'PWC_MA_PARAM_WNDW'
/
select * from fa_mass_additions
where book_type_code = 'IE FV'
--and queue_name != 'POSTING'
/
delete from bne_param_defns_tl where param_defn_code = 'BOOK_TYPE_CODE'
/
select * from fa_locations
/
select * from fnd_id_flexs where application_table_name = 'FA_LOCATIONS'
/
select * from fnd_id_flex_segments where application_id = 140 and id_flex_code = 'LOC#'