-- +===================================================================================================================+
-- | FILENAME                                                                                                          |
-- |    create_mass_additions_dff.sql                                                                                  |
-- |                                                                                                                   |
-- | DESCRIPTION                                                                                                       |
-- |    Creates an additional segment (ATTRIBUTE15) in the mass additions descriptive flexfield.                       |
-- |                                                                                                                   |
-- | NOTES                                                                                                             |
-- |                                                                                                                   |
-- | HISTORY                                                                                                           |
-- |    Stephen Anderson 27/08/2018 Created                                                                            |
-- +===================================================================================================================+
SET LINESIZE 250

WHENEVER SQLERROR CONTINUE

declare
    lr_flex_context      xxfnd_installer.flex_context_record;
    lr_flex_segment      xxfnd_installer.flex_segment_record;
    lar_flex_segments    xxfnd_installer.flex_segment_table;
    lcv_dff_name         constant fnd_descriptive_flexs.descriptive_flexfield_name%type := 'FA_MASS_ADDITIONS';
    lcv_fa_asn           constant fnd_application.application_short_name%type := 'OFA';
begin
    --
    -- Flexfield context
    --
    lr_flex_context.context_code                 := 'PWC';
    lr_flex_context.context_name                 := 'PWC';
    lr_flex_context.context_description          := 'PWC Mass Addition DFF';
    lr_flex_context.reference_field              := NULL;
    lr_flex_context.ref_description              := NULL;
    lr_flex_context.context_prompt               := 'Context';
    lr_flex_context.default_context_value        := 'PWC';
    lr_flex_context.value_required               := 'N';
    lr_flex_context.override_allowed             := 'N';
    lr_flex_context.context_value_set_name       := NULL;
    lr_flex_context.context_synchronization_flag := 'N';
    --
    -- Flexfield segments
    --
    lr_flex_segment.segment_name           := 'CUSTOM_CODE';
    lr_flex_segment.column_name            := 'ATTRIBUTE15';
    lr_flex_segment.description            := 'Skip Custom Processing';
    lr_flex_segment.segment_num            := 150;
    lr_flex_segment.value_set_name         := 'IEX_YES_NO';
    lr_flex_segment.enabled_flag           := 'Y';
    lr_flex_segment.displayed_flag         := 'Y';
    lr_flex_segment.security_enabled       := 'N';
    lr_flex_segment.default_type_code      := NULL;
    lr_flex_segment.default_type_mean      := NULL;
    lr_flex_segment.default_value          := NULL;
    lr_flex_segment.required_flag          := 'N';
    lr_flex_segment.display_size           := 5;
    lr_flex_segment.description_size       := 50;
    lr_flex_segment.concat_desc_size       := 25;
    lr_flex_segment.lov_prompt             := 'Skip Custom Processing';
    lr_flex_segment.window_prompt          := 'Skip Custom Processing';
    lr_flex_segment.range_type_mean        := NULL;
    lr_flex_segment.range_type_code        := NULL;
    xxfnd_installer.add_segment(
        pr_segment    => lr_flex_segment
      , par_segments  => lar_flex_segments
    );
    --
    -- Create DFF Segment and Recompile DFF
    --
    if xxfnd_installer.create_desc_flexfield(
        pv_appl_short_name => lcv_fa_asn
      , pv_flexfield_name  => lcv_dff_name
      , pr_flex_context    => lr_flex_context
      , par_segments       => lar_flex_segments
    ) then
      xxfnd_installer.compile_descriptive_flex(
        pv_flexfield_name  => lcv_dff_name,
        pv_appl_short_name => lcv_fa_asn);
    end if;    
end;
/
