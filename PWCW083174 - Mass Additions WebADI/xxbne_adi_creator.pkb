CREATE OR REPLACE PACKAGE BODY xxbne_adi_creator IS
-- +===================================================================================+
-- |   NAME:       XXBNE_ADI_CREATOR                                                   |
-- |                                                                                   |
-- |   PURPOSE:                                                                        |
-- |   Bypass the flakey GUI wizard and create ADIs.                                   |
-- |                                                                                   |
-- |   REVISIONS:                                                                      |
-- |                                                                                   |
-- |   Version   Date          Author           Description                            |
-- |   --------  ------------  ---------------  ------------------------------------   |
-- |   1.0                     S Anderson       Initial Version                        |
-- |   1.1       20-Sep-12     W Soo            Always display hints on line level when|
-- |                                            creating a default layout. Add "*" as  |
-- |                                            hint when text field is mandatory.     | 
-- |   1.2       12-Nov-12     S Anderson       Added download layout functionality    |
-- |   1.3       20-Nov-12     W Soo            Hide the import row if no importer is  |
-- |                                            specified. Amended so that session date|
-- |                                            format is not relied upon. Increased   |
-- |                                            dummy column sequence value to fit more|
-- |                                            columns.                               |
-- |   1.4       28-Nov-12     W Soo            Properly hide the import row if no     |
-- |                                            importer is specified.                 |
-- |   1.5       07-Dec-12     S Anderson       Major update to handle multi-layout    |
-- |   1.6       15-Jan-13     W Soo            Amended default rows logic to handle   |
-- |                                            layouts with headers and lines.        |
-- |                                            Corrected header block generation.     |
-- |   1.7       17-Jan-13     S Anderson       Cleaned up some logging, fixed bug in  |
-- |                                            rename_to code and create_layouts.     |
-- |                                            Improved kff dummy column creation to  |
-- |                                            correctly allocate block_id            |
-- |   1.8       25-Jan-13     S Anderson       Added intg_prefix as an attribute for  |
-- |                                            integrator_rec.  Ensures re-creation   |
-- |                                            of adi with new integrator code to save|
-- |                                            bouncing as much                       |
-- |   1.9       01-Feb-13     W Soo            Set block ID to 3 instead of 1 when    |
-- |                                            layout only has lines                  |
-- |   1.10      05-Dec-13     S Anderson       Added ability to pass parameters for   |
-- |                                            content sql                            |
-- |   1.11      08-Jan-14     S Anderson       Enhanced form function parameters      |
-- |   1.12      21-Jan-14     S Anderson       Added user_name setting for default    |
-- |                                            content                                |
-- |   1.13      13-Feb-14     S Anderson       Allowed content sql parameter reuse and|
-- |                                            ability to remove empty content        |
-- |   1.14      28-Aug-18     S Anderson       Added pointless KFF prompt prefix.     |
-- +===================================================================================+

  /*
  || Table of sequence numbers, indexed by the 
  || bne_interface_cols_b.interface_col_name they link to
  */
  TYPE seq_table IS TABLE OF NUMBER INDEX BY VARCHAR2(50);
  -- store the sysadmin user id (which should be 0 from autoinstall)
  gn_sysadmin_id NUMBER;

  type output_type is record
    (requested_width NUMBER,
     message         VARCHAR2(32767));

  type valid_vals_table is table of varchar2(100);
  /*
  || Logging levels
  */
  gcn_unexpected                constant NUMBER := fnd_log.level_unexpected;
  gcn_error                     constant NUMBER := fnd_log.level_error;
  gcn_exception                 constant NUMBER := fnd_log.level_exception;
  gcn_event                     constant NUMBER := fnd_log.level_event;
  gcn_procedure                 constant NUMBER := fnd_log.level_procedure;
  gcn_statement                 constant NUMBER := fnd_log.level_statement;
  
  gcn_dummy_seq                 constant number := 999999; -- 1.3 (WS): increased to accomodate more columns
  gcn_webadi_appl               NUMBER;
  gcv_date_format               constant varchar2(20) := 'YYYY/MM/DD';


    gcv_create      constant varchar2(1) := 'C';
    gcv_update      constant varchar2(1) := 'U';
    gcv_match       constant varchar2(1) := 'M';

    gcn_miss_num    constant number := fnd_api.g_miss_num;
    gcv_miss_char   constant varchar2(1) := fnd_api.g_miss_char;
    gcd_miss_date   constant date   := fnd_api.g_miss_date;

  /*
  || Routine to break up the message into dbms_output sized chunks
  */
  PROCEDURE output_log_message(
    pr_message_out   IN out nocopy output_type)
  is
    ln_msg_length     NUMBER; 
    ln_width          number;
    ln_offset         number := 1;
    lv_partial_string varchar2(255);
    ln_max_width      number := 255;
  BEGIN    
    ln_msg_length := LENGTH(pr_message_out.message);
    ln_width := least(nvl(pr_message_out.requested_width,ln_max_width),ln_max_width);
    /*
    || Check if the string is wider than the requested width
    */
    IF ln_msg_length > ln_width THEN
      /*
      || Loop through the string breaking it up into chunks
      */

      while ln_offset <= ln_msg_length loop
        lv_partial_string := substr(pr_message_out.message, ln_offset, ln_width);
        dbms_output.put_line(lv_partial_string);
        ln_offset := ln_offset + ln_width;
      END loop;
    ELSE
      /*
      || Simply Print the String
      */
      dbms_output.put_line(pr_message_out.message);
    END IF;  
  END output_log_message;
  /*
  ------------------------------------------------------------------------------
  || Basic logging routine 
  */
  procedure log_msg(
    pv_procedure       in varchar2,
    pv_message         in varchar2,
    pn_requested_width IN NUMBER   DEFAULT 32767)
  IS
    lv_message       varchar2(32767);
    ln_level         NUMBER;
    --
    lr_message_out   output_type;
  BEGIN
    --
    lr_message_out.requested_width := pn_requested_width;
    lr_message_out.message         := substr('['||to_char(SYSTIMESTAMP, 'HH24:MI:SS.FF')||']xxbne_adi_creator.'||pv_procedure||': '||pv_message,1,32767);
    output_log_message(lr_message_out);
  END log_msg;  

  PROCEDURE log_heading(
    pv_procedure       in varchar2,
    pv_message         in varchar2)
  IS
  BEGIN
    log_msg(pv_procedure,rpad('-',80,'-'));
    log_msg(pv_procedure,pv_message);
    log_msg(pv_procedure,rpad('-',80,'-'));
  end log_heading;

  procedure field_validate(
    pv_field_name  in varchar2,
    pv_field_val   in out nocopy varchar2,
    pav_vals       in valid_vals_table,
    pv_default_val in varchar2 default NULL)
  is
    ln_index number;
    lb_ok    boolean := false;
  begin
    if pv_field_val is not null or nvl(pv_default_val,'X') != 'NULL' then
      pv_field_val := upper(nvl(pv_field_val,pv_default_val));
      ln_index := pav_vals.first;
      while ln_index is not null loop
        if pv_field_val = pav_vals(ln_index) then
          lb_ok := true;
          ln_index := null;
        else  
          ln_index := pav_vals.next(ln_index);
        end if;
      end loop;
      if not lb_ok then
        raise_application_error(-20001,'Value ('||pv_field_val||') in '||pv_field_name||' is invalid.');
      end if;
    end if;
  end field_validate;

  FUNCTION get_application_id(
    pv_asn IN VARCHAR2) 
  RETURN NUMBER 
    DETERMINISTIC
  IS
    ln_return_val NUMBER;
  BEGIN
    BEGIN
      SELECT application_id 
      INTO   ln_return_val
      FROM   fnd_application
      WHERE  application_short_name = pv_asn;
    exception
      WHEN no_data_found THEN
        raise_application_error(-20001,'Unable to find application with short name: '||pv_asn);
    END;
    RETURN(ln_return_val);
  END get_application_id;

    FUNCTION get_asn(
        pn_appl_id IN NUMBER) 
    RETURN VARCHAR2 
        DETERMINISTIC
    IS
        lv_return_val fnd_application.application_short_name%type;
    BEGIN
        if pn_appl_id is not null then
            BEGIN
                SELECT application_short_name 
                INTO   lv_return_val
                FROM   fnd_application
                WHERE  application_id = pn_appl_id;
            exception
                WHEN no_data_found THEN
                    raise_application_error(-20001,'Unable to find application with id: '||pn_appl_id);
            END;
        END IF;
        RETURN(lv_return_val);
    END get_asn;

  FUNCTION get_user_id(
    pv_username IN VARCHAR2) 
  RETURN NUMBER IS
    ln_return_val NUMBER;
  BEGIN
    BEGIN
      SELECT user_id 
      INTO   ln_return_val
      FROM   fnd_user
      WHERE  user_name = pv_username;
    exception
      WHEN no_data_found THEN
        raise_application_error(-20001,'Unable to find user with user name: '||pv_username);
    END;
    RETURN(ln_return_val);
  END get_user_id;
  
  FUNCTION find_display_seq(
    pv_interface_col_name   IN interface_col_name,
    par_display_seq         IN out nocopy display_seq_table
    ) 
  RETURN NUMBER IS
    ln_display_seq_num    number;
    lcv_proc_name         constant varchar2(30) := 'find_display_seq';
  BEGIN
    ln_display_seq_num := par_display_seq.FIRST;
    while ln_display_seq_num IS NOT NULL loop
      IF pv_interface_col_name = par_display_seq(ln_display_seq_num) THEN
        exit;
      END IF;
      ln_display_seq_num := par_display_seq.NEXT(ln_display_seq_num);
    END loop;
    IF ln_display_seq_num IS NULL THEN
      raise_application_error(-20001,'Interface column '||pv_interface_col_name||' not provided in par_display_seq');
    END IF;
    RETURN(ln_display_seq_num);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END find_display_seq;

  FUNCTION create_update_layout(
    pr_intg IN out nocopy integrator_rec)
  RETURN BOOLEAN IS
  BEGIN
    RETURN(pr_intg.update_map.content_sql IS NOT NULL);
  END create_update_layout;
  
  function create_unique_param_defn_code(
    pv_prefix      IN VARCHAR2,
    pn_appl_id     IN NUMBER,
    pb_skip_suffix IN BOOLEAN DEFAULT FALSE) 
  return varchar2 is
    lcv_proc_name        constant varchar2(30) := 'create_unique_param_defn_code';
    lv_param_defn_code   bne_param_list_items.param_defn_code%type;
    lb_successful        boolean := false;
    lv_dummy             varchar2(25);
  BEGIN
    while NOT lb_successful loop
      /*
      || Create a parameter name
      */
      if pb_skip_suffix then 
        lv_param_defn_code := pv_prefix;
      else
        SELECT pv_prefix||bne_param_list_s.nextval
        INTO   lv_param_defn_code
        FROM   dual;  
      end if;
      /*
      || Check if it already exists
      */
      BEGIN
        SELECT 'already exists'
        INTO   lv_dummy
        from   bne_param_defns_b
        where  application_id = pn_appl_id
        and    param_defn_code = lv_param_defn_code;
        
        if pb_skip_suffix then
          raise_application_error(-20001,'PARAM_DEFN_CODE: '||pn_appl_id||':'||pv_prefix||' was not unique and suffix flagged to be skipped');
        end if;
      exception
        when no_data_found then
          log_msg(lcv_proc_name,'Generated Unique Param Defn Code ['||pn_appl_id||':'||lv_param_defn_code||']');
          lb_successful := TRUE;
      END;
    END loop;
    RETURN(lv_param_defn_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_unique_param_defn_code;

  FUNCTION create_unique_param_list_code(
    pv_prefix IN VARCHAR2,
    pn_appl_id IN NUMBER) 
  RETURN VARCHAR2 IS
    lcv_proc_name        constant varchar2(30) := 'create_unique_param_list_code';
    lv_param_list_code   bne_param_lists_b.param_list_code%TYPE;
    lb_successful        boolean := FALSE;
    lv_dummy             VARCHAR2(25);
  BEGIN
    while NOT lb_successful loop
      /*
      || Create a parameter name
      */
      SELECT pv_prefix||'_'||bne_param_list_s.nextval
      INTO   lv_param_list_code
      FROM   dual;
      /*
      || Check if it already exists
      */
      BEGIN
        SELECT 'already exists'
        INTO   lv_dummy
        FROM   bne_param_lists_b
        WHERE  application_id = pn_appl_id
        and    param_list_code = lv_param_list_code;
      exception  
        when no_data_found then
          log_msg(lcv_proc_name,'Generated Unique Param List Code ['||pn_appl_id||':'||lv_param_list_code||']');
          lb_successful := TRUE;
      END;
    END loop;
    RETURN(lv_param_list_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_unique_param_list_code;

  FUNCTION create_unique_attribute_code(
    pv_prefix  in varchar2,
    pn_appl_id IN NUMBER) 
  return varchar2 is
    lcv_proc_name        constant varchar2(30) := 'create_unique_attribute_code';
    lv_attribute_code    bne_attributes.attribute_code%TYPE;
    lb_successful        boolean := FALSE;
    lv_dummy             VARCHAR2(25);
  BEGIN
    while NOT lb_successful loop
      /*
      || Create a parameter name
      */
      SELECT pv_prefix||'_'||bne_attribute_s.nextval
      INTO   lv_attribute_code
      FROM   dual;
      /*
      || Check if it already exists
      */
      BEGIN
        SELECT 'already exists'
        INTO   lv_dummy
        FROM   bne_attributes
        WHERE  application_id = pn_appl_id
        AND    attribute_code = lv_attribute_code;
      exception
        when no_data_found then
          log_msg(lcv_proc_name,'Generated Unique Attribute Code ['||pn_appl_id||':'||lv_attribute_code||']');
          lb_successful := TRUE;
      END;
    END loop;
    RETURN(lv_attribute_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_unique_attribute_code;

  function CREATE_UNIQUE_LAYOUT_CODE(
    pr_intg       IN OUT NOCOPY integrator_rec,
    pn_layout_num IN NUMBER) 
  RETURN VARCHAR2 IS
    lcv_proc_name        constant varchar2(30) := 'create_unique_layout_code';
    lv_layout_code       bne_layouts_b.layout_code%TYPE;
    lb_successful        boolean := FALSE;
    lv_dummy             VARCHAR2(25);
    LN_LAYOUT            number := PN_LAYOUT_NUM;
    ln_layout_index      number;
    lb_in_array          boolean := FALSE;
  BEGIN
    while NOT lb_successful loop
      /*
      || Create a layout code
      */
      if PR_INTG.LAYOUTS(PN_LAYOUT_NUM).UPDATEABLE_LAYOUT then
        lv_layout_code := pr_intg.intg_code||'_UPD'||ln_layout;
      ELSE
        lv_layout_code := pr_intg.intg_code||'_DFLT'||ln_layout;
      END IF;
      /*
      || Check if it already exists
      */
      BEGIN
        SELECT 'already exists'
        INTO   lv_dummy
        FROM   bne_layouts_b
        where  APPLICATION_ID = PR_INTG.APPL_ID
        AND    layout_code = lv_layout_code;
      exception
        when no_data_found then
          /*
          || Should be unique - unless it's in out layouts array already 
          || and we haven't created it in the table yet
          */
          lb_in_array := FALSE;
          LN_LAYOUT_INDEX := PR_INTG.LAYOUTS.first;
          WHILE LN_LAYOUT_INDEX is not null LOOP
            if LV_LAYOUT_CODE = PR_INTG.layouts(LN_LAYOUT_INDEX).LAYOUT_CODE then
              lb_in_array := TRUE;
              exit;
            else
              LN_LAYOUT_INDEX := PR_INTG.LAYOUTS.next(LN_LAYOUT_INDEX);
            end if;
          end loop;
          if not lb_in_array then
            log_msg(lcv_proc_name,'Generated Unique Layout Code ['||PR_INTG.APPL_ID||':'||lv_layout_code||']');
            lb_successful := TRUE;
          end if;
      END;
      ln_layout := ln_layout + 1;      
    END loop;
    RETURN(lv_layout_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_unique_layout_code;

  FUNCTION create_unique_content_code(
    pn_appl_id    IN NUMBER,
    pv_intg_code  IN VARCHAR2,
    pn_layout_num IN NUMBER) 
  RETURN VARCHAR2 IS
    lcv_proc_name        constant varchar2(30) := 'create_unique_content_code';
    lv_content_code      bne_contents_b.content_code%TYPE;
    lb_successful        boolean := FALSE;
    lv_dummy             VARCHAR2(25);
    ln_content           number := pn_layout_num;
  BEGIN
    while NOT lb_successful loop
      /*
      || Create a layout code
      */
      lv_content_code := pv_intg_code||'_DL'||ln_content;
      /*
      || Check if it already exists
      */
      BEGIN
        SELECT 'already exists'
        INTO   lv_dummy
        FROM   bne_contents_b
        WHERE  application_id = pn_appl_id
        AND    content_code = lv_content_code;
        ln_content := ln_content + 1;
      exception
        when no_data_found then
          log_msg(lcv_proc_name,'Generated Unique Content Code ['||pn_appl_id||':'||lv_content_code||']');
          lb_successful := TRUE;
      END;
    END loop;
    RETURN(lv_content_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_unique_content_code;

  FUNCTION create_unique_mapping_code(
    pn_appl_id    IN NUMBER,
    pv_intg_code  IN VARCHAR2,
    pn_layout_num IN NUMBER) 
  RETURN VARCHAR2 IS
    lcv_proc_name        constant varchar2(30) := 'create_unique_mapping_code';
    lv_mapping_code      bne_mappings_b.mapping_code%TYPE;
    lb_successful        boolean := FALSE;
    lv_dummy             VARCHAR2(25);
    ln_mapping           number := pn_layout_num;
  BEGIN
    while NOT lb_successful loop
      /*
      || Create a layout code
      */
      lv_mapping_code := pv_intg_code||'_MAP'||ln_mapping;
      /*
      || Check if it already exists
      */
      BEGIN
        SELECT 'already exists'
        INTO   lv_dummy
        FROM   bne_mappings_b
        where  application_id = pn_appl_id
        AND    mapping_code = lv_mapping_code;
        ln_mapping := ln_mapping + 1;
      exception
        when no_data_found then
          log_msg(lcv_proc_name,'Generated Unique Mapping Code ['||pn_appl_id||':'||lv_mapping_code||']');
          lb_successful := TRUE;
      END;
    END loop;
    RETURN(lv_mapping_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_unique_mapping_code;
  
  PROCEDURE create_bne_attribute(
    pv_asn              IN VARCHAR2,
    pv_attr_code        IN VARCHAR2,
    pv_object_version   IN VARCHAR2 DEFAULT '1.0',
    pv_attribute1       IN VARCHAR2 DEFAULT NULL,
    pv_attribute2       IN VARCHAR2 DEFAULT NULL,
    pv_attribute3       IN VARCHAR2 DEFAULT NULL,
    pv_attribute4       IN VARCHAR2 DEFAULT NULL,
    pv_attribute5       IN VARCHAR2 DEFAULT NULL,
    pv_attribute6       IN VARCHAR2 DEFAULT NULL,
    pv_attribute7       IN VARCHAR2 DEFAULT NULL,
    pv_attribute8       IN VARCHAR2 DEFAULT NULL,
    pv_attribute9       IN VARCHAR2 DEFAULT NULL,
    pv_attribute10      IN VARCHAR2 DEFAULT NULL,
    pv_attribute11      IN VARCHAR2 DEFAULT NULL,
    pv_attribute12      IN VARCHAR2 DEFAULT NULL,
    pv_attribute13      IN VARCHAR2 DEFAULT NULL,
    pv_attribute14      IN VARCHAR2 DEFAULT NULL,
    pv_attribute15      IN VARCHAR2 DEFAULT NULL,
    pv_attribute16      IN VARCHAR2 DEFAULT NULL,
    pv_attribute17      IN VARCHAR2 DEFAULT NULL,
    pv_attribute18      IN VARCHAR2 DEFAULT NULL,
    pv_attribute19      IN VARCHAR2 DEFAULT NULL,
    pv_attribute20      IN VARCHAR2 DEFAULT NULL,
    pv_attribute21      IN VARCHAR2 DEFAULT NULL,
    pv_attribute22      IN VARCHAR2 DEFAULT NULL,
    pv_attribute23      IN VARCHAR2 DEFAULT NULL,
    pv_attribute24      IN VARCHAR2 DEFAULT NULL,
    pv_attribute25      IN VARCHAR2 DEFAULT NULL,
    pv_attribute26      IN VARCHAR2 DEFAULT NULL,
    pv_attribute27      IN VARCHAR2 DEFAULT NULL,
    pv_attribute28      IN VARCHAR2 DEFAULT NULL,
    pv_attribute29      IN VARCHAR2 DEFAULT NULL,
    pv_attribute30      IN VARCHAR2 DEFAULT NULL,
    pv_created_by       IN VARCHAR2,
    pd_last_update_date IN DATE DEFAULT SYSDATE,
    pv_custom_mode      IN VARCHAR2 DEFAULT 'FORCE'
  ) is
    lcv_proc_name constant varchar2(30) := 'create_bne_attribute';
    procedure print_attribute(
      pn_attribute number,
      pv_attribute varchar2)
    is
    begin
      if pv_attribute is not null then
        log_msg(lcv_proc_name,'  Attribute'||pn_attribute||' '''||pv_attribute||'''');
      end if;
    end print_attribute;
    
    
  begin
    log_msg(lcv_proc_name,'Creating bne_attribute ['||pv_asn||':'||pv_attr_code||']');
    print_attribute(1,pv_attribute1);
    print_attribute(2,pv_attribute2);
    print_attribute(3,pv_attribute3);
    print_attribute(4,pv_attribute4);
    print_attribute(5,pv_attribute5);
    print_attribute(6,pv_attribute6);
    print_attribute(7,pv_attribute7);
    print_attribute(8,pv_attribute8);
    print_attribute(9,pv_attribute9);
    print_attribute(10,pv_attribute10);
    print_attribute(11,pv_attribute11);
    print_attribute(12,pv_attribute12);
    print_attribute(13,pv_attribute13);
    print_attribute(14,pv_attribute14);
    print_attribute(15,pv_attribute15);
    print_attribute(16,pv_attribute16);
    print_attribute(17,pv_attribute17);
    print_attribute(18,pv_attribute18);
    print_attribute(19,pv_attribute19);
    print_attribute(20,pv_attribute20);
    print_attribute(21,pv_attribute21);
    print_attribute(22,pv_attribute22);
    print_attribute(23,pv_attribute23);
    print_attribute(24,pv_attribute24);
    print_attribute(25,pv_attribute25);
    print_attribute(26,pv_attribute26);
    print_attribute(27,pv_attribute27);
    print_attribute(28,pv_attribute28);
    print_attribute(29,pv_attribute29);
    print_attribute(30,pv_attribute30);

    bne_attributes_pkg.load_row(
      x_attribute_asn         => pv_asn,
      x_attribute_code        => pv_attr_code,
      x_object_version_number => pv_object_version,
      x_attribute1            => pv_attribute1,
      x_attribute2            => pv_attribute2,
      x_attribute3            => pv_attribute3,
      x_attribute4            => pv_attribute4,
      x_attribute5            => pv_attribute5,
      x_attribute6            => pv_attribute6,
      x_attribute7            => pv_attribute7,
      x_attribute8            => pv_attribute8,
      x_attribute9            => pv_attribute9,
      x_attribute10           => pv_attribute10,
      x_attribute11           => pv_attribute11,
      x_attribute12           => pv_attribute12,
      x_attribute13           => pv_attribute13,
      x_attribute14           => pv_attribute14,
      x_attribute15           => pv_attribute15,
      x_attribute16           => pv_attribute16,
      x_attribute17           => pv_attribute17,
      x_attribute18           => pv_attribute18,
      x_attribute19           => pv_attribute19,
      x_attribute20           => pv_attribute20,
      x_attribute21           => pv_attribute21,
      x_attribute22           => pv_attribute22,
      x_attribute23           => pv_attribute23,
      x_attribute24           => pv_attribute24,
      x_attribute25           => pv_attribute25,
      x_attribute26           => pv_attribute26,
      x_attribute27           => pv_attribute27,
      x_attribute28           => pv_attribute28,
      x_attribute29           => pv_attribute29,
      x_attribute30           => pv_attribute30,
      x_owner                 => pv_created_by,
      x_last_update_date      => to_char(pd_last_update_date,gcv_date_format),
      x_custom_mode           => pv_custom_mode);  
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_bne_attribute;

  PROCEDURE delete_param_list(pn_appl_id         IN NUMBER,
                              pv_param_list_code in varchar2) is
    lcv_proc_name constant varchar2(30) := 'delete_param_list';
  begin
    log_msg(lcv_proc_name,'Deleting parameter list: '||pn_appl_id||':'||pv_param_list_code);
--    /*
--    || Clear out any attributes
--    */
--    FOR datarec IN (SELECT attribute_app_id, 
--                           attribute_code
--                    FROM   bne_param_list_items
--                    WHERE  application_id = pn_appl_id
--                    and    param_list_code = pv_param_list_code
--                    AND    nvl(attribute_app_id,-1) = 20003
--                    ORDER BY attribute_code DESC) loop
--      BEGIN
--        bne_attributes_pkg.delete_row(
--          x_application_id => datarec.attribute_app_id,
--          x_attribute_code => datarec.attribute_code);
--        log_msg(lcv_proc_name,'Deleted attribute: '||datarec.attribute_app_id||':'||datarec.attribute_code);
--      exception
--        WHEN no_data_found THEN
--          log_msg(lcv_proc_name,'Already deleted attribute: '||datarec.attribute_app_id||':'||datarec.attribute_code);
--      END;
--    end loop;
--    /*
--    || Clear out any parameter definitions
--    */
--    for datarec in (select bpd.application_id, 
--                           bpd.param_defn_code, 
--                           decode(val_type,
--                             4,substr(val_value,1,instr(val_value,':')-1),
--                             null) query_app_id,
--                           decode(val_type,
--                             4,substr(val_value,instr(val_value,':')+1),
--                             null) query_code
--                    from   bne_param_list_items bpli,
--                           bne_param_defns_b bpd
--                    where  bpli.application_id = pn_appl_id
--                    and    bpli.param_list_code = pv_param_list_code
--                    and    bpli.param_defn_app_id = bpd.application_id
--                    and    bpli.param_defn_code = bpd.param_defn_code) loop
--      if datarec.query_app_id = 20003 then
--        begin
--          bne_integrator_utils.delete_query(
--            x_application_id => datarec.query_app_id,
--            x_query_code     => datarec.query_code);
--          log_msg(lcv_proc_name,'Deleted simple query: '||datarec.query_app_id||':'||datarec.query_code);
--          
--          
--          
--        exception
--          WHEN no_data_found THEN
--            log_msg(lcv_proc_name,'Already deleted simple query: '||datarec.query_app_id||':'||datarec.query_code);
--        end;
--      end if;
--      begin
--        bne_param_defns_pkg.delete_row(
--          x_application_id  => datarec.application_id,
--          X_PARAM_DEFN_CODE => datarec.param_defn_code);
--        log_msg(lcv_proc_name,'Deleted parameter definition: '||datarec.application_id||':'||datarec.param_defn_code);
--      exception
--        when no_data_found then
--          log_msg(lcv_proc_name,'Already deleted parameter definition: '||datarec.application_id||':'||datarec.param_defn_code);
--      end;
--          
--    end loop;
    /*
    || Clear out any sub parameter lists
    */
    FOR datarec IN (SELECT substr(string_value,1,instr(string_value,':')-1) sub_param_app_id, 
                           substr(string_value,instr(string_value,':')+1) sub_param_list_code
                    FROM   bne_param_list_items
                    WHERE  application_id = pn_appl_id
                    AND    param_list_code = pv_param_list_code
                    AND    string_value LIKE '%:%'
                    AND    substr(string_value,1,instr(string_value,':')-1) = 20003
                    order by 2 desc) loop
      log_msg(lcv_proc_name,'Deleted '||to_char(
        bne_integrator_utils.delete_param_list
          (p_application_id  => datarec.sub_param_app_id,
           p_param_list_code => datarec.sub_param_list_code))||' parameter lists for: '||datarec.sub_param_app_id||':'||datarec.sub_param_list_code);
    end loop;
    /*
    || Delete parameter list
    */
    log_msg(lcv_proc_name,'Deleted '||to_char(
      bne_integrator_utils.delete_param_list
        (p_application_id  => pn_appl_id,
         p_param_list_code => pv_param_list_code))||' parameter lists for: '||pn_appl_id||':'||pv_param_list_code);
--    
--    
--    /*
--    || Could call bne_param_lists_items_pkg.delete_row but it would be 1 by 1
--    || for each entry, and there's no validation in that package anyway
--    */
--    DELETE FROM bne_param_list_items
--    WHERE application_id  = pn_appl_id
--    AND   param_list_code = pv_param_list_code;
--    log_msg(lcv_proc_name,'Deleted '||SQL%rowcount||' records for parameter list: '||pn_appl_id||':'||pv_param_list_code);
--    BEGIN
--      bne_param_lists_pkg.delete_row(
--        x_application_id  => pn_appl_id,
--        x_param_list_code => pv_param_list_code);
--      log_msg(lcv_proc_name,'Deleted parameter list: '||pn_appl_id||':'||pv_param_list_code);
--    exception
--      WHEN no_data_found THEN
--        log_msg(lcv_proc_name,'Already deleted parameter list: '||pn_appl_id||':'||pv_param_list_code);
--    END;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END delete_param_list;

  PROCEDURE delete_integrator(
    pr_intg IN out nocopy integrator_rec)
  is
    lcv_proc_name       constant varchar2(30) := 'delete_integrator';
    ln_dummy            NUMBER;
    lv_integrator_code  bne_integrators_b.integrator_code%type;
    CURSOR integrator_exists IS
      SELECT  decode(user_name,pr_intg.intg_name,'Y','N') name_matches,
              user_name current_name,
              import_param_list_app_id,
              import_param_list_code
      FROM    bne_integrators_vl
      WHERE   application_id = pr_intg.appl_id
      AND     integrator_code = lv_integrator_code;
      
    CURSOR prefix_exists IS
      select  integrator_code, 
              rtrim(ltrim(integrator_code,pr_intg.intg_prefix),'_INTG') id_num,
              count(integrator_code) over (order by integrator_code 
                                           rows between unbounded preceding 
                                                    and unbounded following) integrator_count
      from    bne_integrators_vl
      where   application_id = pr_intg.appl_id
      and     integrator_code like pr_intg.intg_prefix||'%_INTG';
    
    CURSOR contents_exists IS
      select bcb.application_id,
             bcb.content_code,
             bcb.param_list_app_id,
             bcb.param_list_code
      from   bne_contents_b bcb
      where  bcb.application_id = pr_intg.appl_id
      and    bcb.integrator_code = lv_integrator_code;
    
    prefix_rec          prefix_exists%rowtype;
    integrator_rec      integrator_exists%rowtype;
  begin
    
    IF pr_intg.appl_id IS NULL THEN
      pr_intg.appl_id := get_application_id(pr_intg.asn);
    END IF;

    log_heading(lcv_proc_name,'Deleting Old Integrator');
    if pr_intg.intg_prefix is not null and pr_intg.intg_code is not null then
      raise_application_error(-20001,'Please provide only one of INTG_PREFIX or INTG_CODE.');
    elsif pr_intg.intg_code is not null then
      lv_integrator_code := pr_intg.intg_code||'_INTG';
    elsif pr_intg.intg_prefix is not null then
      open prefix_exists;
      fetch prefix_exists into prefix_rec;
      if prefix_exists%notfound then
        close prefix_exists;
        log_msg(lcv_proc_name,'Found no Integrators with Prefix: '||pr_intg.intg_prefix);
        pr_intg.intg_code := pr_intg.intg_prefix||'1';
        lv_integrator_code := pr_intg.intg_code;
      else
        close prefix_exists;
        log_msg(lcv_proc_name,'Found '||prefix_rec.integrator_count||' integrators prefixed by '||pr_intg.intg_prefix);
        if prefix_rec.integrator_count = 1 then
          lv_integrator_code := prefix_rec.integrator_code;
          begin
            pr_intg.intg_code := pr_intg.intg_prefix||
                                   to_char(to_number(prefix_rec.id_num)+1);
          exception
            when value_error then
              log_msg(lcv_proc_name,'ID_NUM ('||prefix_rec.id_num||') derived from the INTG_PREFIX ('||pr_intg.intg_prefix||') is not a number');
              raise_application_error(-20001,'Unable to generate a new integrator code using prefix '||pr_intg.intg_prefix);
          end;
        else
          raise_application_error(-20001,'Too many integrators with prefix: '||pr_intg.intg_prefix);
        end if;
      end if;
    end if;
    --
    log_msg(lcv_proc_name, 'Deleting '||lv_integrator_code||' if it already exists');
    
    OPEN integrator_exists;
    fetch integrator_exists INTO integrator_rec;
    IF integrator_exists%found THEN
      CLOSE integrator_exists;
      IF integrator_rec.name_matches = 'N' THEN
        log_msg(lcv_proc_name,'Integrator '||lv_integrator_code||' exists but name doesn''t match');
        log_msg(lcv_proc_name,'Current Name '||integrator_rec.current_name);
        log_msg(lcv_proc_name,'Name passed to API '||pr_intg.intg_name);
      END IF;
      IF pr_intg.force_replace OR integrator_rec.name_matches = 'Y' THEN
        IF pr_intg.force_replace THEN
          log_msg(lcv_proc_name,'Force replace enabled - deleting anyway');
        END IF;
        
        if integrator_rec.import_param_list_app_id is not null then
          log_msg(lcv_proc_name,'Deleting the import parameter list for integrator: '||lv_integrator_code);
          delete_param_list(
            pn_appl_id         => integrator_rec.import_param_list_app_id,
            pv_param_list_code => integrator_rec.import_param_list_code);
        END IF;
        /*
        || Remove any contents
        */
        for content_codes in contents_exists loop
          if content_codes.param_list_app_id is not null then
            log_msg(lcv_proc_name,'Deleting contents parameter list for content_code: '||content_codes.content_code);
            delete_param_list(
              pn_appl_id         => content_codes.param_list_app_id,
              pv_param_list_code => content_codes.param_list_code);
          end if;
          log_msg(lcv_proc_name,'Deleted '||to_char(
            bne_integrator_utils.delete_content(
              p_application_id => content_codes.application_id,
              p_content_code   => content_codes.content_code))||' content for: '||content_codes.application_id||':'||content_codes.content_code);
        end loop;
        /*
        || Remove any layouts
        */
          log_msg(lcv_proc_name,'Deleted '||to_char(
            bne_integrator_utils.delete_all_layouts(
              P_APPLICATION_ID  => pr_intg.appl_id,
              P_INTEGRATOR_CODE => lv_integrator_code))||' layouts for: '||pr_intg.appl_id||':'||lv_integrator_code);
--        for layouts in (select application_id, layout_code
--                        from   bne_layouts_b
--                        where  integrator_app_id = pr_intg.appl_id
--                        and    integrator_code = lv_integrator_code) loop
--          /*
--          || Loop through the blocks and remove any columns associated with
--          || them
--          */
--          for layout_blocks in (select block_id
--                                from   bne_layout_blocks_b
--                                where  application_id = layouts.application_id
--                                and    layout_code = layouts.layout_code) loop
--            delete from BNE_LAYOUT_COLS
--            where  application_id = layouts.application_id
--            and    layout_code = layouts.layout_code
--            and    block_id = layout_blocks.block_id;
--            log_msg(lcv_proc_name,'Removed '||SQL%ROWCOUNT||' records from bne_layout_cols for ['||layouts.application_id||':'||layouts.layout_code||':'||layout_blocks.block_id||']');
--            bne_layout_blocks_pkg.delete_row(
--              x_application_id => layouts.application_id,
--              x_layout_code    => layouts.layout_code,
--              x_block_id       => layout_blocks.block_id);
--            log_msg(lcv_proc_name,'Removed ['||layouts.application_id||':'||layouts.layout_code||':'||layout_blocks.block_id||'] from bne_layout_blocks');
--          end loop;
--          /*
--          || Handle a bug that was introduced in the Auto KFF Fields code
--          */
--          delete from bne_layout_cols
--          where  application_id = layouts.application_id
--          and    layout_code = layouts.layout_code
--          and    block_id = 3;
--          /*
--          || Now remove the layout all together.
--          */            
--          bne_layouts_pkg.delete_row(
--            x_application_id => layouts.application_id,
--            x_layout_code    => layouts.layout_code);
--         log_msg(lcv_proc_name,'Removed layout ['||layouts.application_id||':'||layouts.layout_code||'] from bne_layouts');
--        end loop;

        ln_dummy := bne_integrator_utils.delete_integrator(
          p_application_id   => pr_intg.appl_id,
          p_integrator_code  => lv_integrator_code);
        log_msg(lcv_proc_name,'Deleted '||ln_dummy||' integrators');
        COMMIT;
      ELSE
        raise_application_error(-20001,'Integrator Name Mismatch and Force Replace is FALSE');
      END IF;
    ELSE
      log_msg(lcv_proc_name,'Integrator '||lv_integrator_code||' not found to delete.');
      CLOSE integrator_exists;
    end if;
    
    log_msg(lcv_proc_name,'Checking for orphan records for this integrator.');
    delete from bne_layout_cols blc
    where  application_id = pr_intg.appl_id
    and    layout_code like nvl(pr_intg.intg_prefix,pr_intg.intg_code)||'%'
    AND    not exists (select 'layout for this column' 
                       from   bne_layouts_b bl  
                       where  bl.application_id = blc.application_id 
                       and    bl.layout_code = blc.layout_code);
    log_msg(lcv_proc_name,'Deleted '||SQL%ROWCOUNT||' orphan records for this integrator.');

  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END delete_integrator;
  
  PROCEDURE load_sequence_numbers(
    pr_intf      IN out nocopy interface_rec,
    pan_seq_nums IN out nocopy seq_table) 
  IS
    lcv_proc_name constant varchar2(30) := 'load_sequence_numbers';
  BEGIN
    log_heading(lcv_proc_name,'Loading Sequence Numbers for '||pr_intf.appl_id||','||pr_intf.interface_code);
    FOR datarec IN (SELECT sequence_num, interface_col_name
                    FROM   bne_interface_cols_b
                    WHERE  application_id = pr_intf.appl_id
                    AND    interface_code = pr_intf.interface_code) loop
      pan_seq_nums(datarec.interface_col_name) := datarec.sequence_num;
      log_msg(lcv_proc_name,'Loaded seq number: '||datarec.sequence_num||' for column: '||datarec.interface_col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END load_sequence_numbers;

  PROCEDURE load_content_seq_num(
    pn_appl_id      IN NUMBER,
    pv_content_code IN bne_content_cols_b.content_code%TYPE,
    pan_seq_nums    IN out nocopy seq_table) 
  IS
    lcv_proc_name constant varchar2(30) := 'load_content_seq_num';
  BEGIN
    pan_seq_nums.delete;
    log_msg(lcv_proc_name,'Loading Content Sequence Numbers for '||pn_appl_id||','||pv_content_code);
    FOR datarec IN (SELECT sequence_num, col_name
                    FROM   bne_content_cols_b
                    WHERE  application_id = pn_appl_id
                    AND    content_code = pv_content_code) loop
      pan_seq_nums(datarec.col_name) := datarec.sequence_num;
      log_msg(lcv_proc_name,'Loaded seq number: '||datarec.sequence_num||' for column: '||datarec.col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END load_content_seq_num;
  
  PROCEDURE update_interface_columns(
    pr_intf       IN out nocopy interface_rec,
    pan_seq_nums  IN out nocopy seq_table,
    par_intf_cols IN out nocopy interface_col_table) 
  IS
    lcv_proc_name          constant varchar2(30) := 'update_interface_columns';
    lv_interface_col_name  bne_interface_cols_b.interface_col_name%TYPE;
    ln_sequence_num        bne_interface_cols_b.sequence_num%type;
    ln_index               number;
  BEGIN
    lv_interface_col_name := par_intf_cols.FIRST;
    while lv_interface_col_name IS NOT NULL loop
      IF pan_seq_nums.EXISTS(lv_interface_col_name) THEN
        field_validate(
          pv_field_name  => 'interface column '||lv_interface_col_name||' - required_flag',
          pv_field_val   => par_intf_cols(lv_interface_col_name).required_flag,
          pav_vals       => new valid_vals_table('Y','N'),
          pv_default_val => 'N');
        field_validate(
          pv_field_name  => 'interface column '||lv_interface_col_name||' - display_flag',
          pv_field_val   => par_intf_cols(lv_interface_col_name).display_flag,
          pav_vals       => new valid_vals_table('Y','N'),
          pv_default_val => 'Y');
        ln_index := par_intf_cols(lv_interface_col_name).layout_attr.first;
        while ln_index is not null loop
          field_validate(
            pv_field_name => 'interface column '||lv_interface_col_name||' layout '||ln_index||' - include_in_layout',
            pv_field_val  => par_intf_cols(lv_interface_col_name).layout_attr(ln_index).include_in_layout,
            pav_vals      => new valid_vals_table('Y','N'));
          field_validate(
            pv_field_name => 'interface column '||lv_interface_col_name||' layout '||ln_index||' - read_only_flag',
            pv_field_val  => par_intf_cols(lv_interface_col_name).layout_attr(ln_index).read_only_flag,
            pav_vals      => new valid_vals_table('Y','N'));
          field_validate(
            pv_field_name  => 'interface column '||lv_interface_col_name||' layout '||ln_index||' - display_meaning',
            pv_field_val   => par_intf_cols(lv_interface_col_name).layout_attr(ln_index).display_meaning,
            pav_vals       => NEW valid_vals_table('Y','N'),
            pv_default_val => 'N');
          ln_index := par_intf_cols(lv_interface_col_name).layout_attr.next(ln_index);
        end loop;
        ln_sequence_num := pan_seq_nums(lv_interface_col_name);
        UPDATE bne_interface_cols_tl
        SET    prompt_above = par_intf_cols(lv_interface_col_name).prompt,
               prompt_left = par_intf_cols(lv_interface_col_name).prompt,
               -- 11-Oct-2012 (WS): automatically prepend "* Required" if this
               -- is a required field
               user_hint = (
                 CASE
                   WHEN par_intf_cols(lv_interface_col_name).required_flag= 'Y' THEN
                     '* Required'||decode(par_intf_cols(lv_interface_col_name).user_hint, 
                       NULL, NULL, 
                       ' - ' || par_intf_cols(lv_interface_col_name).user_hint)
                   ELSE 
                     par_intf_cols(lv_interface_col_name).user_hint
                 END)
        WHERE  application_id = pr_intf.appl_id
        AND    interface_code = pr_intf.interface_code
        AND    language = 'US'
        AND    sequence_num = ln_sequence_num;
        log_msg(lcv_proc_name,'Updated column prompt for: '||lv_interface_col_name);  
        IF par_intf_cols(lv_interface_col_name).default_type IS NOT NULL OR
           par_intf_cols(lv_interface_col_name).default_value IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    default_type  = nvl(par_intf_cols(lv_interface_col_name).default_type,default_type),
                 default_value = nvl(par_intf_cols(lv_interface_col_name).default_value,default_value)
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Added default value for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).display_flag IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    display_flag  = par_intf_cols(lv_interface_col_name).display_flag
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated display flag for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).group_name IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    group_name  = par_intf_cols(lv_interface_col_name).group_name
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated group name for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).val_type IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    val_type  = par_intf_cols(lv_interface_col_name).val_type
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated val type for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).oa_concat_flex IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    oa_concat_flex  = par_intf_cols(lv_interface_col_name).oa_concat_flex
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated oa_concat_flex for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).required_flag IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    required_flag  = par_intf_cols(lv_interface_col_name).required_flag,
                 not_null_flag  = par_intf_cols(lv_interface_col_name).required_flag
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated required flag and not null flag for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).interface_col_type IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    interface_col_type  = par_intf_cols(lv_interface_col_name).interface_col_type
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated interface_col_type for: '||lv_interface_col_name);  
        END IF;
        IF par_intf_cols(lv_interface_col_name).field_size IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    field_size = par_intf_cols(lv_interface_col_name).field_size
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Updated field_size for: '||lv_interface_col_name);  
        end if;
        if par_intf_cols(lv_interface_col_name).rename_to is not null and
          lv_interface_col_name != par_intf_cols(lv_interface_col_name).rename_to
        THEN
          UPDATE bne_interface_cols_b
          SET    interface_col_name = par_intf_cols(lv_interface_col_name).rename_to
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          log_msg(lcv_proc_name,'Renamed : '||lv_interface_col_name||' to be '||par_intf_cols(lv_interface_col_name).rename_to);  
          par_intf_cols(par_intf_cols(lv_interface_col_name).rename_to) := par_intf_cols(lv_interface_col_name);
          log_msg(lcv_proc_name,'Copied par_intf_cols record to new location');  
          par_intf_cols.DELETE(lv_interface_col_name);
          log_msg(lcv_proc_name,'Removed original entry in par_intf_cols');  
        END IF;
      ELSE
        log_msg(lcv_proc_name,'WARNING [update_interface_columns]: Interface Column Name: '||lv_interface_col_name||' not found in this interface!?');  
      END IF;
      lv_interface_col_name := par_intf_cols.NEXT(lv_interface_col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END update_interface_columns;

  PROCEDURE add_table_lovs(
    pr_intf        IN out nocopy interface_rec,
    pan_seq_nums   IN out nocopy seq_table,
    par_table_lovs in out nocopy table_lov_table) 
  IS
    lcv_proc_name         constant varchar2(30) := 'add_table_lovs';
    lv_interface_col_name bne_interface_cols_b.interface_col_name%type;
    ln_sequence_num       bne_interface_cols_b.sequence_num%TYPE;
  begin
    log_msg(lcv_proc_name,'Associating '||par_table_lovs.count||' Table Validators');
    lv_interface_col_name := par_table_lovs.FIRST;
    while lv_interface_col_name IS NOT NULL loop
      IF pan_seq_nums.EXISTS(lv_interface_col_name) THEN
        field_validate(
          pv_field_name  => 'Table Lov '||lv_interface_col_name||' - poplist_flag',
          pv_field_val   => par_table_lovs(lv_interface_col_name).poplist_flag,
          pav_vals       => new valid_vals_table('Y','N'),
          pv_default_val => 'N');      
      
        ln_sequence_num := pan_seq_nums(lv_interface_col_name);
        bne_integrator_utils.create_table_lov(
          p_application_id     => pr_intf.appl_id,
          p_interface_code     => pr_intf.interface_code,
          p_interface_col_name => lv_interface_col_name,
          p_id_col             => par_table_lovs(lv_interface_col_name).id_col,
          p_mean_col           => par_table_lovs(lv_interface_col_name).mean_col,
          p_desc_col           => par_table_lovs(lv_interface_col_name).desc_col,
          p_table              => par_table_lovs(lv_interface_col_name).table_name,
          p_addl_w_c           => par_table_lovs(lv_interface_col_name).addl_w_c,
          p_window_caption     => par_table_lovs(lv_interface_col_name).window_caption,
          p_window_width       => par_table_lovs(lv_interface_col_name).window_width,
          p_window_height      => par_table_lovs(lv_interface_col_name).window_height,
          p_table_block_size   => par_table_lovs(lv_interface_col_name).table_block_size,
          p_table_sort_order   => par_table_lovs(lv_interface_col_name).table_sort_order,
          p_table_column_alias => par_table_lovs(lv_interface_col_name).table_column_alias,
          p_user_id            => pr_intf.user_id,
          p_poplist_flag       => par_table_lovs(lv_interface_col_name).poplist_flag);   
          log_msg(lcv_proc_name,'Created Table LOV for column: '||lv_interface_col_name);
      ELSE
        log_msg(lcv_proc_name,'WARNING [add_table_lovs]: Interface Column Name: '||lv_interface_col_name||' not found in this interface!?');  
      END IF;
      lv_interface_col_name := par_table_lovs.NEXT(lv_interface_col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END add_table_lovs;  
  
  PROCEDURE create_param_list_item(
    pv_created_by        IN VARCHAR2,
    pv_param_list_asn    IN VARCHAR2,
    pv_param_list_code   IN VARCHAR2,
    pn_seq_num           in out nocopy number,
    pv_param_name        in varchar2 DEFAULT NULL,
    pv_string_value      IN VARCHAR2 DEFAULT NULL,
    pv_desc_value        IN VARCHAR2 DEFAULT NULL,
    pv_param_defn_asn    IN VARCHAR2 DEFAULT NULL,
    pv_param_defn_code   IN VARCHAR2 DEFAULT NULL,
    pv_attrb_defn_asn    IN VARCHAR2 DEFAULT NULL,
    pv_attrb_defn_code   in varchar2 default null,
    pb_increment_seq     IN boolean DEFAULT TRUE
    )
  IS
    lcv_proc_name      constant varchar2(30) := 'create_param_list_item';  
  begin
    log_msg(lcv_proc_name,'Creating seq '||pn_seq_num||' in Param List ['||pv_param_list_asn||':'||pv_param_list_code||']');
    log_msg(lcv_proc_name,'  Param Defn ['||pv_param_defn_asn||':'||pv_param_defn_code||'] Param Name '''||pv_param_name||'''');
    log_msg(lcv_proc_name,'  Attribute ['||pv_attrb_defn_asn||':'||pv_attrb_defn_code||'] String '''||pv_string_value||'''');

    bne_param_list_items_pkg.load_row (
      x_param_list_asn        => pv_param_list_asn,
      x_param_list_code       => pv_param_list_code,
      x_sequence_num          => pn_seq_num,
      x_object_version_number => 1,
      x_param_defn_asn        => pv_param_defn_asn,
      x_param_defn_code       => pv_param_defn_code,
      x_param_name            => pv_param_name,
      x_attribute_asn         => pv_attrb_defn_asn,
      x_attribute_code        => pv_attrb_defn_code,
      x_string_value          => pv_string_value,
      x_date_value            => NULL,
      x_number_value          => NULL,
      x_boolean_value_flag    => NULL,
      x_formula_value         => NULL,
      x_desc_value            => pv_desc_value,
      x_owner                 => pv_created_by, 
      x_last_update_date      => to_char(SYSDATE,gcv_date_format),
      x_custom_mode           => 'FORCE');
    IF pb_increment_seq THEN
      pn_seq_num := pn_seq_num + 1;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_param_list_item;  

  procedure ADD_JAVA_LOVS(
    pv_created_by_user in varchar2,
    pr_intf            IN out nocopy interface_rec,
    pan_seq_nums       IN out nocopy seq_table,
    par_java_lovs      IN out nocopy java_lov_table) 
  IS
    lcv_proc_name         constant varchar2(30) := 'add_java_lovs';
    lv_interface_col_name bne_interface_cols_b.interface_col_name%type;
    ln_sequence_num       bne_interface_cols_b.sequence_num%TYPE;
  begin
    log_msg(lcv_proc_name,'Associating '||par_java_lovs.count||' Java Validators');
    lv_interface_col_name := par_java_lovs.FIRST;
    while lv_interface_col_name IS NOT NULL loop
      IF pan_seq_nums.EXISTS(lv_interface_col_name) THEN
        ln_sequence_num := pan_seq_nums(lv_interface_col_name);
        IF par_java_lovs(lv_interface_col_name).component_class IS NOT NULL THEN
          bne_integrator_utils.create_java_lov(
            p_application_id       => pr_intf.appl_id,
            p_interface_code       => pr_intf.interface_code,
            p_interface_col_name   => lv_interface_col_name,
            p_java_class           => par_java_lovs(lv_interface_col_name).component_class,
            p_window_caption       => par_java_lovs(lv_interface_col_name).window_caption,
            p_window_width         => par_java_lovs(lv_interface_col_name).window_width,
            p_window_height        => par_java_lovs(lv_interface_col_name).window_height,
            p_table_block_size     => par_java_lovs(lv_interface_col_name).table_block_size,
            p_table_columns        => par_java_lovs(lv_interface_col_name).table_columns,
            p_table_select_columns => par_java_lovs(lv_interface_col_name).table_select_columns,
            p_table_column_alias   => par_java_lovs(lv_interface_col_name).table_column_alias,
            p_table_headers        => par_java_lovs(lv_interface_col_name).table_headers,
            p_table_sort_order     => par_java_lovs(lv_interface_col_name).table_sort_order,
            p_user_id              => pr_intf.user_id);         
          LOG_MSG(LCV_PROC_NAME,'Created Java LOV for column: '||LV_INTERFACE_COL_NAME);
          
          if PAR_JAVA_LOVS(LV_INTERFACE_COL_NAME).TABLE_HIDDEN_COLUMNS is not null then
            declare
              LV_PARAM_LIST_ASN      FND_APPLICATION.APPLICATION_SHORT_NAME%type;
              LV_PARAM_LIST_CODE     BNE_PARAM_LIST_ITEMS.PARAM_LIST_CODE%type;
              ln_tch_seq_num         bne_param_list_items.sequence_num%type;
            begin
              select FA.APPLICATION_SHORT_NAME, 
                     BC.PARAM_LIST_CODE, 
                     max(BPLI.SEQUENCE_NUM)+1 TCH_SEQ_NUM
              into   LV_PARAM_LIST_ASN,
                     LV_PARAM_LIST_CODE,
                     ln_tch_seq_num
              from   BNE_INTERFACE_COLS_B BIC,
                     BNE_COMPONENTS_B BC,
                     BNE_PARAM_LIST_ITEMS BPLI,
                     FND_APPLICATION FA
              where  BIC.APPLICATION_ID = PR_INTF.APPL_ID
              and    bic.interface_code = pr_intf.interface_code
              and    BIC.interface_col_name = lv_interface_col_name
              and    bic.val_component_app_id = bc.application_id
              and    BIC.VAL_COMPONENT_CODE = BC.COMPONENT_CODE
              and    BC.PARAM_LIST_APP_ID = BPLI.APPLICATION_ID
              and    BC.PARAM_LIST_CODE = BPLI.PARAM_LIST_CODE
              and    BC.PARAM_LIST_APP_ID = FA.APPLICATION_ID
              group by fa.application_short_name, BC.PARAM_LIST_CODE;
  
              CREATE_PARAM_LIST_ITEM(
                PV_CREATED_BY        => pv_created_by_user,
                PV_PARAM_LIST_ASN    => LV_PARAM_LIST_ASN,
                pv_param_list_code   => LV_PARAM_LIST_CODE,
                pn_seq_num           => ln_tch_seq_num,
                PV_PARAM_NAME        => 'table-column-hidden',
                PV_STRING_VALUE      => PAR_JAVA_LOVS(LV_INTERFACE_COL_NAME).TABLE_HIDDEN_COLUMNS,
                pv_desc_value        => 'Table Hidden Columns',
                pb_increment_seq     => FALSE);
            end;
          end if;
        END IF;

        IF par_java_lovs(lv_interface_col_name).validator_class IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    val_type = 'JAVA',
                 val_obj_name = par_java_lovs(lv_interface_col_name).validator_class,
                 offline_lov_enabled_flag = 'N'
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          IF SQL%rowcount = 1 THEN
            log_msg(lcv_proc_name,'Updated Java Validator for column: '||lv_interface_col_name);
          ELSE
            log_msg(lcv_proc_name,'WARNING [add_java_lovs]: Failed to update validator class for Interface Column Name: '||lv_interface_col_name);  
          END IF;
        END IF;
        
        IF par_java_lovs(lv_interface_col_name).val_component_app_id IS NOT NULL AND
           par_java_lovs(lv_interface_col_name).val_component_code IS NOT NULL THEN
          UPDATE bne_interface_cols_b
          SET    val_component_app_id = par_java_lovs(lv_interface_col_name).val_component_app_id
               , val_component_code = par_java_lovs(lv_interface_col_name).val_component_code
               , lov_type = case when par_java_lovs(lv_interface_col_name).component_class IS NULL then 'NONE' else lov_type end
          WHERE  application_id = pr_intf.appl_id
          AND    interface_code = pr_intf.interface_code
          AND    sequence_num = ln_sequence_num;
          IF SQL%rowcount = 1 THEN
            log_msg(lcv_proc_name,'Updated Java Component for column: '||lv_interface_col_name);
          ELSE
            log_msg(lcv_proc_name,'WARNING [add_java_lovs]: Failed to update validator component for Interface Column Name: '||lv_interface_col_name);  
          END IF;
        END IF;
        
        IF par_java_lovs(lv_interface_col_name).long_list THEN
          /*
          || Documentation says this should make it happen.  Java class
          || BneAbstractListOfValues even seems to make reference to this
          || parameter but sadly it doesn't seem to have any effect :(
          */
          DECLARE
            ln_param_app_id      bne_components_b.param_list_app_id%TYPE;
            lv_param_list_code   bne_components_b.param_list_code%TYPE;
            ln_seq_num           bne_param_list_items.sequence_num%TYPE;
          BEGIN
            SELECT bc.param_list_app_id,
                   bc.param_list_code
            INTO   ln_param_app_id,
                   lv_param_list_code
            FROM   bne_interface_cols_b bic,
                   bne_components_b bc
            WHERE  bic.application_id = pr_intf.appl_id
            AND    bic.interface_code = pr_intf.interface_code
            AND    bic.interface_col_name = lv_interface_col_name
            AND    bic.val_component_app_id = bc.application_id
            AND    bic.val_component_code = bc.component_code;
            log_msg(lcv_proc_name,'Creating table-long-list list item for '||ln_param_app_id||':'||lv_param_list_code);
            ln_seq_num := bne_parameter_utils.create_list_items_all   (
              p_application_id      => ln_param_app_id,
              p_param_list_code     => lv_param_list_code,
              p_param_defn_app_id   => NULL,
              p_param_defn_code     => NULL,
              p_param_name          => 'table-long-list',
              p_attribute_app_id    => NULL,
              p_attribute_code      => NULL,
              p_string_val          => 'true',
              p_date_val            => NULL,
              p_number_val          => NULL,
              p_boolean_val         => NULL,
              p_formula             => NULL,
              p_desc_val            => 'true');
            log_msg(lcv_proc_name,'Added "long list" list item.');
          END;
        END IF;
      else
--        log_msg(lcv_proc_name,'WARNING [add_java_lovs]: Interface Column Name: '||lv_interface_col_name||' not found in this interface!?');  
        raise_application_error(-20001,'Java LOV entry exists for '||LV_INTERFACE_COL_NAME||' but this is not in par_intf_cols');                
      END IF;
      lv_interface_col_name := par_java_lovs.NEXT(lv_interface_col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END add_java_lovs;  

  PROCEDURE create_oa_flex(
    pn_application_id IN NUMBER) 
  is
    lcv_proc_name     constant varchar2(30) := 'create_oa_flex';
    lcd_creation_date constant DATE := to_date('01-JAN-1950','DD-MON-YYYY');
  BEGIN
    /*
    || Create component if required
    */
    INSERT INTO bne_components_b(
      application_id,
      component_code,
      object_version_number,
      component_java_class,
      param_list_app_id,
      param_list_code,
      created_by,
      creation_date,
      last_updated_by,
      last_update_login,
      last_update_date)
    SELECT pn_application_id                                         application_id,
           'OA_FLEX'                                                 component_code,
           1                                                         object_version_number,
           'oracle.apps.bne.integrator.component.BneOAFlexComponent' component_java_class,
           NULL                                                      param_list_app_id,
           NULL                                                      param_list_code,
           2                                                         created_by,
           lcd_creation_date                                         creation_date,
           2                                                         last_updated_by,
           0                                                         last_update_login,
           SYSDATE                                                   last_update_date
    FROM   sys.dual
    WHERE  NOT EXISTS (SELECT 'already created'
                      FROM   bne_components_b
                      WHERE  application_id = pn_application_id
                      AND    component_code = 'OA_FLEX');
    IF SQL%rowcount = 1 THEN
      log_msg(lcv_proc_name,'Created OA_FLEX component in application_id: '||pn_application_id);
    ELSE
      log_msg(lcv_proc_name,'OA_FLEX component already exists for application_id: '||pn_application_id);
    END IF;
    /*
    || Create matching TL table entry if required
    */
    INSERT INTO bne_components_tl(
      application_id,
      component_code,
      language,
      source_lang,
      user_name,
      created_by,
      creation_date,
      last_updated_by,
      last_update_login,
      last_update_date)
    SELECT pn_application_id                                         application_id,
           'OA_FLEX'                                                 component_code,
           'US'                                                      language,
           'US'                                                      source_lang,
           application_short_name||' OA_FLEX Component'              user_name,
           2                                                         created_by,
           lcd_creation_date                                         creation_date,
           2                                                         last_updated_by,
           0                                                         last_update_login,
           SYSDATE                                                   last_update_date
    FROM   fnd_application
    WHERE  application_id = pn_application_id
    AND    NOT EXISTS  ( SELECT 'x'
                         FROM   bne_components_tl b
                         WHERE  b.application_id = pn_application_id
                         AND    b.component_code = 'OA_FLEX'
                         AND    b.language       = 'US');    
    IF SQL%rowcount = 1 THEN
      log_msg(lcv_proc_name,'Created OA_FLEX component translation in application_id: '||pn_application_id);
    ELSE
      log_msg(lcv_proc_name,'OA_FLEX component translation already exists for application_id: '||pn_application_id);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_oa_flex;
  
  PROCEDURE add_dff_lovs(
    pr_intf        IN out nocopy interface_rec,
    par_dff_lovs   IN out nocopy dff_lov_table) 
  IS
    lcv_proc_name     constant varchar2(30) := 'add_dff_lovs';
    ln_index          NUMBER;
    ln_application_id NUMBER;
    cursor component(
      pv_group_name IN VARCHAR2) 
    IS
      SELECT val_component_app_id,
             val_component_code
      FROM   bne_interface_cols_b
      WHERE  interface_code = pr_intf.interface_code
      AND    group_name = pv_group_name
      and    val_type = 'DESCFLEX';
    cursor context_validation(
      pv_context_col in VARCHAR2) 
    is
      select val_type, 
             val_component_app_id, 
             val_component_code, 
             val_obj_name,
             lov_type, 
             offline_lov_enabled_flag
      from   bne_interface_cols_b
      where  interface_code = pr_intf.interface_code
      and    interface_col_name = pv_context_col;    
    lr_component_rec           component%rowtype;
    lr_context_validation_rec  context_validation%rowtype;
  BEGIN
    log_msg(lcv_proc_name,'Creating '||par_dff_lovs.count||' DFFs');
    ln_index := par_dff_lovs.FIRST;
    while ln_index IS NOT NULL loop
      
      field_validate(
        pv_field_name => 'DFF Lov '||ln_index||' - required_flag',
        pv_field_val  => par_dff_lovs(ln_index).required_flag,
        pav_vals      => new valid_vals_table('Y','N'));
      /*
      || Check if we created a table LOV on the context field earlier.
      || If we did, capture the various fields this API will overwrite, so we
      || can restore them after
      */
      if par_dff_lovs(ln_index).context_col_name is not null then
        log_msg(lcv_proc_name,'Getting existing validation details for context column: '||par_dff_lovs(ln_index).context_col_name);
        open context_validation(
          pv_context_col => par_dff_lovs(ln_index).context_col_name);
        fetch context_validation into lr_context_validation_rec;
        if context_validation%notfound then
          close context_validation;
          raise_application_error(-20001,'Unable to find context column: '||par_dff_lovs(ln_index).context_col_name||' in bne_interface_cols_b ['||pr_intf.interface_code||']');
        else
          close context_validation;
          log_msg(lcv_proc_name,'val_type                 '||lr_context_validation_rec.val_type);
          log_msg(lcv_proc_name,'val_component_app_id     '||lr_context_validation_rec.val_component_app_id);
          log_msg(lcv_proc_name,'val_component_code       '||lr_context_validation_rec.val_component_code);
          log_msg(lcv_proc_name,'lov_type                 '||lr_context_validation_rec.lov_type);
          log_msg(lcv_proc_name,'offline_lov_enabled_flag '||lr_context_validation_rec.offline_lov_enabled_flag);
        end if;
      else
        lr_context_validation_rec := null;
      end if;

      log_msg(lcv_proc_name,'Calling bne_integrator_utils.create_dff for group: '||par_dff_lovs(ln_index).group_name);
      bne_integrator_utils.create_dff                                   
      ( p_application_id            => pr_intf.appl_id,
        p_interface_code            => pr_intf.interface_code,
        p_flex_seg_col_name_prefix  => par_dff_lovs(ln_index).flex_seg_col_name_prefix,
        p_context_col_name          => par_dff_lovs(ln_index).context_col_name,
        p_group_name                => par_dff_lovs(ln_index).group_name,
        p_required_flag             => par_dff_lovs(ln_index).required_flag,         
        p_flex_application_id       => par_dff_lovs(ln_index).flex_application_id,              
        p_flex_code                 => par_dff_lovs(ln_index).flex_code,
        p_vrule                     => par_dff_lovs(ln_index).vrule,                   
        p_effective_date_col        => par_dff_lovs(ln_index).effective_date_col,          
        p_prompt_above              => par_dff_lovs(ln_index).prompt_above, 
        p_prompt_left               => par_dff_lovs(ln_index).prompt_left,
        p_user_hint                 => par_dff_lovs(ln_index).user_hint,     
        p_user_id                   => pr_intf.user_id);   
      log_msg(lcv_proc_name,'Created DFF LOV for DFF: '||par_dff_lovs(ln_index).flex_code);
      /*
      || Get the application_id of the DFF
      */
      SELECT application_id
      INTO   ln_application_id
      FROM   fnd_descriptive_flexs_vl
      WHERE  descriptive_flexfield_name = par_dff_lovs(ln_index).flex_code;
      /*
      || Ensure there is an OA_FLEX component for this application
      */
      create_oa_flex(ln_application_id);
      /*
      || Delete the component the API created
      */
      OPEN component(par_dff_lovs(ln_index).group_name);
      fetch component INTO lr_component_rec;
      IF component%found THEN
        CLOSE component;
        IF bne_integrator_utils.delete_component(
          p_application_id => lr_component_rec.val_component_app_id,
          p_component_code => lr_component_rec.val_component_code) = 1 THEN
          log_msg(lcv_proc_name,'Successfully removed component '||lr_component_rec.val_component_code);
        ELSE
          log_msg(lcv_proc_name,'WARNING: Failed to remove component '||lr_component_rec.val_component_code);        
        END IF;
      ELSE
        CLOSE component;
      END IF;
      /*
      || Update the LOV_TYPE and OFFLINE_LOV_ENABLED and component fields
      */
      UPDATE bne_interface_cols_b
      set    lov_type=case
                      when val_type = 'DESCFLEX' then
                        null--'STANDARD'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.lov_type is null then
                        null--'STANDARD'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.lov_type is not null then
                        lr_context_validation_rec.lov_type 
                      when val_type = 'DESCFLEXSEG' then
                        'NONE'
                      else
                        lov_type
                      end,
             oa_concat_flex=
                      case
                      when val_type = 'DESCFLEXCONTEXT' then
                        'N'
                      else
                        oa_concat_flex
                      end,
             offline_lov_enabled_flag=
                      case
                      when val_type = 'DESCFLEX' then
                        null--'Y'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.offline_lov_enabled_flag is null then
                        null--'Y'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.offline_lov_enabled_flag is not null then
                        lr_context_validation_rec.offline_lov_enabled_flag
                      when val_type = 'DESCFLEXSEG' then
                        'N'
                      else
                        offline_lov_enabled_flag
                      end,
             val_component_app_id = 
                      case
                      when val_type = 'DESCFLEX' then
                        ln_application_id
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_component_app_id is null then
                        ln_application_id
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_component_app_id is not null then
                        lr_context_validation_rec.val_component_app_id
                      when val_type = 'DESCFLEXSEG' then
                        null
                      else
                        val_component_app_id
                      end,
             val_component_code = 
                      case
                      when val_type = 'DESCFLEX' then
                        'OA_FLEX'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_component_code is null then
                        'OA_FLEX'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_component_code is not null then
                        lr_context_validation_rec.val_component_code
                      when val_type = 'DESCFLEXSEG' then
                        null
                      else
                        val_component_code
                      end,
             val_obj_name = 
                      case
                      when val_type = 'DESCFLEX' then
                        'oracle.apps.bne.integrator.validators.BneDFFValidator'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_obj_name is null then
                        'oracle.apps.bne.integrator.validators.BneDFFValidator'
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_obj_name is not null then
                        lr_context_validation_rec.val_obj_name
                      else
                        val_obj_name
                      end,
             val_type = 
                      case
                      when val_type = 'DESCFLEXCONTEXT' and
                        lr_context_validation_rec.val_type is not null then
                        lr_context_validation_rec.val_type
                      else
                        val_type
                      end
      WHERE  interface_code = pr_intf.interface_code
      AND    group_name = par_dff_lovs(ln_index).group_name
      and    VAL_TYPE in ('DESCFLEX','DESCFLEXCONTEXT','DESCFLEXSEG');

      log_msg(lcv_proc_name,'Fixed DFF LOV_TYPE for DFF: '||par_dff_lovs(ln_index).flex_code||' for '||sql%rowcount||' columns');
      ln_index := par_dff_lovs.NEXT(ln_index);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END add_dff_lovs;
  
  PROCEDURE add_kff_lovs(
    pr_intf        IN out nocopy interface_rec,
    pan_seq_nums   IN out nocopy seq_table,
    par_kff_lovs   IN out nocopy kff_lov_table) 
  IS
    lcv_proc_name     constant varchar2(30) := 'add_kff_lovs';  
    lv_interface_col_name bne_interface_cols_b.interface_col_name%TYPE;
    ln_sequence_num       bne_interface_cols_b.sequence_num%TYPE;
  BEGIN
    log_msg(lcv_proc_name,'Creating '||par_kff_lovs.count||' KFFs');
    lv_interface_col_name := par_kff_lovs.FIRST;
    while lv_interface_col_name IS NOT NULL loop
      IF pan_seq_nums.EXISTS(lv_interface_col_name) THEN
        ln_sequence_num := pan_seq_nums(lv_interface_col_name);
        UPDATE bne_interface_cols_b
        SET    display_flag = 'Y',
               interface_col_type = 1,
               group_name=par_kff_lovs(lv_interface_col_name).group_name,
               val_type='KEYFLEXID',
               val_obj_name='oracle.apps.bne.integrator.validators.BneKFFValidator',
               val_component_code='OA_FLEX',
               val_component_app_id=231, -- BNE
               oa_flex_code=par_kff_lovs(lv_interface_col_name).oa_flex_code,
               oa_flex_num=par_kff_lovs(lv_interface_col_name).oa_flex_num,
               oa_concat_flex='N',
               oa_flex_application_id=par_kff_lovs(lv_interface_col_name).oa_flex_application_id,
               upload_param_list_item_num=sequence_num 
        WHERE  application_id = pr_intf.appl_id
        AND    interface_code = pr_intf.interface_code
        AND    interface_col_name = lv_interface_col_name;        
        log_msg(lcv_proc_name,'Added KFF LOV for column: '||lv_interface_col_name);
      ELSE
        log_msg(lcv_proc_name,'WARNING [add_kff_lovs]: Interface Column Name: '||lv_interface_col_name||' not found in this interface!?');  
      END IF;
      lv_interface_col_name := par_kff_lovs.NEXT(lv_interface_col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END add_kff_lovs;  

  PROCEDURE add_calendar_lovs(
    pr_intf        in out nocopy interface_rec,
    pan_seq_nums   IN out nocopy seq_table,
    pav_clndr_cols IN out nocopy calendar_headings_table) 
  IS
    lcv_proc_name         constant varchar2(30) := 'add_calendar_lovs';  
    lv_interface_col_name bne_interface_cols_b.interface_col_name%TYPE;
    ln_sequence_num       bne_interface_cols_b.sequence_num%TYPE;
  BEGIN
    log_msg(lcv_proc_name,'Associating '||pav_clndr_cols.count||' Calendar LOVs');
    lv_interface_col_name := pav_clndr_cols.FIRST;
    while lv_interface_col_name IS NOT NULL loop
      IF pan_seq_nums.EXISTS(lv_interface_col_name) THEN
        ln_sequence_num := pan_seq_nums(lv_interface_col_name);
        bne_integrator_utils.create_calendar_lov(
          p_application_id       => pr_intf.appl_id,
          p_interface_code       => pr_intf.interface_code,
          p_interface_col_name   => lv_interface_col_name,
          p_window_caption       => pav_clndr_cols(lv_interface_col_name),
          p_window_width         => 250,
          p_window_height        => 200,
          p_table_columns        => NULL,
          p_user_id              => pr_intf.user_id);
        log_msg(lcv_proc_name,'Created Calendar LOV for column: '||lv_interface_col_name);
      ELSE
        log_msg(lcv_proc_name,'WARNING [add_calendar_lovs]: Interface Column Name: '||lv_interface_col_name||' not found in this interface!?');  
      END IF;
      lv_interface_col_name := pav_clndr_cols.NEXT(lv_interface_col_name);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END add_calendar_lovs;  
   
  PROCEDURE create_dummy_columns(
    pr_intf        IN out nocopy interface_rec,
    par_dummy_cols IN out nocopy dummy_cols_table) 
  is
    lcv_proc_name    constant varchar2(30) := 'create_dummy_columns';  
    ln_index         NUMBER;
    ln_sequence_num  number;    
    ln_display_order NUMBER;  
  begin
    log_heading(lcv_proc_name,'Creating interface columns for '||par_dummy_cols.count||' dummy columns');
    ln_index := par_dummy_cols.FIRST;
    while ln_index is not null loop
      select max(sequence_num)+1, 
             MAX(display_order)+1
      INTO   ln_sequence_num,
             ln_display_order
      FROM   bne_interface_cols_b
      WHERE  application_id = pr_intf.appl_id
      AND    interface_code = pr_intf.interface_code;
--      ln_sequence_num  := round(ln_sequence_num + 10,-1);
--      ln_display_order := round(ln_display_order + 10,-1);      
      bne_integrator_utils.upsert_interface_column
       (p_application_id             => pr_intf.appl_id,
        p_interface_code             => pr_intf.interface_code,
        p_sequence_num               => ln_sequence_num,
        p_interface_col_type         => 2, -- in ADI, not in interface
        p_interface_col_name         => par_dummy_cols(ln_index).interface_col_name,
        p_enabled_flag               => par_dummy_cols(ln_index).enabled_flag,
        p_required_flag              => par_dummy_cols(ln_index).required_flag,
        p_display_flag               => par_dummy_cols(ln_index).display_flag,
        p_field_size                 => par_dummy_cols(ln_index).field_size,
        p_default_type               => par_dummy_cols(ln_index).default_type,
        p_default_value              => par_dummy_cols(ln_index).default_value,
        p_segment_number             => par_dummy_cols(ln_index).segment_number,
        p_group_name                 => par_dummy_cols(ln_index).group_name,
        p_oa_flex_code               => par_dummy_cols(ln_index).oa_flex_code,
        p_oa_concat_flex             => par_dummy_cols(ln_index).oa_concat_flex,
        p_read_only_flag             => par_dummy_cols(ln_index).read_only_flag,
        p_val_type                   => par_dummy_cols(ln_index).val_type,
        p_val_id_col                 => par_dummy_cols(ln_index).val_id_col,
        p_val_mean_col               => par_dummy_cols(ln_index).val_mean_col,
        p_val_desc_col               => par_dummy_cols(ln_index).val_desc_col,
        p_val_obj_name               => par_dummy_cols(ln_index).val_obj_name,
        p_val_addl_w_c               => par_dummy_cols(ln_index).val_addl_w_c,
        p_data_type                  => par_dummy_cols(ln_index).data_type,
        p_not_null_flag              => par_dummy_cols(ln_index).not_null_flag,
        p_val_component_app_id       => par_dummy_cols(ln_index).val_component_app_id,
        p_val_component_code         => par_dummy_cols(ln_index).val_component_code,
        p_summary_flag               => par_dummy_cols(ln_index).summary_flag,
        p_mapping_enabled_flag       => par_dummy_cols(ln_index).mapping_enabled_flag,
        p_prompt_left                => par_dummy_cols(ln_index).prompt_left,
        p_prompt_above               => par_dummy_cols(ln_index).prompt_above,
        p_user_hint                  => par_dummy_cols(ln_index).user_hint,
        p_user_help_text             => par_dummy_cols(ln_index).user_help_text,
        p_language                   => 'US',
        p_source_lang                => 'US',
        p_oa_flex_num                => par_dummy_cols(ln_index).oa_flex_num,
        p_oa_flex_application_id     => par_dummy_cols(ln_index).oa_flex_application_id,
        p_display_order              => ln_display_order,
        p_upload_param_list_item_num => par_dummy_cols(ln_index).upload_param_list_item_num,
        p_expanded_sql_query         => par_dummy_cols(ln_index).expanded_sql_query,
        p_lov_type                   => par_dummy_cols(ln_index).lov_type,
        p_offline_lov_enabled_flag   => par_dummy_cols(ln_index).offline_lov_enabled_flag,
        p_variable_data_type_class   => par_dummy_cols(ln_index).variable_data_type_class,
        p_user_id                    => pr_intf.user_id);
      log_msg(lcv_proc_name,'Created Dummy Interface Column: '||par_dummy_cols(ln_index).interface_col_name||'('||ln_sequence_num||','||ln_display_order||')');
      ln_index := par_dummy_cols.NEXT(ln_index);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_dummy_columns;

  PROCEDURE update_layout_column(
    pn_appl_id      IN NUMBER,
    pv_layout_code  IN VARCHAR2,
    pn_seq_num      IN NUMBER,
    pv_column_name  IN VARCHAR2,
    pr_layout_attr IN OUT NOCOPY layout_col_attr)
  IS
    lcv_proc_name    constant varchar2(30) := 'update_layout_column';  
    ln_block_dummy   number;
  BEGIN
    if pr_layout_attr.include_in_layout = 'Y' then 
    
      UPDATE bne_layout_cols
      SET    read_only_flag = nvl(pr_layout_attr.read_only_flag,'N'),
             block_id       = nvl(pr_layout_attr.internal_block_id,block_id),
             default_type   = decode(pr_layout_attr.default_type,
                                chr(0), NULL,
                                pr_layout_attr.default_type),
             default_value  = decode(pr_layout_attr.default_value,
                                chr(0), NULL,
                                pr_layout_attr.default_value),
             display_width  = nvl(pr_layout_attr.display_width,display_width)
      WHERE  application_id = pn_appl_id
      AND    layout_code = pv_layout_code
      and    interface_seq_num = pn_seq_num
      RETURNING block_id INTO ln_block_dummy;
      if sql%rowcount = 1 then
        log_msg(lcv_proc_name,'Updated layout attributes for '||pv_column_name||'['||pn_appl_id||':'||pv_layout_code||':'||pn_seq_num||'] - (Block ID: '||ln_block_dummy||')');
      ELSE
        log_msg(lcv_proc_name,'WARNING: Failed to update '||pv_column_name);
      END IF;  
    ELSE
      DELETE FROM bne_layout_cols
      WHERE  application_id = pn_appl_id
      AND    layout_code = pv_layout_code
      AND    interface_seq_num = pn_seq_num;
      IF SQL%rowcount = 1 THEN
        log_msg(lcv_proc_name,'Removed '||pv_column_name||' from layout ');
      else
        log_msg(lcv_proc_name,'WARNING [update_layout_column]: Failed to remove ');
      END IF;  
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END update_layout_column;

  PROCEDURE update_layouts(
    pr_intg                IN OUT NOCOPY integrator_rec,
    pv_interface_user_name IN VARCHAR2,
    pan_seq_nums           IN out nocopy seq_table,
    par_intf_cols          IN out nocopy interface_col_table) 
  IS
    lcv_proc_name         constant varchar2(30) := 'update_layouts';  
    lv_interface_col_name bne_interface_cols_b.interface_col_name%TYPE;
    ln_sequence_num       bne_interface_cols_b.sequence_num%type;
    ln_layout_index       number;
  begin
    log_msg(lcv_proc_name,'Updating '||pr_intg.layouts.count||' layouts');
    ln_layout_index := pr_intg.layouts.FIRST;
    while ln_layout_index is not null loop
    
      log_heading(lcv_proc_name, 'Updating layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
 
      lv_interface_col_name := par_intf_cols.FIRST;
      while lv_interface_col_name IS NOT NULL loop
        IF pan_seq_nums.EXISTS(lv_interface_col_name) THEN
          ln_sequence_num := pan_seq_nums(lv_interface_col_name);
          update_layout_column(
            pn_appl_id      => pr_intg.appl_id,
            pv_layout_code  => pr_intg.layouts(ln_layout_index).layout_code,
            pn_seq_num      => ln_sequence_num,
            pv_column_name  => lv_interface_col_name,
            pr_layout_attr => par_intf_cols(lv_interface_col_name).layout_attr(ln_layout_index));
        ELSE
          log_msg(lcv_proc_name,'WARNING [update_layouts]: Interface Column Name: '||lv_interface_col_name||' not found in interface '||pv_interface_user_name);  
        END IF;
        lv_interface_col_name := par_intf_cols.NEXT(lv_interface_col_name);
      END loop;
      ln_layout_index := pr_intg.layouts.NEXT(ln_layout_index);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END update_layouts;
  
  PROCEDURE set_layout_positions(
    pr_intg         IN OUT NOCOPY integrator_rec,
    pan_seq_nums    IN out nocopy seq_table,
    par_intf_cols   IN out nocopy interface_col_table)
  is
    lcv_proc_name           constant varchar2(30) := 'set_layout_positions';
    ln_layout_index         NUMBER;
    lv_index                interface_col_name;
    
    ln_interface_seq_num    NUMBER;
    ln_display_seq_num      NUMBER;
    
    ln_dummy_seq            NUMBER := gcn_dummy_seq;
    ln_moved_column         NUMBER;
    
    CURSOR ranked_layout_cols IS
      SELECT interface_seq_num, 
             rank() OVER (PARTITION BY application_id, layout_code, block_id ORDER BY sequence_num) ranked_seq_num
      FROM   bne_layout_cols
      WHERE  application_id = pr_intg.appl_id
      AND    layout_code = pr_intg.layouts(ln_layout_index).layout_code
      FOR UPDATE OF sequence_num;
    
  BEGIN
    ln_layout_index := pr_intg.layouts.first;
    while ln_layout_index is not null loop
      log_heading(lcv_proc_name, 'Setting layout Positions in Layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);

      lv_index := par_intf_cols.first;
      while lv_index is not null loop
        if pan_seq_nums.exists(lv_index) then
          ln_interface_seq_num := pan_seq_nums(lv_index);
        else
          raise_application_error(-20001,'Interface Sequence Number For: '||lv_index||' unknown.');
        end if;
        ln_display_seq_num   := par_intf_cols(lv_index).layout_attr(ln_layout_index).display_seq_num;
        /*
        || Move the existing layout entry out of the way (if req'd)
        */
        UPDATE bne_layout_cols
        SET    sequence_num = ln_dummy_seq
        WHERE  application_id = pr_intg.appl_id
        AND    layout_code = pr_intg.layouts(ln_layout_index).layout_code
        and    sequence_num = ln_display_seq_num
        returning interface_seq_num into ln_moved_column;
        IF SQL%ROWCOUNT = 1 THEN
          ln_dummy_seq := ln_dummy_seq + 1;
          log_msg(lcv_proc_name,'Moved interface_seq_num ['||ln_moved_column||'] out of layout column '||ln_display_seq_num);
        END IF;
        /*
        || Move the column into the display sequence we want
        */
        UPDATE bne_layout_cols
        SET    sequence_num = ln_display_seq_num
        WHERE  application_id = pr_intg.appl_id
        AND    layout_code = pr_intg.layouts(ln_layout_index).layout_code
        AND    interface_seq_num = ln_interface_seq_num;
        if sql%rowcount = 1 then
          log_msg(lcv_proc_name,'Moved '||lv_index||' into column '||ln_display_seq_num);
        end if;
        lv_index := par_intf_cols.next(lv_index);
      END loop;
      /*
      || Finished setting sequence numbers.  If there have been fields removed
      || from the layout - then these sequence numbers will have gaps.  So
      || quick loop to remove them
      */
      FOR datarec IN ranked_layout_cols LOOP
        UPDATE bne_layout_cols
        SET    sequence_num = datarec.ranked_seq_num
        WHERE CURRENT OF ranked_layout_cols;
      END LOOP;

      ln_layout_index := pr_intg.layouts.NEXT(ln_layout_index);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END set_layout_positions;

    function param_defn_exists(
        pr_param_defn in out nocopy param_defn_rec
    ) return varchar2 is
        lv_return_val  varchar2(1) := gcv_create;
        cursor param_defn_csr is
            select *
            from   bne_param_defns_vl
            where  application_id = pr_param_defn.appl_id
            and    param_defn_code = pr_param_defn.param_defn_code;
        lr_param_defn param_defn_csr%rowtype;
    begin
        open param_defn_csr;
        fetch param_defn_csr into lr_param_defn;
        if param_defn_csr%notfound then
            close param_defn_csr;
            lv_return_val := gcv_create;
        else
            pr_param_defn.object_version_number := lr_param_defn.object_version_number;
            close param_defn_csr;
            if NOT (
                pr_param_defn.appl_id                                              = lr_param_defn.application_id and
                pr_param_Defn.param_defn_code                                      = lr_param_defn.param_defn_code and
                pr_param_Defn.param_name                                           = lr_param_defn.param_name and
                pr_param_Defn.param_source                                         = lr_param_defn.param_source and
                pr_param_Defn.param_category                                       = lr_param_defn.param_category and
                pr_param_Defn.user_name                                            = lr_param_defn.user_name and
                pr_param_Defn.datatype                                             = lr_param_defn.datatype and
                coalesce(pr_param_Defn.attribute_app_id,gcn_miss_num)              = coalesce(lr_param_defn.attribute_app_id,gcn_miss_num) and
                coalesce(pr_param_Defn.attribute_code,gcv_miss_char)               = coalesce(lr_param_defn.attribute_code,gcv_miss_char) and
                coalesce(pr_param_Defn.param_resolver,gcv_miss_char)               = coalesce(lr_param_defn.param_resolver,gcv_miss_char) and
                pr_param_Defn.default_required_flag                                = lr_param_defn.default_required_flag and
                pr_param_Defn.default_visible_flag                                 = lr_param_defn.default_visible_flag and
                pr_param_Defn.default_user_modifyable_flag                         = lr_param_defn.default_user_modifyable_flag and
                coalesce(pr_param_Defn.default_string,gcv_miss_char)               = coalesce(lr_param_defn.default_string,gcv_miss_char) and
                coalesce(pr_param_Defn.default_String_trans_flag,gcv_miss_char)    = coalesce(lr_param_defn.default_String_trans_flag,gcv_miss_char) and
                coalesce(pr_param_Defn.default_date,gcd_miss_date)                 = coalesce(lr_param_defn.default_date,gcd_miss_date) and
                coalesce(pr_param_Defn.default_number,gcn_miss_num)                = coalesce(lr_param_defn.default_number,gcn_miss_num) and
                coalesce(pr_param_Defn.default_boolean_flag,gcv_miss_char)         = coalesce(lr_param_defn.default_boolean_flag,gcv_miss_char) and
                coalesce(pr_param_Defn.default_formula,gcv_miss_char)              = coalesce(lr_param_defn.default_formula,gcv_miss_char) and
                pr_param_Defn.val_type                                             = lr_param_defn.val_type and
                coalesce(pr_param_Defn.val_value,gcv_miss_char)                    = coalesce(lr_param_defn.val_value,gcv_miss_char) and
                pr_param_Defn.max_size                                             = lr_param_defn.max_size and
                pr_param_Defn.display_type                                         = lr_param_defn.display_type and
                pr_param_Defn.display_style                                        = lr_param_defn.display_style and
                pr_param_Defn.display_size                                         = lr_param_defn.display_size and
                coalesce(pr_param_Defn.default_desc,gcv_miss_char)                 = coalesce(lr_param_defn.default_desc,gcv_miss_char) and
                coalesce(pr_param_Defn.prompt_left,gcv_miss_char)                  = coalesce(lr_param_defn.prompt_left,gcv_miss_char) and
                coalesce(pr_param_Defn.prompt_above,gcv_miss_char)                 = coalesce(lr_param_defn.prompt_above,gcv_miss_char) and
                coalesce(pr_param_Defn.user_tip,gcv_miss_char)                     = coalesce(lr_param_defn.user_tip,gcv_miss_char) and
                coalesce(pr_param_Defn.help_url,gcv_miss_char)                     = coalesce(lr_param_defn.help_url,gcv_miss_char) and
                coalesce(pr_param_Defn.format_mask,gcv_miss_char)                  = coalesce(lr_param_defn.format_mask,gcv_miss_char) and
                coalesce(pr_param_Defn.access_key,gcv_miss_char)                   = coalesce(lr_param_defn.access_key,gcv_miss_char) and
                coalesce(pr_param_Defn.oa_flex_application_id,gcn_miss_num)        = coalesce(lr_param_defn.oa_flex_application_id,gcn_miss_num) and
                coalesce(pr_param_Defn.oa_flex_code,gcv_miss_char)                 = coalesce(lr_param_defn.oa_flex_code,gcv_miss_char) and
                coalesce(pr_param_Defn.oa_flex_num,gcv_miss_char)                  = coalesce(lr_param_defn.oa_flex_num,gcv_miss_char)
            ) then
                lv_return_val := gcv_update;
            else
                lv_return_Val := gcv_match;
            end if;
        end if;
        return(lv_return_val);
    end param_defn_exists;

    function param_list_item_exists(
        pn_appl_id         in number
      , pv_param_list_code in varchar2
      , pr_param_list_item in out nocopy param_list_item_rec
    ) return varchar2 is
        lv_return_val  varchar2(1) := gcv_create;
        cursor param_list_item_csr is
            select *
            from   bne_param_list_items
            where  application_id = pn_appl_id
            and    param_list_code = pv_param_list_code
            and    sequence_num = pr_param_list_item.sequence_num;
        lr_param_list_item param_list_item_csr%rowtype;
    begin
        open param_list_item_csr;
        fetch param_list_item_csr into lr_param_list_item;
        if param_list_item_csr%notfound then
            close param_list_item_csr;
            lv_return_val := gcv_create;
        else
            pr_param_list_item.object_version_number := lr_param_list_item.object_version_number;
            close param_list_item_csr;
            if NOT (
                pr_param_list_item.sequence_num                                       = lr_param_list_item.sequence_num and
                coalesce(pr_param_list_item.attribute_app_id,gcn_miss_num)            = coalesce(lr_param_list_item.attribute_app_id,gcn_miss_num) and
                coalesce(pr_param_list_item.attribute_code,gcv_miss_char)             = coalesce(lr_param_list_item.attribute_code,gcv_miss_char) and
                coalesce(pr_param_list_item.boolean_value_flag,gcv_miss_char)         = coalesce(lr_param_list_item.boolean_value_flag,gcv_miss_char) and
                coalesce(pr_param_list_item.date_value,gcd_miss_date)                 = coalesce(lr_param_list_item.date_value,gcd_miss_date) and
                coalesce(pr_param_list_item.desc_value,gcv_miss_char)                 = coalesce(lr_param_list_item.desc_value,gcv_miss_char) and            
                coalesce(pr_param_list_item.formula_value,gcv_miss_char)              = coalesce(lr_param_list_item.formula_value,gcv_miss_char) and
                coalesce(pr_param_list_item.number_value,gcn_miss_num)                = coalesce(lr_param_list_item.number_value,gcn_miss_num) and
                coalesce(pr_param_list_item.param_defn.appl_id,gcn_miss_num)          = coalesce(lr_param_list_item.param_defn_app_id,gcn_miss_num) and
                coalesce(pr_param_list_item.param_defn.param_defn_code,gcv_miss_char) = coalesce(lr_param_list_item.param_defn_code,gcv_miss_char) and
                coalesce(pr_param_list_item.param_name,gcv_miss_char)                 = coalesce(lr_param_list_item.param_name,gcv_miss_char) and
                coalesce(pr_param_list_item.string_value,gcv_miss_char)               = coalesce(lr_param_list_item.string_value,gcv_miss_char)
            ) then
                lv_return_val := gcv_update;
            else
                lv_return_Val := gcv_match;
            end if;
        end if;
        return(lv_return_val);
    end param_list_item_exists;

    function list_group_exists(
        pn_appl_id         in number
      , pv_param_list_code in varchar2
      , pr_param_group in out nocopy param_group_rec
    ) return varchar2 is
        lv_return_val  varchar2(1) := gcv_create;
        cursor param_group_csr is
            select *
            from   bne_param_groups_vl
            where  application_id = pn_appl_id
            and    param_list_code = pv_param_list_code
            and    sequence_num = pr_param_group.sequence_num;
        lr_param_group param_group_csr%rowtype;
    begin
        open param_group_csr;
        fetch param_group_csr into lr_param_group;
        if param_group_csr%notfound then
            close param_group_csr;
            lv_return_val := gcv_create;
        else
            pr_param_group.object_version_number := lr_param_group.object_version_number;
            close param_group_csr;
            if NOT (
                pr_param_group.sequence_num                                       = lr_param_group.sequence_num and
                pr_param_group.user_name                                          = lr_param_group.user_name and
                coalesce(pr_param_group.attribute_app_id,gcn_miss_num)            = coalesce(lr_param_group.attribute_app_id,gcn_miss_num) and
                coalesce(pr_param_group.attribute_code,gcv_miss_char)             = coalesce(lr_param_group.attribute_code,gcv_miss_char) and
                coalesce(pr_param_group.group_resolver,gcv_miss_char)             = coalesce(lr_param_group.group_resolver,gcv_miss_char)
            ) then
                lv_return_val := gcv_update;
            else
                lv_return_Val := gcv_match;
            end if;
        end if;
        return(lv_return_val);
    end list_group_exists;

    function list_group_item_exists(
        pn_appl_id         in number
      , pv_param_list_code in varchar2
      , pr_param_list_item in out nocopy param_list_item_rec
    ) return varchar2 is
        lv_return_val  varchar2(1) := gcv_create;
        cursor param_group_item_csr is
            select *
            from   bne_param_group_items
            where  application_id = pn_appl_id
            and    param_list_code = pv_param_list_code
            and    group_seq_num = 1
            and    sequence_num = pr_param_list_item.sequence_num;
        lr_group_list_item param_group_item_csr%rowtype;
    begin
        open param_group_item_csr;
        fetch param_group_item_csr into lr_group_list_item;
        if param_group_item_csr%notfound then
            close param_group_item_csr;
            lv_return_val := gcv_create;
        else
            -- This is ugly, but we only use it directly after this call so I'll put up with it for now
            pr_param_list_item.object_version_number := lr_group_list_item.object_version_number;
            
            close param_group_item_csr;
            if NOT (
                pr_param_list_item.sequence_num                                      = lr_group_list_item.param_seq_num
            ) then
                lv_return_val := gcv_update;
            else
                lv_return_Val := gcv_match;
            end if;
        end if;
        return(lv_return_val);
    end list_group_item_exists;

    function param_list_exists(
        pr_param_list in out nocopy param_list_rec
    ) return varchar2 is
        lv_return_val  varchar2(1) := gcv_create;
        cursor param_list_csr is
            select *
            from   bne_param_lists_vl
            where  application_id = pr_param_list.appl_id
            and    param_list_code = pr_param_list.param_list_code;
        lr_param_list param_list_csr%rowtype;
    begin
        open param_list_csr;
        fetch param_list_csr into lr_param_list;
        if param_list_csr%notfound then
            close param_list_csr;
            lv_return_val := gcv_create;
        else
            close param_list_csr;
            pr_param_list.object_version_number := lr_param_list.object_version_number;
            if NOT (
                pr_param_list.appl_id           = lr_param_list.application_id and
                pr_param_list.param_list_code   = lr_param_list.param_list_code and
                pr_param_list.persistent_flag   = lr_param_list.persistent_flag and
                pr_param_list.user_name         = lr_param_list.user_name and
                coalesce(pr_param_list.comments,gcv_miss_char)          = coalesce(lr_param_list.comments,gcv_miss_char) and
                coalesce(pr_param_list.attribute_app_id,gcn_miss_num)   = coalesce(lr_param_list.attribute_app_id,gcn_miss_num) and
                coalesce(pr_param_list.attribute_code,gcv_miss_char)    = coalesce(lr_param_list.attribute_code,gcv_miss_char) and
                coalesce(pr_param_list.list_resolver,gcv_miss_char)     = coalesce(lr_param_list.list_resolver,gcv_miss_char) and
                coalesce(pr_param_list.user_tip,gcv_miss_char)          = coalesce(lr_param_list.user_tip,gcv_miss_char) and
                coalesce(pr_param_list.prompt_left,gcv_miss_char)       = coalesce(lr_param_list.prompt_left,gcv_miss_char) and
                coalesce(pr_param_list.prompt_above,gcv_miss_char)      = coalesce(lr_param_list.prompt_above,gcv_miss_char)
            ) then
                lv_return_val := gcv_update;
            else
                lv_return_Val := gcv_match;
            end if;
        end if;
        return(lv_return_val);
    end param_list_exists;
    
    PROCEDURE create_param_list(
        pv_created_by_user in varchar2
      , pr_param_list      in out nocopy param_list_rec
    ) IS
        li_idx integer;
    BEGIN
        --
        -- Check the parameter list exists and matches the definition passed in
        --
        if param_list_exists(
            pr_param_list => pr_param_list
        ) != gcv_match then
            bne_param_lists_pkg.load_row(
                x_param_list_asn        => get_asn(pr_param_list.appl_id)
              , x_param_list_code       => pr_param_list.param_list_code      
              , x_object_version_number => coalesce(pr_param_list.object_version_number,1)
              , x_persistent_flag       => pr_param_list.persistent_flag      
              , x_comments              => pr_param_list.comments             
              , x_attribute_asn         => get_asn(pr_param_list.attribute_app_id)
              , x_attribute_code        => pr_param_list.attribute_code       
              , x_list_resolver         => pr_param_list.list_resolver        
              , x_user_tip              => pr_param_list.user_tip             
              , x_prompt_left           => pr_param_list.prompt_left          
              , x_prompt_above          => pr_param_list.prompt_above         
              , x_user_name             => pr_param_list.user_name            
              , x_owner                 => pv_created_by_user
              , x_last_update_date      => null   -- defaults to sysdate internally
              , x_custom_mode           => 'FORCE'
            );
        end if;
        --
        -- Add groups
        --
        li_idx := pr_param_list.list_groups.first;
        while li_idx is not null loop
            if list_group_exists(
                pn_appl_id         => pr_param_list.appl_id
              , pv_param_list_code => pr_param_list.param_list_code
              , pr_param_group     => pr_param_list.list_groups(li_idx)
            ) != gcv_match then
                dbms_output.put_line('Calling bne_param_groups_pkg.load_row with sequence_num: '||pr_param_list.list_groups(li_idx).sequence_num);
                bne_param_groups_pkg.load_row(
                    x_param_list_asn        => get_asn(pr_param_list.appl_id)
                  , x_param_list_code       => pr_param_list.param_list_code      
                  , x_sequence_num          => pr_param_list.list_groups(li_idx).sequence_num
                  , x_object_version_number => coalesce(pr_param_list.list_groups(li_idx).object_version_number,1)
                  , x_attribute_asn         => get_asn(pr_param_list.list_groups(li_idx).attribute_app_id)
                  , x_attribute_code        => pr_param_list.list_groups(li_idx).attribute_code        
                  , x_group_resolver        => pr_param_list.list_groups(li_idx).group_resolver        
                  , x_user_name             => pr_param_list.list_groups(li_idx).user_name             
                  , x_owner                 => pv_created_by_user
                  , x_last_update_date      => null   -- defaults to sysdate internally
                  , x_custom_mode           => 'FORCE'
                );
            end if;
            li_idx := pr_param_list.list_groups.next(li_idx);
        end loop;
        --
        -- Add items
        --
        li_idx := pr_param_list.list_items.first;
        while li_idx is not null loop
            -- 
            -- Check parameter definition
            --
            if param_defn_exists(
                pr_param_defn => pr_param_list.list_items(li_idx).param_defn
            ) != gcv_match then
                bne_param_defns_pkg.load_row(
                    x_param_defn_asn               => get_asn(pr_param_list.list_items(li_idx).param_defn.appl_id)
                  , x_param_defn_code              => pr_param_list.list_items(li_idx).param_defn.param_defn_code             
                  , x_object_version_number        => coalesce(pr_param_list.list_items(li_idx).param_defn.object_version_number,1)
                  , x_param_name                   => pr_param_list.list_items(li_idx).param_defn.param_name                  
                  , x_param_source                 => pr_param_list.list_items(li_idx).param_defn.param_source                
                  , x_param_category               => pr_param_list.list_items(li_idx).param_defn.param_category              
                  , x_datatype                     => pr_param_list.list_items(li_idx).param_defn.datatype                    
                  , x_attribute_asn                => get_asn(pr_param_list.list_items(li_idx).param_defn.attribute_app_id)
                  , x_attribute_code               => pr_param_list.list_items(li_idx).param_defn.attribute_code              
                  , x_param_resolver               => pr_param_list.list_items(li_idx).param_defn.param_resolver              
                  , x_default_required_flag        => pr_param_list.list_items(li_idx).param_defn.default_required_flag       
                  , x_default_visible_flag         => pr_param_list.list_items(li_idx).param_defn.default_visible_flag        
                  , x_default_user_modifyable_flag => pr_param_list.list_items(li_idx).param_defn.default_user_modifyable_flag
                  , x_default_string               => pr_param_list.list_items(li_idx).param_defn.default_string              
                  , x_default_string_trans_flag    => pr_param_list.list_items(li_idx).param_defn.default_string_trans_flag   
                  , x_default_date                 => pr_param_list.list_items(li_idx).param_defn.default_date                
                  , x_default_number               => pr_param_list.list_items(li_idx).param_defn.default_number              
                  , x_default_boolean_flag         => pr_param_list.list_items(li_idx).param_defn.default_boolean_flag        
                  , x_default_formula              => pr_param_list.list_items(li_idx).param_defn.default_formula             
                  , x_val_type                     => pr_param_list.list_items(li_idx).param_defn.val_type                    
                  , x_val_value                    => pr_param_list.list_items(li_idx).param_defn.val_value                   
                  , x_max_size                     => pr_param_list.list_items(li_idx).param_defn.max_size                    
                  , x_display_type                 => pr_param_list.list_items(li_idx).param_defn.display_type                
                  , x_display_style                => pr_param_list.list_items(li_idx).param_defn.display_style               
                  , x_display_size                 => pr_param_list.list_items(li_idx).param_defn.display_size                
                  , x_help_url                     => pr_param_list.list_items(li_idx).param_defn.help_url                    
                  , x_format_mask                  => pr_param_list.list_items(li_idx).param_defn.format_mask                 
                  , x_user_name                    => pr_param_list.list_items(li_idx).param_defn.user_name                   
                  , x_default_desc                 => pr_param_list.list_items(li_idx).param_defn.default_desc                
                  , x_prompt_left                  => pr_param_list.list_items(li_idx).param_defn.prompt_left                 
                  , x_prompt_above                 => pr_param_list.list_items(li_idx).param_defn.prompt_above                
                  , x_user_tip                     => pr_param_list.list_items(li_idx).param_defn.user_tip                    
                  , x_access_key                   => pr_param_list.list_items(li_idx).param_defn.access_key                  
                  , x_owner                        => pv_created_by_user                       
                  , x_last_update_date             => null   -- defaults to sysdate internally
                  , x_custom_mode                  => 'FORCE'
                  , x_oa_flex_asn                  => get_asn(pr_param_list.list_items(li_idx).param_defn.oa_flex_application_id)                 
                  , x_oa_flex_code                 => pr_param_list.list_items(li_idx).param_defn.oa_flex_code                
                  , x_oa_flex_num                  => pr_param_list.list_items(li_idx).param_defn.oa_flex_num                 
                );
            end if;
            --
            -- Check List Item
            -- 
            if param_list_item_exists(
                pn_appl_id         => pr_param_list.appl_id
              , pv_param_list_code => pr_param_list.param_list_code
              , pr_param_list_item => pr_param_list.list_items(li_idx)
            ) != gcv_match then
                bne_param_list_items_pkg.load_row (
                    x_param_list_asn        => get_asn(pr_param_list.appl_id)
                  , x_param_list_code       => pr_param_list.param_list_code      
                  , x_sequence_num          => pr_param_list.list_items(li_idx).sequence_num         
                  , x_object_version_number => coalesce(pr_param_list.list_items(li_idx).object_version_number,1)
                  , x_param_defn_asn        => get_asn(pr_param_list.list_items(li_idx).param_defn.appl_id)
                  , x_param_defn_code       => pr_param_list.list_items(li_idx).param_defn.param_defn_code
                  , x_param_name            => pr_param_list.list_items(li_idx).param_name           
                  , x_attribute_asn         => get_asn(pr_param_list.list_items(li_idx).attribute_app_id)
                  , x_attribute_code        => pr_param_list.list_items(li_idx).attribute_code       
                  , x_string_value          => pr_param_list.list_items(li_idx).string_value         
                  , x_date_value            => pr_param_list.list_items(li_idx).date_value           
                  , x_number_value          => pr_param_list.list_items(li_idx).number_value         
                  , x_boolean_value_flag    => pr_param_list.list_items(li_idx).boolean_value_flag   
                  , x_formula_value         => pr_param_list.list_items(li_idx).formula_value        
                  , x_desc_value            => pr_param_list.list_items(li_idx).desc_value           
                  , x_owner                 => pv_created_by_user                       
                  , x_last_update_date      => null   -- defaults to sysdate internally
                  , x_custom_mode           => 'FORCE'
                );
            end if;
            --
            -- Check Group Item
            --;
            if list_group_item_exists(
                pn_appl_id         => pr_param_list.appl_id
              , pv_param_list_code => pr_param_list.param_list_code
              , pr_param_list_item => pr_param_list.list_items(li_idx)
            ) != gcv_match then
                bne_param_group_items_pkg.load_row(
                    x_param_list_asn        => get_asn(pr_param_list.appl_id)
                  , x_param_list_code       => pr_param_list.param_list_code
                  , x_group_seq_num         => 1
                  , x_sequence_num          => pr_param_list.list_items(li_idx).sequence_num         
                  , x_object_version_number => coalesce(pr_param_list.list_items(li_idx).object_version_number,1)
                  , x_param_seq_num         => pr_param_list.list_items(li_idx).sequence_num        
                  , x_owner                 => pv_created_by_user                       
                  , x_last_update_date      => null   -- defaults to sysdate internally
                  , x_custom_mode           => 'FORCE'
                );
            end if;
            li_idx := pr_param_list.list_items.next(li_idx);
        end loop;
    END create_param_list;

  PROCEDURE create_integrator_int(
    pr_intg IN out nocopy integrator_rec)
  is
    lcv_proc_name  constant varchar2(30) := 'create_integrator_int';  
  BEGIN
    log_heading(lcv_proc_name, 'Creating Integrator '||pr_intg.intg_code||'..');
    /*
    || Create a new integrator
    */
    bne_integrator_utils.create_integrator(
      p_application_id       => pr_intg.appl_id,
      p_object_code          => pr_intg.intg_code,
      p_integrator_user_name => pr_intg.intg_name,
      p_language             => 'US',
      p_source_language      => 'US',
      p_user_id              => pr_intg.user_id,
      p_integrator_code      => pr_intg.integrator_code);  
    log_msg(lcv_proc_name,'Successfully created integrator: '||pr_intg.integrator_code);
    /*
    || Crewate parameter list (if required)
    */
    if pr_intg.create_doc_list.appl_id is not null then
        create_param_list(
            pv_created_by_user => pr_intg.created_by_user
          , pr_param_list      => pr_intg.create_doc_list
        );
    end if;
    /*
    || Flag as custom
    */
    UPDATE bne_integrators_b
    SET    created_by = pr_intg.user_id
         , last_updated_by = pr_intg.user_id
         , display_flag = 'Y'
         , SOURCE = 'C'
         , create_doc_list_app_id = coalesce(pr_intg.create_doc_list.appl_id, create_doc_list_app_id)
         , create_doc_list_code = coalesce(pr_intg.create_doc_list.param_list_code, create_doc_list_code)
    WHERE  INTEGRATOR_CODE = PR_INTG.INTEGRATOR_CODE;
    LOG_MSG(LCV_PROC_NAME,'Flagged integrator: '||PR_INTG.INTEGRATOR_CODE||' as custom');
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_integrator_int;

  procedure register_table(
    pr_intf            IN out nocopy interface_rec)  
  is
    lcv_proc_name  constant varchar2(30) := 'register_table';  
    CURSOR dba_tables_info IS
      SELECT dt.table_name,
             nvl(dt.initial_extent, dts.initial_extent) initial_extent,
             coalesce(dt.next_extent, dts.next_extent, dt.initial_extent, dts.initial_extent) next_extent,
             dt.pct_free,
             nvl(dt.pct_used,40) pct_used -- locally managed
      FROM   dba_tables dt,
             dba_tablespaces dts
      WHERE  dt.table_name = pr_intf.interface_table_name
      AND    dt.owner      = pr_intf.interface_table_owner
      AND    dt.tablespace_name = dts.tablespace_name;
    lr_tab_info dba_tables_info%rowtype;
  begin
    log_heading(lcv_proc_name,'Registering Interface table '||pr_intf.interface_table_owner||'.'||pr_intf.interface_table_name||' with AOL');      
    /*
    || Register the table (if required)
    */
    OPEN dba_tables_info;
    fetch dba_tables_info into lr_tab_info;
    IF dba_tables_info%notfound then
      close dba_tables_info;
      raise_application_error(-20001,'Unable to find table '||pr_intf.interface_table_owner||'.'||pr_intf.interface_table_name||' in dba_tables...');
    ELSE
      close dba_tables_info;
      /*
      || Remove it first just in case
      */
      ad_dd.delete_table(
        p_appl_short_name => pr_intf.interface_table_asn,
        p_tab_name        => lr_tab_info.table_name);
      /*
      || Now register it
      */
      ad_dd.register_table (
        p_appl_short_name             => pr_intf.interface_table_asn,
        p_tab_name                    => lr_tab_info.table_name,
        p_tab_type                    => 'T',
        p_next_extent                 => lr_tab_info.next_extent,
        p_pct_free                    => lr_tab_info.pct_free,
        p_pct_used                    => lr_tab_info.pct_used);
      log_msg(lcv_proc_name,'Registered Table Successfully');
      /*
      || Register Columns
      */
      for col_info in (select column_name, 
                              column_id, 
                              data_type, 
                              data_length, 
                              nullable, 
                              data_precision, 
                              data_scale
                       FROM   dba_tab_columns
                       where  table_name = pr_intf.interface_table_name
                       and    owner = pr_intf.interface_table_owner
                       order by column_id) 
      loop
        log_msg(lcv_proc_name,'Registering Column: '||col_info.column_name||' ['||col_info.column_id||']'); 
        ad_dd.register_column (
          p_appl_short_name             => pr_intf.interface_table_asn,
          p_tab_name                    => pr_intf.interface_table_name,
          p_col_name                    => col_info.column_name,
          p_col_seq                     => col_info.column_id,
          p_col_type                    => col_info.data_type,
          p_col_width                   => col_info.data_length,
          p_nullable                    => col_info.nullable,
          p_translate                   => 'N',
          p_precision                   => col_info.data_precision,
          p_scale                       => col_info.data_scale);
        log_msg(lcv_proc_name,'Registering Column: '||col_info.column_name);
      end loop;

      for primary_key in (select constraint_name, 
                              constraint_type,
                              decode(status,
                                'ENABLED','Y',
                                'DISABLED','N') enabled_flag
                       from   dba_constraints
                       where  constraint_type = 'P' 
                       and    table_name = pr_intf.interface_table_name
                       and    owner = pr_intf.interface_table_owner)
      LOOP
        ad_dd.register_primary_key (
          p_appl_short_name             => pr_intf.interface_table_asn,
          p_key_name                    => primary_key.constraint_name,
          p_tab_name                    => pr_intf.interface_table_name,
          p_description                 => 'Register primary key',
          p_key_type                    => 'S',  -- standard
          p_audit_flag                  => 'N',
          p_enabled_flag                => primary_key.enabled_flag);
        log_msg(lcv_proc_name,'Registered Primary Key: '||primary_key.constraint_name);
          
        for pk_column in (select column_name, position
                          from   dba_cons_columns
                          where  table_name = pr_intf.interface_table_name
                          and    owner = pr_intf.interface_table_owner
                          and    constraint_name = primary_key.constraint_name
                          order by position)
        LOOP
          ad_dd.register_primary_key_column (
            p_appl_short_name             => pr_intf.interface_table_asn,
            p_key_name                    => primary_key.constraint_name,
            p_tab_name                    => pr_intf.interface_table_name,
            p_col_name                    => pk_column.column_name,
            p_col_sequence                => pk_column.position);
          log_msg(lcv_proc_name,'Registered Primary Key Column: '||pk_column.column_name);
        end loop;
      END LOOP;
    end if;
    log_msg(lcv_proc_name,'Finished Registering Table');
  end register_table;

  PROCEDURE create_interface_for_table(
    pr_intg            IN out nocopy integrator_rec,
    pr_intf            IN out nocopy interface_rec)  
  IS
    lcv_proc_name  constant varchar2(30) := 'create_interface_for_table';  
    lv_dummy       VARCHAR2(50);
    CURSOR in_fnd_tables IS
      SELECT 'already in'
      FROM   fnd_tables ft,
             fnd_application fa
      WHERE  ft.table_name = pr_intf.interface_table_name
      AND    FA.APPLICATION_SHORT_NAME = PR_INTF.INTERFACE_TABLE_OWNER;
      
  BEGIN
    /*
    || First make sure the table is in FND_TABLES - otherwise we can't 
    || reference it from the web pages
    */
    OPEN in_fnd_tables;
    FETCH IN_FND_TABLES INTO LV_DUMMY;
    IF IN_FND_TABLES%NOTFOUND THEN
      close in_fnd_tables;
      LOG_MSG(LCV_PROC_NAME,'Interface table '||PR_INTF.INTERFACE_TABLE_OWNER||'.'||PR_INTF.INTERFACE_TABLE_NAME||' not found in fnd_tables');
      register_table(pr_intf);
    ELSE
      /*
      || Already there - we're happy
      */
      CLOSE in_fnd_tables;
    END IF;
    /*
    || Create the webADI interface for the interface table.
    */
    bne_integrator_utils.create_interface_for_table (
      p_application_id        => pr_intf.appl_id,
      p_object_code           => pr_intg.intg_code, 
      p_integrator_code       => pr_intg.integrator_code,
      p_interface_table_name  => pr_intf.interface_table_name,
      p_interface_user_name   => pr_intf.interface_user_name,
      p_language              => 'US', 
      p_source_lang           => 'US', 
      p_user_id               => pr_intg.user_id,
      p_interface_code        => pr_intf.interface_code,
      p_use_fnd_metadata      => FALSE, -- use all_tables and all_tab_columns
      p_interface_table_owner => pr_intf.interface_table_owner);
    log_msg(lcv_proc_name,'Created Interface: '||pr_intf.interface_code);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_interface_for_table;
  /* 
  || bne_integrator_utils has a bug in their CREATE_API_PARAMETER_LIST 
  || procedure as the API_PARAMS_C cursor can return NULL interface_col_names
  ||
  || Have duplicated it here - and put in a workaround for the cursor.  
  || (quicker than trying to log it as a bug and get it fixed properly!
  ||
  || Copied from bneintgb.pls 120.9.12010000.2 2010/02/04 06:19:51 
  ||
  */

  PROCEDURE create_api_parameter_list(p_param_list_name    IN VARCHAR2,
    p_api_package_name   IN VARCHAR2,
    p_api_procedure_name IN VARCHAR2,
    p_api_type           IN VARCHAR2,
    p_api_return_type    IN VARCHAR2,
    p_language           IN VARCHAR2,
    p_source_lang        IN VARCHAR2,
    p_user_id            IN NUMBER,
    p_overload           IN NUMBER,
    p_application_id     IN NUMBER,
    p_object_code        IN VARCHAR2,
    p_param_list_code    out nocopy VARCHAR2)
  IS
    lcv_proc_name         constant varchar2(30) := 'create_api_parameter_list';
  
    CURSOR api_params_c (cp_api_package_name   IN VARCHAR2,
                         cp_api_procedure_name IN VARCHAR2,
                         cp_overload           IN NUMBER,
                         cp_application_id     IN NUMBER,
                         cp_object_code        IN VARCHAR2,
                         cp_user_id            IN NUMBER) IS
      SELECT cp_application_id application_id,
             cp_object_code||'_P'||to_char(A.SEQUENCE#)||'_ATT' attribute_code,
             A.argument                              param_name,
             decode(A.TYPE#, 252, 'boolean',
                             12, 'date',
                             2, 'number',
                             1, 'varchar2',
                                'varchar2')          attribute2,
             decode(A.in_out,1,'OUT',2,'INOUT','IN') attribute3,
             'N'                                     attribute4,
             decode(A.TYPE#, 252, NULL,
                             12, NULL,
                             2, NULL,
                             1, '2000')              attribute6,
             cp_object_code    param_list_code,
             A.SEQUENCE#       seq_num,
             cp_user_id        created_by,
             SYSDATE           creation_date,
             cp_user_id        last_updated_by,
             SYSDATE           last_update_date
      FROM   sys.argument$ A,
             user_objects b
      WHERE  A.obj# = b.object_id
      AND    b.object_name = cp_api_package_name
      AND    A.procedure$ = cp_api_procedure_name
      AND    A.LEVEL# = 0
      AND    A.overload# = cp_overload
      ORDER BY A.SEQUENCE#;
  
    vv_attribute_code      bne_attributes.attribute_code%TYPE;
    vv_temp_attribute_code bne_attributes.attribute_code%TYPE;
    vv_persistent          bne_param_lists_b.persistent_flag%TYPE;
    ln_sequence_sub        NUMBER := 0;
  BEGIN
    p_param_list_code := NULL;
    vv_attribute_code := NULL;
    vv_temp_attribute_code := NULL;
    vv_persistent := 'Y';
  
    -- Only create the API Parameter List and Attributes if the OBJECT_CODE supplied is VALID.
  
    IF bne_integrator_utils.is_valid_appl_id(p_application_id) AND 
       bne_integrator_utils.is_valid_object_code(p_object_code) THEN
  
      -- Check to see if the Param List Code already exists. (The Param List Code will
      -- always use the OBJECT_CODE as it is passed, therefore no temporary variables
      -- need to be defined.
  
      -- *** NOTE: This DOES NOT check the Parameter List Name in the USER_NAME column
      --           in the TL table.
  
      BEGIN
        SELECT A.param_list_code
        INTO   p_param_list_code
        FROM   bne_param_lists_b A, bne_param_lists_tl b
        WHERE  A.application_id = b.application_id
        AND    A.param_list_code = b.param_list_code
        AND    b.language = p_language
        AND    A.application_id = p_application_id
        AND    A.param_list_code = p_object_code;
      exception
        WHEN no_data_found THEN NULL;
      END;
  
      --  If this is a new Parameter List, then create it and
      --  the associated Attributes and Parameter List Items.
  
      IF ( p_param_list_code IS NULL ) THEN
  
          -- Set the PARAM_LIST_CODE
  
          p_param_list_code := p_object_code;
  
          -- Derive the ATTRIBUTE_CODE for the API Call - derived here
          -- so it can be inserted as part of the parameter list
          -- As the ATTRIBUTE_CODE for the API parameters includes the API Parameter sequence number,
          -- the ATTRIBUTE_CODE for the API Call will use '0' (zero), e.g. 'PARAM_LIST_CODE_P0_ATT'.
  
          vv_attribute_code := p_object_code||'_P0_ATT';
  
          -- Check to see if the ATTRIBUTE_CODE is unique
  
          BEGIN
            SELECT DISTINCT attribute_code
            INTO   vv_temp_attribute_code
            FROM   bne_attributes
            WHERE  application_id = p_application_id
            AND    attribute_code = vv_attribute_code;
          exception
            WHEN no_data_found THEN NULL;
          END;
  
          IF (vv_temp_attribute_code IS NULL) THEN
  
            -- Insert a new record into the BNE_PARAM_LISTS_B table
  
            INSERT INTO bne_param_lists_b
             (application_id, param_list_code, object_version_number, persistent_flag, comments,
              attribute_app_id, attribute_code, created_by, creation_date,
              last_updated_by, last_update_date)
            VALUES
            (p_application_id, p_param_list_code, 1, vv_persistent, p_param_list_name,
             p_application_id, vv_attribute_code, p_user_id, SYSDATE,
             p_user_id, SYSDATE);
  
            -- Insert a new record into the BNE_PARAM_LISTS_TL table
  
            INSERT INTO bne_param_lists_tl
             (application_id, param_list_code, language, source_lang, user_name,
              created_by, creation_date, last_updated_by, last_update_date)
            VALUES
            (p_application_id, p_param_list_code, p_language, p_source_lang, p_param_list_name,
             p_user_id, SYSDATE, p_user_id, SYSDATE);
  
  
            -- Insert the Attribute for the API Call
  
            INSERT INTO bne_attributes
              (application_id,
               attribute_code,
               object_version_number,
               attribute1,
               attribute2,
               attribute3,
               attribute4,
               attribute5,
               created_by,
               creation_date,
               last_updated_by,
               last_update_date)
            VALUES
              (p_application_id,
               vv_attribute_code,
               1,
               p_api_type,
               p_api_package_name||'.'||p_api_procedure_name,
               p_api_return_type,
               'N',
               'Y',
               p_user_id,
               SYSDATE,
               p_user_id,
               SYSDATE);
  
            FOR api_params_rec IN api_params_c(p_api_package_name,
                                               p_api_procedure_name,
                                               p_overload,
                                               p_application_id,
                                               p_object_code,
                                               p_user_id) loop

              IF api_params_rec.param_name IS NULL THEN
                ln_sequence_sub := ln_sequence_sub + 1;
              ELSE
                /*
                || Fixup the sequence fields to exclude the null argument.
                || (which appears to be in the cursor results if it's a 
                || FUNCTION being queried).
                */
                api_params_rec.seq_num        := api_params_rec.seq_num - ln_sequence_sub;
                api_params_rec.attribute_code := p_object_code||'_P'||to_char(api_params_rec.seq_num)||'_ATT';
  
                -- Generate the Attributes for the API Parameters
    
                INSERT INTO bne_attributes
                  (application_id,
                   attribute_code,
                   object_version_number,
                   attribute1,
                   attribute2,
                   attribute3,
                   attribute4,
                   attribute6,
                   created_by,
                   creation_date,
                   last_updated_by,
                   last_update_date)
                VALUES
                  (api_params_rec.application_id,
                   api_params_rec.attribute_code,
                   1,
                   api_params_rec.param_name,
                   api_params_rec.attribute2,
                   api_params_rec.attribute3,
                   api_params_rec.attribute4,
                   api_params_rec.attribute6,
                   api_params_rec.created_by,
                   api_params_rec.creation_date,
                   api_params_rec.last_updated_by,
                   api_params_rec.last_update_date);
    
                -- Generate the Parameter List Items
    
                INSERT INTO bne_param_list_items
                  (application_id,
                   param_list_code,
                   sequence_num,
                   object_version_number,
                   param_name,
                   attribute_app_id,
                   attribute_code,
                   created_by,
                   creation_date,
                   last_updated_by,
                   last_update_date)
                VALUES
                  (api_params_rec.application_id,
                   api_params_rec.param_list_code,
                   api_params_rec.seq_num,
                   1,
                   api_params_rec.param_name,
                   api_params_rec.application_id,
                   api_params_rec.attribute_code,
                   api_params_rec.created_by,
                   api_params_rec.creation_date,
                   api_params_rec.last_updated_by,
                   api_params_rec.last_update_date);
              END IF;
              
              exit WHEN api_params_c%notfound;
  
            END loop;
  
        END IF;
      ELSE
        -- If the ATTRIBUTE_CODE is non-unique, ie. already exists...return an error message.
        NULL;
      END IF;
    ELSE
     raise_application_error(-20000,'Object code invalid, Integrator: ' || p_application_id || ':' || p_object_code || ' has already been created');
  
    END IF;  
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_api_parameter_list;
  /* 
  || bne_integrator_utils has a bug in their create_interface_for_api 
  || procedure as the API_PARAMS_C cursor can return NULL interface_col_names
  ||
  || Have duplicated it here - and put in a workaround for the cursor.  
  || (quicker than trying to log it as a bug and get it fixed properly!
  ||
  || Copied from bneintgb.pls 120.9.12010000.2 2010/02/04 06:19:51 
  ||
  */
  PROCEDURE create_interface_for_api (p_application_id      IN NUMBER,
                                      p_object_code         IN VARCHAR2,
                                      p_integrator_code     IN VARCHAR2,
                                      p_api_package_name    IN VARCHAR2,
                                      p_api_procedure_name  IN VARCHAR2,
                                      p_interface_user_name IN VARCHAR2,
                                      p_param_list_name     IN VARCHAR2,
                                      p_api_type            IN VARCHAR2,
                                      p_api_return_type     IN VARCHAR2,
                                      p_upload_type         IN NUMBER,
                                      p_language            IN VARCHAR2,
                                      p_source_lang         IN VARCHAR2,
                                      p_user_id             IN NUMBER,
                                      p_param_list_code     out nocopy VARCHAR2,
                                      p_interface_code      out nocopy VARCHAR2)
  IS
    CURSOR api_params_c (cp_application_id     IN NUMBER,
                         cp_interface_code     IN VARCHAR2,
                         cp_api_package_name   IN VARCHAR2,
                         cp_api_procedure_name IN VARCHAR2,
                         cp_overload           IN NUMBER,
                         cp_language           IN VARCHAR2,
                         cp_source_lang        IN VARCHAR2,
                         cp_user_id            IN NUMBER) IS
      SELECT cp_application_id         application_id,
             cp_interface_code         interface_code,
             1                         object_version_number,
             A.SEQUENCE#               sequence_num,
             1                         interface_col_type,
             decode(A.TYPE#, 252, '2',
                             12,  '3',
                             2,   '1',
                             1,   '2',
                                  '2') data_type,
             A.argument                interface_col_name,
             'N'                       not_null_flag,
             'N'                       summary_flag,
             'Y'                       enabled_flag,
             'Y'                       display_flag,
             'Y'                       mapping_enabled_flag,
             decode(DEFAULT#,NULL,decode(in_out,NULL,'Y','N'),'N') required_flag,
             'N'                       read_only_flag,
             (A.SEQUENCE# * 10)        display_order,
             A.SEQUENCE#               upload_param_list_item_num,
             substr(A.argument,3)      prompt_left,
             substr(A.argument,3)      prompt_above,
             cp_language               language,
             cp_source_lang            source_lang,
             cp_user_id                created_by,
             SYSDATE                   creation_date,
             cp_user_id                last_updated_by,
             SYSDATE                   last_update_date
        FROM   sys.argument$ A, user_objects b
        WHERE  A.obj# = b.object_id
        AND    b.object_name = cp_api_package_name
        AND    A.procedure$ = cp_api_procedure_name
        AND    A.LEVEL# = 0
        AND    A.overload# = cp_overload
        ORDER BY A.SEQUENCE#;
  
    lcv_proc_name         constant varchar2(30) := 'create_interface_for_api';
    vv_interface_code   bne_interfaces_b.interface_code%TYPE;
    vn_interface_exists NUMBER;
    vn_overload         NUMBER;
    ln_sequence_sub     NUMBER := 0;
  BEGIN
    IF bne_integrator_utils.is_valid_appl_id(p_application_id) AND 
       bne_integrator_utils.is_valid_object_code(p_object_code) THEN
  
      vv_interface_code := NULL;
      p_interface_code := p_object_code||'_INTF';
      vn_interface_exists := 0;
      p_param_list_code := NULL;
  
      -- Check that the OBJECT_CODE for this Interface is unique for the Application ID.
  
      BEGIN
        SELECT interface_code
        INTO   vv_interface_code
        FROM   bne_interfaces_b
        WHERE  application_id = p_application_id
        AND    interface_code = p_interface_code;
      exception
          WHEN no_data_found THEN NULL;
      END;
  
      -- Check to see if the Interface already exists by querying on the API Procedure name
  
      BEGIN
        SELECT 1
        INTO   vn_interface_exists
        FROM   bne_interfaces_b bib, bne_interfaces_tl bit
        WHERE  bib.application_id = bit.application_id
        AND    bib.interface_code = bit.interface_code
        AND    bib.integrator_app_id = p_application_id
        AND    bib.integrator_code = p_integrator_code
        AND    bit.source_lang = p_source_lang
        AND    bit.language = p_language
        AND    bib.application_id = p_application_id
        AND    bib.interface_name = p_api_procedure_name;
      exception
        WHEN no_data_found THEN NULL;
      END;
  
      IF (vn_interface_exists = 0) AND (vv_interface_code IS NULL) THEN
  
        -- Retrieve the minimum Overload for the package.procedure
  
        vn_overload := 0;
        BEGIN
          SELECT MIN(A.overload#)
          INTO   vn_overload
          FROM   sys.argument$ A,
                 user_objects b
          WHERE  A.obj# = b.object_id
          AND    b.object_name = p_api_package_name
          AND    A.procedure$  = p_api_procedure_name
          AND    A.LEVEL# = 0;
        exception
          WHEN no_data_found THEN NULL;
        END;
  
        -- create the API parameter list
        create_api_parameter_list (
          p_param_list_name,
          p_api_package_name,
          p_api_procedure_name,
          p_api_type,
          p_api_return_type,
          p_language,
          p_source_lang,
          p_user_id,
          vn_overload,
          p_application_id,
          p_object_code,
          p_param_list_code);
  
        -- Create the interface in the BNE_INTERFACES_B table
  
        INSERT INTO bne_interfaces_b
         (application_id,
          interface_code,
          object_version_number,
          integrator_app_id,
          integrator_code,
          interface_name,
          upload_type,
          upload_order,
          upload_param_list_app_id,
          upload_param_list_code,
          created_by,
          creation_date,
          last_updated_by,
          last_update_date)
        VALUES
          (p_application_id,
           p_interface_code,
           1,
           p_application_id,
           p_integrator_code,
           p_api_procedure_name,
           p_upload_type,
           1,
           p_application_id,
           p_param_list_code,
           p_user_id,
           SYSDATE,
           p_user_id,
           SYSDATE);

  
        -- Create the interface in the BNE_INTERFACES_TL table
  
        INSERT INTO bne_interfaces_tl
         (application_id,
          interface_code,
          language,
          source_lang,
          user_name,
          created_by,
          creation_date,
          last_updated_by,
          last_update_date)
        VALUES
         (p_application_id,
          p_interface_code,
          p_language,
          p_source_lang,
          p_interface_user_name,
          p_user_id,
          SYSDATE,
          p_user_id,
          SYSDATE);
  
        FOR api_param_rec IN api_params_c(p_application_id,
                                          p_interface_code,
                                          p_api_package_name,
                                          p_api_procedure_name,
                                          vn_overload,
                                          p_language,
                                          p_source_lang,
                                          p_user_id) loop
  
          IF api_param_rec.interface_col_name IS NULL THEN
            ln_sequence_sub := ln_sequence_sub + 1;
          ELSE
            /*
            || Fixup the sequence fields to exclude the null argument.
            || (which appears to be in the cursor results if it's a 
            || FUNCTION being queried).
            */
            api_param_rec.sequence_num               := api_param_rec.sequence_num - ln_sequence_sub;
            api_param_rec.display_order              := (api_param_rec.sequence_num * 10);
            api_param_rec.upload_param_list_item_num := api_param_rec.sequence_num;

            -- Generate the Interface Columns in table BNE_INTERFACE_COLS_B
    
            INSERT INTO bne_interface_cols_b
              (application_id,
               interface_code,
               object_version_number,
               sequence_num,
               interface_col_type,
               data_type,
               interface_col_name,
               not_null_flag,
               summary_flag,
               enabled_flag,
               display_flag,
               mapping_enabled_flag,
               required_flag,
               read_only_flag,
               created_by,
               creation_date,
               last_updated_by,
               last_update_date,
               display_order,
               upload_param_list_item_num)
             VALUES
              (api_param_rec.application_id,
               api_param_rec.interface_code,
               api_param_rec.object_version_number,
               api_param_rec.sequence_num,
               api_param_rec.interface_col_type,
               api_param_rec.data_type,
               api_param_rec.interface_col_name,
               api_param_rec.not_null_flag,
               api_param_rec.summary_flag,
               api_param_rec.enabled_flag,
               api_param_rec.display_flag,
               api_param_rec.mapping_enabled_flag,
               api_param_rec.required_flag,
               api_param_rec.read_only_flag,
               api_param_rec.created_by,
               api_param_rec.creation_date,
               api_param_rec.last_updated_by,
               api_param_rec.last_update_date,
               api_param_rec.display_order,
               api_param_rec.upload_param_list_item_num);
    
            -- Generate the Interface columns in table BNE_INTERFACE_COLS_TL
            
            INSERT INTO bne_interface_cols_tl
             (application_id,
              interface_code,
              sequence_num,
              language,
              source_lang,
              prompt_left,
              prompt_above,
              created_by,
              creation_date,
              last_updated_by,
              last_update_date)
            VALUES
             (api_param_rec.application_id,
              api_param_rec.interface_code,
              api_param_rec.sequence_num,
              api_param_rec.language,
              api_param_rec.source_lang,
              api_param_rec.prompt_left,
              api_param_rec.prompt_above,
              api_param_rec.created_by,
              api_param_rec.creation_date,
              api_param_rec.last_updated_by,
              api_param_rec.last_update_date);
    
            exit WHEN api_params_c%notfound;
          END IF;
  
        END loop;
  
        bne_integrator_utils.link_list_to_interface (
          p_application_id,
          p_param_list_code,
          p_application_id,
          p_interface_code);
  
      END IF;
  
    ELSE
     raise_application_error(-20000,'Object code invalid, Integrator: ' || p_application_id || ':' || p_object_code || ' has already been created');
    END IF;  
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_interface_for_api;


  PROCEDURE create_interface_for_api(
    pr_intg            IN out nocopy integrator_rec,
    pr_intf            IN out nocopy interface_rec,
    pv_param_list_code out bne_param_lists_b.param_list_code%TYPE) 
  IS
    lcv_proc_name       constant varchar2(30) := 'create_interface_for_api';  
    lv_integrator_code  bne_integrators_b.integrator_code%TYPE;
  BEGIN
    /*
    || Call our customised version of the bne_integrator_utils code.
    */
    create_interface_for_api(
      p_application_id        => pr_intf.appl_id,
      p_object_code           => pr_intg.intg_code, 
      p_integrator_code       => pr_intg.integrator_code,
      p_api_package_name      => pr_intf.api_package_name,
      p_api_procedure_name    => pr_intf.api_procedure_name,
      p_interface_user_name   => pr_intf.interface_user_name,
      p_param_list_name       => 'Parameter List for Interface '||pr_intg.intg_code||'_INTF', 
      p_api_type              => pr_intf.api_type,
      p_api_return_type       => pr_intf.api_return_type,
      p_upload_type           => 2, -- API
      p_language              => 'US', 
      p_source_lang           => 'US', 
      p_user_id               => pr_intg.user_id,
      p_param_list_code       => pv_param_list_code, 
      p_interface_code        => pr_intf.interface_code);
    log_msg(lcv_proc_name,'Successfully created Parameter List: '||pv_param_list_code);
    log_msg(lcv_proc_name,'Successfully created Interface: '||pr_intf.interface_code);
      
    UPDATE bne_attributes
    SET    attribute4 = NULL,
           attribute5 = 'N'
    WHERE  (application_id, attribute_code) = 
              (SELECT attribute_app_id, attribute_code
               FROM   bne_param_lists_b 
               WHERE  application_id = pr_intg.appl_id
               AND    param_list_code = pv_param_list_code);
    IF SQL%rowcount = 1 THEN
      log_msg(lcv_proc_name,'Updated flags in bne_attributes.');
    ELSE
      log_msg(lcv_proc_name,'WARNING: Failed to updat flags in bne_attributes.');
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_interface_for_api;

  PROCEDURE create_form_function(
    pr_intg         IN out nocopy integrator_rec)
  IS 
    lcv_proc_name       constant varchar2(30) := 'create_form_function';  
    lv_jsp_agent        VARCHAR2(2000) := fnd_profile.VALUE('APPS_JSP_AGENT');
    lv_function_name    fnd_form_functions.function_name%TYPE;
    lv_parameters       fnd_form_functions.parameters%type;  
    
    procedure build_param_string(pv_param_string in varchar2) is
    begin
      if lv_parameters is null then
        lv_parameters := pv_param_string;
      else
        lv_parameters := lv_parameters || chr(38) || pv_param_string;
      end if;
    end build_param_string;
      
  BEGIN
    IF lv_jsp_agent IS NULL THEN
      raise_application_error(-20001,'''Applications JSP Agent'' Profile Option not set');
    end if;
    lv_function_name := 'BNE'||nvl(pr_intg.intg_prefix,pr_intg.intg_code);    
    build_param_string('bne:page=BneCreateDoc');
    build_param_string('bne:integrator='||pr_intg.integrator_code);    
    if pr_intg.viewer is not null then
      build_param_string('bne:viewer='||pr_intg.viewer);
    end if;     
    build_param_string('bne_reporting=N');
    if pr_intg.layouts.count = 1 then
      --build_param_string('bne:layout='||pr_intg.appl_id||':'||pr_intg.layouts(pr_intg.layouts.first).layout_code);
      if pr_intg.layouts(pr_intg.layouts.first).update_map.force_content then
        build_param_string('bne:content='||pr_intg.appl_id||':'||pr_intg.layouts(pr_intg.layouts.first).update_map.sql_content_code);
        build_param_string('bne:map='||pr_intg.appl_id||':'||pr_intg.layouts(pr_intg.layouts.first).update_map.mapping_code);
      end if;
    end if;
    
    log_msg(lcv_proc_name,'Form Function Parameters will be: '||lv_parameters);
                               
    fnd_form_functions_pkg.load_row (
      x_function_name            => lv_function_name,
      x_application_short_name   => NULL,
      x_form_name                => NULL,
      x_parameters               => lv_parameters,
      x_type                     => 'SERVLET',
      x_web_host_name            => lv_jsp_agent,
      x_web_agent_name           => NULL,
      x_web_html_call            => 'BneApplicationService',
      x_web_encrypt_parameters   => NULL,
      x_web_secured              => NULL,
      x_web_icon                 => NULL,
      x_object_name              => NULL,
      x_region_application_name  => NULL,
      x_region_code              => NULL,
      x_user_function_name       => lv_function_name,
      x_description              => 'Created by Web ADI Create Document Process',
      x_owner                    => 'ORACLE',
      x_custom_mode              => NULL,  -- (could be FORCE)
      x_last_update_date         => to_char(SYSDATE,gcv_date_format),
      x_maintenance_mode_support => 'NONE',
      x_context_dependence       => 'RESP',
      x_jrad_ref_path            => NULL
    );
    log_msg(lcv_proc_name,'Created / updated form function: '||lv_function_name);
    update fnd_form_functions
    set    parameters = lv_parameters
    where  function_name = lv_function_name
    and    parameters != lv_parameters;
    if sql%rowcount = 1 then
      log_msg(lcv_proc_name,'Corrected function parameters for function: '||lv_function_name);
    end if;
    /*
    || Link security rule to the function
    */
    bne_security_utils_pkg.add_object_rules (
      p_application_id => pr_intg.appl_id,
      p_object_code    => pr_intg.integrator_code,
      p_object_type    => 'INTEGRATOR',
      p_security_code  => pr_intg.integrator_code,
      p_security_type  => 'FUNCTION',
      p_security_value => lv_function_name,
      p_user_id        => pr_intg.user_id);
    log_msg(lcv_proc_name,'Linked function: '||lv_function_name||' to integrator '||pr_intg.integrator_code);    
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_form_function;

  PROCEDURE create_layouts(
    pr_intg                IN out nocopy integrator_rec,
    pr_intf                IN out nocopy interface_rec)
  IS 
    lcv_proc_name   constant varchar2(30) := 'create_layouts';  
    ln_layout_index number;
        
    PROCEDURE call_layout_apis(
      pv_layout_code         IN out nocopy VARCHAR2,
      pv_layout_name         IN VARCHAR2,
      pb_create_header_block IN BOOLEAN) 
    IS
      CURSOR layout_csr IS
        SELECT blb.*, blt.user_name, blt.prompt_above
        FROM   bne_layout_blocks_b blb,
               bne_layout_blocks_tl blt
        WHERE  blb.application_id = pr_intg.appl_id
        AND    blb.layout_code = pv_layout_code
        AND    blb.application_id = blt.application_id
        AND    blb.layout_code = blt.layout_code
        AND    blt.language = 'US';
      lr_layout layout_csr%rowtype;    
    BEGIN
      /*
      || Create a layout for the webadi
      */
      log_msg(lcv_proc_name,'Calling bne_integrator_utils.create_default_layout with:');
      log_msg(lcv_proc_name,'appl_id        : '||pr_intg.appl_id);
      log_msg(lcv_proc_name,'intg_code      : '||pr_intg.intg_code);
      log_msg(lcv_proc_name,'integrator_code: '||pr_intg.integrator_code);
      log_msg(lcv_proc_name,'interface_code : '||pr_intf.interface_code);
      bne_integrator_utils.create_default_layout(
        p_application_id       => pr_intg.appl_id,
        p_object_code          => pr_intg.intg_code, 
        p_integrator_code      => pr_intg.integrator_code,
        p_interface_code       => pr_intf.interface_code,
        p_user_id              => pr_intg.user_id,
        p_force                => FALSE,
        p_all_columns          => TRUE,
        p_layout_code          => pv_layout_code);
      log_msg(lcv_proc_name,'Created default layout '||pv_layout_code);    
      
      /*
      || Update the name of the layout if required
      */
      IF pv_layout_name IS NOT NULL THEN
        UPDATE bne_layouts_tl
        SET    user_name      = pv_layout_name
        WHERE  application_id = pr_intg.appl_id
        AND    layout_code    = pv_layout_code
        AND    language       = userenv('LANG');
    
        IF(SQL%rowcount = 1) THEN
          log_msg(lcv_proc_name,'Update layout User Name to "'||pv_layout_name||'"');    
        ELSE
          log_msg(lcv_proc_name,'WARNING: [call_layout_api] Failed to set User Name for layout '||pv_layout_code);
        END IF;
      END IF;
      /*
      || Enable hints display if there are any
      */
      UPDATE bne_layout_blocks_b
      SET    hint_displayed_flag = 'Y'
      WHERE  application_id  = pr_intg.appl_id
      and    layout_code     = pv_layout_code
      AND    row_style_class = 'BNE_LINES_ROW'
      AND    exists (SELECT 'user hint exists' 
                     FROM   bne_layout_cols blc,
                            bne_interface_cols_tl bict
                     WHERE  blc.application_id = pr_intg.appl_id
                     AND    blc.layout_code = pv_layout_code
                     AND    blc.interface_app_id = bict.application_ID
                     AND    blc.interface_code = bict.interface_code
                     AND    blc.interface_seq_num = bict.sequence_num
                     AND    bict.language = userenv('LANG')
                     AND    bict.user_hint IS NOT NULL);
      IF SQL%rowcount = 1 THEN
        log_msg(lcv_proc_name,'Enabled hints for layout '||pr_intg.appl_id||':'||pv_layout_code);
      END IF;
      
      IF pb_create_header_block THEN
        /*
        || Create a header block
        */
        log_heading(lcv_proc_name,'Creating a header block for layout_code: '||pr_intg.appl_id||':'||pv_layout_code);
        BEGIN
          /*
          || Move the lines block to level 2 so we can have a header block.  
          */
          UPDATE bne_layout_blocks_tl
          set    block_id = 2
          where  (application_id, layout_code, block_id, language)
                   in (select application_id, layout_code, block_id, 'US'
                       from   bne_layout_blocks_b
                       where  application_id = pr_intg.appl_id
                       and    layout_code    = pv_layout_code
                       and    row_style_class = 'BNE_LINES_ROW');
          log_msg(lcv_proc_name,'Moved '||sql%rowcount||' bne_layout_blocks_tl records for layout_code: '||pr_intg.appl_id||':'||pv_layout_code);

          UPDATE bne_layout_blocks_b
          SET    block_id = 2, parent_id = 1, sequence_num = 2
          WHERE  application_id = pr_intg.appl_id
          AND    layout_code    = pv_layout_code
          and    row_style_class = 'BNE_LINES_ROW';
          log_msg(lcv_proc_name,'Moved '||SQL%rowcount||' bne_layout_blocks_b records for layout_code: '||pr_intg.appl_id||':'||pv_layout_code);
          
          /*
          || Get the attributes from the existing layout so we can call the
          || api to create the header block
          */
          OPEN layout_csr;
          fetch layout_csr INTO lr_layout;
          IF layout_csr%notfound THEN
            CLOSE layout_csr;
            raise_application_error(-20001,'Unable to find layout: '||pv_layout_code);
          END IF;
          CLOSE layout_csr;          
          /*
          || Create a header block
          */
          log_msg(lcv_proc_name,'Loading header row into bne_layout_blocks_b for layout_code: '||pr_intg.appl_id||':'||pv_layout_code);
          bne_layout_blocks_pkg.load_row(
            x_layout_asn            => pr_intg.asn,
            x_layout_code           => pv_layout_code,
            x_block_id              => 1,
            x_object_version_number => lr_layout.object_version_number,
            x_parent_id             => NULL, -- 1.6 (WS): parent ID should be NULL and not what the line is
            x_layout_element        => 'HEADER 1',
            x_style_class           => 'BNE_HEADER',
            x_style                 => lr_layout.style,
            x_row_style_class       => 'BNE_HEADER_ROW',
            x_row_style             => lr_layout.row_style,
            x_col_style_class       => lr_layout.col_style_class,
            x_col_style             => lr_layout.col_style,
            x_prompt_displayed_flag => lr_layout.prompt_displayed_flag,
            x_prompt_style_class    => 'BNE_HEADER_HEADER',
            x_prompt_style          => lr_layout.prompt_style,
            x_hint_displayed_flag   => 'Y',
            x_hint_style_class      => 'BNE_HEADER_HINT',
            x_hint_style            => lr_layout.hint_style,
            x_orientation           => 'HORIZONTAL',
            x_layout_control        => 'COLUMN_FLOW',
            x_display_flag          => lr_layout.display_flag,
            x_blocksize             => 1,
            x_minsize               => lr_layout.minsize,
            x_maxsize               => lr_layout.MAXSIZE,
            x_sequence_num          => 1,
            x_prompt_colspan        => 2,
            x_hint_colspan          => 1,
            x_row_colspan           => 2,
            x_summary_style_class   => lr_layout.summary_style_class,
            x_summary_style         => lr_layout.summary_style,
            x_user_name             => 'Header 1', -- 15-Jan-2013 (WS): corrected --lr_layout.user_name,
            x_owner                 => 'ORACLE',
            x_last_update_date      => to_char(lr_layout.last_update_date,gcv_date_format),
            x_custom_mode           => NULL,
            x_prompt_above          => lr_layout.prompt_above,
            x_title_style_class     => lr_layout.title_style_class,
            x_title_style           => lr_layout.title_style);

          update bne_layout_blocks_b
          SET    created_by =  pr_intg.user_id, last_updated_by =  pr_intg.user_id, last_update_login =  pr_intg.user_id
          WHERE  application_id = pr_intg.appl_id
          AND    layout_code    = pv_layout_code
          AND    block_id       = 1;
          log_msg(lcv_proc_name,'Corrected WHO columns for '||SQL%rowcount||' blocks in bne_layout_blocks_b for layout_code: '||pr_intg.appl_id||':'||pv_layout_code);
          
          UPDATE bne_layout_blocks_tl
          SET    created_by =  pr_intg.user_id, last_updated_by =  pr_intg.user_id, last_update_login =  pr_intg.user_id
          WHERE  application_id = pr_intg.appl_id
          AND    layout_code    = pv_layout_code
          and    block_id       = 1;
          log_msg(lcv_proc_name,'Corrected WHO columns for '||SQL%rowcount||' blocks in bne_layout_blocks_tl for layout_code: '||pr_intg.appl_id||':'||pv_layout_code);
        END;
      END IF;      
    END call_layout_apis;    
  BEGIN
    ln_layout_index := pr_intg.layouts.FIRST;
    while ln_layout_index is not null loop
      log_heading(lcv_proc_name,'Calling layout APIS for Layout ['||ln_layout_index||'] '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
      /*
      || Create default layout
      */
      call_layout_apis(
        pv_layout_code         => pr_intg.layouts(ln_layout_index).layout_code,
        pv_layout_name         => pr_intg.layouts(ln_layout_index).layout_name,
        pb_create_header_block => pr_intg.layouts(ln_layout_index).create_header_block);
                
      /*
      || Adjust the default number of rows displayed
      */
      IF nvl(pr_intg.layouts(ln_layout_index).number_of_rows,10) != 10 THEN
        UPDATE bne_layout_blocks_b
        SET    BLOCKSIZE      = pr_intg.layouts(ln_layout_index).number_of_rows
        WHERE  application_id = pr_intg.appl_id
        AND    layout_code    = pr_intg.layouts(ln_layout_index).layout_code
        AND    row_style_class = 'BNE_LINES_ROW';
        IF SQL%rowcount = 1 THEN
          log_msg(lcv_proc_name,'Adjusted layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code||' to have '||pr_intg.layouts(ln_layout_index).number_of_rows||' rows in the spreadsheet.');
        ELSE
          log_msg(lcv_proc_name,'WARNING: [create_layouts] Failed to adjust layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code||' to have '||pr_intg.layouts(ln_layout_index).number_of_rows||' rows in the spreadsheet.');
        END IF;
      END IF;
      ln_layout_index := pr_intg.layouts.next(ln_layout_index);
    end loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_layouts;
  
  function imp_param_and_imp_not_reqd(
    pv_param_name         in bne_param_defns_b.param_name%type,
    pv_conc_prog          in fnd_concurrent_programs.concurrent_program_name%type,
    pv_plsql_api          in bne_param_list_items.param_name%type)
  return varchar2 is
    lv_return_val        varchar2(1) := 'N';
  begin
    if pv_param_name = 'bne:import' and
      (pv_conc_prog is null and
       pv_plsql_api is null) then
      lv_return_val := 'Y';
    end if;
    return lv_return_val;
  end imp_param_and_imp_not_reqd;
  
  FUNCTION create_param_list(
    pr_intg         in out nocopy integrator_rec,
    pv_prefix       in varchar2,
    pv_user_name    in varchar2,
    pv_persistent   in varchar2 default 'Y',
    pv_comments     in varchar2 default NULL,
    pv_prompt_above in varchar2 default NULL) 
  return varchar2 is
    lv_param_list_code   bne_param_lists_b.param_list_code%TYPE;  
    lv_key               varchar2(250);
    lcv_proc_name        constant varchar2(30) := 'create_param_list';  
  BEGIN
    /*
    || Generate a unique param_list_code
    */  
    lv_param_list_code := create_unique_param_list_code(
      pv_prefix  => pv_prefix,
      pn_appl_id => case pv_prefix
                    when 'UPL' then
                      gcn_webadi_appl
                    else
                      pr_intg.appl_id
                    end);
    /*
    || Call the API to create it
    */    
    lv_key := bne_parameter_utils.create_param_list_all(  
      p_application_id   => pr_intg.appl_id,
      p_param_list_code  => lv_param_list_code,
      p_persistent       => pv_persistent,
      p_comments         => pv_comments,
      p_attribute_app_id => NULL,
      p_attribute_code   => NULL,
      p_list_resolver    => NULL,
      p_prompt_left      => null,
      p_prompt_above     => pv_prompt_above,
      p_user_name        => pv_user_name,
      p_user_tip         => null);
    log_msg(lcv_proc_name,'Created Parameter List ['||pr_intg.appl_id||':'||lv_param_list_code||']');
    /*
    || Correct the who fields
    */
    UPDATE bne_param_lists_b
    SET    created_by        = pr_intg.user_id, 
           last_updated_by   = pr_intg.user_id, 
           last_update_login =  pr_intg.user_id
    WHERE  application_id  = pr_intg.appl_id
    AND    param_list_code = lv_param_list_code;
    
    UPDATE bne_param_lists_tl
    SET    created_by        = pr_intg.user_id, 
           last_updated_by   = pr_intg.user_id, 
           last_update_login =  pr_intg.user_id
    WHERE  application_id = pr_intg.appl_id
    and    param_list_code = lv_param_list_code;
    log_msg(lcv_proc_name,'Updated WHO information for Parameter List ['||lv_key||']');
    RETURN (lv_param_list_code);
  END create_param_list;
  
  PROCEDURE create_default_uploader(
    pr_intg                IN out nocopy integrator_rec,
    pr_conc_prog           IN out nocopy conc_prog_rec,
    pr_plsql_api           IN out nocopy plsql_api_rec)
  is
    lcv_proc_name        constant varchar2(30) := 'create_default_uploader';  
    lv_param_list_code   bne_param_lists_b.param_list_code%TYPE;
    lv_param_defn_code   bne_param_list_items.param_defn_code%TYPE;
    ln_sequence_num      bne_param_list_items.sequence_num%TYPE;
    lv_key               VARCHAR2(250);
    lv_key2              VARCHAR2(250);
    lv_bool_flag         VARCHAR2(1);
  begin
    lv_param_list_code := create_param_list(
      pr_intg         => pr_intg,
      pv_prefix       => 'UPL',
      pv_user_name    => pr_intg.intg_name||' - Uploader',
      pv_comments     => 'Upload Page',
      pv_prompt_above => pr_intg.intg_name||' - Uploader');
    /*
    || Create uploader list items
    */
    FOR datarec IN (SELECT bd.param_name, 
                           bd.param_source,
                           bd.param_category,
                           bd.datatype,
                           bd.attribute_app_id,
                           bd.attribute_code,
                           bd.param_resolver,
                           case
                           when imp_param_and_imp_not_reqd(
                             bd.param_name, 
                             pr_conc_prog.concurrent_program_name, 
                             pr_plsql_api.procedure_name) = 'Y' then
                             'N'
                           else
                             bd.default_required_flag
                           end default_required_flag,
                           case
                           when imp_param_and_imp_not_reqd(
                             bd.param_name, 
                             pr_conc_prog.concurrent_program_name, 
                             pr_plsql_api.procedure_name) = 'Y' then
                             'N'
                           else
                             bd.default_visible_flag
                           end default_visible_flag,
                           bd.default_user_modifyable_flag,
                           decode(bd.param_name,
                             'bne:rows','FLAGGED',
                             bd.default_string) default_string,
                           bd.default_date,
                           bd.default_number,
                           case
                           when imp_param_and_imp_not_reqd(
                             bd.param_name, 
                             pr_conc_prog.concurrent_program_name, 
                             pr_plsql_api.procedure_name) = 'Y' then
                             'N'
                           when bd.param_name = 'bne:import' then
                             'Y'
                           else
                             bd.default_boolean_flag
                           end default_boolean_flag,
                           bd.default_formula,
                           bd.val_type,
                           bd.val_value,
                           bd.max_size,
                           bd.display_type,
                           bd.display_style,
                           bd.display_size,
                           bd.help_url,
                           bd.format_mask,
                           bdt.default_desc, 
                           bdt.prompt_left, 
                           bdt.prompt_above, 
                           bdt.user_name, 
                           case
                           when bd.param_name = 'bne:import' then
                             case
                             when pr_conc_prog.user_conc_prog_name is not null then
                               'Submit "'||pr_conc_prog.user_conc_prog_name||'" if all records validate and upload successfully'
                             when pr_plsql_api.procedure_name is not null then
                               'Run "'||pr_plsql_api.importer_name||'" if all records validate and upload successfully'
                             else
                               bdt.user_tip
                             end
                           else
                             bdt.user_tip
                           end user_tip, 
                           bdt.access_key
                    FROM   bne_param_list_items bl, 
                           bne_param_defns_b bd,
                           bne_param_defns_tl bdt
                    WHERE  bl.param_list_code = 'UPL_TEMPLATE'
                    AND    bl.param_defn_code = bd.param_defn_code
                    AND    bl.param_defn_app_id = bd.application_id
                    AND    bd.param_defn_code = bdt.param_defn_code
                    AND    bd.application_id = bdt.application_id
                    AND    bdt.language = 'US'
                    ORDER BY bl.sequence_num) loop
      /*
      || Generate a unique param_defn_code
      */
      lv_param_defn_code := create_unique_param_defn_code(
        pv_prefix       => 'UPL_PARAM_',
        pn_appl_id      => gcn_webadi_appl);
      /*
      || Call the API to create it
      */
      log_msg(lcv_proc_name,'Creating Parameter ['||gcn_webadi_appl||':'||lv_param_defn_code||'] User Name '''||datarec.user_name||'''');
      log_msg(lcv_proc_name,'  Param Name '||datarec.param_name||' Source '||datarec.param_source||'''');
      log_msg(lcv_proc_name,'  Attribute ['||datarec.attribute_app_id||':'||datarec.attribute_code||']');
      
      lv_key2 := bne_parameter_utils.create_param_all(  
        p_application_id    => gcn_webadi_appl,
        p_param_code        => lv_param_defn_code,
        p_param_name        => datarec.param_name,
        p_param_source      => datarec.param_source,
        p_category          => datarec.param_category,
        p_data_type         => datarec.datatype,
        p_attribute_app_id  => datarec.attribute_app_id,
        p_attribute_code    => datarec.attribute_code,
        p_param_resolver    => datarec.param_resolver,
        p_required          => datarec.default_required_flag,
        p_visible           => datarec.default_visible_flag,
        p_modifyable        => datarec.default_user_modifyable_flag,
        p_default_string    => datarec.default_string,
        p_default_date      => datarec.default_date,
        p_default_num       => datarec.default_number,
        p_default_boolean   => datarec.default_boolean_flag,
        p_default_formula   => datarec.default_formula,
        p_val_type          => datarec.val_type,
        p_val_value         => datarec.val_value,
        p_maximum_size      => datarec.max_size,
        p_display_type      => datarec.display_type,
        p_display_style     => datarec.display_style,
        p_display_size      => datarec.display_size,
        p_help_url          => datarec.help_url,
        p_format_mask       => datarec.format_mask,
        p_default_desc      => datarec.default_desc,
        p_prompt_left       => datarec.prompt_left,
        p_prompt_above      => datarec.prompt_above,
        p_user_name         => datarec.user_name,
        p_user_tip          => datarec.user_tip,
        p_access_key        => datarec.access_key);
      /*
      || Correct the who fields
      */
      UPDATE bne_param_defns_b
      SET    created_by        = pr_intg.user_id, 
             last_updated_by   = pr_intg.user_id, 
             last_update_login = pr_intg.user_id
      WHERE  application_id  = gcn_webadi_appl
      AND    param_defn_code = lv_param_defn_code;
      
      UPDATE bne_param_defns_tl
      SET    created_by        = pr_intg.user_id, 
             last_updated_by   = pr_intg.user_id, 
             last_update_login = pr_intg.user_id
      WHERE  application_id  = gcn_webadi_appl
      AND    param_defn_code = lv_param_defn_code;
      
      -- 1.3 (WS): hide the import row if no importer is specified
      IF datarec.param_name = 'bne:import' AND pr_conc_prog.concurrent_program_name IS NULL AND pr_plsql_api.procedure_name IS NULL THEN
        UPDATE bne_param_defns_b
        SET    default_visible_flag = 'N', 
               default_required_flag= 'N' 
        WHERE  application_id  = gcn_webadi_appl
        AND    param_defn_code = lv_param_defn_code;
      END IF;
      
      log_msg(lcv_proc_name,'Created Parameter: '||gcn_webadi_appl||':'||lv_param_defn_code);
      /*
      || Create the parameter list item
      */
      IF datarec.param_name = 'bne:import' THEN
         IF pr_conc_prog.concurrent_program_name IS NOT NULL OR
           pr_plsql_api.procedure_name IS NOT NULL THEN
           lv_bool_flag := 'Y';
         ELSE
           lv_bool_flag := 'N';
         END IF;
      ELSE
        lv_bool_flag := NULL;
      END IF;
        
      IF (datarec.param_name = 'bne:import' AND lv_bool_flag IS NOT NULL) OR
         datarec.param_name != 'bne:import' THEN
        ln_sequence_num := bne_parameter_utils.create_list_items_all(
          p_application_id      => pr_intg.appl_id,
          p_param_list_code     => lv_param_list_code,
          p_param_defn_app_id   => gcn_webadi_appl,
          p_param_defn_code     => lv_param_defn_code,
          p_param_name          => datarec.param_name,
          p_attribute_app_id    => NULL,
          p_attribute_code      => NULL,
          p_string_val          => NULL,
          p_date_val            => NULL,
          p_number_val          => NULL,
          p_boolean_val         => lv_bool_flag,
          p_formula             => NULL,
          p_desc_val            => lv_bool_flag);
        /*
        || Correct the who fields
        */
        UPDATE bne_param_list_items
        SET    created_by        = pr_intg.user_id, 
               last_updated_by   = pr_intg.user_id, 
               last_update_login = pr_intg.user_id
        WHERE  application_id  = pr_intg.appl_id
        AND    param_list_code = lv_param_list_code
        AND    sequence_num    = ln_sequence_num;
        log_msg(lcv_proc_name,'Created list item: '||pr_intg.appl_id||':'||lv_param_list_code||' - '||lv_param_defn_code);
      END IF;
    END loop;                  
    /*
    || Link the uploader to the integrator
    */
    UPDATE bne_integrators_b
    SET    upload_param_list_app_id = pr_intg.appl_id,
           upload_param_list_code = lv_param_list_code
    WHERE  integrator_code = pr_intg.integrator_code;
    log_msg(lcv_proc_name,'Linked the default uploader to the integrator');
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_default_uploader;
  
  FUNCTION create_pre_import_rules(
    pr_intg         IN out nocopy integrator_rec,
    pr_conc_prog    in out nocopy conc_prog_rec)
  return number is
    lcv_proc_name      constant varchar2(30) := 'create_pre_import_rules';  
    ln_pir_index       NUMBER;
    ln_pir_col_index   NUMBER;
    lv_sql_grp_plc     bne_param_lists_b.param_list_code%TYPE;
    lv_attribute3      bne_attributes.attribute3%TYPE;
    lv_attribute4      bne_attributes.attribute4%type;
    ln_seq_num         number := 1;
    ln_sql_grp_seq_num NUMBER;
  BEGIN
    ln_pir_index := pr_conc_prog.pre_import_rules.FIRST;
    while ln_pir_index IS NOT NULL loop
      /*
      || Create the parameter list parent record for the import sql statement
      */
      lv_sql_grp_plc := create_unique_param_list_code('SQL',pr_conc_prog.appl_id);
      
      bne_param_lists_pkg.load_row(
        x_param_list_asn        => pr_conc_prog.asn,
        x_param_list_code       => lv_sql_grp_plc,
        x_object_version_number => 1,
        x_persistent_flag       => 'Y',
        x_comments              => pr_conc_prog.pre_import_rules(ln_pir_index).rule_desc,
        x_attribute_asn         => NULL,
        x_attribute_code        => NULL,
        x_list_resolver         => NULL,
        x_user_tip              => NULL,
        x_prompt_left           => NULL,
        x_prompt_above          => NULL,
        x_user_name             => pr_conc_prog.pre_import_rules(ln_pir_index).rule_name,
        x_owner                 => pr_intg.created_by_user,
        x_last_update_date      => to_char(SYSDATE,gcv_date_format),
        x_custom_mode           => 'FORCE');
      log_msg(lcv_proc_name,'Created Parameter List '''||lv_sql_grp_plc||''' for import rule sql #'||ln_pir_index);
      /*
      || Create a parameter list item for the sql statement
      */
      ln_sql_grp_seq_num := 1;
      create_param_list_item(
        pv_created_by        => pr_intg.created_by_user,
        pv_param_list_asn    => pr_conc_prog.asn,
        pv_param_list_code   => lv_sql_grp_plc,
        pn_seq_num           => ln_sql_grp_seq_num,
        pv_param_name        => 'SQL Statement',
        pv_string_value      => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement,
        pv_desc_value        => pr_conc_prog.pre_import_rules(ln_pir_index).rule_desc,
        pb_increment_seq     => FALSE);
        
      log_msg(lcv_proc_name,'Created SQL Parameter List Item');
      /*
      || Now loop through the columns from the statement creating attributes
      || and parameter list items
      */
      ln_pir_col_index := pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols.FIRST;
      while ln_pir_col_index IS NOT NULL loop
        pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).attribute_code := 
          create_unique_attribute_code(
            pv_prefix  => 'ATT',
            pn_appl_id => pr_conc_prog.appl_id);
        
        field_validate(
          pv_field_name => 'concurrent program pre import rule '||ln_pir_index||' column '||ln_pir_col_index||' - sql_col_type',
          pv_field_val  => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).sql_col_type,
          pav_vals      => NEW valid_vals_table('BIND','RETURN'));        
        field_validate(
          pv_field_name  => 'concurrent program pre import rule '||ln_pir_index||' column '||ln_pir_col_index||' - data_type',
          pv_field_val   => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).data_type,
          pav_vals       => NEW valid_vals_table('DATE','NUMBER','VARCHAR2'),
          pv_default_val => 'NULL');           
        
        IF pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).sql_col_type = 'BIND' THEN
          lv_attribute3 := 'IMPORT';
          lv_attribute4 := pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).import_source;
        ELSE
          lv_attribute3 := NULL;
          lv_attribute4 := NULL;
        END IF;
        
        create_bne_attribute(
          pv_asn              => pr_conc_prog.asn,
          pv_attr_code        => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).attribute_code,
          pv_attribute1       => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).sql_col_type,
          pv_attribute2       => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).adi_reference_name,
          pv_attribute3       => lv_attribute3,
          pv_attribute4       => lv_attribute4,
          pv_attribute5       => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).data_type,
          pv_attribute6       => ln_pir_col_index,
          pv_created_by       => pr_intg.created_by_user);
        log_msg(lcv_proc_name,'Loaded Attribute: '||pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).attribute_code||' for import rule sql #'||ln_pir_index);
        
        ln_sql_grp_seq_num := 1 + ln_pir_col_index;
        create_param_list_item(
          pv_created_by        => pr_intg.created_by_user,
          pv_param_list_asn    => pr_conc_prog.asn,
          pv_param_list_code   => lv_sql_grp_plc,
          pn_seq_num           => ln_sql_grp_seq_num,
          pv_param_name        => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).sql_col_name,
          pv_attrb_defn_asn    => pr_conc_prog.asn,
          pv_attrb_defn_code   => pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).attribute_code,
          pb_increment_seq     => FALSE);
        log_msg(lcv_proc_name,'Created Parameter List Item for column '||pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols(ln_pir_col_index).sql_col_name);
        ln_pir_col_index := pr_conc_prog.pre_import_rules(ln_pir_index).sql_statement_cols.NEXT(ln_pir_col_index);
      END loop;
      /*
      || Now attach the pre-import rule to the importer
      */
      log_msg(lcv_proc_name,'Creating Parameter List Item pre-import sql #'||ln_pir_index);
      create_param_list_item(
        pv_created_by        => pr_intg.created_by_user,
        pv_param_list_asn    => pr_conc_prog.asn,
        pv_param_list_code   => pr_conc_prog.param_list_code,
        pn_seq_num           => ln_seq_num,
        pv_param_name        => 'SQL',
        pv_string_value      => pr_conc_prog.appl_id||':'||lv_sql_grp_plc,
        pv_desc_value        => pr_conc_prog.pre_import_rules(ln_pir_index).rule_desc);
      ln_pir_index := pr_conc_prog.pre_import_rules.NEXT(ln_pir_index);
    END loop;  
    RETURN(ln_seq_num);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_pre_import_rules;
  
  PROCEDURE create_conc_prog_importer(
    pr_intg         IN out nocopy integrator_rec,
    pr_conc_prog    IN out nocopy conc_prog_rec)
  IS
    lcv_proc_name  constant varchar2(30) := 'create_conc_prog_importer';  
    lv_param_defn  bne_param_defns_vl.param_defn_code%TYPE;
    ln_seq_num     NUMBER;
    
    TYPE param_rec IS record
    (asn          fnd_application.application_short_name%TYPE,
     appl_id      bne_param_lists_b.application_id%TYPE,
     param_list   bne_param_lists_b.param_list_code%TYPE,
     string_val   bne_param_list_items.string_value%TYPE);
    TYPE param_table IS TABLE OF param_rec INDEX BY VARCHAR2(20);
    lar_std_params   param_table;

    TYPE attrib_rec IS record
    (asn          fnd_application.application_short_name%TYPE,
     appl_id      bne_param_list_items.attribute_app_id%TYPE,
     attr_code    bne_param_list_items.attribute_code%TYPE,
     param_name   bne_param_list_items.param_name%TYPE,
     attr_name    bne_attributes.attribute1%TYPE,
     attr_length  fnd_descr_flex_col_usage_vl.display_size%TYPE);
    TYPE attrib_table IS TABLE OF attrib_rec INDEX BY binary_integer;
    lar_attrib   attrib_table;
  BEGIN
    /*
    || Create success param defn
    */
    lv_param_defn := substr('SUCCESS_MSG_'||pr_conc_prog.concurrent_program_name,1,30);
    pr_conc_prog.appl_id := pr_intg.appl_id;
    
    bne_param_defns_pkg.load_row(
      x_param_defn_asn               => pr_conc_prog.asn,
      x_param_defn_code              => lv_param_defn,
      x_object_version_number        => 1,
      x_param_name                   => 'Success Message',
      x_param_source                 => 'WEBADI:ImporterSuccess',
      x_param_category               => 5,
      x_datatype                     => 1,
      x_attribute_asn                => NULL,
      x_attribute_code               => NULL,
      x_param_resolver               => NULL,
      x_default_required_flag        => 'Y',
      x_default_visible_flag         => 'Y',
      x_default_user_modifyable_flag => 'Y',
      x_default_string               => pr_conc_prog.user_conc_prog_name||' Request ID $import$.requestid',
      x_default_string_trans_flag    => 'Y',
      x_default_date                 => NULL,
      x_default_number               => NULL,
      x_default_boolean_flag         => NULL,
      x_default_formula              => NULL,
      x_val_type                     => 1,
      x_val_value                    => NULL,
      x_max_size                     => 100,
      x_display_type                 => 3,
      x_display_style                => 1,
      x_display_size                 => 100,
      x_help_url                     => NULL,
      x_format_mask                  => NULL,
      x_user_name                    => 'Success Message',
      x_default_desc                 => 'Message returned upon '||pr_conc_prog.user_conc_prog_name||' execution',
      x_prompt_left                  => NULL,
      x_prompt_above                 => NULL,
      x_user_tip                     => NULL,
      x_access_key                   => NULL,
      x_owner                        => pr_intg.created_by_user,
      x_last_update_date             => to_char(SYSDATE,gcv_date_format),
      x_custom_mode                  => 'FORCE',
      x_oa_flex_asn                  => NULL,
      x_oa_flex_code                 => NULL,
      x_oa_flex_num                  => NULL);
    log_msg(lcv_proc_name,'Created ''Success Message'' Parameter Definition');
    /*
    || Now create the 6 standard param list items for the 
    || pr_conc_prog.param_list_code parameter list.
    */
    lar_std_params('GROUP').asn        := pr_conc_prog.asn;
    lar_std_params('GROUP').appl_id    := pr_conc_prog.appl_id;
    lar_std_params('GROUP').param_list := create_unique_param_list_code(
                                            pv_prefix  => 'GRP',
                                            pn_appl_id => pr_conc_prog.appl_id);
    lar_std_params('GROUP').string_val := lar_std_params('GROUP').appl_id||':'||lar_std_params('GROUP').param_list;
     
    lar_std_params('ROWM').asn         := pr_conc_prog.asn;
    lar_std_params('ROWM').appl_id     := pr_conc_prog.appl_id;
    lar_std_params('ROWM').param_list  := create_unique_param_list_code(
                                            pv_prefix  => 'RWM',
                                            pn_appl_id => pr_conc_prog.appl_id);
    lar_std_params('ROWM').string_val  := lar_std_params('ROWM').appl_id||':'||lar_std_params('ROWM').param_list;

    lar_std_params('CONCP').asn        := pr_conc_prog.asn;
    lar_std_params('CONCP').appl_id    := pr_conc_prog.appl_id;
    lar_std_params('CONCP').param_list := create_unique_param_list_code(
                                            pv_prefix  => 'CPR',
                                            pn_appl_id => pr_conc_prog.appl_id);
    lar_std_params('CONCP').string_val := lar_std_params('CONCP').appl_id||':'||lar_std_params('CONCP').param_list;

    lar_std_params('ERRR').asn         := pr_conc_prog.asn;
    lar_std_params('ERRR').appl_id     := pr_conc_prog.appl_id;
    lar_std_params('ERRR').param_list  := create_unique_param_list_code(
                                            pv_prefix  => 'ERR',
                                            pn_appl_id => pr_conc_prog.appl_id);
    lar_std_params('ERRR').string_val  := lar_std_params('ERRR').appl_id||':'||lar_std_params('ERRR').param_list;

    lar_std_params('ERRL').asn         := pr_conc_prog.asn;
    lar_std_params('ERRL').appl_id     := pr_conc_prog.appl_id;
    lar_std_params('ERRL').param_list  := create_unique_param_list_code(
                                            pv_prefix  => 'ERL',
                                            pn_appl_id => pr_conc_prog.appl_id);
    lar_std_params('ERRL').string_val  := lar_std_params('ERRL').appl_id||':'||lar_std_params('ERRL').param_list;
    /*
    || Clear out any parameter list entries
    */
    DECLARE
      lv_index VARCHAR2(150);
    BEGIN
      lv_index := lar_std_params.FIRST;
      while lv_index IS NOT NULL loop
        DELETE FROM bne_param_list_items
        WHERE  param_list_code = lar_std_params(lv_index).param_list
        AND    application_id = lar_std_params(lv_index).appl_id;
        log_msg(lcv_proc_name,'Deleted '||SQL%rowcount||' records for param_list_code: '||lar_std_params(lv_index).param_list);
        lv_index := lar_std_params.NEXT(lv_index);
      END loop;
    END;
    /*
    || Clear out the parameter list
    */
    DELETE FROM bne_param_list_items
    WHERE  param_list_code = pr_conc_prog.param_list_code
    AND    application_id = pr_conc_prog.appl_id;
    log_msg(lcv_proc_name,'Deleted '||SQL%rowcount||' records for param_list_code: '||pr_conc_prog.param_list_code);

    ln_seq_num := create_pre_import_rules(
      pr_intg         => pr_intg,
      pr_conc_prog    => pr_conc_prog);
      
    IF pr_conc_prog.group_fields.count > 0 THEN
      create_param_list_item(
        pv_created_by      => pr_intg.created_by_user,
        pv_param_list_asn  => pr_conc_prog.asn,
        pv_param_list_code => pr_conc_prog.param_list_code,
        pn_seq_num         => ln_seq_num,
        pv_param_name      => 'GROUP',
        pv_string_value    => lar_std_params('GROUP').string_val,
        pv_desc_value      => 'Group Definition for '||pr_conc_prog.concurrent_program_name);

      bne_param_lists_pkg.load_row(
        x_param_list_asn        => lar_std_params('GROUP').asn,
        x_param_list_code       => lar_std_params('GROUP').param_list,
        x_object_version_number => 1,
        x_persistent_flag       => 'Y',
        x_comments              => 'WebADI: Group definition for the '||pr_conc_prog.concurrent_program_name||' Importer',
        x_attribute_asn         => NULL,
        x_attribute_code        => NULL,
        x_list_resolver         => NULL,
        x_user_tip              => NULL,
        x_prompt_left           => NULL,
        x_prompt_above          => NULL,
        x_user_name             => 'WebADI: '||pr_conc_prog.concurrent_program_name||' Group definition',
        x_owner                 => pr_intg.created_by_user,
        x_last_update_date      => to_char(SYSDATE,gcv_date_format),
        x_custom_mode           => 'FORCE');

      DECLARE
        ln_index     NUMBER;
        ln_grp_index NUMBER := 1;
      BEGIN
        ln_index :=  pr_conc_prog.group_fields.FIRST;
        while ln_index IS NOT NULL loop
          create_param_list_item(
            pv_created_by        => pr_intg.created_by_user,
            pv_param_list_asn    => pr_conc_prog.asn,
            pv_param_list_code   => lar_std_params('GROUP').param_list,
            pn_seq_num           => ln_grp_index,
            pv_param_name        => pr_conc_prog.group_fields(ln_index));
          log_msg(lcv_proc_name,'Created List Item for Group Field '||pr_conc_prog.group_fields(ln_index));
          ln_index :=  pr_conc_prog.group_fields.NEXT(ln_index);
        END loop;
      END;
    END IF;

    create_param_list_item(
      pv_created_by      => pr_intg.created_by_user,
      pv_param_list_asn  => pr_conc_prog.asn,
      pv_param_list_code => pr_conc_prog.param_list_code,
      pn_seq_num         => ln_seq_num,
      pv_param_name      => 'CONCURRENT_REQUEST',
      pv_string_value    => lar_std_params('CONCP').string_val,
      pv_desc_value      => 'Kicks off the '||pr_conc_prog.user_conc_prog_name||' Program');

    create_param_list_item(
      pv_created_by      => pr_intg.created_by_user,
      pv_param_list_asn  => pr_conc_prog.asn,
      pv_param_list_code => pr_conc_prog.param_list_code,
      pn_seq_num         => ln_seq_num,
      pv_param_name      => 'SUCCESS_MESSAGE',
      pv_param_defn_asn  => pr_conc_prog.asn,
      pv_param_defn_code => lv_param_defn);
    /*
    || Now create the header record for the concurrent program list items.
    */
    bne_param_lists_pkg.load_row(
      x_param_list_asn        => pr_conc_prog.asn,
      x_param_list_code       => pr_conc_prog.param_list_code,
      x_object_version_number => 1,
      x_persistent_flag       => 'Y',
      x_comments              => 'Importer to call '||pr_conc_prog.user_conc_prog_name||' Program',
      x_attribute_asn         => NULL,
      x_attribute_code        => NULL,
      x_list_resolver         => NULL,
      x_user_tip              => 'This parameter allows users to kick-off the '||pr_conc_prog.user_conc_prog_name||' Program',
      x_prompt_left           => 'Run '||pr_conc_prog.user_conc_prog_name,
      x_prompt_above          => 'Run '||pr_conc_prog.user_conc_prog_name,
      x_user_name             => pr_conc_prog.user_conc_prog_name,
      x_owner                 => pr_intg.created_by_user,
      x_last_update_date      => to_char(SYSDATE,gcv_date_format),
      x_custom_mode           => 'FORCE');
    log_msg(lcv_proc_name,'Created Parameter List '||pr_conc_prog.param_list_code);
    /*
    || Define the attributes for the default parameters to fnd_request.submit_request
    */
    lar_attrib(1).appl_id     := pr_conc_prog.appl_id;
    lar_attrib(1).asn         := pr_conc_prog.asn;
    lar_attrib(1).attr_code   := create_unique_attribute_code(
                                    pv_prefix  => 'CPR',
                                    pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(1).param_name  := 'Application';
    lar_attrib(1).attr_name   := 'APPLICATION';

    lar_attrib(2).appl_id     := pr_conc_prog.appl_id;
    lar_attrib(2).asn         := pr_conc_prog.asn;
    lar_attrib(2).attr_code   := create_unique_attribute_code(
                                    pv_prefix  => 'PRG',
                                    pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(2).param_name  := 'Program';
    lar_attrib(2).attr_name   := 'PROGRAM';

    lar_attrib(3).appl_id     := pr_conc_prog.appl_id;
    lar_attrib(3).asn         := pr_conc_prog.asn;
    lar_attrib(3).attr_code   := create_unique_attribute_code(
                                    pv_prefix  => 'DSC',
                                    pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(3).param_name  := 'Description';
    lar_attrib(3).attr_name   := 'DESCRIPTION';

    lar_attrib(4).appl_id     := pr_conc_prog.appl_id;
    lar_attrib(4).asn         := pr_conc_prog.asn;
    lar_attrib(4).attr_code   := create_unique_attribute_code(
                                    pv_prefix  => 'DTE',
                                    pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(4).param_name  := 'Run Date';
    lar_attrib(4).attr_name   := 'RUN_DATE';

    lar_attrib(5).appl_id     := pr_conc_prog.appl_id;
    lar_attrib(5).asn         := pr_conc_prog.asn;
    lar_attrib(5).attr_code   := create_unique_attribute_code(
                                    pv_prefix  => 'SUB',
                                    pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(5).param_name  := 'Sub request';
    lar_attrib(5).attr_name   := 'SUB_REQUEST';
    
    /*
    || Query up the concurrent program parameters and create array entries for each
    || in the attributes array
    */
    FOR datarec IN (SELECT fdf.form_left_prompt,
                           upper(REPLACE(fdf.end_user_column_name,' ','_')) attr_name,
                           fdf.display_size
                    FROM   fnd_descr_flex_col_usage_vl fdf,
                           fnd_application fa
                    WHERE  fdf.application_id                = fa.application_id
                    AND    fdf.descriptive_flexfield_name    = '$SRS$.'||pr_conc_prog.concurrent_program_name
                    AND    fdf.descriptive_flex_context_code = 'Global Data Elements'
                    AND    fa.application_short_name         = pr_conc_prog.program_asn) loop    
      lar_attrib(lar_attrib.count+1).appl_id   := pr_conc_prog.appl_id;
      lar_attrib(lar_attrib.count).asn         := pr_conc_prog.asn;
      lar_attrib(lar_attrib.count).attr_code   := create_unique_attribute_code(
                                                    pv_prefix  => 'P'||to_char(lar_attrib.count-5),
                                                    pn_appl_id => pr_conc_prog.appl_id);
      lar_attrib(lar_attrib.count).param_name  := datarec.form_left_prompt;
      lar_attrib(lar_attrib.count).attr_name   := datarec.attr_name;
      lar_attrib(lar_attrib.count).attr_length := datarec.display_size;
    END loop;
    /*
    || Define the final two attributes for the attribute array
    */
    lar_attrib(lar_attrib.count+1).appl_id   := pr_conc_prog.appl_id;
    lar_attrib(lar_attrib.count).asn         := pr_conc_prog.asn;
    lar_attrib(lar_attrib.count).attr_code   := create_unique_attribute_code(
                                                  pv_prefix  => 'END',
                                                  pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(lar_attrib.count).param_name  := 'End Of Concurrent Request Parameters';
    lar_attrib(lar_attrib.count).attr_name   := 'END';

    lar_attrib(lar_attrib.count+1).appl_id   := pr_conc_prog.appl_id;
    lar_attrib(lar_attrib.count).asn         := pr_conc_prog.asn;
    lar_attrib(lar_attrib.count).attr_code   := create_unique_attribute_code(
                                                  pv_prefix  => 'RST',
                                                  pn_appl_id => pr_conc_prog.appl_id);
    lar_attrib(lar_attrib.count).param_name  := 'Rest of Parameter Defaults';
    lar_attrib(lar_attrib.count).attr_name   := '''''';
    
    log_msg(lcv_proc_name,'Loaded '||lar_attrib.count||' records into the attributes table');
    /*
    || Now load the attribute array
    */
    DECLARE
      lv_attribute2   bne_attributes.attribute2%TYPE;
      lv_attribute3   bne_attributes.attribute3%TYPE;
      lv_attribute4   bne_attributes.attribute4%TYPE;
      lv_attribute5   bne_attributes.attribute5%TYPE;
      lv_attribute7   bne_attributes.attribute7%TYPE;
      lv_attribute8   bne_attributes.attribute8%TYPE;
      ln_index        NUMBER;
      lb_import_param boolean := FALSE;
      lb_deflt_param  boolean := FALSE;          
    BEGIN
      ln_index := lar_attrib.FIRST;
      while ln_index IS NOT NULL loop
        /*
        || Work out if we have a default value or import source for this
        || parameter.  (Haven't coded environment parameters yet)
        */
        lb_import_param := FALSE;
        lb_deflt_param  := FALSE;          
        IF pr_conc_prog.conc_param_dflts.EXISTS(ln_index-5) THEN
          IF pr_conc_prog.conc_param_dflts(ln_index-5).default_value IS NOT NULL THEN
            lb_deflt_param := TRUE;
          elsif pr_conc_prog.conc_param_dflts(ln_index-5).import_column IS NOT NULL THEN
            lb_import_param := TRUE;
          END IF;
        END IF;
        
        CASE lar_attrib(ln_index).attr_name
        WHEN 'SUB_REQUEST' THEN
          lv_attribute2 := 'BOOLEAN';
        WHEN '''''' THEN
          lv_attribute2 := 106 - lar_attrib.count; -- number of CHR(0)'s to pass 
                                                   -- into fnd_request.request
        ELSE
          lv_attribute2 := 'VARCHAR2';
        END CASE;
        
        IF lar_attrib(ln_index).attr_name = '''''' THEN
          lv_attribute3 := NULL;
          lv_attribute4 := NULL;
        ELSE
          lv_attribute3 := 'IN';
          lv_attribute4 := 'N';
        END IF;
        
        CASE lar_attrib(ln_index).attr_name
        WHEN 'APPLICATION' THEN
          lv_attribute5 := ''''||pr_conc_prog.program_asn||'''';
        WHEN 'PROGRAM' THEN
          lv_attribute5 := ''''||pr_conc_prog.concurrent_program_name||'''';
        WHEN 'DESCRIPTION' THEN
          lv_attribute5 := '''''';
        WHEN 'RUN_DATE' THEN
          lv_attribute5 := '''''';
        WHEN 'SUB_REQUEST' THEN
          lv_attribute5 := 'FALSE';
        WHEN 'END' THEN
          lv_attribute5 := 'CHR(0)';
        WHEN '''''' THEN
          lv_attribute5 := NULL;
        ELSE
          IF lb_deflt_param THEN
            lv_attribute5 := ''''||pr_conc_prog.conc_param_dflts(ln_index-5).default_value||'''';
          ELSE
            lv_attribute5 := NULL;
          END IF;
        END CASE;
        
        IF lb_import_param THEN
          lv_attribute7 := 'IMPORT';
          lv_attribute8 := pr_conc_prog.conc_param_dflts(ln_index-5).import_column;
        ELSE
          lv_attribute7 := NULL;
          lv_attribute8 := NULL;
        END IF;

        create_bne_attribute(
          pv_asn              => lar_attrib(ln_index).asn,
          pv_attr_code        => lar_attrib(ln_index).attr_code,
          pv_attribute1       => lar_attrib(ln_index).attr_name,
          pv_attribute2       => lv_attribute2,
          pv_attribute3       => lv_attribute3,
          pv_attribute4       => lv_attribute4,
          pv_attribute5       => lv_attribute5,
          pv_attribute6       => lar_attrib(ln_index).attr_length,
          pv_attribute7       => lv_attribute7,   -- source /import/lov/etc..?
          pv_attribute8       => lv_attribute8,   -- field name for source..?
          pv_created_by       => pr_intg.created_by_user);
        log_msg(lcv_proc_name,'Loaded Attribute: '||lar_attrib(ln_index).attr_name||' '||lar_attrib(ln_index).appl_id||':'||lar_attrib(ln_index).attr_code);
      
        create_param_list_item(
          pv_created_by      => pr_intg.created_by_user,
          pv_param_list_asn  => pr_conc_prog.asn,
          pv_param_list_code   => lar_std_params('CONCP').param_list,
          pn_seq_num           => ln_seq_num,
          pv_param_name        => lar_attrib(ln_index).param_name,
          pv_attrb_defn_asn    => lar_attrib(ln_index).asn,
          pv_attrb_defn_code   => lar_attrib(ln_index).attr_code);
        log_msg(lcv_proc_name,'Created Param List '||lar_attrib(ln_index).param_name||' for attrib '||lar_attrib(ln_index).appl_id||':'||lar_attrib(ln_index).attr_code);
      
        ln_index := lar_attrib.NEXT(ln_index);
      END loop;
      
      bne_param_lists_pkg.load_row(
        x_param_list_asn        => lar_std_params('CONCP').asn,
        x_param_list_code       => lar_std_params('CONCP').param_list,
        x_object_version_number => 1,
        x_persistent_flag       => 'Y',
        x_comments              => 'Importer to call '||pr_conc_prog.user_conc_prog_name||' Program',
        x_attribute_asn         => NULL,
        x_attribute_code        => NULL,
        x_list_resolver         => NULL,
        x_user_tip              => 'This parameter allows users to kick-off the '||pr_conc_prog.user_conc_prog_name||' Program',
        x_prompt_left           => 'Run '||pr_conc_prog.user_conc_prog_name,
        x_prompt_above          => 'Run '||pr_conc_prog.user_conc_prog_name,
        x_user_name             => pr_conc_prog.user_conc_prog_name||': Concurrent Request Step',
        x_owner                 => pr_intg.created_by_user,
        x_last_update_date      => to_char(SYSDATE,gcv_date_format),
        x_custom_mode           => 'FORCE');
    END;
    /*
    || Create a record in import programs to match
    */
    bne_import_programs_pkg.load_row(
      x_integrator_asn              => pr_intg.asn,
      x_integrator_code             => pr_intg.intg_code,
      x_sequence_num                => 10,
      x_object_version_number       => 1,
      x_parent_seq_num              => NULL,
      x_import_type                 => 2,
      x_import_param_list_asn       => pr_conc_prog.asn,
      x_import_param_code           => pr_conc_prog.param_list_code,
      x_owner                       => pr_intg.created_by_user,
      x_last_update_date            => to_char(SYSDATE,gcv_date_format),
      x_custom_mode                 => 'FORCE');  
    /*
    || Now associate the importer with the integrator
    */
    UPDATE bne_integrators_b
    SET    import_param_list_app_id = pr_conc_prog.appl_id,
           import_param_list_code   = pr_conc_prog.param_list_code,
           import_type              = 1 -- asynch conc req
    WHERE  integrator_code = pr_intg.integrator_code
    AND    application_id = pr_intg.appl_id;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_conc_prog_importer;

  PROCEDURE create_plsql_importer(
    pr_intg         IN out nocopy integrator_rec,
    pr_plsql_api    IN out nocopy plsql_api_rec)
  IS
    lcv_proc_name  constant varchar2(30) := 'create_plsql_importer';  
    lv_param_defn  bne_param_defns_vl.param_defn_code%TYPE;
    ln_imp_seq_num     NUMBER := 1;
    --ln_grp_seq_num     NUMBER := 1;
    
    TYPE param_rec IS record
    (asn          fnd_application.application_short_name%TYPE,
     appl_id      bne_param_lists_b.application_id%TYPE,
     param_list   bne_param_lists_b.param_list_code%TYPE,
     string_val   bne_param_list_items.string_value%TYPE);
    TYPE param_table IS TABLE OF param_rec INDEX BY VARCHAR2(20);
    lar_std_params   param_table;
    
    lv_attribute_code bne_param_list_items.attribute_code%TYPE;
  BEGIN
    /*
    || Create the Importer Parameter List
    */
    pr_plsql_api.param_list_code := create_unique_param_list_code(
      pv_prefix  => 'IMP',
      pn_appl_id => pr_intg.appl_id);    
    bne_param_lists_pkg.load_row(
      x_param_list_asn        => pr_intg.asn,
      x_param_list_code       => pr_plsql_api.param_list_code,
      x_object_version_number => 1,
      x_persistent_flag       => 'Y',
      x_comments              => NULL,
      x_attribute_asn         => NULL,
      x_attribute_code        => NULL,
      x_list_resolver         => NULL,
      x_user_tip              => NULL,
      x_prompt_left           => NULL,
      x_prompt_above          => NULL,
      x_user_name             => pr_plsql_api.importer_name,
      x_owner                 => pr_intg.created_by_user,
      x_last_update_date      => to_char(SYSDATE,gcv_date_format),
      x_custom_mode           => 'FORCE');
    /*
    || Now define the 5 standard param list items for the  plsql api call 
    || parameter list.
    */
    lar_std_params('GROUP').asn        := pr_intg.asn;
    lar_std_params('GROUP').appl_id    := pr_intg.appl_id;
    lar_std_params('GROUP').param_list := create_unique_param_list_code(
                                            pv_prefix  => 'GRP',
                                            pn_appl_id => pr_intg.appl_id);
    lar_std_params('GROUP').string_val := lar_std_params('GROUP').appl_id||':'||lar_std_params('GROUP').param_list;
     
    lar_std_params('ROWM').asn         := pr_intg.asn;
    lar_std_params('ROWM').appl_id     := pr_intg.appl_id;
    lar_std_params('ROWM').param_list  := create_unique_param_list_code(
                                            pv_prefix  => 'RM',
                                            pn_appl_id => pr_intg.appl_id);
    lar_std_params('ROWM').string_val  := lar_std_params('ROWM').appl_id||':'||lar_std_params('ROWM').param_list;

    lar_std_params('PLSQL').asn        := pr_intg.asn;
    lar_std_params('PLSQL').appl_id    := pr_intg.appl_id;
    lar_std_params('PLSQL').param_list := create_unique_param_list_code(
                                            pv_prefix  => 'PL',
                                            pn_appl_id => pr_intg.appl_id);
    lar_std_params('PLSQL').string_val := lar_std_params('PLSQL').appl_id||':'||lar_std_params('PLSQL').param_list;

    lar_std_params('ERRR').asn         := pr_intg.asn;
    lar_std_params('ERRR').appl_id     := pr_intg.appl_id;
    lar_std_params('ERRR').param_list  := create_unique_param_list_code(
                                            pv_prefix  => 'ER',
                                            pn_appl_id => pr_intg.appl_id);
    lar_std_params('ERRR').string_val  := lar_std_params('ERRR').appl_id||':'||lar_std_params('ERRR').param_list;

    lar_std_params('ERRL').asn         := pr_intg.asn;
    lar_std_params('ERRL').appl_id     := pr_intg.appl_id;
    lar_std_params('ERRL').param_list  := create_unique_param_list_code(
                                            pv_prefix  => 'EL',
                                            pn_appl_id => pr_intg.appl_id);
    lar_std_params('ERRL').string_val  := lar_std_params('ERRL').appl_id||':'||lar_std_params('ERRL').param_list;
    /*
    || Clear out any parameter list entries with these names
    */
    DECLARE
      lv_index VARCHAR2(150);
    BEGIN
      lv_index := lar_std_params.FIRST;
      while lv_index IS NOT NULL loop
        DELETE FROM bne_param_list_items
        where  param_list_code = lar_std_params(lv_index).param_list
        and    application_id = lar_std_params(lv_index).appl_id;
        log_msg(lcv_proc_name,'Deleted '||SQL%rowcount||' records for param_list_code ['||lar_std_params(lv_index).string_val||']');
        lv_index := lar_std_params.NEXT(lv_index);
      END loop;
    END;
    /*
    || Clear out the importer parameter list
    */
    DELETE FROM bne_param_list_items
    WHERE  param_list_code = pr_plsql_api.param_list_code
    AND    application_id = pr_intg.appl_id;
    log_msg(lcv_proc_name,'Deleted '||SQL%rowcount||' records for param_list_code: '||pr_plsql_api.param_list_code);
    /*
    || Create group fields parameter and entries
    */
    IF pr_plsql_api.group_fields.count > 0 THEN
      /*
      || Create the list item in the importer list
      */
      create_param_list_item(
        pv_created_by      => pr_intg.created_by_user,
        pv_param_list_asn  => pr_intg.asn,
        pv_param_list_code => pr_plsql_api.param_list_code,
        pn_seq_num         => ln_imp_seq_num,
        pv_param_name      => 'GROUP',
        pv_string_value    => lar_std_params('GROUP').string_val,
        pv_desc_value      => NULL);
      /*
      || Define the group parameter list
      */
      bne_param_lists_pkg.load_row(
        x_param_list_asn        => lar_std_params('GROUP').asn,
        x_param_list_code       => lar_std_params('GROUP').param_list,
        x_object_version_number => 1,
        x_persistent_flag       => 'Y',
        x_comments              => 'Group Definition',
        x_attribute_asn         => NULL,
        x_attribute_code        => NULL,
        x_list_resolver         => NULL,
        x_user_tip              => NULL,
        x_prompt_left           => NULL,
        x_prompt_above          => NULL,
        x_user_name             => 'Group Definition',
        x_owner                 => pr_intg.created_by_user,
        x_last_update_date      => to_char(SYSDATE,gcv_date_format),
        x_custom_mode           => 'FORCE');
      /*
      || Create the parameter list items in the group list
      */
      DECLARE
        ln_index     NUMBER;
        ln_grp_index NUMBER := 1;
      BEGIN
        ln_index :=  pr_plsql_api.group_fields.FIRST;
        while ln_index IS NOT NULL loop
          create_param_list_item(
            pv_created_by        => pr_intg.created_by_user,
            pv_param_list_asn    => pr_intg.asn,
            pv_param_list_code   => lar_std_params('GROUP').param_list,
            pn_seq_num           => ln_grp_index,
            pv_param_name        => pr_plsql_api.group_fields(ln_index));
          log_msg(lcv_proc_name,'Created List Item for Group Field '||pr_plsql_api.group_fields(ln_index));
          ln_index :=  pr_plsql_api.group_fields.NEXT(ln_index);
        END loop;
      END;
    END IF;
    /*
    || Create row mapping entry in the importer parameter list.
    */
    IF pr_plsql_api.unique_row_fields.count > 0 THEN
      /*
      || Create the row mapping entry in the importer list
      */
      create_param_list_item(
        pv_created_by      => pr_intg.created_by_user,
        pv_param_list_asn  => pr_intg.asn,
        pv_param_list_code => pr_plsql_api.param_list_code,
        pn_seq_num         => ln_imp_seq_num,
        pv_param_name      => 'ROW_MAPPING',
        pv_string_value    => lar_std_params('ROWM').string_val);
      /*
      || Create the row mapping parameter list
      */
      bne_param_lists_pkg.load_row(
        x_param_list_asn        => lar_std_params('ROWM').asn,
        x_param_list_code       => lar_std_params('ROWM').param_list,
        x_object_version_number => 1,
        x_persistent_flag       => 'Y',
        x_comments              => 'Document Row : Interface Attribute Mapping',
        x_attribute_asn         => NULL,
        x_attribute_code        => NULL,
        x_list_resolver         => NULL,
        x_user_tip              => NULL,
        x_prompt_left           => NULL,
        x_prompt_above          => NULL,
        x_user_name             => 'Document Row : Interface Attribute Mapping',
        x_owner                 => pr_intg.created_by_user,
        x_last_update_date      => to_char(SYSDATE,gcv_date_format),
        x_custom_mode           => 'FORCE');
      /*
      || Create the "unique row" parameter list items.
      */
      DECLARE
        ln_index     NUMBER;
        ln_grp_index NUMBER := 1;
      BEGIN
        ln_index :=  pr_plsql_api.unique_row_fields.FIRST;
        while ln_index IS NOT NULL loop
          create_param_list_item(
            pv_created_by        => pr_intg.created_by_user,
            pv_param_list_asn    => pr_intg.asn,
            pv_param_list_code   => lar_std_params('ROWM').param_list,
            pn_seq_num           => ln_grp_index,
            pv_param_name        => pr_plsql_api.unique_row_fields(ln_index));
          log_msg(lcv_proc_name,'Created List Item for Group Field '||pr_plsql_api.unique_row_fields(ln_index));
          ln_index :=  pr_plsql_api.unique_row_fields.NEXT(ln_index);
        END loop;
      END;
    END IF;
    /*
    || Create PL/SQL Call parameter list item in the importer parameter list.
    */
    create_param_list_item(
      pv_created_by      => pr_intg.created_by_user,
      pv_param_list_asn  => pr_intg.asn,
      pv_param_list_code => pr_plsql_api.param_list_code,
      pn_seq_num         => ln_imp_seq_num,
      pv_param_name      => 'PLSQL',
      pv_string_value    => lar_std_params('PLSQL').string_val);
    /*
    || Create PL/SQL Call Parameter List.
    */
    bne_param_lists_pkg.load_row(
      x_param_list_asn        => lar_std_params('PLSQL').asn,
      x_param_list_code       => lar_std_params('PLSQL').param_list,
      x_object_version_number => 1,
      x_persistent_flag       => 'Y',
      x_comments              => 'PLSQL',
      x_attribute_asn         => NULL,
      x_attribute_code        => NULL,
      x_list_resolver         => NULL,
      x_user_tip              => NULL,
      x_prompt_left           => NULL,
      x_prompt_above          => NULL,
      x_user_name             => 'PLSQL',
      x_owner                 => pr_intg.created_by_user,
      x_last_update_date      => to_char(SYSDATE,gcv_date_format),
      x_custom_mode           => 'FORCE');
    /*
    || Create attribute for plsql api definition in the pl/sql api call list
    */
    lv_attribute_code := create_unique_attribute_code(
      pv_prefix  => 'ATT',
      pn_appl_id => pr_intg.appl_id);      
    create_bne_attribute(
      pv_asn              => lar_std_params('PLSQL').asn,
      pv_attr_code        => lv_attribute_code,
      pv_attribute1       => 'PROCEDURE',
      pv_attribute2       => pr_plsql_api.procedure_name,
      pv_created_by       => pr_intg.created_by_user);
    log_msg(lcv_proc_name,'Loaded Attribute: '||lv_attribute_code||' '||lar_std_params('PLSQL').asn);
    /*
    || Create bne parameter for plsql api definition in the pl/sql api call list
    */
    DECLARE
      ln_index     NUMBER;
      ln_grp_index NUMBER := 1;
    begin
      create_param_list_item(
        pv_created_by        => pr_intg.created_by_user,
        pv_param_list_asn    => lar_std_params('PLSQL').asn,
        pv_param_list_code   => lar_std_params('PLSQL').param_list,
        pn_seq_num           => ln_grp_index,
        pv_param_name        => pr_plsql_api.procedure_name,
        pv_desc_value        => 'PLSQL',
        pv_attrb_defn_asn    => lar_std_params('PLSQL').asn,
        pv_attrb_defn_code   => lv_attribute_code);
      /*
      || Create attributes and bne parameters for the pl/sql api parameters
      || in the pl/sql api call list
      */
      ln_index :=  pr_plsql_api.procedure_params.FIRST;
      while ln_index IS NOT NULL loop
        lv_attribute_code := create_unique_attribute_code(
          pv_prefix  => 'ATT',
          pn_appl_id => pr_intg.appl_id);
        /*
        || Create bne attribute for plsql api parameter
        */
        create_bne_attribute(
          pv_asn              => lar_std_params('PLSQL').asn,
          pv_attr_code        => lv_attribute_code,
          pv_attribute1       => pr_plsql_api.procedure_params(ln_index).param_name,
          pv_attribute2       => pr_plsql_api.procedure_params(ln_index).param_type,
          pv_attribute3       => pr_plsql_api.procedure_params(ln_index).direction,
          pv_attribute4       => 'N',
          pv_attribute7       => pr_plsql_api.procedure_params(ln_index).source_type,
          pv_attribute8       => pr_plsql_api.procedure_params(ln_index).source_field,
          pv_created_by       => pr_intg.created_by_user);
        /*
        || Create bne parameter list item for plsql api parameter
        */      
        create_param_list_item(
          pv_created_by        => pr_intg.created_by_user,
          pv_param_list_asn    => lar_std_params('PLSQL').asn,
          pv_param_list_code   => lar_std_params('PLSQL').param_list,
          pn_seq_num           => ln_grp_index,
          pv_param_name        => pr_plsql_api.procedure_params(ln_index).param_name,
          pv_attrb_defn_asn    => lar_std_params('PLSQL').asn,
          pv_attrb_defn_code   => lv_attribute_code);
        log_msg(lcv_proc_name,'Created List Item for API Parameter Field '||pr_plsql_api.procedure_params(ln_index).param_name);
        ln_index :=  pr_plsql_api.procedure_params.NEXT(ln_index);
      END loop;
    END;
    /*
    || Create Errored Rows parameter list item in the importer parameter list.
    */
    create_param_list_item(
      pv_created_by      => pr_intg.created_by_user,
      pv_param_list_asn  => pr_intg.asn,
      pv_param_list_code => pr_plsql_api.param_list_code,
      pn_seq_num         => ln_imp_seq_num,
      pv_param_name      => 'ERRORED_ROWS',
      pv_string_value    => lar_std_params('ERRR').string_val);    
    /*
    || Create Errored Rows Parameter List
    */
    bne_param_lists_pkg.load_row(
      x_param_list_asn        => lar_std_params('ERRR').asn,
      x_param_list_code       => lar_std_params('ERRR').param_list,
      x_object_version_number => 1,
      x_persistent_flag       => 'Y',
      x_comments              => 'ERRORED_ROWS',
      x_attribute_asn         => NULL,
      x_attribute_code        => NULL,
      x_list_resolver         => NULL,
      x_user_tip              => NULL,
      x_prompt_left           => NULL,
      x_prompt_above          => NULL,
      x_user_name             => 'ERRORED_ROWS',
      x_owner                 => pr_intg.created_by_user,
      x_last_update_date      => to_char(SYSDATE,gcv_date_format),
      x_custom_mode           => 'FORCE');
    /*
    || Create bne parameter for sql definition in the errored rows parameter 
    || list
    */
    DECLARE
      ln_index     NUMBER;
      ln_grp_index NUMBER := 1;
    begin
      create_param_list_item(
        pv_created_by        => pr_intg.created_by_user,
        pv_param_list_asn    => lar_std_params('ERRR').asn,
        pv_param_list_code   => lar_std_params('ERRR').param_list,
        pn_seq_num           => ln_grp_index,
        pv_param_name        => 'SQL',
        pv_string_value      => pr_plsql_api.errored_rows_sql,
        pv_desc_value        => 'SQL Errored Rows');
      /*
      || Create attributes and bne parameters for the errored rows sql parameters
      || in the errored rows list
      */    
      ln_index :=  pr_plsql_api.er_sql_params.FIRST;
      while ln_index IS NOT NULL loop
        lv_attribute_code := create_unique_attribute_code(
          pv_prefix  => 'ATT',
          pn_appl_id => pr_intg.appl_id);
        /*
        || Create bne attribute for errored rows sql parameter
        */
        create_bne_attribute(
          pv_asn              => lar_std_params('ERRR').asn,
          pv_attr_code        => lv_attribute_code,
          pv_attribute1       => pr_plsql_api.er_sql_params(ln_index).direction,
          pv_attribute2       => pr_plsql_api.er_sql_params(ln_index).param_name,
          pv_attribute3       => pr_plsql_api.er_sql_params(ln_index).source_type,
          pv_attribute4       => pr_plsql_api.er_sql_params(ln_index).source_field,
          pv_attribute5       => pr_plsql_api.er_sql_params(ln_index).param_type,
          pv_created_by       => pr_intg.created_by_user);
        /*
        || Create bne parameter list item for errored rows sql parameter
        */      
        create_param_list_item(
          pv_created_by        => pr_intg.created_by_user,
          pv_param_list_asn    => lar_std_params('ERRR').asn,
          pv_param_list_code   => lar_std_params('ERRR').param_list,
          pn_seq_num           => ln_grp_index,
          pv_param_name        => pr_plsql_api.er_sql_params(ln_index).param_name,
          pv_attrb_defn_asn    => lar_std_params('ERRR').asn,
          pv_attrb_defn_code   => lv_attribute_code);
        log_msg(lcv_proc_name,'Created List Item for Errored Rows SQL '||pr_plsql_api.er_sql_params(ln_index).param_name);
        ln_index :=  pr_plsql_api.er_sql_params.NEXT(ln_index);
      END loop;
    END;
    /*
    || Create Error Lookup parameter list item in the importer parameter list.
    */
    create_param_list_item(
      pv_created_by      => pr_intg.created_by_user,
      pv_param_list_asn  => pr_intg.asn,
      pv_param_list_code => pr_plsql_api.param_list_code,
      pn_seq_num         => ln_imp_seq_num,
      pv_param_name      => 'ERROR_LOOKUP',
      pv_string_value    => lar_std_params('ERRL').string_val);    
    /*
    || Create Errored Rows Parameter List
    */
    bne_param_lists_pkg.load_row(
      x_param_list_asn        => lar_std_params('ERRL').asn,
      x_param_list_code       => lar_std_params('ERRL').param_list,
      x_object_version_number => 1,
      x_persistent_flag       => 'Y',
      x_comments              => 'ERROR_LOOKUP',
      x_attribute_asn         => NULL,
      x_attribute_code        => NULL,
      x_list_resolver         => NULL,
      x_user_tip              => NULL,
      x_prompt_left           => NULL,
      x_prompt_above          => NULL,
      x_user_name             => 'ERROR_LOOKUP',
      x_owner                 => pr_intg.created_by_user,
      x_last_update_date      => to_char(SYSDATE,gcv_date_format),
      x_custom_mode           => 'FORCE');
    /*
    || Create bne parameter for sql definition in the error lookup parameter 
    || list
    */
    DECLARE
      ln_index     NUMBER;
      ln_grp_index NUMBER := 1;
    begin
      create_param_list_item(
        pv_created_by        => pr_intg.created_by_user,
        pv_param_list_asn    => lar_std_params('ERRL').asn,
        pv_param_list_code   => lar_std_params('ERRL').param_list,
        pn_seq_num           => ln_grp_index,
        pv_param_name        => 'SQL',
        pv_string_value      => pr_plsql_api.error_lkup_sql,
        pv_desc_value        => 'ERROR_LOOKUP');
      /*
      || Create attributes and bne parameters for the error lookup sql parameters
      || in the error lookup list
      */    
      ln_index :=  pr_plsql_api.el_sql_params.FIRST;
      while ln_index IS NOT NULL loop
        lv_attribute_code := create_unique_attribute_code(
          pv_prefix  => 'ATT',
          pn_appl_id => pr_intg.appl_id);
        /*
        || Create bne attribute for errored rows sql parameter
        */
        create_bne_attribute(
          pv_asn              => lar_std_params('ERRL').asn,
          pv_attr_code        => lv_attribute_code,
          pv_attribute1       => pr_plsql_api.el_sql_params(ln_index).direction,
          pv_attribute2       => pr_plsql_api.el_sql_params(ln_index).param_name,
          pv_attribute3       => pr_plsql_api.el_sql_params(ln_index).source_type,
          pv_attribute4       => pr_plsql_api.el_sql_params(ln_index).source_field,
          pv_attribute5       => pr_plsql_api.el_sql_params(ln_index).param_type,
          pv_attribute6       => '1',
          pv_created_by       => pr_intg.created_by_user);
        /*
        || Create bne parameter list item for errored rows sql parameter
        */      
        create_param_list_item(
          pv_created_by        => pr_intg.created_by_user,
          pv_param_list_asn    => lar_std_params('ERRL').asn,
          pv_param_list_code   => lar_std_params('ERRL').param_list,
          pn_seq_num           => ln_grp_index,
          pv_param_name        => pr_plsql_api.el_sql_params(ln_index).param_name,
          pv_attrb_defn_asn    => lar_std_params('ERRL').asn,
          pv_attrb_defn_code   => lv_attribute_code);
        log_msg(lcv_proc_name,'Created List Item for Error Lookup SQL '||pr_plsql_api.el_sql_params(ln_index).param_name);
        ln_index :=  pr_plsql_api.el_sql_params.NEXT(ln_index);
      END loop;
    END;    
    /*
    || Now associate the importer with the integrator
    */
    UPDATE bne_integrators_b
    SET    import_param_list_app_id = pr_intg.appl_id,
           import_param_list_code   = pr_plsql_api.param_list_code,
           import_type              = 3 -- plsql api
    WHERE  integrator_code = pr_intg.integrator_code
    AND    application_id = pr_intg.appl_id;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_plsql_importer;

  PROCEDURE copy_dummy_col_to_intf(
    par_intf_cols   IN out nocopy interface_col_table,
    par_dummy_cols  IN out nocopy dummy_cols_table,
    pv_index        IN varchar2,
    pn_index        IN varchar2)
  IS
    lcv_proc_name constant varchar2(30) := 'copy_dummy_col_to_intf';
    lv_action  varchar2(20) := 'Created';
  BEGIN
    IF par_intf_cols.EXISTS(pv_index) THEN
      lv_action := 'Updated';
      IF NOT par_intf_cols(pv_index).dummy_column THEN
        log_msg(lcv_proc_name,'Interface Column: '||pv_index||' has been provided in par_intf_cols and par_dummy_cols.');
        log_msg(lcv_proc_name,'The par_intf_cols entry must be flagged with dummy_column = TRUE or renamed');
        raise_application_error(-20001,'Invalid parameters');
      END IF;
    ELSE
      par_intf_cols(pv_index).dummy_column       := TRUE;    
    END IF;
    par_intf_cols(pv_index).prompt               := nvl(par_intf_cols(pv_index).prompt,par_dummy_cols(pn_index).prompt_above);
    par_intf_cols(pv_index).field_size           := nvl(par_intf_cols(pv_index).field_size,par_dummy_cols(pn_index).field_size);
    par_intf_cols(pv_index).display_flag         := nvl(par_intf_cols(pv_index).display_flag,par_dummy_cols(pn_index).display_flag);
    par_intf_cols(pv_index).rename_to            := nvl(par_intf_cols(pv_index).rename_to,par_dummy_cols(pn_index).interface_col_name);
    par_intf_cols(pv_index).group_name           := nvl(par_intf_cols(pv_index).group_name,par_dummy_cols(pn_index).group_name);
    par_intf_cols(pv_index).oa_concat_flex       := nvl(par_intf_cols(pv_index).oa_concat_flex,par_dummy_cols(pn_index).oa_concat_flex);
    par_intf_cols(pv_index).val_type             := nvl(par_intf_cols(pv_index).val_type,par_dummy_cols(pn_index).val_type);
    par_intf_cols(pv_index).interface_col_type   := nvl(par_intf_cols(pv_index).interface_col_type,par_dummy_cols(pn_index).interface_col_type);
    par_intf_cols(pv_index).required_flag        := nvl(par_intf_cols(pv_index).required_flag,par_dummy_cols(pn_index).required_flag);
    par_intf_cols(pv_index).default_type         := nvl(par_intf_cols(pv_index).default_type,par_dummy_cols(pn_index).default_type);
    par_intf_cols(pv_index).default_value        := nvl(par_intf_cols(pv_index).default_value,par_dummy_cols(pn_index).default_value);
    par_intf_cols(pv_index).user_hint            := nvl(par_intf_cols(pv_index).user_hint,par_dummy_cols(pn_index).user_hint);
    par_intf_cols(pv_index).dummy_column         := TRUE;    
    log_msg(lcv_proc_name,lv_action||' interface column '||pv_index||' from dummy column '||pn_index);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END copy_dummy_col_to_intf;

  PROCEDURE copy_dummy_cols_to_intf(
    par_intf_cols   IN out nocopy interface_col_table,
    par_dummy_cols  IN out nocopy dummy_cols_table)
  IS
    ln_index      NUMBER;
    lv_index      interface_col_name;
    lcv_proc_name constant varchar2(30) := 'copy_dummy_cols_to_intf';
  BEGIN
    ln_index := par_dummy_cols.first;
    while ln_index IS NOT NULL loop
      lv_index := par_dummy_cols(ln_index).interface_col_name;
      
      copy_dummy_col_to_intf(
        par_intf_cols   => par_intf_cols,
        par_dummy_cols  => par_dummy_cols,
        pv_index        => lv_index,
        pn_index        => ln_index);
      
      ln_index := par_dummy_cols.next(ln_index);
    END loop;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END copy_dummy_cols_to_intf;

  procedure add_query_column(
    pv_content_sql      in out nocopy varchar2,
    pv_original_column  in varchar2,
    pv_dummy_column     in varchar2) 
  is
    lcv_proc_name constant varchar2(30) := 'add_query_column';
    lv_suffix              varchar2(4000) := ltrim(substr(pv_content_sql,12),', ');
    lv_prefix              varchar2(4000) := 'select dq.*, dq.'||pv_original_column||' '||pv_dummy_column;
  begin
    log_msg(lcv_proc_name,'Starting with content_sql: '||pv_content_sql);
    if not substr(pv_content_sql,-4) = ') dq' then
      pv_content_sql := 'select dq.* from ('||pv_content_sql||') dq';
    end if;
    
    lv_suffix := ltrim(substr(pv_content_sql,12),', ');
    if substr(lv_suffix,1,4) = 'from' then
      pv_content_sql := lv_prefix||' '||lv_suffix;
    else
      pv_content_sql := lv_prefix||', '||lv_suffix;
    end if;
    log_msg(lcv_proc_name,'Finished with content_sql: '||pv_content_sql);
  end add_query_column;

  PROCEDURE apply_defaults(
    pr_intg         IN out nocopy integrator_rec,
    pr_intf         IN out nocopy interface_rec,  
    par_intf_cols   IN out nocopy interface_col_table,
    par_kff_lovs    IN out nocopy kff_lov_table,
    par_dummy_cols  IN out nocopy dummy_cols_table,
    par_table_lovs  IN OUT NOCOPY table_lov_table,
    par_display_seq IN out nocopy display_seq_table)  
  IS
    lcv_proc_name           constant varchar2(30) := 'apply_defaults';
    ln_layout_index         NUMBER;
    ln_dummy_index          NUMBER;
    ln_default_layout       NUMBER := pr_intg.layouts.first;
    lv_index                interface_col_name;
    --ln_disp_seq_num         NUMBER;
    lb_create_update_layout boolean := create_update_layout(pr_intg);
    lb_updatable_layouts    boolean := FALSE;
    lb_found_segments       boolean := FALSE;
  begin
    log_heading(lcv_proc_name,'Applying default values where none are supplied');
    /*
    || Derive the application_id if required
    */
    IF pr_intg.appl_id IS NULL THEN
      pr_intg.appl_id := get_application_id(pr_intg.asn);
    END IF;
    /*
    || Derive the user id if required
    */
    IF pr_intg.user_id IS NULL THEN
      pr_intg.user_id := get_user_id(pr_intg.created_by_user);
    END IF;
    /*
    || Copy the application_id and user_id into the interface record to save 
    || passing the intergrator_rec everywhere
    */
    pr_intf.appl_id := pr_intg.appl_id;
    pr_intf.user_id := pr_intg.user_id;  
    /*
    || Check if we need to create any Key Flexfield Columns
    */
    declare
      ln_disp_seq_num number;
      ln_flex_num     number;
    begin
      IF par_kff_lovs.count > 0 THEN
        lv_index := par_kff_lovs.FIRST;
        while lv_index IS NOT NULL loop
          log_heading(lcv_proc_name,'Creating dummy columns for KFF on '||lv_index);
          /*
          || Get the display sequence for the ID field
          */
          ln_disp_seq_num := find_display_seq(
              pv_interface_col_name => lv_index,
              PAR_DISPLAY_SEQ       => PAR_DISPLAY_SEQ);
          
          LOG_MSG(LCV_PROC_NAME,'KFF Display sequence for '||LV_INDEX||' is '||LN_DISP_SEQ_NUM);
          log_msg(lcv_proc_name,'Searching for segments for '||par_kff_lovs(lv_index).oa_flex_application_id||'.'||PAR_KFF_LOVS(LV_INDEX).OA_FLEX_CODE||'.'||par_kff_lovs(lv_index).oa_flex_num);
          /*
          || Find the KFF segments from the AOL config
          */
          
          begin
              ln_flex_num := par_kff_lovs(lv_index).oa_flex_num;
          exception
              when others then
                  select distinct 
                         first_value(id_flex_num) over (partition by 'x' order by seg_count desc rows between unbounded preceding and unbounded following)
                  into   ln_flex_num
                  from   (select id_flex_num, count(*) seg_count
                          from   fnd_id_flex_segments 
                          where  application_id = par_kff_lovs(lv_index).oa_flex_application_id
                          and    id_flex_code = par_kff_lovs(lv_index).oa_flex_code
                          and    enabled_flag = 'Y'
                          group by id_flex_num);
          end;
          
          lb_found_segments := FALSE;
          FOR datarec IN (SELECT application_column_name, 
                                 segment_name, 
                                 segment_num,
                                 required_flag,
                                 rownum seg_num
                          FROM   fnd_id_flex_segments
                          WHERE  application_id = par_kff_lovs(lv_index).oa_flex_application_id
                          AND    id_flex_code = par_kff_lovs(lv_index).oa_flex_code
                          AND    id_flex_num = ln_flex_num
                          and    enabled_flag = 'Y'
                          ORDER  BY segment_num) loop
            
            ln_dummy_index := par_dummy_cols.count+1;
  
            IF NOT lb_found_segments THEN
              /*
              || First segment for this key flexfield - setup all the default values
              */
              log_msg(lcv_proc_name,'Loading first segment, so populating all fields in dummy record');
              par_dummy_cols(ln_dummy_index).enabled_flag               := 'Y';
              par_dummy_cols(ln_dummy_index).required_flag              := datarec.required_flag;
              par_dummy_cols(ln_dummy_index).display_flag               := 'N';
              par_dummy_cols(ln_dummy_index).field_size                 := NULL;
              par_dummy_cols(ln_dummy_index).default_type               := NULL;
              par_dummy_cols(ln_dummy_index).default_value              := NULL;
              par_dummy_cols(ln_dummy_index).group_name                 := par_kff_lovs(lv_index).group_name;
              par_dummy_cols(ln_dummy_index).oa_flex_code               := NULL;
              par_dummy_cols(ln_dummy_index).oa_concat_flex             := NULL;
              par_dummy_cols(ln_dummy_index).read_only_flag             := 'N';
              par_dummy_cols(ln_dummy_index).val_type                   := 'KEYFLEXSEG';
              par_dummy_cols(ln_dummy_index).val_id_col                 := NULL;
              par_dummy_cols(ln_dummy_index).val_mean_col               := NULL;
              par_dummy_cols(ln_dummy_index).val_desc_col               := NULL;
              par_dummy_cols(ln_dummy_index).val_obj_name               := NULL;
              par_dummy_cols(ln_dummy_index).val_addl_w_c               := NULL;
              par_dummy_cols(ln_dummy_index).data_type                  := 2;
              par_dummy_cols(ln_dummy_index).not_null_flag              := 'N';
              par_dummy_cols(ln_dummy_index).val_component_app_id       := NULL;
              par_dummy_cols(ln_dummy_index).val_component_code         := NULL;
              par_dummy_cols(ln_dummy_index).summary_flag               := 'N';
              par_dummy_cols(ln_dummy_index).mapping_enabled_flag       := 'Y';
              par_dummy_cols(ln_dummy_index).user_hint                  := NULL;
              par_dummy_cols(ln_dummy_index).user_help_text             := NULL;
              par_dummy_cols(ln_dummy_index).oa_flex_num                := NULL;
              par_dummy_cols(ln_dummy_index).oa_flex_application_id     := NULL;
              par_dummy_cols(ln_dummy_index).upload_param_list_item_num := NULL;
              par_dummy_cols(ln_dummy_index).expanded_sql_query         := NULL;
              par_dummy_cols(ln_dummy_index).lov_type                   := NULL;
              par_dummy_cols(ln_dummy_index).offline_lov_enabled_flag   := NULL;
              par_dummy_cols(ln_dummy_index).variable_data_type_class   := NULL;
              lb_found_segments := TRUE;
            ELSE
              /*
              || Copy all the defaults from the previous segment and clear out 
              || the hint
              */
              log_msg(lcv_proc_name,'Loading segment '||ln_dummy_index||', so copying from '||ln_dummy_index||'-1');
              par_dummy_cols(ln_dummy_index) := par_dummy_cols(ln_dummy_index-1);
              par_dummy_cols(ln_dummy_index).user_hint                  := NULL;
            END IF;
            /*
            || Setup the only fields that change for each segment
            */
            par_dummy_cols(ln_dummy_index).interface_col_name         := lv_index||'_'||datarec.application_column_name;
            par_dummy_cols(ln_dummy_index).prompt_left                := par_kff_lovs(lv_index).column_prefix ||' '|| datarec.segment_name;
            par_dummy_cols(ln_dummy_index).prompt_above               := par_kff_lovs(lv_index).column_prefix ||' '|| datarec.segment_name;
            par_dummy_cols(ln_dummy_index).segment_number             := datarec.seg_num;
            log_msg(lcv_proc_name,'Created Dummy Col: '||par_dummy_cols(ln_dummy_index).interface_col_name);
            /*
            || Add the new field into the display sequence array
            */
            declare
              ln_disp_index           NUMBER;
              ln_next_disp_index      NUMBER;
            begin
              ln_disp_index := par_display_seq.LAST;
              if ln_disp_index IS NULL THEN
                raise_application_error(-20001,'par_display_seq is empty!');
              else
                log_msg(lcv_proc_name,lv_index||' is currently in position: '||ln_disp_seq_num);
                while ln_disp_seq_num + datarec.seg_num <= ln_disp_index loop
                  ln_next_disp_index := nvl(par_display_seq.next(ln_disp_index),ln_disp_index + 10);
                  par_display_seq(ln_next_disp_index) := par_display_seq(ln_disp_index);
                  log_msg(lcv_proc_name,'Moved: '||par_display_seq(ln_disp_index)||' from '||ln_disp_index||' to '||ln_next_disp_index);
                  ln_disp_index := par_display_seq.prior(ln_disp_index);
                end loop;
                ln_disp_index := par_display_seq.next(ln_disp_index);
                par_display_seq(ln_disp_index) := par_dummy_cols(ln_dummy_index).interface_col_name;
                log_msg(lcv_proc_name,'Added: '||par_dummy_cols(ln_dummy_index).interface_col_name||' into '||ln_disp_index);
              end if;
            end;
            /*
            || Copy it into the interface columns array
            */
            log_msg(lcv_proc_name,'Copying dummy record: '||ln_dummy_index||' into part_intf_cols('||par_dummy_cols(ln_dummy_index).interface_col_name||')');
            copy_dummy_col_to_intf(
              par_intf_cols   => par_intf_cols,
              par_dummy_cols  => par_dummy_cols,
              pv_index        => par_dummy_cols(ln_dummy_index).interface_col_name,
              pn_index        => ln_dummy_index);       
            /*
            || Ensure the layout block for the dummy column is the same as the
            || KFF column
            */
            if not PAR_INTF_COLS.exists(LV_INDEX) then
              raise_application_error(-20001,'par_intf_cols must include '||lv_index);
            end if;
            if par_intf_cols.exists(par_dummy_cols(ln_dummy_index).interface_col_name) then
              declare
                ln_layout_index number;
                lv_src_index    interface_col_name := lv_index;
                lv_tgt_index    interface_col_name := par_dummy_cols(ln_dummy_index).interface_col_name;
              begin
                ln_layout_index := pr_intg.layouts.first;
                WHILE LN_LAYOUT_INDEX is not null LOOP
                  if not par_intf_cols(lv_tgt_index).layout_attr.exists(ln_layout_index) then
                    par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).read_only_flag := 'N';
                  end if;
                  -- 
                 if par_intf_cols(lv_src_index).layout_attr.exists(ln_layout_index) 
                  then
                    par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).internal_block_id :=
                      nvl(par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).internal_block_id,
                          par_intf_cols(lv_src_index).layout_attr(ln_layout_index).internal_block_id);
                    par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).include_in_layout :=
                      coalesce(
                        par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).include_in_layout,
                        par_intf_cols(lv_src_index).layout_attr(ln_layout_index).include_in_layout,
                        'Y');
                    log_msg(lcv_proc_name,'Assigned '||lv_tgt_index||' [Inc in Layout '||ln_layout_index||', Layout Block]: '||
                      par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).include_in_layout||','||
                      par_intf_cols(lv_tgt_index).layout_attr(ln_layout_index).internal_block_id);
                  end if;
                  ln_layout_index := pr_intg.layouts.next(ln_layout_index);
                end loop;
              end;
            end if;
            log_msg(lcv_proc_name,'Finished processing this KFF segment');
          END loop;
           
          IF NOT lb_found_segments THEN
            raise_application_error(-20001,'KFF Not Found: '||par_kff_lovs(lv_index).oa_flex_application_id||' '||par_kff_lovs(lv_index).oa_flex_code||' '||par_kff_lovs(lv_index).oa_flex_num);
          END IF;        
          lv_index := par_kff_lovs.NEXT(lv_index);
        end loop;
      end if;
    end;
    /*
    || Do a quick check for updatable layouts 
    */
    log_heading(lcv_proc_name,'Checking for Updateable Layouts');
    ln_layout_index := pr_intg.layouts.first;
    while ln_layout_index IS NOT NULL loop
      /*
      || Check if this is an updateable layout
      */
      pr_intg.layouts(ln_layout_index).updateable_layout := pr_intg.layouts(ln_layout_index).update_map.content_sql IS NOT NULL;
      if pr_intg.layouts(ln_layout_index).updateable_layout then
        log_msg(lcv_proc_name,'Layout '||ln_layout_index||' is a download and update layout.');
        lb_updatable_layouts := TRUE;
      else
        log_msg(lcv_proc_name,'Layout '||ln_layout_index||' is an upload only layout.');      
      end if;
      ln_layout_index := pr_intg.layouts.NEXT(ln_layout_index);
    end loop;
    /*
    || Now loop through the layouts and set everything up
    */
    log_heading(lcv_proc_name,'Applying Field Defaults on all Layouts');
    ln_layout_index := pr_intg.layouts.first;
    while ln_layout_index IS NOT NULL loop
      log_heading(lcv_proc_name,'Applying Defaults for Layout '||ln_layout_index);
      /*
      || Check the layout code/name are set
      */
      IF ln_layout_index = ln_default_layout THEN
        pr_intg.layouts(ln_layout_index).layout_code := nvl(pr_intg.layouts(ln_layout_index).layout_code,pr_intg.intg_code||'_DFLT');
        pr_intg.layouts(ln_layout_index).layout_name := nvl(pr_intg.layouts(ln_layout_index).layout_name,'Default');
      ELSE
        pr_intg.layouts(ln_layout_index).layout_code := 
          nvl(
              pr_intg.layouts(ln_layout_index).layout_code,
              CREATE_UNIQUE_LAYOUT_CODE(
                pr_intg       => pr_intg,
                pn_layout_num => ln_layout_index)
             );
        IF pr_intg.layouts(ln_layout_index).updateable_layout THEN
          pr_intg.layouts(ln_layout_index).layout_name :=
            nvl(
                pr_intg.layouts(ln_layout_index).layout_name,
                'Update'||ln_layout_index);
        ELSE
          pr_intg.layouts(ln_layout_index).layout_name :=
            nvl(
                pr_intg.layouts(ln_layout_index).layout_name,
                'Layout'||ln_layout_index);
        END IF;
      END IF;
      pr_intg.layouts(ln_layout_index).create_header_block := nvl(pr_intg.layouts(ln_layout_index).create_header_block,FALSE);
      /*
      || Copy all the interface column layout details from the default layout 
      || (layout 1) and ensure that we have interface column layout details
      || for each layout :-)
      */
      lv_index := par_intf_cols.first;
      while lv_index is not null loop
        log_msg(lcv_proc_name,'Setting up defaults for '||lv_index||' in layout '||pr_intg.layouts(ln_layout_index).layout_code);
        /*
        || Confirm we have layout attributes for this column for this layout
        */
        IF NOT par_intf_cols(lv_index).layout_attr.EXISTS(ln_layout_index) THEN
          par_intf_cols(lv_index).layout_attr(ln_layout_index).include_in_layout := 'Y';
        ELSE
          par_intf_cols(lv_index).layout_attr(ln_layout_index).include_in_layout := nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).include_in_layout,'Y');
        END IF;
        /*
        || Flag if there is a header block required in this layout.
        */
        IF par_intf_cols(lv_index).layout_attr(ln_layout_index).header_field THEN
          /*
          || Found a field that is supposed to be in the header, so flag the
          || layout as needing a header block created.
          */
          pr_intg.layouts(ln_layout_index).create_header_block := TRUE;
        END IF;        
        /*
        || Populate layout attributes
        */
        IF ln_layout_index = ln_default_layout THEN
          /*
          || Default Layout 
          */
          case 
          when nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type,'x') = gcv_assign_default_null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type := null;
          when par_intf_cols(lv_index).default_type is not null and
               par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type is null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type := par_intf_cols(lv_index).default_type;
          else
            -- leave whatever is in the layout index default
            null;
          end case;
          
          case
          when nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value,'x') = gcv_assign_default_null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value := NULL;
          when par_intf_cols(lv_index).default_value is not null and
               par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value is null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value := par_intf_cols(lv_index).default_value;
          else
            -- leave whatever is in the layout index default
            null;
          end case;
          
          if par_intf_cols(lv_index).layout_attr(ln_layout_index).read_only_flag is null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).read_only_flag  := 'N';
          end if;
        ELSE
          /*
          || Not the default layout.
          */
          case
          when nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type,'x') = gcv_assign_default_null THEN
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type := null;
          when par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type is null and
               par_intf_cols(lv_index).layout_attr(ln_default_layout).default_type is not null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type  := nvl(par_intf_cols(lv_index).default_type, par_intf_cols(lv_index).layout_attr(ln_default_layout).default_type);
          else
            -- leave whatever is in the layout index default
            null;
          end case;

          case
          when nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value,'x') = gcv_assign_default_null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value := NULL;
          when par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value is null and
               par_intf_cols(lv_index).layout_attr(ln_default_layout).default_value is not null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value := nvl(par_intf_cols(lv_index).default_value, par_intf_cols(lv_index).layout_attr(ln_default_layout).default_value);
          else
            -- leave whatever is in the layout index default
            null;
          end case;

          if par_intf_cols(lv_index).layout_attr(ln_layout_index).read_only_flag is null then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).read_only_flag  := par_intf_cols(lv_index).layout_attr(ln_default_layout).read_only_flag;
          end if;

        END IF;

        if par_intf_cols(lv_index).layout_attr(ln_layout_index).display_width is null then
          par_intf_cols(lv_index).layout_attr(ln_layout_index).display_width := nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).display_width,par_intf_cols(lv_index).layout_attr(ln_default_layout).display_width);
        end if;

        lv_index := par_intf_cols.NEXT(lv_index);
      END loop;  
      
      ln_layout_index := pr_intg.layouts.NEXT(ln_layout_index);
    end loop;
    /*
    || Now that we know if we need a header block or not, reloop through all 
    || the layouts and interface columns and flag them with the appropriate 
    || block ids
    */
    ln_layout_index := pr_intg.layouts.first;
    while ln_layout_index is not null loop
      lv_index := par_intf_cols.first;
      while lv_index is not null loop
        if pr_intg.layouts(ln_layout_index).create_header_block then
          /*
          || There is a header block required in this layout, so put header 
          || fields in block_id 1 and line fields in block_id 2.
          */
          if par_intf_cols(lv_index).layout_attr(ln_layout_index).header_field then
            par_intf_cols(lv_index).layout_attr(ln_layout_index).internal_block_id := 1;
          else
            par_intf_cols(lv_index).layout_attr(ln_layout_index).internal_block_id := 2;
          end if;
        else
          /*
          || No header block required, so everything is in block id 3
          || (as specified in BNE_INTEGRATOR_UTILS.CREATE_DEFAULT_LAYOUT).
          */
          par_intf_cols(lv_index).layout_attr(ln_layout_index).internal_block_id := 3; -- 1.9 (WS)
        end if;

        lv_index := par_intf_cols.NEXT(lv_index);
      END loop;  
      ln_layout_index := pr_intg.layouts.NEXT(ln_layout_index);
    end loop;
    /*
    || If there are updatable layouts - 
    || Build content sql - then do a check for the required / read only problem
    */
    IF lb_updatable_layouts THEN
      /*
      || Now loop through the layouts and set sql column mapping
      */
      log_heading(lcv_proc_name,'Generating SQL Content Columns for layouts if required.');
      ln_layout_index := pr_intg.layouts.first;
      while ln_layout_index IS NOT NULL loop
        if pr_intg.layouts(ln_layout_index).updateable_layout then
    
          lv_index := par_intf_cols.first;
          while lv_index is not null loop
            /*
            || Populate the list of content columns for update layouts
            */
            if par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value is null then
              par_intf_cols(lv_index).layout_attr(ln_layout_index).sql_column := 
                nvl(
                    par_intf_cols(lv_index).layout_attr(ln_layout_index).sql_column,
                    lv_index
                   );
            end if;
            lv_index := par_intf_cols.NEXT(lv_index);
          end loop;
          
          IF pr_intg.layouts(ln_layout_index).update_map.content_cols IS NULL THEN
            log_msg(lcv_proc_name,'Generating Content Columns from: '||pr_intg.layouts(ln_layout_index).update_map.content_sql);
            declare
              lv_fields varchar2(3000);
              type vctable is table of varchar2(30) index by binary_integer;
              lav_fields vctable;
--              ln_index number;
--              ln_end_index number;
--              ln_start_index number;
--              lb_last_column boolean := FALSE;
              li_idx integer;
              c number;
              col_cnt number;
              desc_t dbms_sql.desc_tab;
            begin
              begin
                  log_msg(lcv_proc_name,'Opening cursor for: '||pr_intg.layouts(ln_layout_index).update_map.content_sql);
                  c := dbms_sql.open_cursor;
                  dbms_sql.parse(c, pr_intg.layouts(ln_layout_index).update_map.content_sql, dbms_sql.native);
                  dbms_sql.describe_columns(c, col_cnt, desc_t);
                  dbms_sql.close_cursor(c);
                  
                  log_msg(lcv_proc_name,'Found '||col_cnt||' columns in the cursor.');
              exception
                  when others then
                     log_msg(lcv_proc_name,'Failed to parse SQL due to: '||SQLERRM);
                     raise;
              end;
              pr_intg.layouts(ln_layout_index).update_map.content_sql := pr_intg.layouts(ln_layout_index).update_map.content_sql || pr_intg.layouts(ln_layout_index).update_map.content_where;
              
              li_idx := desc_t.last;
              while li_idx is not null loop
                lav_fields(lav_fields.count+1) := desc_t(li_idx).col_name;
                log_msg(lcv_proc_name,'Column '||desc_t(li_idx).col_name||' added to lav_fields array.');
                li_idx := desc_t.prior(li_idx);
              end loop;
              
--              lv_fields := ltrim(ltrim(upper(replace(pr_intg.layouts(ln_layout_index).update_map.content_sql,chr(10),' '))), 'SELECT ');
--              lv_fields := rtrim(substr(lv_fields, 1, instr(lv_fields,' FROM ')));
--              
--              log_msg(lcv_proc_name,'Fields to breakup should be in here: '||lv_fields);
--              ln_end_index := length(lv_fields);
--              FOR i IN 1 .. 501 loop
--                log_msg(lcv_proc_name,'lv_fields: '||lv_fields);
--                ln_start_index := greatest(instr(lv_fields,' ',-1), instr(lv_fields,'.',-1),instr(lv_fields,',',-1));
--                if ln_start_index is null then
--                  -- ran out of fields
--                  exit;
--                end if;
--                log_msg(lcv_proc_name,'ln_start_index: '||ln_start_index||' ln_end_index: '||ln_end_index);
--                lav_fields(lav_fields.count+1) := ltrim(substr(lv_fields,ln_start_index+1,(ln_end_index - ln_start_index)));
--                lv_fields := rtrim(substr(lv_fields,1,ln_start_index),' ');
--                if instr(lv_fields,',',-1) > 0 then
--                  lv_fields := substr(lv_fields,1,instr(lv_fields,',',-1)-1);
--                elsif instr(lv_fields,'.',-1) > 0 or lb_last_column then
--                  -- only a table alias or aliased column left
--                  exit;
--                end if;
--                
--                IF instr(lv_fields,' ',-1) > 0 AND instr(lv_fields,',',-1) = 0 THEN
--                  lb_last_column := TRUE;
--                end if;
--                
--                if i > 500 then
--                  raise_application_error(-20001,'SQL Statement Parser in apply_defaults failed miserably');
--                end if;
--              end loop;

              li_idx := lav_fields.last;
              while li_idx is not null loop
                if pr_intg.layouts(ln_layout_index).update_map.content_cols is null then
                  pr_intg.layouts(ln_layout_index).update_map.content_cols := lav_fields(li_idx);
                ELSE
                  pr_intg.layouts(ln_layout_index).update_map.content_cols := pr_intg.layouts(ln_layout_index).update_map.content_cols||','||lav_fields(li_idx);
                end if;
                li_idx := lav_fields.prior(li_idx);
              end loop;
            end;          
          else
            log_msg(lcv_proc_name,'Content Columns for this SQL was passed to the API - using it instead of deriving');
          end if;
          log_msg(lcv_proc_name,'Layout '||ln_layout_index||' is updateable.  Content Columns = '||pr_intg.layouts(ln_layout_index).update_map.content_cols);
        END IF;
        ln_layout_index := pr_intg.layouts.NEXT(ln_layout_index);
      end loop;
      
      declare
        lv_dummy_field        varchar2(200);
        --
        ln_dummy_index        number;
        ln_dummy_layout_index number;
        --
        ln_disp_index         number;
        ln_next_disp_index    number;
        ln_disp_seq_num       number;
      begin
        
        log_heading(lcv_proc_name,'Handling ADI Required/ReadOnly Bug');
        lv_index := par_intf_cols.first;
        while lv_index is not null loop
          /*
          || If the field is required - check all the updateable layouts to see if 
          || it's read only in them - if it is ADI causes problems.. (It doesn't send the read only
          || fields and so errors saying it's not populated!?)
          */
          if (par_intf_cols(lv_index).upload_read_only or par_intf_cols(lv_index).required_flag = 'Y') 
              and NOT par_intf_cols(lv_index).dummy_column then
            ln_layout_index := par_intf_cols(lv_index).layout_attr.first;
            while ln_layout_index is not null loop
              if par_intf_cols(lv_index).layout_attr(ln_layout_index).read_only_flag  = 'Y' then
                log_msg(lcv_proc_name,'Field '||lv_index||' will require a dummy display field');
                /*
                || Create Dummy field Name to Use as an Interface Column
                */
                lv_dummy_field := lv_index||'_DMYX';
                if par_intf_cols.exists(lv_dummy_field) then
                  -- this is lazy, but it'll do for now
                  raise_application_error(-20001,'Dummy field creation failed - already exists!?');
                else
                  /*
                  || Create a dummy field in the interface array
                  */
                  par_intf_cols(lv_dummy_field) := par_intf_cols(lv_index);    
                  par_intf_cols(lv_dummy_field).required_flag               := 'Y';
                  par_intf_cols(lv_dummy_field).dummy_column                := true;
                  log_msg(lcv_proc_name,'Created dummy field '||lv_dummy_field);
                  /*
                  || Create a dummy field in the dummy array
                  */
                  ln_dummy_index := par_dummy_cols.count+1;
                  par_dummy_cols(ln_dummy_index).interface_col_name         := lv_dummy_field;
                  par_dummy_cols(ln_dummy_index).enabled_flag               := 'Y';
                  par_dummy_cols(ln_dummy_index).required_flag              := par_intf_cols(lv_dummy_field).required_flag;
                  par_dummy_cols(ln_dummy_index).display_flag               := 'Y';
                  par_dummy_cols(ln_dummy_index).field_size                 := par_intf_cols(lv_dummy_field).field_size;
                  par_dummy_cols(ln_dummy_index).default_type               := par_intf_cols(lv_dummy_field).default_type;
                  par_dummy_cols(ln_dummy_index).default_value              := par_intf_cols(lv_dummy_field).default_value;
                  par_dummy_cols(ln_dummy_index).segment_number             := null;
                  par_dummy_cols(ln_dummy_index).group_name                 := NULL;
                  par_dummy_cols(ln_dummy_index).oa_flex_code               := NULL;
                  par_dummy_cols(ln_dummy_index).oa_concat_flex             := null;
                  par_dummy_cols(ln_dummy_index).read_only_flag             := 'Y';
                  par_dummy_cols(ln_dummy_index).val_type                   := NULL;
                  par_dummy_cols(ln_dummy_index).val_id_col                 := NULL;
                  par_dummy_cols(ln_dummy_index).val_mean_col               := NULL;
                  par_dummy_cols(ln_dummy_index).val_desc_col               := NULL;
                  par_dummy_cols(ln_dummy_index).val_obj_name               := NULL;
                  par_dummy_cols(ln_dummy_index).val_addl_w_c               := NULL;
                  par_dummy_cols(ln_dummy_index).data_type                  := 2;
                  par_dummy_cols(ln_dummy_index).not_null_flag              := 'N';
                  par_dummy_cols(ln_dummy_index).val_component_app_id       := NULL;
                  par_dummy_cols(ln_dummy_index).val_component_code         := NULL;
                  par_dummy_cols(ln_dummy_index).summary_flag               := 'N';
                  par_dummy_cols(ln_dummy_index).mapping_enabled_flag       := 'N';
                  par_dummy_cols(ln_dummy_index).user_hint                  := par_intf_cols(lv_dummy_field).user_hint;
                  par_dummy_cols(ln_dummy_index).user_help_text             := NULL;
                  par_dummy_cols(ln_dummy_index).oa_flex_num                := NULL;
                  par_dummy_cols(ln_dummy_index).oa_flex_application_id     := NULL;
                  par_dummy_cols(ln_dummy_index).upload_param_list_item_num := NULL;
                  par_dummy_cols(ln_dummy_index).expanded_sql_query         := NULL;
                  par_dummy_cols(ln_dummy_index).lov_type                   := NULL;
                  par_dummy_cols(ln_dummy_index).offline_lov_enabled_flag   := null;
                  par_dummy_cols(ln_dummy_index).variable_data_type_class   := null;
                  log_msg(lcv_proc_name,'Loaded dummy column '||ln_dummy_index);
                  /*
                  || Tweak the required flags
                  */
                  ln_dummy_layout_index := par_intf_cols(lv_index).layout_attr.first;
                  while ln_dummy_layout_index is not null loop
                    if par_intf_cols(lv_index).layout_attr(ln_dummy_layout_index).read_only_flag  = 'Y' and
                       pr_intg.layouts(ln_dummy_layout_index).updateable_layout then
                      /*
                      || Field is read only in an updatable layout - we need to hide it,
                      || and show our dummy field
                      ||
                      || So setup the "real" field to be required, updateable
                      || but 0 width (ie hidden from the user)
                      */
                      log_msg(lcv_proc_name,'Hiding '||lv_index||' and showing '||lv_dummy_field||' in layout '||ln_dummy_layout_index);
                      
                      IF pr_intg.layouts.EXISTS(ln_dummy_layout_index) THEN
                        IF pr_intg.layouts(ln_dummy_layout_index).update_map.content_cols is not null then
                          pr_intg.layouts(ln_dummy_layout_index).update_map.content_cols := 
                            pr_intg.layouts(ln_dummy_layout_index).update_map.content_cols||','||lv_dummy_field;
                        END IF;
                      END IF;
                      par_intf_cols(lv_index).layout_attr(ln_dummy_layout_index).display_width   := 0;
                      par_intf_cols(lv_index).layout_attr(ln_dummy_layout_index).read_only_flag  := 'N';
                      /*
                      || And set out dummy field to be non required and read only
                      || (so shown to the user)
                      */
                      par_intf_cols(lv_dummy_field).layout_attr(ln_dummy_layout_index).read_only_flag  := 'Y';
                      /*
                      || Add an extra column into the sql output
                      */
                      add_query_column(
                        pv_content_sql     => pr_intg.layouts(ln_dummy_layout_index).update_map.content_sql,
                        pv_original_column => nvl(par_intf_cols(lv_index).layout_attr(ln_dummy_layout_index).sql_column,lv_index),
                        pv_dummy_column    => lv_dummy_field);
                      par_intf_cols(lv_dummy_field).layout_attr(ln_dummy_layout_index).sql_column := lv_dummy_field;
                    else
                      /*
                      || Field isn't read only, or this isn't an updateable layout
                      || either way - we don't need to show our dummy field
                      */
                      log_msg(lcv_proc_name,'Removing '||lv_dummy_field||' from layout '||ln_dummy_layout_index);
                      par_intf_cols(lv_dummy_field).layout_attr(ln_dummy_layout_index).include_in_layout := 'N';
                    end if;
                    ln_dummy_layout_index := par_intf_cols(lv_index).layout_attr.next(ln_dummy_layout_index);
                  end loop;
                  log_msg(lcv_proc_name,'Fixed all layouts for '||lv_index);
                  /*
                  || Get the display sequence for the field
                  */
                  ln_disp_seq_num := find_display_seq(
                      pv_interface_col_name => lv_index,
                      par_display_seq       => par_display_seq);
                  /*
                  || Add the new field into the display sequence array
                  */
                  ln_disp_index := par_display_seq.LAST;
                  if ln_disp_index IS NULL THEN
                    raise_application_error(-20001,'par_display_seq is empty!');
                  ELSE
                    while ln_disp_seq_num < ln_disp_index loop
                      ln_next_disp_index := nvl(par_display_seq.next(ln_disp_index),ln_disp_index + 10);
                      par_display_seq(ln_next_disp_index) := par_display_seq(ln_disp_index);
                      log_msg(lcv_proc_name,'Moved: '||par_display_seq(ln_disp_index)||' from '||ln_disp_index||' to '||ln_next_disp_index);
                      ln_disp_index := par_display_seq.prior(ln_disp_index);
                    END loop;
                    ln_disp_index := par_display_seq.next(ln_disp_index);
                    par_display_seq(ln_disp_index) := par_dummy_cols(ln_dummy_index).interface_col_name;
                    log_msg(lcv_proc_name,'Added: '||par_dummy_cols(ln_dummy_index).interface_col_name||' into '||ln_disp_index);
                  END IF;
                  exit; -- exit layout loop
                end if;
              end if; -- found a layout that is a problem
              ln_layout_index := par_intf_cols(lv_index).layout_attr.next(ln_layout_index);
            end loop;
          end if;
          lv_index := par_intf_cols.NEXT(lv_index);
        end loop;  
      end;
      /*
      || Checking display_meaning flag ( only required if there are updatable layouts )
      */
      log_heading(lcv_proc_name,'Checking "display_meaning" flag for table lov fields in updateable layouts');
      lv_index := par_table_lovs.first;
      while lv_index IS NOT NULL loop
        IF par_table_lovs(lv_index).id_col != par_table_lovs(lv_index).mean_col THEN
          log_msg(lcv_proc_name,lv_index||' has a table LOV with different ID and MEANING column sources');
          ln_layout_index := pr_intg.layouts.first;
          while ln_layout_index IS NOT NULL loop
            IF pr_intg.layouts(ln_layout_index).updateable_layout THEN
              IF par_intf_cols.EXISTS(lv_index) THEN
                IF par_intf_cols(lv_index).layout_attr.EXISTS(ln_layout_index) THEN
                  CASE nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).display_meaning,'X')
                  WHEN 'N' THEN
                    log_msg(lcv_proc_name,'WARNING: Display Meaning not flagged in layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
                  WHEN 'X' THEN
                    par_intf_cols(lv_index).layout_attr(ln_layout_index).display_meaning := 'Y';
                    log_msg(lcv_proc_name,'Defaulted display_meaning to Y in layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
                  WHEN 'Y' THEN
                    log_msg(lcv_proc_name,'Display Meaning flagged as '||par_intf_cols(lv_index).layout_attr(ln_layout_index).display_meaning||' in layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
                  ELSE
                    raise_application_error(-20001,'Display Meaning flagged as '||par_intf_cols(lv_index).layout_attr(ln_layout_index).display_meaning||' in layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
                  END CASE;
                ELSE
                  par_intf_cols(lv_index).layout_attr(ln_layout_index).display_meaning := 'Y';
                  log_msg(lcv_proc_name,'Defaulted display_meaning to Y in layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code);
                END IF;
              ELSE
                raise_application_error(-20001,'Table LOV entry exists for '||lv_index||' but this is not in par_intf_cols');
              END IF;
            END IF;
            ln_layout_index := pr_intg.layouts.next(ln_layout_index);
          END LOOP;
        END IF;
        lv_index := par_table_lovs.next(lv_index);
      end loop;
    end if; -- updateable layout code.
    /*
    || BNE_INTERFACE_COLS needs to have the default value in it if there is a default
    || layout with a default So make sure we set the interface col level defaults
    */
    log_heading(lcv_proc_name,'Checking default values are valid.');
    declare
      lv_col_default_type    bne_interface_cols_b.default_type%type;
      lv_col_default_value   bne_interface_cols_b.default_value%type;
    begin
      lv_index := par_intf_cols.first;
      ln_layout_index := pr_intg.layouts.first;
      
      while lv_index is not null loop
        lv_col_default_type  := nvl(par_intf_cols(lv_index).default_type,'XX');
        lv_col_default_value := nvl(par_intf_cols(lv_index).default_value,'XX');
        
        if lv_col_default_type = 'XX' or lv_col_default_value = 'XX' then
          log_msg(lcv_proc_name,'Column: '||lv_index||' has no default at the interface level');
          if nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type,'XX') != 'XX' then
            par_intf_cols(lv_index).default_type := par_intf_cols(lv_index).layout_attr(ln_layout_index).default_type;
            log_msg(lcv_proc_name,'Updated interface col default type to: '||par_intf_cols(lv_index).default_type);
          end if;
          if nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value,'XX') != 'XX' then
            par_intf_cols(lv_index).default_value := par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value;
            log_msg(lcv_proc_name,'Updated interface col default value to: '||par_intf_cols(lv_index).default_value);
          end if;          
        end if;
        lv_index := par_intf_cols.next(lv_index);
      END loop;  
    end;
    /*
    || Now loop through the layouts and set display sequences
    */
    log_heading(lcv_proc_name,'Assigning Display Sequences');
    declare
      ln_disp_seq_num number;
    begin
      lv_index := par_intf_cols.first;
      while lv_index is not null loop
        log_msg(lcv_proc_name,'Setting up display_sequences for '||lv_index);
        /*
        || Find its display sequence
        */
        ln_disp_seq_num := find_display_seq(
          pv_interface_col_name => lv_index,
          par_display_seq       => par_display_seq);
        /*
        || Populate layout display sequence in all layouts linked to the column.
        */
        ln_layout_index := par_intf_cols(lv_index).layout_attr.first;
        while ln_layout_index is not null loop
          if par_intf_cols(lv_index).layout_attr(ln_layout_index).include_in_layout = 'Y' then
            log_msg(lcv_proc_name,'Setting Display Sequences for Layout '||ln_layout_index);
          end if;
          par_intf_cols(lv_index).layout_attr(ln_layout_index).display_seq_num := ln_disp_seq_num;
          ln_layout_index := par_intf_cols(lv_index).layout_attr.NEXT(ln_layout_index);
        end loop;

        lv_index := par_intf_cols.NEXT(lv_index);
      END loop;  
    end;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END apply_defaults;
  
    PROCEDURE add_sql_mapping(
        pr_intg         IN out nocopy integrator_rec,
        pr_intf         IN out nocopy interface_rec,
        par_intf_cols   IN out nocopy interface_col_table,
        pan_seq_nums    IN OUT NOCOPY seq_table)
    IS
        lcv_proc_name            constant varchar2(30) := 'add_sql_mapping';  
        ln_layout_index          number;
        ln_param_index           number;
        lv_param_list_code       bne_param_lists_b.param_list_code%type;      
        lv_param_defn_code       bne_param_list_items.param_defn_code%type;
        lv_short_param_defn_code bne_param_list_items.param_defn_code%type;
        lv_query_code            bne_simple_query.query_code%type;
        lan_sql_content_seq_nums seq_table;
        lv_index                 interface_col_name;
        ln_intf_seq_num          NUMBER;
        ln_map_seq_num           number;
        ln_sql_content_seq_num   NUMBER;
        ln_param_seq_num         number;
        lb_one_param_required    boolean := false;
        lb_includes_update_map   boolean := false;
    BEGIN
        ln_layout_index := pr_intg.layouts.FIRST;
        while ln_layout_index is not null loop
            if pr_intg.layouts(ln_layout_index).updateable_layout then
                lb_includes_update_map := TRUE;
                log_heading(lcv_proc_name,'Adding SQL Mapping for Layout '||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).layout_code||' (if required)');
                /*
                || Setup the sql statement 
                */
                pr_intg.layouts(ln_layout_index).update_map.content_code := nvl(
                    pr_intg.layouts(ln_layout_index).update_map.content_code
                  , create_unique_content_code(
                        pn_appl_id    => pr_intg.appl_id,
                        pv_intg_code  => pr_intg.intg_code,
                        pn_layout_num => ln_layout_index
                    )
                );
                --        log_msg(lcv_proc_name,'Creating Content Code'||pr_intg.layouts(ln_layout_index).update_map.content_code);
                --        log_msg(lcv_proc_name,'Content Name '||pr_intg.layouts(ln_layout_index).update_map.content_name);
                --        log_msg(lcv_proc_name,'Content Cols '||pr_intg.layouts(ln_layout_index).update_map.content_cols);
                log_msg(lcv_proc_name,'Creating content stored sql ['||pr_intg.appl_id||':'||pr_intg.layouts(ln_layout_index).update_map.content_code||'] for integrator: '||pr_intg.integrator_code);
                bne_content_utils.create_content_stored_sql (
                    p_application_id  => pr_intg.appl_id,
                    p_object_code     => pr_intg.layouts(ln_layout_index).update_map.content_code,
                    p_integrator_code => pr_intg.integrator_code,
                    p_content_desc    => pr_intg.layouts(ln_layout_index).update_map.content_name,
                    p_col_list        => pr_intg.layouts(ln_layout_index).update_map.content_cols,
                    p_query           => trim(pr_intg.layouts(ln_layout_index).update_map.content_sql),
                    p_language        => 'US',
                    p_source_language => 'US',
                    p_user_id         => pr_intg.user_id,
                    p_content_code    => pr_intg.layouts(ln_layout_index).update_map.sql_content_code
                );  
                log_msg(lcv_proc_name,'Stored SQL Content code: '||pr_intg.layouts(ln_layout_index).update_map.sql_content_code);
                /*
                || Check if there are parameters for the content sql
                */
                log_msg(lcv_proc_name,'There are '||to_char(pr_intg.layouts(ln_layout_index).update_map.content_sql_params.count)||' parameter(s) for this content sql');        
                if pr_intg.layouts(ln_layout_index).update_map.content_sql_params.count > 0 then
                    /*
                    || Content SQL requires parameters - create parameter list to store them
                    */
                    lv_param_list_code := create_param_list(
                        pr_intg       => pr_intg,
                        pv_persistent => 'N',
                        pv_prefix     => 'CNTP',
                        pv_user_name  => substr(pr_intg.layouts(ln_layout_index).update_map.content_name,1,230)||' - Params',
                        pv_comments   => 'Parameters for the '||pr_intg.layouts(ln_layout_index).update_map.content_code||' download content'
                    );
                    log_msg(lcv_proc_name, 'Created parameter list '||lv_param_list_code||' to store the parameters');
                    /*
                    || Load up the parameters
                    */
                    ln_param_index := pr_intg.layouts(ln_layout_index).update_map.content_sql_params.first;
                    ln_param_seq_num := 1;
                    while ln_param_index is not null loop
                        if pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).reuse_existing_flag then
                            log_msg(lcv_proc_name,'SQL parameter '||pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name||' is flagged as "reuse existing"');
                            begin
                                select param_defn_code
                                into   lv_short_param_defn_code
                                from   bne_param_defns_b
                                where  application_id = pr_intg.appl_id
                                and    param_defn_code = upper(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name);
                            exception
                                when no_data_found then 
                                    raise_application_error(-20001,'Unable to find param_defn: '||upper(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name));
                            end;
                        else
                            log_msg(lcv_proc_name,'Validating sql parameter '||pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name);
                            field_validate(
                                pv_field_name => 'content_sql_param '||ln_param_index||' - datatype',
                                pv_field_val  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).datatype,
                                pav_vals      => new valid_vals_table('V','D','N')
                            );
                            field_validate(
                                pv_field_name => 'content_sql_param '||ln_param_index||' - display_type',
                                pv_field_val  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).display_type,
                                pav_vals      => new valid_vals_table('1','2','3','4')
                            );
                            field_validate(
                                pv_field_name => 'content_sql_param '||ln_param_index||' - validation_type',
                                pv_field_val  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).validation_type,
                                pav_vals      => new valid_vals_table('N','V','F','B')
                            );
                            field_validate(
                                pv_field_name => 'content_sql_param '||ln_param_index||' - default_required_flag',
                                pv_field_val  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_required_flag,
                                pav_vals      => new valid_vals_table('Y','N')
                            );
                            if pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_required_flag = 'Y' then
                                lb_one_param_required := TRUE;
                            end if;
                            field_validate(
                                pv_field_name => 'content_sql_param '||ln_param_index||' - default_visible_flag',
                                pv_field_val  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_visible_flag,
                                pav_vals      => new valid_vals_table('Y','N')
                            );
                            field_validate(
                                pv_field_name => 'content_sql_param '||ln_param_index||' - default_user_mod_flag',
                                pv_field_val  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_user_mod_flag,
                                pav_vals      => new valid_vals_table('Y','N')
                            );
                            field_validate(
                                pv_field_name  => 'content_sql_param '||ln_param_index||' - default_boolean',
                                pv_field_val   => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_boolean,
                                pav_vals       => new valid_vals_table('Y','N'),
                                pv_default_val => 'NULL'
                            );
                            log_msg(lcv_proc_name,'Creating content sql parameter '||pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name);
                            if pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).validation_type = 'B' then
                                -- app_id:query_code needs to fit into bne_param_defns_b.validation value which is vc2(30)
                                lv_query_code := 'XX'||upper(substr(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name,1,(30-length(pr_intg.appl_id||':XX_LOV_Q1')))||'_LOV_Q1');
                                
                                log_msg(lcv_proc_name,'Deleting Simple Query: '||lv_query_code||' (if required)');
                                bne_query_utils.delete_query(
                                    p_application_id          => pr_intg.appl_id,
                                    p_query_code              => lv_query_code
                                );
                                log_msg(lcv_proc_name,'Deleting Simple Query: '||lv_query_code||' to validate this parameter');
                                bne_query_utils.create_simple_query(
                                    p_application_id          => pr_intg.appl_id,
                                    p_query_code              => lv_query_code,
                                    p_id_col                  => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.id_col,
                                    p_id_col_alias            => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.id_col_alias,
                                    p_meaning_col             => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.meaning_col,
                                    p_meaning_col_alias       => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.meaning_col_alias,
                                    p_description_col         => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.description_col,
                                    p_description_col_alias   => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.description_col_alias,
                                    p_additional_cols         => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.additional_cols,
                                    p_object_name             => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.object_name,
                                    p_additional_where_clause => nvl(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.additional_where_clause,' '),
                                    p_order_by_clause         => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query.order_by_clause,
                                    p_user_name               => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_seq_num).validation_query_name,
                                    p_user_id                 => pr_intg.user_id
                                );
                                log_msg(lcv_proc_name,'Setting validation value to be: '||pr_intg.appl_id||':'||lv_query_code);
                                pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).validation_value := pr_intg.appl_id||':'||lv_query_code;
                            end if;
                            log_msg(lcv_proc_name,'Creating unique parameter definition code');
                            lv_short_param_defn_code := create_unique_param_defn_code(
                                pv_prefix       => upper(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name),
                                pn_appl_id      => pr_intg.appl_id,
                                pb_skip_suffix  => TRUE
                            );
                            
                            log_msg(lcv_proc_name,'Creating Parameter: '||pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name);
                            log_msg(lcv_proc_name,'Passing param_defn_code: '||lv_short_param_defn_code);
                
                            lv_param_defn_code :=   bne_parameter_utils.create_param_all(
                                p_application_id    => pr_intg.appl_id,
                                p_param_code        => lv_short_param_defn_code,
                                p_param_name        => upper(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name),
                                p_param_source      => pr_intg.asn||':'||upper(pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name),
                                p_category          => 6,
                                p_data_type         => case pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).datatype
                                                       -- (V)archar2/(D)ate/(N)umber
                                                       when 'V' then 
                                                         1
                                                       when 'N' then
                                                         2
                                                       when 'D' then
                                                         3
                                                       end,
                                p_attribute_app_id  => null,
                                p_attribute_code    => null,
                                p_param_resolver    => null,
                                p_required          => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_required_flag,
                                p_visible           => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_visible_flag,
                                p_modifyable        => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_user_mod_flag,
                                p_default_string    => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_string,
                                p_default_date      => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_date,
                                p_default_num       => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_number,
                                p_default_boolean   => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_boolean,
                                p_default_formula   => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_formula,
                                p_val_type          => case pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).validation_type
                                                       -- (N)one, (V)alue Sets, (F)nd Lookups, (B)ne Query
                                                       when 'N' then 
                                                         1
                                                       when 'V' then
                                                         2
                                                       when 'F' then
                                                         3
                                                       when 'B' then
                                                         4
                                                       end,
                                p_val_value         => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).validation_value,
                                p_maximum_size      => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).max_size,
                                p_display_type      => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).display_type,
                                p_display_style     => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).display_style,
                                p_display_size      => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).display_size,
                                p_help_url          => null,
                                p_format_mask       => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).format_mask,
                                p_default_desc      => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).default_desc,
                                p_prompt_left       => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).prompt_left,
                                p_prompt_above      => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).prompt_above,
                                p_user_name          => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).user_name,
                                p_user_tip          => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).user_tip,
                                p_access_key        => null
                            );
                        end if;
                    
                        log_msg(lcv_proc_name,'Creating list item with parameter defn code: '||lv_short_param_defn_code);
                        create_param_list_item(
                            pv_created_by        => pr_intg.created_by_user,
                            pv_param_list_asn    => pr_intg.asn,
                            pv_param_list_code   => lv_param_list_code,
                            pv_param_defn_asn    => pr_intg.asn,
                            pv_param_defn_code   => lv_short_param_defn_code,
                            pn_seq_num           => ln_param_seq_num,
                            pv_param_name        => pr_intg.layouts(ln_layout_index).update_map.content_sql_params(ln_param_index).param_name
                        );
                        
                        ln_param_index := pr_intg.layouts(ln_layout_index).update_map.content_sql_params.next(ln_param_index);
                    end loop;
                    /*
                    || Assign parameter list of query parameters (if required)
                    */
                    log_msg(lcv_proc_name,'Assigning param list code '||lv_param_list_code||' to content '||pr_intg.layouts(ln_layout_index).update_map.sql_content_code);
                    bne_content_utils.assign_param_list_to_content(
                        p_content_app_id    => pr_intg.appl_id,
                        p_content_code      => pr_intg.layouts(ln_layout_index).update_map.sql_content_code,
                        p_param_list_app_id => pr_intg.appl_id,
                        p_param_list_code   => lv_param_list_code
                    );
                elsif pr_intg.layouts(ln_layout_index).update_map.param_list.appl_id is not null then
                    lb_one_param_required := true;
                    create_param_list(
                        pv_created_by_user => pr_intg.created_by_user
                        , pr_param_list      => pr_intg.layouts(ln_layout_index).update_map.param_list
                    );
                    /*
                    || Assign parameter list of query parameters (if required)
                    */
                    log_msg(lcv_proc_name,'Assigning param list code '||lv_param_list_code||' to content '||pr_intg.layouts(ln_layout_index).update_map.sql_content_code);
                    bne_content_utils.assign_param_list_to_content(
                        p_content_app_id    => pr_intg.appl_id,
                        p_content_code      => pr_intg.layouts(ln_layout_index).update_map.sql_content_code,
                        p_param_list_app_id => pr_intg.layouts(ln_layout_index).update_map.param_list.appl_id,
                        p_param_list_code   => pr_intg.layouts(ln_layout_index).update_map.param_list.param_list_code
                    );
                else
                    lb_one_param_required := true;
                end if;
        
                if not lb_one_param_required then
                    raise_application_error(-20001,'At least one SQL parameter must be required for them to display (dumb ADI requirement)');
                end if;
                /*
                || Create a mapping header record
                */
                log_msg(lcv_proc_name,'Creating a mapping header for this content to the interface records');
                pr_intg.layouts(ln_layout_index).update_map.mapping_code := nvl(
                    pr_intg.layouts(ln_layout_index).update_map.mapping_code
                  , create_unique_mapping_code(
                        pn_appl_id    => pr_intg.appl_id,
                        pv_intg_code  => pr_intg.intg_code,
                        pn_layout_num => ln_layout_index
                    )
                );
                --        log_msg(lcv_proc_name,'Loading Mapping Code '||pr_intg.layouts(ln_layout_index).update_map.mapping_code);
                --        log_msg(lcv_proc_name,'Mapping Name '||pr_intg.layouts(ln_layout_index).update_map.mapping_name);
                
                bne_mappings_pkg.load_row(
                    x_mapping_asn              => pr_intg.asn,
                    x_mapping_code             => pr_intg.layouts(ln_layout_index).update_map.mapping_code,
                    x_integrator_asn           => pr_intg.asn,
                    x_integrator_code          => pr_intg.integrator_code,
                    x_reporting_flag           => 'N',
                    x_reporting_interface_asn  => NULL,
                    x_reporting_interface_code => null,
                    x_user_name                => pr_intg.layouts(ln_layout_index).update_map.mapping_name,
                    x_object_version_number    => 1,
                    x_owner                    => pr_intg.created_by_user,
                    x_last_update_date         => to_char(SYSDATE,gcv_date_format),
                    x_custom_mode              => 'NORMAL'
                );
                /*
                || Load up the sequence numbers for the sql statement content
                */
                load_content_seq_num(
                    pn_appl_id      => pr_intg.appl_id,
                    pv_content_code => pr_intg.layouts(ln_layout_index).update_map.sql_content_code,
                    pan_seq_nums    => lan_sql_content_seq_nums
                );
                /*
                || Loop through the interface columns and link the sql column to the 
                || interface column
                */
                log_msg(lcv_proc_name,'Linking SQL Statement fields');        
                ln_map_seq_num := 1;
                lv_index := par_intf_cols.FIRST;
                while lv_index is not null loop
                    if par_intf_cols(lv_index).layout_attr(ln_layout_index).include_in_layout = 'Y' and
                        par_intf_cols(lv_index).layout_attr(ln_layout_index).default_value is null
                    THEN
                        /*
                        || Get sequence numbers for the interface and sql columns
                        */
                        IF pan_seq_nums.EXISTS(lv_index) THEN
                            ln_intf_seq_num := pan_seq_nums(lv_index);
                        ELSE
                            raise_application_error(-20001,'Can''t find sequence number for interface column: '||lv_index);
                        END IF;
                        IF lan_sql_content_seq_nums.EXISTS(par_intf_cols(lv_index).layout_attr(ln_layout_index).sql_column) THEN
                            ln_sql_content_seq_num := lan_sql_content_seq_nums(par_intf_cols(lv_index).layout_attr(ln_layout_index).sql_column);
                            /*
                            || Load the mapping
                            */
                            bne_mapping_lines_pkg.load_row(
                                x_mapping_asn           => pr_intg.asn,
                                x_mapping_code          => pr_intg.layouts(ln_layout_index).update_map.mapping_code,
                                x_interface_asn         => pr_intg.asn,
                                x_interface_code        => pr_intf.interface_code,
                                x_interface_seq_num     => to_char(ln_intf_seq_num),
                                x_decode_flag           => nvl(par_intf_cols(lv_index).layout_attr(ln_layout_index).display_meaning,'N'),
                                x_object_version_number => 1,
                                x_sequence_num          => to_char(ln_map_seq_num),
                                x_content_asn           => pr_intg.asn,
                                x_content_code          => pr_intg.layouts(ln_layout_index).update_map.sql_content_code,
                                x_content_seq_num       => to_char(ln_sql_content_seq_num),
                                x_owner                 => pr_intg.created_by_user,
                                x_last_update_date      => to_char(sysdate,gcv_date_format),
                                x_custom_mode           => 'NORMAL'
                            ); 
                            log_msg(lcv_proc_name,'Linked SQL Column: '||par_intf_cols(lv_index).layout_attr(ln_layout_index).sql_column||' to '||lv_index);
                            ln_map_seq_num := ln_map_seq_num + 1;
                        else
                            log_msg(lcv_proc_name,'**** Content SQL Mapping Failure');
                            log_msg(lcv_proc_name,'Interface Column ['||lv_index||'] Expected SQL Column ['||par_intf_cols(lv_index).layout_attr(ln_layout_index).sql_column||'] not found');
                            declare
                                lv_column varchar2(100); 
                            begin
                                lv_column := lan_sql_content_seq_nums.first;
                                while lv_column  is not null loop
                                    log_msg(lcv_proc_name, 'SQL Column: '||lv_column);
                                    lv_column := lan_sql_content_seq_nums.next(lv_column);
                                end loop;
                            end;
                        END IF;
                    END IF;
                    lv_index := par_intf_cols.NEXT(lv_index);
                END loop;    
            END IF;
            ln_layout_index := pr_intg.layouts.next(ln_layout_index);      
        end loop;
        /*
        || If we loaded an updatable map in, then there will be a content called
        || "None".  Check if the caller wants to rename it
        */
        if lb_includes_update_map then
            if pr_intg.empty_content_name is not null then
                UPDATE bne_contents_tl
                SET    user_name      = pr_intg.empty_content_name
                where  application_id = pr_intg.appl_id
                and    content_code   = pr_intg.intg_code||'_CNT'
                and    language       = userenv('LANG');
                if sql%rowcount = 1 then
                    log_msg(lcv_proc_name,'Updated default content name to '||pr_intg.empty_content_name);
                else
                    LOG_MSG(LCV_PROC_NAME,'Didn''t find default content for '||PR_INTG.APPL_ID||':'||PR_INTG.INTG_CODE||'_CNT');
                end if;
            ELSIF PR_INTG.NO_EMPTY_CONTENT then
                if BNE_INTEGRATOR_UTILS.DELETE_CONTENT
                    (P_APPLICATION_ID => PR_INTG.APPL_ID,
                    P_CONTENT_CODE   => PR_INTG.INTG_CODE||'_CNT') = 1 then
                    LOG_MSG(LCV_PROC_NAME,'Removed empty content '||PR_INTG.INTG_CODE||'_CNT for integrator');
                else
                    LOG_MSG(LCV_PROC_NAME,'Failed to removed empty content '||PR_INTG.INTG_CODE||'_CNT for integrator');
                end if;
            end if;
        end if;
    EXCEPTION
        WHEN OTHERS THEN
            log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
            raise;
    END add_sql_mapping;
  
  PROCEDURE create_integrator(
    pr_intg         IN out nocopy integrator_rec,
    pr_intf         IN out nocopy interface_rec,
    pr_conc_prog    IN out nocopy conc_prog_rec,
    pr_plsql_api    IN out nocopy plsql_api_rec,
    par_intf_cols   IN out nocopy interface_col_table,
    par_table_lovs  IN out nocopy table_lov_table,
    par_java_lovs   IN out nocopy java_lov_table,
    par_kff_lovs    IN out nocopy kff_lov_table,
    par_dff_lovs    IN out nocopy dff_lov_table,
    pav_clndr_cols  IN out nocopy calendar_headings_table,
    par_dummy_cols  IN out nocopy dummy_cols_table,
    par_display_seq IN out nocopy display_seq_table)
  IS
    lcv_proc_name          constant varchar2(30) := 'create_integrator';  
    lv_param_list_code     bne_param_lists_b.param_list_code%TYPE;
    lan_seq_nums           seq_table;
    lv_content_cols        VARCHAR2(4000) := NULL;
  begin
    log_msg(lcv_proc_name,'Starting Creation of Integrator: '||pr_intg.intg_code);
    /*
    || Delete the existing integrator (if required)
    */
    delete_integrator(
      pr_intg => pr_intg);
    /*
    || Apply defaults to the various record types
    */
    apply_defaults(
      pr_intg         => pr_intg,
      pr_intf         => pr_intf,
      par_intf_cols   => par_intf_cols,
      par_kff_lovs    => par_kff_lovs,
      par_dummy_cols  => par_dummy_cols,
      par_table_lovs  => par_table_lovs,
      par_display_seq => par_display_seq);
    /*
    || Verify integrator_name length
    */
    IF LENGTH(pr_intg.appl_id||':'||pr_intg.intg_code) > 20 THEN
      raise_application_error(-20001,pr_intg.appl_id||':'||pr_intg.intg_code||' must be 20 characters or less');
    end if;
    /*
    || Create the integrator
    */
    create_integrator_int(
      pr_intg => pr_intg);
    /*
    || Create interface 
    */
    IF pr_intf.interface_table_name IS NOT NULL THEN
      /*
      || Try creating the interface for this table
      */      
      create_interface_for_table(
        pr_intg            => pr_intg,
        pr_intf            => pr_intf);
    elsif pr_intf.api_package_name IS NOT NULL THEN
      create_interface_for_api(
        pr_intg            => pr_intg,
        pr_intf            => pr_intf,
        pv_param_list_code => lv_param_list_code);
    ELSE
      raise_application_error(-20001,'Interface must specify table name or api to call');
    END IF;
    /*
    || Create any dummy columns required for KFF's or java columns
    */
    log_msg(lcv_proc_name,'Creating '||par_dummy_cols.count||' Dummy Columns');
    create_dummy_columns(
      pr_intf        => pr_intf,
      par_dummy_cols => par_dummy_cols);
    /*
    || Reload the sequence numbers to include these additional columns
    */
    log_msg(lcv_proc_name,'Loading Interface Column Sequence Numbers');
    lan_seq_nums.DELETE;
    load_sequence_numbers(
      pr_intf      => pr_intf,
      pan_seq_nums => lan_seq_nums);
    /*
    || Update the interface column prompts and default values.
    */
    log_msg(lcv_proc_name,'Updating '||par_intf_cols.count||' interface columns');
    update_interface_columns (
      pr_intf       => pr_intf,
      pan_seq_nums  => lan_seq_nums,
      par_intf_cols => par_intf_cols);      
    /*
    || Add table LOVs
    */
    log_msg(lcv_proc_name,'Adding '||par_table_lovs.count||' Table LOVs');
    add_table_lovs(
      pr_intf        => pr_intf,
      pan_seq_nums   => lan_seq_nums,
      par_table_lovs => par_table_lovs);
    /*
    || Add Calendar LOVs
    */
    log_msg(lcv_proc_name,'Adding '||pav_clndr_cols.count||' Calendar LOVs');
    add_calendar_lovs(
      pr_intf        => pr_intf,
      pan_seq_nums   => lan_seq_nums,
      pav_clndr_cols => pav_clndr_cols);
    /*
    || Create KFF Lovs
    */
    log_msg(lcv_proc_name,'Adding '||par_kff_lovs.count||' KFF LOVs');
    add_kff_lovs(
      pr_intf        => pr_intf,
      pan_seq_nums   => lan_seq_nums,
      par_kff_lovs   => par_kff_lovs);
    /*
    || Create DFF Lovs
    */
    log_msg(lcv_proc_name,'Adding '||par_dff_lovs.count||' DFF LOVs');
    add_dff_lovs(
      pr_intf        => pr_intf,
      par_dff_lovs   => par_dff_lovs);
    /*
    || Add java LOV's and validators
    */
    log_msg(lcv_proc_name,'Adding '||par_java_lovs.count||' JAVA LOVs');
    add_java_lovs(
      pv_created_by_user => pr_intg.created_by_user,
      pr_intf            => pr_intf,
      pan_seq_nums       => lan_seq_nums,
      par_java_lovs      => par_java_lovs);
    /*
    || Create a layout for the webadi
    */
    log_msg(lcv_proc_name,'Creating Layouts');
    create_layouts(
      pr_intg                => pr_intg,
      pr_intf                => pr_intf);
    /*
    || Add default values, update block ids and read only flags in each of the
    || layouts.
    */
    log_msg(lcv_proc_name,'Loading Interface Column Sequence Numbers before update_layouts');
    lan_seq_nums.DELETE;
    load_sequence_numbers(
      pr_intf      => pr_intf,
      PAN_SEQ_NUMS => LAN_SEQ_NUMS);
      
    log_msg(lcv_proc_name,'Calling update_layouts');
    update_layouts(
      pr_intg                => pr_intg,
      pv_interface_user_name => pr_intf.interface_user_name,
      pan_seq_nums           => lan_seq_nums,
      par_intf_cols          => par_intf_cols);    
    /*
    || Update the display sequence for the columns in each of the layouts
    */
    log_msg(lcv_proc_name,'Calling set_layout_positions');
    set_layout_positions(
      pr_intg       => pr_intg,
      pan_seq_nums  => lan_seq_nums,
      par_intf_cols => par_intf_cols);
    /*
    || Add SQL Mapping to those layouts that need it
    */
    log_msg(lcv_proc_name,'Calling add_sql_mapping');
    add_sql_mapping(
      pr_intg       => pr_intg,
      pr_intf       => pr_intf,
      par_intf_cols => par_intf_cols,
      pan_seq_nums  => lan_seq_nums);
    /*
    || Create form function linked to the integrator
    */
    log_msg(lcv_proc_name,'Calling create_form_function');
    create_form_function(
      pr_intg        => pr_intg);      
    /*
    || Derive the user concurrent program name if required
    */
    IF pr_conc_prog.concurrent_program_name IS NOT NULL THEN
      BEGIN
        SELECT fcp.user_concurrent_program_name, fcp.application_id
        INTO   pr_conc_prog.user_conc_prog_name, 
               pr_conc_prog.program_appl_id
        FROM   fnd_concurrent_programs_vl fcp,
               fnd_application fa
        WHERE  fcp.concurrent_program_name = pr_conc_prog.concurrent_program_name
        AND    fcp.application_id = fa.application_id
        AND    fa.application_short_name = pr_conc_prog.program_asn;
      exception
        WHEN no_data_found THEN
          raise_application_error(-20001,'Failed to query concurrent program ['||pr_conc_prog.concurrent_program_name||'] in application ['||pr_conc_prog.program_asn||']');
      END;    
    END IF;
    /*
    || Create uploader
    */
    log_msg(lcv_proc_name,'Creating Uploader');
    create_default_uploader(
      pr_intg         => pr_intg,
      pr_conc_prog    => pr_conc_prog,
      pr_plsql_api    => pr_plsql_api);
    /*
    || Create importer if required
    */
    IF pr_conc_prog.concurrent_program_name IS NOT NULL THEN
      log_msg(lcv_proc_name,'Creating Concurrent Program Importer');
      create_conc_prog_importer(
        pr_intg            => pr_intg,
        pr_conc_prog       => pr_conc_prog);
    elsif pr_plsql_api.procedure_name IS NOT NULL THEN
      log_msg(lcv_proc_name,'Creating PL/SQL Importer');
      create_plsql_importer(
        pr_intg            => pr_intg,
        pr_plsql_api       => pr_plsql_api);
    ELSE
      log_msg(lcv_proc_name,'No importer specified.');    
    end if;
    log_msg(lcv_proc_name,'Finished creating integrator: '||pr_intg.intg_name||' ['||pr_intg.intg_code||']');
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_integrator;

  PROCEDURE create_integrator(
    pr_intg         IN out nocopy integrator_rec,
    pr_intf         IN out nocopy interface_rec,
    pr_conc_prog    IN out nocopy conc_prog_rec,
    par_intf_cols   IN out nocopy interface_col_table,
    par_table_lovs  IN out nocopy table_lov_table,
    par_java_lovs   IN out nocopy java_lov_table,
    par_kff_lovs    IN out nocopy kff_lov_table,
    par_dff_lovs    IN out nocopy dff_lov_table,
    pav_clndr_cols  IN out nocopy calendar_headings_table,
    par_dummy_cols  IN out nocopy dummy_cols_table,
    par_display_seq IN out nocopy display_seq_table)
  IS
    lr_plsql_api     plsql_api_rec;
    lcv_proc_name         constant varchar2(30) := 'create_integrator';
  BEGIN
    create_integrator(
      pr_intg         => pr_intg,
      pr_intf         => pr_intf,
      pr_conc_prog    => pr_conc_prog,
      pr_plsql_api    => lr_plsql_api,
      par_intf_cols   => par_intf_cols,
      par_table_lovs  => par_table_lovs,
      par_java_lovs   => par_java_lovs,
      par_kff_lovs    => par_kff_lovs,
      par_dff_lovs    => par_dff_lovs,
      pav_clndr_cols  => pav_clndr_cols,
      par_dummy_cols  => par_dummy_cols,
      par_display_seq => par_display_seq);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_integrator;

  PROCEDURE create_integrator(
    pr_intg         IN out nocopy integrator_rec,
    pr_intf         IN out nocopy interface_rec,
    par_intf_cols   IN out nocopy interface_col_table,
    par_table_lovs  IN out nocopy table_lov_table,
    par_java_lovs   IN out nocopy java_lov_table,
    par_kff_lovs    IN out nocopy kff_lov_table,
    par_dff_lovs    IN out nocopy dff_lov_table,
    pav_clndr_cols  IN out nocopy calendar_headings_table,
    par_dummy_cols  IN out nocopy dummy_cols_table,
    par_display_seq IN out nocopy display_seq_table)
  IS
    lr_plsql_api     plsql_api_rec;
    lr_conc_prog     conc_prog_rec;
    lcv_proc_name         constant varchar2(30) := 'create_integrator';
    
  BEGIN
    create_integrator(
      pr_intg         => pr_intg,
      pr_intf         => pr_intf,
      pr_conc_prog    => lr_conc_prog,
      pr_plsql_api    => lr_plsql_api,
      par_intf_cols   => par_intf_cols,
      par_table_lovs  => par_table_lovs,
      par_java_lovs   => par_java_lovs,
      par_kff_lovs    => par_kff_lovs,
      par_dff_lovs    => par_dff_lovs,
      pav_clndr_cols  => pav_clndr_cols,
      par_dummy_cols  => par_dummy_cols,
      par_display_seq => par_display_seq);
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_integrator;

  PROCEDURE create_adi_menu(
    pv_menu_name      IN VARCHAR2,
    pv_user_menu_name IN VARCHAR2,
    par_integrators   IN out nocopy integrator_table) IS
    
    /*
    || NB: Cursor will fail if the same form function is assigned into the menu
    || twice
    */
    CURSOR menu_entry(lr_int IN out nocopy int_menu_rec) IS
      SELECT nvl(
                 (SELECT fme.entry_sequence
                  FROM   fnd_menu_entries fme,
                         fnd_menus fm
                  WHERE  fm.menu_name = pv_menu_name
                  AND    fm.menu_id = fme.menu_id
                  AND    fme.function_id = ff.function_id),
                 (SELECT nvl(MAX(fme.entry_sequence),0) + 10
                  FROM   fnd_menu_entries fme,
                         fnd_menus fm
                  WHERE  fm.menu_name = pv_menu_name
                  AND    fm.menu_id = fme.menu_id)
                ) entry_sequence,
             nvl(
                 (SELECT 'Y'
                  FROM   fnd_menu_entries fme,
                         fnd_menus fm
                  WHERE  fm.menu_name = pv_menu_name
                  AND    fm.menu_id = fme.menu_id
                  AND    fme.function_id = ff.function_id),
                 'N') already_exists,
             ff.function_name, 
             bi.user_name 
      FROM   bne_secured_objects bso,
             bne_integrators_vl bi,
             bne_security_rules bsr,
             fnd_form_functions ff
      WHERE  bi.integrator_code = bso.object_code
      AND    bi.application_id = lr_int.application_id
      AND    bso.object_type = 'INTEGRATOR'
      AND    bi.integrator_code = lr_int.code
      AND    bso.security_rule_app_id = bsr.application_id
      AND    bso.security_rule_code = bsr.security_code
      AND    bsr.security_value = ff.function_name
      ORDER BY entry_sequence, user_name;
      
    lcv_proc_name       constant varchar2(30) := 'create_adi_menu';  
    lr_entry_rec        menu_entry%rowtype;
    
    ln_entry_sequence   NUMBER := 10;
    ln_menu_id          NUMBER;
    ln_entries_compiled NUMBER;
    ln_index            NUMBER;
    
    lb_changed_menu     boolean := FALSE;
  
    PROCEDURE compile_menu(
      pn_menu_id IN NUMBER) 
    IS
    BEGIN
      log_msg(lcv_proc_name,'Calling fnd_function.compile_menu_marked for menu_id '||pn_menu_id);
      ln_entries_compiled := fnd_function.compile_menu_marked(
        p_menu_id    => pn_menu_id,
        p_force      => 'Y');
      log_msg(lcv_proc_name,'Finished fnd_function.compile_menu_marked ('||ln_entries_compiled||')');
    END compile_menu;  
  BEGIN
    /*
    || Create the custom menu if required.  
    */
    fnd_menus_pkg.set_new_menu;
    fnd_menus_pkg.load_row (
      x_menu_name      => pv_menu_name,
      x_menu_type      => NULL,
      x_user_menu_name => pv_user_menu_name,
      x_description    => NULL,
      x_owner          => 'ORACLE',
      x_custom_mode    => 'N');
    log_msg(lcv_proc_name,'Loaded Menu '||pv_menu_name);
    /*
    || Loop through all the functions securing the custom ADIs and load them
    || onto the menu.  If they're already there, the current sequence number
    || will be passed in and load row will update the existing record
    */
    ln_index := par_integrators.FIRST;
    while ln_index IS NOT NULL loop
      log_msg(lcv_proc_name,'Processing Integrator '||par_integrators(ln_index).application_id||':'||par_integrators(ln_index).code);
      OPEN menu_entry (par_integrators(ln_index));
      fetch menu_entry INTO lr_entry_rec;
      IF menu_entry%notfound THEN
        CLOSE menu_entry;
        log_msg(lcv_proc_name,'WARNING: Integrator '||par_integrators(ln_index).application_id||':'||par_integrators(ln_index).code||' not found.');
      ELSE
        CLOSE menu_entry;
        IF lr_entry_rec.already_exists = 'N' THEN
          fnd_menu_entries_pkg.load_row(
            x_mode             => 'MERGE',
            x_ent_sequence     => lr_entry_rec.entry_sequence,
            x_menu_name        => pv_menu_name,
            x_sub_menu_name    => NULL,
            x_function_name    => lr_entry_rec.function_name,
            x_grant_flag       => 'Y',
            x_prompt           => substr(lr_entry_rec.user_name,1,60),
            x_description      => lr_entry_rec.user_name,
            x_owner            => 'ORACLE',
            x_custom_mode      => 'FORCE',
            x_last_update_date => to_char(SYSDATE,gcv_date_format));
          lb_changed_menu := TRUE;
          log_msg(lcv_proc_name,'Loaded Function '||lr_entry_rec.function_name||' as entry '||lr_entry_rec.entry_sequence);
        ELSE
          log_msg(lcv_proc_name,'Integrator '||par_integrators(ln_index).application_id||':'||par_integrators(ln_index).code||' already exists in the menu');
        END IF;
      END IF;
      ln_index := par_integrators.NEXT(ln_index);
    END loop;
    /*
    || Once we're finished - compile the menu
    */
    IF lb_changed_menu THEN
      SELECT menu_id
      INTO   ln_menu_id
      FROM   fnd_menus
      WHERE  menu_name = pv_menu_name;
      compile_menu(ln_menu_id);
    
      FOR datarec IN (SELECT menu_id
                      FROM   fnd_menu_entries
                      WHERE  sub_menu_id = ln_menu_id) loop
        compile_menu(datarec.menu_id);
      END loop;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      log_msg(lcv_proc_name,'Failed with: '||SQLERRM);
      raise;
  END create_adi_menu;

begin
  gcn_webadi_appl := get_application_id('BNE');
    
  SELECT user_id 
  INTO   gn_sysadmin_id
  FROM   fnd_user 
  WHERE  user_name = 'SYSADMIN';
  
  EXECUTE IMMEDIATE 'alter session set NLS_LANGUAGE= ''AMERICAN'' NLS_TERRITORY= ''AMERICA''';
END xxbne_adi_creator;