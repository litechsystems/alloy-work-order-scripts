set serveroutput on size 1000000
set linesize 254

prompt creating fa mass additions adi
declare
  lar_intf_cols   xxbne_adi_creator.interface_col_table;
  lar_table_lovs  xxbne_adi_creator.table_lov_table;
  lar_java_lovs   xxbne_adi_creator.java_lov_table;
  lav_clndr_cols  xxbne_adi_creator.calendar_headings_table;
  lar_dummy_cols  xxbne_adi_creator.dummy_cols_table;
  lar_display_seq xxbne_adi_creator.display_seq_table;
  lr_integrator   xxbne_adi_creator.integrator_rec;
  lr_interface    xxbne_adi_creator.interface_rec ;
  lar_kff_lovs    xxbne_adi_creator.kff_lov_table;
  lar_dff_lovs    xxbne_adi_creator.dff_lov_table;

  lr_conc_prog    xxbne_adi_creator.conc_prog_rec;
  lr_plsql_api    xxbne_adi_creator.plsql_api_rec;

  cursor username is
    select user_name
    from   fnd_user
    where  user_name in ('SANDERSON','SYSADMIN')
    order by decode(user_name,'SANDERSON',1,'SYSADMIN',2);
  
  procedure setup_int_cols is
    ln_transaction_type_id number;
  begin
    lar_intf_cols('MASS_ADDITION_ID').prompt                         := 'Mass Addition ID';
    lar_intf_cols('MASS_ADDITION_ID').field_size                     := 15;
    lar_intf_cols('MASS_ADDITION_ID').layout_attr(1).display_width   := 0;
    lar_intf_cols('MASS_ADDITION_ID').required_flag                  := 'Y';
    lar_intf_cols('MASS_ADDITION_ID').upload_read_only               := true;
    lar_intf_cols('MASS_ADDITION_ID').layout_attr(1).read_only_flag  := 'Y';

    lar_intf_cols('BOOK_TYPE_CODE').prompt                         := 'Asset Book';
    lar_intf_cols('BOOK_TYPE_CODE').field_size                     := 15;
    lar_intf_cols('BOOK_TYPE_CODE').layout_attr(1).display_width   := 25;
    lar_intf_cols('BOOK_TYPE_CODE').required_flag                  := 'Y';
    lar_intf_cols('BOOK_TYPE_CODE').layout_attr(1).header_field    := TRUE;
    lar_intf_cols('BOOK_TYPE_CODE').layout_attr(1).read_only_flag  := 'Y';

    lar_intf_cols('SERIAL_NUMBER').prompt                           := 'Serial Number';
    lar_intf_cols('SERIAL_NUMBER').field_size                       := 35;
    lar_intf_cols('SERIAL_NUMBER').layout_attr(1).display_width     := 14;

    lar_intf_cols('TAG_NUMBER').prompt                              := 'Tag Number';
    lar_intf_cols('TAG_NUMBER').field_size                          := 15;
    lar_intf_cols('TAG_NUMBER').layout_attr(1).display_width        := 9;

    lar_intf_cols('PROJECT_NUMBER').prompt                          := 'Project';
    lar_intf_cols('PROJECT_NUMBER').field_size                      := 30;
    lar_intf_cols('PROJECT_NUMBER').layout_attr(1).display_width    := 10;

    lar_intf_cols('TASK_NUMBER').prompt                       := 'Task';
    lar_intf_cols('TASK_NUMBER').field_size                   := 30;
    lar_intf_cols('TASK_NUMBER').layout_attr(1).display_width := 10;
    
    lar_intf_cols('QUEUE_NAME').prompt                       := 'Queue Name';
    lar_intf_cols('QUEUE_NAME').field_size                   := 15;
    lar_intf_cols('QUEUE_NAME').layout_attr(1).display_width := 9;
    lar_intf_cols('QUEUE_NAME').required_flag                := 'Y';

    lar_intf_cols('DESCRIPTION').prompt                       := 'Description';
    lar_intf_cols('DESCRIPTION').field_size                   := 80;
    lar_intf_cols('DESCRIPTION').layout_attr(1).display_width := 11;

    lar_intf_cols('FIXED_ASSETS_UNITS').prompt                       := 'Units';
    lar_intf_cols('FIXED_ASSETS_UNITS').field_size                   := 30;
    lar_intf_cols('FIXED_ASSETS_UNITS').layout_attr(1).display_width := 4;

    lar_intf_cols('FIXED_ASSETS_COST').prompt                       := 'Cost';
    lar_intf_cols('FIXED_ASSETS_COST').field_size                   := 30;
    lar_intf_cols('FIXED_ASSETS_COST').layout_attr(1).display_width := 10;

    lar_intf_cols('ASSET_CATEGORY_ID').prompt                       := 'Category';
    lar_intf_cols('ASSET_CATEGORY_ID').field_size                   := 30;
    lar_intf_cols('ASSET_CATEGORY_ID').layout_attr(1).display_width := 25;

    lar_intf_cols('DEPRN_EXPENSE_CCID').prompt                       := 'Expense';
    lar_intf_cols('DEPRN_EXPENSE_CCID').field_size                   := 30;
    lar_intf_cols('DEPRN_EXPENSE_CCID').layout_attr(1).display_width := 30;

    lar_intf_cols('PAYABLES_CCID').prompt                       := 'Clearing';
    lar_intf_cols('PAYABLES_CCID').field_size                   := 30;
    lar_intf_cols('PAYABLES_CCID').layout_attr(1).display_width := 30;

    lar_intf_cols('DATE_PLACED_IN_SERVICE').prompt                       := 'Date In Service';
    lar_intf_cols('DATE_PLACED_IN_SERVICE').layout_attr(1).display_width := 11;

    lar_intf_cols('ASSET_KEY_CCID').prompt                       := 'Asset Key';
    lar_intf_cols('ASSET_KEY_CCID').field_size                   := 30;
    lar_intf_cols('ASSET_KEY_CCID').layout_attr(1).display_width := 20;

    lar_intf_cols('LOCATION_ID').prompt                           := 'Location';
    lar_intf_cols('LOCATION_ID').field_size                       := 20;
    lar_intf_cols('LOCATION_ID').layout_attr(1).display_width     := 15;

    lar_intf_cols('ADD_TO_ASSET_ID').prompt                          := 'Add to Asset';
    lar_intf_cols('ADD_TO_ASSET_ID').field_size                      := 15;
    lar_intf_cols('ADD_TO_ASSET_ID').layout_attr(1).display_width    := 9;

    lar_intf_cols('COMPARE_TO_ASSET').prompt                         := 'Compare Asset';
    lar_intf_cols('COMPARE_TO_ASSET').field_size                     := 15;
    lar_intf_cols('COMPARE_TO_ASSET').layout_attr(1).display_width   := 9;
    lar_intf_cols('COMPARE_TO_ASSET').layout_attr(1).read_only_flag  := 'Y';

    lar_intf_cols('SKIP_CUSTOM_PROCESSING').prompt                           := 'Skip Custom Processing';
    lar_intf_cols('SKIP_CUSTOM_PROCESSING').field_size                       := 3;
    lar_intf_cols('SKIP_CUSTOM_PROCESSING').layout_attr(1).display_width     := 4;

    -- zero width columns
    lar_intf_cols('RUN_ID').prompt                       := 'Run ID';
    lar_intf_cols('RUN_ID').layout_attr(1).display_width := 0;
    lar_intf_cols('RUN_ID').layout_attr(1).header_field  := true;
    lar_intf_cols('RUN_ID').default_type                 := 'SQL';
    lar_intf_cols('RUN_ID').default_value                := 'SELECT XXBNE_BANK_ADI_RUNS_S.NEXTVAL FROM dual';  

    lar_intf_cols('STATUS').prompt                           := 'Status';
    lar_intf_cols('STATUS').layout_attr(1).header_field      := true;
    lar_intf_cols('STATUS').layout_attr(1).display_width     := 0;
    lar_intf_cols('STATUS').layout_attr(1).default_type      := 'CONSTANT';
    lar_intf_cols('STATUS').layout_attr(1).default_value     := 'N'; -- new

    lar_intf_cols('ERROR_DESCRIPTION').prompt                := 'Not Displayed';
    lar_intf_cols('ERROR_DESCRIPTION').display_flag          := 'N';
  end setup_int_cols;
  
  procedure define_table_lovs is
  begin 
    lar_table_lovs('PROJECT_NUMBER').id_col                := 'SEGMENT1';
    lar_table_lovs('PROJECT_NUMBER').mean_col              := 'NAME';
    lar_table_lovs('PROJECT_NUMBER').desc_col              := NULL;
    lar_table_lovs('PROJECT_NUMBER').table_name            := 'PA_PROJECTS_ALL';
    lar_table_lovs('PROJECT_NUMBER').addl_w_c              := '1 = 1 ORDER BY 1';
    lar_table_lovs('PROJECT_NUMBER').window_caption        := 'PA Project';
    lar_table_lovs('PROJECT_NUMBER').window_width          := 400;
    lar_table_lovs('PROJECT_NUMBER').window_height         := 300;
    lar_table_lovs('PROJECT_NUMBER').table_block_size      := 10;
    lar_table_lovs('PROJECT_NUMBER').table_sort_order      := 'yes,no';
    lar_table_lovs('PROJECT_NUMBER').poplist_flag          := 'N';  

    lar_table_lovs('QUEUE_NAME').id_col                := 'LOOKUP_CODE';
    lar_table_lovs('QUEUE_NAME').mean_col              := 'MEANING';
    lar_table_lovs('QUEUE_NAME').desc_col              := 'DESCRIPTION';
    lar_table_lovs('QUEUE_NAME').table_name            := 'XXBNE_FA_LOOKUPS_V';
    lar_table_lovs('QUEUE_NAME').addl_w_c              := 'LOOKUP_TYPE = ''QUEUE NAME'' ORDER BY LOOKUP_CODE';
    lar_table_lovs('QUEUE_NAME').window_caption        := 'Asset Queue';
    lar_table_lovs('QUEUE_NAME').window_width          := 400;
    lar_table_lovs('QUEUE_NAME').window_height         := 300;
    lar_table_lovs('QUEUE_NAME').table_block_size      := 10;
    lar_table_lovs('QUEUE_NAME').table_sort_order      := 'yes,no';
    lar_table_lovs('QUEUE_NAME').poplist_flag          := 'Y';

    lar_table_lovs('ADD_TO_ASSET_ID').id_col                := 'ASSET_ID';
    lar_table_lovs('ADD_TO_ASSET_ID').mean_col              := 'ASSET_NUMBER';
    lar_table_lovs('ADD_TO_ASSET_ID').desc_col              := 'DESCRIPTION';
    lar_table_lovs('ADD_TO_ASSET_ID').table_name            := 'FA_ADDITIONS_VL';
    lar_table_lovs('ADD_TO_ASSET_ID').addl_w_c              := 'IN_USE_FLAG = ''YES''';
    lar_table_lovs('ADD_TO_ASSET_ID').window_caption        := 'Add to Asset';
    lar_table_lovs('ADD_TO_ASSET_ID').window_width          := 600;
    lar_table_lovs('ADD_TO_ASSET_ID').window_height         := 700;
    lar_table_lovs('ADD_TO_ASSET_ID').table_block_size      := 10;
    lar_table_lovs('ADD_TO_ASSET_ID').table_sort_order      := 'yes,no';
    lar_table_lovs('ADD_TO_ASSET_ID').poplist_flag          := 'N';  

    lar_table_lovs('SKIP_CUSTOM_PROCESSING').id_col                := 'LOOKUP_CODE';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').mean_col              := 'MEANING';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').desc_col              := 'DESCRIPTION';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').table_name            := 'XXBNE_WEBADI_LOOKUP_VALUES_V';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').addl_w_c              := 'LOOKUP_TYPE = ''YES/NO'' AND VIEW_APPLICATION_ID = 101';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').window_caption        := 'Skip Custom Processing';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').window_width          := 400;
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').window_height         := 300;
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').table_block_size      := 10;
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').table_sort_order      := 'yes,no';
    lar_table_lovs('SKIP_CUSTOM_PROCESSING').poplist_flag          := 'Y';  
  end define_table_lovs;
  
  procedure define_calendar_lovs is
  begin
    lav_clndr_cols('DATE_PLACED_IN_SERVICE')             := 'Date Placed In Service';
  end define_calendar_lovs;
  
  procedure define_java_lovs is
  begin
    lar_java_lovs('TASK_NUMBER').component_class       := 'xxpwc.oracle.apps.bne.integrator.component.pa.BneTasksComponent';
    lar_java_lovs('TASK_NUMBER').validator_class       := 'xxpwc.oracle.apps.bne.integrator.validators.pa.BneTasksValidator';
  end define_java_lovs;
  
  procedure define_kff_lovs is
  begin
    lar_kff_lovs('ASSET_CATEGORY_ID').group_name             := 'ASSET_CATEGORY';
    lar_kff_lovs('ASSET_CATEGORY_ID').oa_flex_code           := 'CAT#';
    lar_kff_lovs('ASSET_CATEGORY_ID').oa_flex_application_id := 140;  -- (OFA - Oracle Fixed Assets)
    lar_kff_lovs('ASSET_CATEGORY_ID').oa_flex_num            := '$PARAM$.PWC_CATEGORY_FLEXNUM';--101; 

    lar_kff_lovs('DEPRN_EXPENSE_CCID').group_name             := 'EXPENSE_ACCOUNT';
    lar_kff_lovs('DEPRN_EXPENSE_CCID').oa_flex_code           := 'GL#';
    lar_kff_lovs('DEPRN_EXPENSE_CCID').oa_flex_application_id := 101;  -- (SQLGL - General Ledger)
    lar_kff_lovs('DEPRN_EXPENSE_CCID').oa_flex_num            := '$PARAM$.PWC_CHART_OF_ACCOUNTS_ID';--101;
    lar_kff_lovs('DEPRN_EXPENSE_CCID').column_prefix          := 'Expense';

    lar_kff_lovs('PAYABLES_CCID').group_name             := 'CLEARING_ACCOUNT';
    lar_kff_lovs('PAYABLES_CCID').oa_flex_code           := 'GL#';
    lar_kff_lovs('PAYABLES_CCID').oa_flex_application_id := 101;  -- (SQLGL - General Ledger)
    lar_kff_lovs('PAYABLES_CCID').oa_flex_num            := '$PARAM$.PWC_CHART_OF_ACCOUNTS_ID';--101;  --'SOB.ChartOfAccountsID';
    lar_kff_lovs('PAYABLES_CCID').column_prefix          := 'Clearing';

    lar_kff_lovs('ASSET_KEY_CCID').group_name             := 'ASSET_KEY';
    lar_kff_lovs('ASSET_KEY_CCID').oa_flex_code           := 'KEY#';
    lar_kff_lovs('ASSET_KEY_CCID').oa_flex_application_id := 140;  -- (OFA - Oracle Fixed Assets)
    lar_kff_lovs('ASSET_KEY_CCID').oa_flex_num            := '$PARAM$.PWC_ASSETKEY_FLEXNUM';--101;

    lar_kff_lovs('LOCATION_ID').group_name             := 'LOCATION';
    lar_kff_lovs('LOCATION_ID').oa_flex_code           := 'LOC#';
    lar_kff_lovs('LOCATION_ID').oa_flex_application_id := 140;  -- (OFA - Oracle Fixed Assets)
    lar_kff_lovs('LOCATION_ID').oa_flex_num            := '$PARAM$.PWC_LOCATION_FLEXNUM';--101;

  end define_kff_lovs;
  
  procedure define_dff_lovs is
  begin
    null;
  end define_dff_lovs;
  
    procedure define_dummy_cols is
        subtype int_rec is bne_interface_cols_vl%rowtype;
        lr_ro_col int_rec;
        procedure build_dummy_rec(
            pr_ro_col        in out nocopy int_rec
          , pv_column_name   in varchar2
          , pn_display_width in number
          , pv_prompt        in varchar2
          , pn_data_type     in number default 2
        ) is
        begin
            lar_dummy_cols(lar_dummy_cols.count+1) := pr_ro_col;
            lar_dummy_cols(lar_dummy_cols.count).sequence_num       := 51+lar_dummy_cols.count;
            lar_dummy_cols(lar_dummy_cols.count).interface_col_name := pv_column_name;
            lar_dummy_cols(lar_dummy_cols.count).field_size         := pn_display_width;
            lar_dummy_cols(lar_dummy_cols.count).display_width      := pn_display_width;
            lar_dummy_cols(lar_dummy_cols.count).display_order      := 31+lar_dummy_cols.count;
            lar_dummy_cols(lar_dummy_cols.count).prompt_left        := pv_prompt;
            lar_dummy_cols(lar_dummy_cols.count).prompt_above       := pv_prompt;
            lar_dummy_cols(lar_dummy_cols.count).data_type          := pn_data_type;
        end build_dummy_rec;
    begin
        -- setup defaults
        lr_ro_col.interface_col_type         := 1;
        lr_ro_col.enabled_flag               := 'Y';
        lr_ro_col.required_flag              := 'N';
        lr_ro_col.display_flag               := 'Y';
        lr_ro_col.read_only_flag             := 'Y';
        lr_ro_col.not_null_flag              := 'N';
        lr_ro_col.summary_flag               := 'N';
        lr_ro_col.mapping_enabled_flag       := 'Y';
        -- create dummy entries
        build_dummy_rec(lr_ro_col, 'C_MAXIMO_ASSET_NAME',10,'Maximo Asset Name');
        build_dummy_rec(lr_ro_col, 'C_MAXIMO_NUMBER',13,'Maximo Number');
        build_dummy_rec(lr_ro_col, 'C_ASSET_DESCRIPTION',10,'Asset Description');
        build_dummy_rec(lr_ro_col, 'C_ASSET_REGION',8,'Region');
        build_dummy_rec(lr_ro_col, 'C_ASSET_SUBURB',14,'Suburb');
        build_dummy_rec(lr_ro_col, 'C_SITE_LOCATION',10,'Site');
        build_dummy_rec(lr_ro_col, 'C_MAJOR_CATEGORY',5,'Major Category');
        build_dummy_rec(lr_ro_col, 'C_MINOR_CATEGORY',5,'Minor Category');
        build_dummy_rec(lr_ro_col, 'C_CATEGORY_LIFE',5,'Category Life');
        build_dummy_rec(lr_ro_col, 'C_QUANTITY',5,'Quantity',1);
        build_dummy_rec(lr_ro_col, 'C_UNIT_TYPE',5,'Unit Type');
        build_dummy_rec(lr_ro_col, 'C_EXPENSE_ACCOUNT',15,'Expense Account');
        build_dummy_rec(lr_ro_col, 'C_COST_ACCOUNT',15,'Cost Account');
        build_dummy_rec(lr_ro_col, 'C_GL_YEAR',8,'GL Year');
        build_dummy_rec(lr_ro_col, 'C_DATE_IN_SERVICE',12,'Date in Service');
        build_dummy_rec(lr_ro_col, 'C_DATE_RETIRED',11,'Date Retired');
        build_dummy_rec(lr_ro_col, 'C_LIFE_IN_MONTHS',7,'Months',1);
        build_dummy_rec(lr_ro_col, 'C_LIFE_IN_YEARS',7,'Years',1);
        build_dummy_rec(lr_ro_col, 'C_REMAINING_LIFE',9,'Remaining Life',1);
        build_dummy_rec(lr_ro_col, 'C_CATEGORY_PROJECT_NUMBER',10,'Category Project Number');
        build_dummy_rec(lr_ro_col, 'C_SRCLINE_PROJECT_NUMBERS',10,'Source Line Project Number');
        build_dummy_rec(lr_ro_col, 'C_REVALUATION_RESERVE',9,'Revaluation Reserve',1);
        build_dummy_rec(lr_ro_col, 'C_NET_BOOK_VALUE',9,'Net Book Value',1);
        build_dummy_rec(lr_ro_col, 'C_RETIRED',10,'Retired');
    end define_dummy_cols;
    
    procedure add_dummy_layout
    is
        function get_display_width(
          pv_column_name in varchar2
        ) return number is
            li_idx integer;
            ln_return_val number := null;
        begin
            li_idx := lar_dummy_cols.first;
            while li_idx is not null loop
                if lar_dummy_cols(li_idx).interface_col_name = pv_column_name then
                  ln_return_val := lar_dummy_cols(li_idx).display_width;
                  exit;
                end if;
                li_idx := lar_dummy_cols.next(li_idx);
            end loop;
            if ln_return_val is null then
              raise_application_error(-20001,'Dummy Col ['||pv_column_name||'] not found in lar_dummy_cols?');
            end if;
            return ln_return_val;
        end get_display_width;
        
        procedure add_layout_attr(
            pv_column_name    in varchar2
          , pn_attr_num       in number
          , pv_read_only_flag in varchar2
        ) is
            ln_display_width number := get_display_width(pv_column_name);
        begin
            if lar_intf_cols.exists(pv_column_name) then
                lar_intf_cols(pv_column_name).layout_attr(pn_attr_num).display_width  := ln_display_width;
                lar_intf_cols(pv_column_name).layout_attr(pn_attr_num).read_only_flag := pv_read_only_flag;
                lar_intf_cols(pv_column_name).interface_col_type := 2;  -- Custom
            else
                raise_application_error(-20001,'Unable to find column '||pv_column_name||' in lar_intf_cols');
            end if;
        end add_layout_attr;
    begin
        add_layout_attr('C_MAXIMO_ASSET_NAME',      1,'Y');
        add_layout_attr('C_MAXIMO_NUMBER',          1,'Y');
        add_layout_attr('C_ASSET_DESCRIPTION',      1,'Y');
        add_layout_attr('C_ASSET_REGION',           1,'Y');
        add_layout_attr('C_ASSET_SUBURB',           1,'Y');
        add_layout_attr('C_SITE_LOCATION',          1,'Y');
        add_layout_attr('C_MAJOR_CATEGORY',         1,'Y');
        add_layout_attr('C_MINOR_CATEGORY',         1,'Y');
        add_layout_attr('C_CATEGORY_LIFE',          1,'Y');
        add_layout_attr('C_QUANTITY',               1,'Y');
        add_layout_attr('C_UNIT_TYPE',              1,'Y');
        add_layout_attr('C_EXPENSE_ACCOUNT',        1,'Y');
        add_layout_attr('C_COST_ACCOUNT',           1,'Y');
        add_layout_attr('C_GL_YEAR',                1,'Y');
        add_layout_attr('C_DATE_IN_SERVICE',        1,'Y');
        add_layout_attr('C_DATE_RETIRED',           1,'Y');
        add_layout_attr('C_LIFE_IN_MONTHS',         1,'Y');
        add_layout_attr('C_LIFE_IN_YEARS',          1,'Y');
        add_layout_attr('C_REMAINING_LIFE',         1,'Y');
        add_layout_attr('C_CATEGORY_PROJECT_NUMBER',1,'Y');
        add_layout_attr('C_SRCLINE_PROJECT_NUMBERS',1,'Y');
        add_layout_attr('C_REVALUATION_RESERVE',    1,'Y');
        add_layout_attr('C_NET_BOOK_VALUE',         1,'Y');
        add_layout_attr('C_RETIRED',                1,'Y');
    end add_dummy_layout;
  
    procedure define_disp_seq is
        procedure add_entry(pv_col_name in varchar2) is
        begin
            if lar_display_seq.count = 0 then
                lar_display_seq(10) := pv_col_name;
            else
                lar_display_seq((lar_display_seq.count+1) * 10) := pv_col_name;
            end if;
        end add_entry;
    begin
        add_entry('BOOK_TYPE_CODE');
        add_entry('MASS_ADDITION_ID');
        add_entry('SKIP_CUSTOM_PROCESSING');
        add_entry('SERIAL_NUMBER');
        add_entry('TAG_NUMBER');
        add_entry('PROJECT_NUMBER');
        add_entry('TASK_NUMBER');
        add_entry('QUEUE_NAME');
        add_entry('DESCRIPTION');
        add_entry('FIXED_ASSETS_UNITS');
        add_entry('FIXED_ASSETS_COST');
        add_entry('PAYABLES_CCID');
        add_entry('ASSET_CATEGORY_ID');
        add_entry('DEPRN_EXPENSE_CCID');
        add_entry('DATE_PLACED_IN_SERVICE');
        add_entry('ASSET_KEY_CCID');
        add_entry('LOCATION_ID');
        add_entry('ADD_TO_ASSET_ID');
        --
        add_entry('COMPARE_TO_ASSET');
        add_entry('C_MAXIMO_ASSET_NAME');
        add_entry('C_MAXIMO_NUMBER');
        add_entry('C_ASSET_DESCRIPTION');
        add_entry('C_ASSET_REGION');
        add_entry('C_ASSET_SUBURB');
        add_entry('C_SITE_LOCATION');
        add_entry('C_MAJOR_CATEGORY');
        add_entry('C_MINOR_CATEGORY');
        add_entry('C_CATEGORY_LIFE');
        add_entry('C_QUANTITY');
        add_entry('C_UNIT_TYPE');
        add_entry('C_EXPENSE_ACCOUNT');
        add_entry('C_COST_ACCOUNT');
        add_entry('C_GL_YEAR');
        add_entry('C_DATE_IN_SERVICE');
        add_entry('C_DATE_RETIRED');
        add_entry('C_LIFE_IN_MONTHS');
        add_entry('C_LIFE_IN_YEARS');
        add_entry('C_REMAINING_LIFE');
        add_entry('C_CATEGORY_PROJECT_NUMBER');
        add_entry('C_SRCLINE_PROJECT_NUMBERS');
        add_entry('C_REVALUATION_RESERVE');
        add_entry('C_NET_BOOK_VALUE');
        add_entry('C_RETIRED');
        --
        add_entry('RUN_ID');
        add_entry('STATUS');
        add_entry('ERROR_DESCRIPTION');
    end define_disp_seq;
  
  procedure define_importer 
  is
  begin
    lr_plsql_api.procedure_name   := 'xxfa_mass_additions_adis.update_mass_additions';
    lr_plsql_api.importer_name    := 'PWC Mass Additions';
    
    lr_plsql_api.group_fields(1)  := 'XXFA_MASS_ADDITIONS_ADI.RUN_ID';
    
    lr_plsql_api.unique_row_fields(1)  := 'XXFA_MASS_ADDITIONS_ADI.MASS_ADDITION_ID';
    
    lr_plsql_api.procedure_params(1).param_name     := 'P_RUN_ID';
    lr_plsql_api.procedure_params(1).param_type     := 'NUMBER';
    lr_plsql_api.procedure_params(1).direction      := 'IN';
    lr_plsql_api.procedure_params(1).source_type    := 'IMPORT';
    lr_plsql_api.procedure_params(1).source_field   := 'XXFA_MASS_ADDITIONS_ADI.RUN_ID';
      
    lr_plsql_api.errored_rows_sql := 'SELECT * '||
                                     'FROM   XXFA_MASS_ADDITIONS_ADI '||
                                     'WHERE  STATUS = ''E'' '||
                                     'AND    RUN_ID = $PARAM$.RUN_ID_DEV_SEQ';
    lr_plsql_api.er_sql_params(1).param_name           := 'RUN_ID_DEV_SEQ';
    lr_plsql_api.er_sql_params(1).param_type           := 'NUMBER';
    lr_plsql_api.er_sql_params(1).direction            := 'BIND';
    lr_plsql_api.er_sql_params(1).source_type          := 'IMPORT';
    lr_plsql_api.er_sql_params(1).source_field         := 'XXFA_MASS_ADDITIONS_ADI.RUN_ID';
    
    lr_plsql_api.error_lkup_sql := 'SELECT $PARAM$.MESSAGE ERROR_MESSAGE '||
                                   'FROM   DUAL';
    lr_plsql_api.el_sql_params(1).param_name           := 'MESSAGE';
    lr_plsql_api.el_sql_params(1).param_type           := 'VARCHAR2';
    lr_plsql_api.el_sql_params(1).direction            := 'BIND';
    lr_plsql_api.el_sql_params(1).source_type          := 'INTERFACE_TABLE';
    lr_plsql_api.el_sql_params(1).source_field         := 'XXFA_MASS_ADDITIONS_ADI.ERROR_DESCRIPTION';

    lr_plsql_api.el_sql_params(2).param_name           := 'ERROR_MESSAGE';
    lr_plsql_api.el_sql_params(2).param_type           := 'VARCHAR2';
    lr_plsql_api.el_sql_params(2).direction            := 'RETURN';
    lr_plsql_api.el_sql_params(2).source_type          := null;
    lr_plsql_api.el_sql_params(2).source_field         := null;
  end define_importer;
  
  procedure add_to_menu(
    pn_appl_id         in number,
    pv_integrator_code in varchar2,
    pv_menu_name       in varchar2,
    pv_user_menu_name  in varchar2)
  is
    lar_integrators xxbne_adi_creator.integrator_table;
  begin
    lar_integrators(1).application_id := lr_integrator.appl_id;
    lar_integrators(1).code           := lr_integrator.integrator_code;
    xxbne_adi_creator.create_adi_menu(
      pv_menu_name      => pv_menu_name,
      pv_user_menu_name => pv_user_menu_name,
      par_integrators   => lar_integrators);
  end add_to_menu;
  
    function get_param_list 
    return xxbne_adi_creator.param_list_rec
    is
        lr_param_list   xxbne_adi_creator.param_list_rec;
        lr_list_item    xxbne_adi_creator.param_list_item_rec;
        lr_param_defn   xxbne_adi_creator.param_defn_rec;
        lr_list_group   xxbne_adi_creator.param_group_rec;
    begin
        -- Create a copy of the MA_PARAM_WNDW FA Mass Additions Parameters
        lr_param_list.appl_id               := 140;
        lr_param_list.param_list_code       := 'PWC_MA_PARAM_WNDW';
        lr_param_list.persistent_flag       := 'Y';
        lr_param_list.comments              := 'Parameter used for Create Assets parameter window';
        lr_param_list.user_tip              := 'Choose a Corporate Book';
        lr_param_list.prompt_left           := 'Asset Book';
        lr_param_list.prompt_above          := 'Asset Book';
        lr_param_list.user_name             := 'Web ADI: PWC Mass Additions Parameter Window';
        lr_param_list.list_items  := xxbne_adi_creator.list_items_table(null);
        lr_param_list.list_groups := xxbne_adi_creator.list_groups_table(null);
        --
        -- Group
        --
        lr_list_group.sequence_num          := 1;
        lr_list_group.group_resolver        := 'xxpwc.oracle.apps.bne.integrator.parameter.fa.FACOAGroupProcessor';
        lr_list_group.user_name             := 'PWC_MA_PARAM_FLEXNUM_GROUP';
        
        lr_param_list.list_groups(1) := lr_list_group;
        --
        -- Parameter Definition
        --
        lr_param_defn.appl_id                       := 140;
        lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_BOOK';
        lr_param_defn.param_name                    := 'PWC_BOOK_TYPE_CODE';
        lr_param_defn.param_source                  := 'WEBADI:Parameter';
        lr_param_defn.param_category                := 5;
        lr_param_defn.user_name                     := 'PWC_BOOK_TYPE_CODE';
        lr_param_defn.datatype                      := 1;
        lr_param_defn.default_required_flag         := 'N';
        lr_param_defn.default_visible_flag          := 'Y';
        lr_param_defn.default_user_modifyable_flag  := 'Y';
        lr_param_defn.default_String_trans_flag     := 'N';
        lr_param_defn.val_type                      := 2;
        lr_param_defn.val_value                     := 'FA_ADI_CORP_BOOK';
        lr_param_defn.max_size                      := 15;
        lr_param_defn.display_type                  := 3;
        lr_param_defn.display_style                 := 1;
        lr_param_defn.display_size                  := 15;
        lr_param_defn.prompt_left                   := 'Asset Book';
        lr_param_defn.prompt_above                  := 'Asset Book';
        --
        -- List Item
        --
        lr_list_item.sequence_num          := 1;
        lr_list_item.param_defn            := lr_param_defn;
        lr_list_item.param_name            := 'PWC_BOOK_TYPE_CODE';
        lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
        --
        ------------------------------------------------------------------------------------------------------------------
        --
        --
        -- Parameter Definition
        --
        lr_param_defn := null;
        lr_param_defn.appl_id                       := 140;
        lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_COAID';
        lr_param_defn.param_name                    := 'CHART_OF_ACCOUNTS_ID';
        lr_param_defn.param_source                  := 'WEBADI:Parameter';
        lr_param_defn.param_category                := 5;
        lr_param_defn.user_name                     := 'CHART_OF_ACCOUNTS_ID';
        lr_param_defn.datatype                      := 1;
        lr_param_defn.default_required_flag         := 'N';
        lr_param_defn.default_visible_flag          := 'N';
        lr_param_defn.default_user_modifyable_flag  := 'N';
        lr_param_defn.default_String_trans_flag     := 'N';
        lr_param_defn.val_type                      := 1;
        lr_param_defn.max_size                      := 100;
        lr_param_defn.display_type                  := 4;
        lr_param_defn.display_style                 := 1;
        lr_param_defn.display_size                  := 100;
        --
        -- List Item
        --
        lr_list_item := null;
        lr_list_item.sequence_num          := 2;
        lr_list_item.param_defn            := lr_param_defn;
        lr_list_item.param_name            := 'PWC_CHART_OF_ACCOUNTS_ID';
        lr_param_list.list_items.extend;
        lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
        --
        ------------------------------------------------------------------------------------------------------------------
        --
        --
        -- Parameter Definition
        --
        lr_param_defn := null;
        lr_param_defn.appl_id                       := 140;
        lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_LOC';
        lr_param_defn.param_name                    := 'LOCATION_FLEXNUM';
        lr_param_defn.param_source                  := 'WEBADI:Parameter';
        lr_param_defn.param_category                := 5;
        lr_param_defn.user_name                     := 'LOCATION_FLEXNUM';
        lr_param_defn.datatype                      := 1;
        lr_param_defn.default_required_flag         := 'N';
        lr_param_defn.default_visible_flag          := 'N';
        lr_param_defn.default_user_modifyable_flag  := 'N';
        lr_param_defn.default_String_trans_flag     := 'N';
        lr_param_defn.val_type                      := 1;
        lr_param_defn.max_size                      := 100;
        lr_param_defn.display_type                  := 4;
        lr_param_defn.display_style                 := 1;
        lr_param_defn.display_size                  := 100;
        --
        -- List Item
        --
        lr_list_item := null;
        lr_list_item.sequence_num          := 3;
        lr_list_item.param_defn            := lr_param_defn;
        lr_list_item.param_name            := 'PWC_LOCATION_FLEXNUM';
        lr_param_list.list_items.extend;
        lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
        --
        ------------------------------------------------------------------------------------------------------------------
        --
        --
        -- Parameter Definition
        --
        lr_param_defn := null;
        lr_param_defn.appl_id                       := 140;
        lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_CAT';
        lr_param_defn.param_name                    := 'CATEGORY_FLEXNUM';
        lr_param_defn.param_source                  := 'WEBADI:Parameter';
        lr_param_defn.param_category                := 5;
        lr_param_defn.user_name                     := 'CATEGORY_FLEXNUM';
        lr_param_defn.datatype                      := 1;
        lr_param_defn.default_required_flag         := 'N';
        lr_param_defn.default_visible_flag          := 'N';
        lr_param_defn.default_user_modifyable_flag  := 'N';
        lr_param_defn.default_String_trans_flag     := 'N';
        lr_param_defn.val_type                      := 1;
        lr_param_defn.max_size                      := 100;
        lr_param_defn.display_type                  := 4;
        lr_param_defn.display_style                 := 1;
        lr_param_defn.display_size                  := 100;
        --
        -- List Item
        --
        lr_list_item := null;
        lr_list_item.sequence_num          := 4;
        lr_list_item.param_defn            := lr_param_defn;
        lr_list_item.param_name            := 'PWC_CATEGORY_FLEXNUM';
        lr_param_list.list_items.extend;
        lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
        --
        ------------------------------------------------------------------------------------------------------------------
        --
        --
        -- Parameter Definition
        --
        lr_param_defn := null;
        lr_param_defn.appl_id                       := 140;
        lr_param_defn.param_defn_code               := 'PWC_MA_PARAM_WNDW_KEY';
        lr_param_defn.param_name                    := 'ASSETKEY_FLEXNUM';
        lr_param_defn.param_source                  := 'WEBADI:Parameter';
        lr_param_defn.param_category                := 5;
        lr_param_defn.user_name                     := 'ASSETKEY_FLEXNUM';
        lr_param_defn.datatype                      := 1;
        lr_param_defn.default_required_flag         := 'N';
        lr_param_defn.default_visible_flag          := 'N';
        lr_param_defn.default_user_modifyable_flag  := 'N';
        lr_param_defn.default_String_trans_flag     := 'N';
        lr_param_defn.val_type                      := 1;
        lr_param_defn.max_size                      := 100;
        lr_param_defn.display_type                  := 4;
        lr_param_defn.display_style                 := 1;
        lr_param_defn.display_size                  := 100;
        --
        -- List Item
        --
        lr_list_item := null;
        lr_list_item.sequence_num          := 5;
        lr_list_item.param_defn            := lr_param_defn;
        lr_list_item.param_name            := 'PWC_ASSETKEY_FLEXNUM';
        lr_param_list.list_items.extend;
        lr_param_list.list_items(lr_param_list.list_items.count) := lr_list_item;
        return lr_param_list;
    end get_param_list;  
  
    procedure compile_security
    is
        ln_request_id number;
        cursor user_info is
        select fu.user_id,
               fr.responsibility_id, 
               fr.application_id
        from   fnd_user fu,
               fnd_responsibility_vl fr
        where  fu.user_name = 'SYSADMIN'
        and    fr.responsibility_name = 'System Administrator';
        lr_user  user_info%rowtype;  
        msg      varchar2(32767);
    begin
        open user_info;
        fetch user_info into lr_user;
        close user_info;
        fnd_global.apps_initialize(lr_user.user_id, lr_user.responsibility_id, lr_user.application_id);
        ln_request_id := fnd_request.submit_request (
            application          => 'FND'
          , program              => 'FNDSCMPI'
          , start_time           => SYSDATE
          , argument1            => 'Y'    -- Compile all
        ); -- process stuck
        dbms_output.put_line('Submitted Request: '||ln_request_id);
    end compile_security;
begin
  dbms_output.enable(null);

  open username;
  fetch username into lr_integrator.created_by_user;
  close username;

  setup_int_cols;
  define_dummy_cols;
  xxbne_adi_creator.copy_dummy_cols_to_intf(
    par_intf_cols   => lar_intf_cols,
    par_dummy_cols  => lar_dummy_cols);
  add_dummy_layout;
  define_table_lovs;
  define_java_lovs;
  define_kff_lovs;
  define_dff_lovs;
  define_calendar_lovs;
  define_disp_seq;
  define_importer;

  lr_integrator.asn                := 'XXFA';
  lr_integrator.intg_prefix        := '_XXFAADD';  --(use _XX_ prefix so it creates "Custom" and therefore updateable Components)
  lr_integrator.intg_name          := 'PWC Mass Additions';
  lr_integrator.create_doc_list    := get_param_list;  -- These parameters appear to be usable INSIDE the ADI but not in the SQL Download :-(
  lr_integrator.no_empty_content   := TRUE;
  
  lr_integrator.layouts(1).layout_name              := 'Mass Additions';
  lr_integrator.layouts(1).update_map.mapping_name  := 'Mass Additions Mapping';
  lr_integrator.layouts(1).update_map.content_name  := 'Mass Additions Content';

  lr_integrator.layouts(1).update_map.content_sql   := q'[
select x.mass_addition_id, x.book_type_code, x.serial_number, x.tag_number, x.project_number, x.task_number, x.queue_name
       , x.description, x.fixed_assets_units, x.fixed_assets_cost, x.asset_category_id, x.asset_category_id_segment1
       , x.asset_category_id_segment2, x.asset_category_id_segment3, x.deprn_expense_ccid, x.deprn_expense_ccid_segment1
       , x.deprn_expense_ccid_segment2, x.deprn_expense_ccid_segment3, x.deprn_expense_ccid_segment4
       , x.deprn_expense_ccid_segment5, x.payables_ccid, x.payables_ccid_segment1, x.payables_ccid_segment2
       , x.payables_ccid_segment3, x.payables_ccid_segment4, x.payables_ccid_segment5, x.date_placed_in_service
       , x.asset_key_ccid, x.asset_key_ccid_segment1, x.location_id, x.location_id_segment1, x.location_id_segment2
       , x.location_id_segment3, x.add_to_asset_id, x.compare_to_asset, x.skip_custom_processing, x.c_maximo_asset_name
       , x.c_maximo_number, x.c_asset_description, x.c_asset_region, x.c_asset_suburb, x.c_site_location, x.c_major_category
       , x.c_minor_category, x.c_category_life, x.c_quantity, x.c_unit_type, x.c_expense_account, x.c_cost_account, x.c_gl_year
       , x.c_date_in_service, x.c_date_retired, x.c_life_in_months, x.c_life_in_years, x.c_remaining_life
       , x.c_category_project_number, x.c_srcline_project_numbers, x.c_revaluation_reserve, x.c_net_book_value, x.c_retired
       , x.run_id, x.status, x.error_description
from   xxfa_mass_additions_adi_v x
where  2 = 2]';
-- content_where is required to be defined seperately to allow dbms_sql.parse to parse the content_sql (to derive the column
-- names.  (ie $param$.xyz isn't valid sql so it would be rejected)
  lr_integrator.layouts(1).update_map.content_where   := q'[
and    x.book_type_code = $param$.book_type_code
order by x.serial_number]';
--  lr_integrator.layouts(1).update_map.param_list    := get_param_list;

  lr_integrator.layouts(1).update_map.content_sql_params(1).param_name             := 'book_type_code';
  lr_integrator.layouts(1).update_map.content_sql_params(1).user_name              := 'book_type_code';
  lr_integrator.layouts(1).update_map.content_sql_params(1).default_desc           := null;
  lr_integrator.layouts(1).update_map.content_sql_params(1).prompt_left            := 'Asset Book';
  lr_integrator.layouts(1).update_map.content_sql_params(1).prompt_above           := 'Asset Book';
  lr_integrator.layouts(1).update_map.content_sql_params(1).user_tip               := 'Enter an Asset Book to download.';
  lr_integrator.layouts(1).update_map.content_sql_params(1).datatype               := 'V';
  lr_integrator.layouts(1).update_map.content_sql_params(1).max_size               := 240;
  lr_integrator.layouts(1).update_map.content_sql_params(1).display_size           := 50;
  lr_integrator.layouts(1).update_map.content_sql_params(1).display_type           := 3; -- text field
  lr_integrator.layouts(1).update_map.content_sql_params(1).validation_type        := 'V';
  lr_integrator.layouts(1).update_map.content_sql_params(1).validation_value       := 'FA_BOOK_TO_AUTO_PREPARE';--JAI_BOOK_TYPE';--'FA_ADI_CORP_BOOK';
  lr_integrator.layouts(1).update_map.content_sql_params(1).default_required_flag  := 'Y';

  lr_interface.interface_user_name := 'PWC Mass Additions';
  lr_interface.interface_table_name  := 'XXFA_MASS_ADDITIONS_ADI';
  lr_interface.interface_table_asn   := 'XXFA';
  lr_interface.interface_table_owner := 'XXPWC';

  xxbne_adi_creator.create_integrator
    (pr_intg         => lr_integrator,
     pr_intf         => lr_interface,
     pr_conc_prog    => lr_conc_prog,
     pr_plsql_api    => lr_plsql_api,
     par_intf_cols   => lar_intf_cols,
     par_table_lovs  => lar_table_lovs,
     par_java_lovs   => lar_java_lovs,
     par_kff_lovs    => lar_kff_lovs,
     par_dff_lovs    => lar_dff_lovs,
     pav_clndr_cols  => lav_clndr_cols,
     par_dummy_cols  => lar_dummy_cols,
     par_display_seq => lar_display_seq);
  
  add_to_menu(
    pn_appl_id         => lr_integrator.appl_id,
    pv_integrator_code => lr_integrator.integrator_code,
    pv_menu_name       => 'FA_WEBADI_MENU',
    pv_user_menu_name  => 'FA_WEBADI_MENU');

  compile_security;
  
  commit;
end;
/

