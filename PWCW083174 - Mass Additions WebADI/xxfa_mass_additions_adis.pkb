create or replace package body apps.xxfa_mass_additions_adis as
    --
    -- Logger object and log levels
    --
    gci_unexpected  constant integer := xxfnd_log_pkg.gci_unexpected;
    gci_error       constant integer := xxfnd_log_pkg.gci_error;
    gci_exception   constant integer := xxfnd_log_pkg.gci_exception;
    gci_event       constant integer := xxfnd_log_pkg.gci_event;
    gci_procedure   constant integer := xxfnd_log_pkg.gci_procedure;
    gci_statement   constant integer := xxfnd_log_pkg.gci_statement;
    log xxfnd_logger_obj := new xxfnd_logger_obj(
        pv_package_name  => 'XXFA_MASS_ADDITIONS_ADIS'
--      , pi_log_level     => gci_statement
--      , pb_ignore_levels => TRUE
    );
    -- record statuses
    gcv_sts_new        constant varchar2(1) := 'N';
    gcv_sts_processing constant varchar2(1) := 'P';
    gcv_sts_completed  constant varchar2(1) := 'C';
    gcv_sts_error      constant varchar2(1) := 'E';
    gcv_sts_insert     constant varchar2(1) := 'I';
    gcv_sts_update     constant varchar2(1) := 'U';
    /*
    || Common cursor for processing ADI data
    */
    cursor run_records(
        pn_run_id in number,
        pv_status in varchar2
    ) is
        select xma.rowid myrow
             , xma.*
        from   xxfa_mass_additions_adi xma
        where  xma.run_id = pn_run_id
        and    xma.status = pv_status
        order by xma.serial_number;
    subtype run_records_typ is run_records%rowtype;
    /*
    || Updates the record identified by prw_rowid to have the status and error
    || description passed in.  (This was autonomous but it caused various 
    || deadlocking issues when used within the loops)
    */
    procedure update_record_status(
        pr_rec       in out nocopy run_records_typ
    ) is
    begin
        log.set_method('update_record_status');
        
        update xxfa_mass_additions_adi
        set    status = pr_rec.status,
               error_description = pr_rec.error_description
        where  rowid = pr_rec.myrow;
        
        log.end_method;
    exception
        when others then
            log.log_error('Failed to update rowid ['||pr_rec.myrow||'] with status ['||pr_rec.status||']',sqlerrm);
            log.end_method;
    end update_record_status;
    /*
    || Updates the status field on the current record to be whatever status is
    || passed in, unless the current record is already flagged as in error.
    || Appends the error message passed in to the error_description field if it is 
    || not already in the field.
    */
    procedure set_status(
        pv_current_status    in out nocopy varchar2,
        pv_current_error_msg in out nocopy varchar2,
        pv_new_status        in varchar2,
        pv_new_error_msg     in varchar2)
    is
    begin
        log.set_method('set_status');
        /*
        || Update status (once it's E, it can't change)
        */
        if pv_current_status != gcv_sts_error then
            pv_current_status := pv_new_status;
        end if;
        /*
        || If we're flagging the record as in error, add the error message passed 
        || in
        */
        case
        when pv_current_status = gcv_sts_error and 
            pv_current_error_msg is null and
            pv_new_error_msg is not null 
        then
            pv_current_error_msg := pv_new_error_msg;
        when pv_current_status = gcv_sts_error and 
            pv_current_error_msg is not null and
            pv_new_error_msg is not null and
            instr(pv_current_error_msg,pv_new_error_msg) = 0 
        then
            pv_current_error_msg := substr(pv_current_error_msg||', '||pv_new_error_msg,1,2000);
        else
            null; -- nothing to do
        end case;
        log.log_message('Updated status for current record to: ['||pv_current_status||']: '||pv_current_error_msg,gci_statement);
        log.end_method;
    exception
        when others then
            log.log_char('pv_current_status',pv_current_status,gci_error);
            log.log_char('pv_current_error_msg',pv_current_error_msg,gci_error);
            log.log_char('pv_new_status',pv_new_status,gci_error);
            log.log_char('pv_new_error_msg',pv_new_error_msg,gci_error);
            log.log_error('Failed',sqlerrm);
            log.end_method;
    end set_status;
  
    procedure set_status(
        pr_rec       in out nocopy run_records_typ,
        pv_status    in varchar2,
        pv_error_msg in varchar2 default null
    ) is
    begin
        log.set_method('set_status');
        set_status(
            pv_current_status    => pr_rec.status,
            pv_current_error_msg => pr_rec.error_description,
            pv_new_status        => pv_status,
            pv_new_error_msg     => pv_error_msg
        );
        log.log_message('Called set_status for current record to: ['||pv_status||']: '||pv_error_msg,gci_statement);
        log.end_method;
    exception
        when others then
            log.log_char('pv_status',pv_status,gci_error);
            log.log_char('pv_error_msg',pv_error_msg,gci_error);
            log.log_error('Failed',sqlerrm);
            log.end_method;
    end set_status;
  
    function get_api_errors(
        pn_msg_cnt           in number,
        pv_api_name          in varchar2)
    return varchar2 
    is
        ln_idx        number;
        lv_msg_data   varchar2(2000);
        lv_errors     varchar2(2000);
    begin
        log.set_method('get_api_errors');
        for ln_idx in 1 .. pn_msg_cnt loop
            lv_msg_data :=  trim(
            substr(
                fnd_msg_pub.get(
                    p_msg_index => ln_idx,
                    p_encoded   => 'F'),
                    1,2000
                )
            );
            if lv_msg_data is not null then
                lv_msg_data := '('||ln_idx||') '||lv_msg_data;
                log.log_message(lv_msg_data, gci_statement);
                if lv_errors is null then
                    lv_errors := substr(pv_api_name||' -> '||lv_msg_data,1,2000);
                else
                    lv_errors := substr(lv_errors||' '||lv_msg_data,1,2000);
                end if;
            end if;
        end loop;
        
        fnd_msg_pub.initialize;
        
        log.end_method;
        return(nvl(lv_errors,pv_api_name||' Failed but returned no messages'));
    exception
        when others then
            log.log_number('pn_msg_cnt',pn_msg_cnt,gci_error);
            log.log_char('pv_api_name',pv_api_name,gci_error);
            log.log_error('Failed',sqlerrm);
            log.end_method;
    end get_api_errors;
    
    procedure load_api_errors(
        pr_rec       in out nocopy run_records_typ,
        pn_msg_cnt   in number,
        pv_api_name  in varchar2
    ) is
        lv_errors     varchar2(2000) := get_api_errors(
            pn_msg_cnt  => pn_msg_cnt,
            pv_api_name => pv_api_name
        );
    begin
        log.set_method('load_api_errors');
        set_status(
            pr_rec       => pr_rec,
            pv_status    => gcv_sts_error,
            pv_error_msg => lv_errors
        );
        log.end_method;
    exception
        when others then
            log.log_number('pn_msg_cnt',pn_msg_cnt,gci_error);
            log.log_char('pv_api_name',pv_api_name,gci_error);
            log.log_error('Failed',sqlerrm);
            log.end_method;
    end load_api_errors;
  
    --==========================================================================================================================
    -- Name :         update_mass_addition
    -- Description :  Private method that updates the mass_addition record.  Assumes that the record is already validated.
    --
    --==========================================================================================================================
    procedure update_mass_addition(
        pr_rec       in out nocopy run_records_typ
    ) is
        lv_err_msg    varchar2(4000);
        lv_err_desc   varchar2(1000);
    
        lr_bank         run_records_typ;

        -- Return variables from IBY_EXT_BANKACCT_PUB API
        ln_msg_cnt    number;
        lv_msg_data   varchar2(2000);
        lv_rtn_sts    varchar2(1);
        
        li_rowcount   integer;
    begin
        log.set_method('update_mass_addition');
        if pr_rec.mass_addition_id is not null then
            update fa_massadd_distributions
            set    deprn_expense_ccid = pr_rec.deprn_expense_ccid
                 , location_id        = pr_rec.location_id
            where  mass_addition_id = pr_rec.mass_addition_id;
            li_rowcount := sql%rowcount;
            if li_rowcount > 1 then
                raise_application_error(-20001,'Multiple Distributions for Mass Addition Id ['||pr_rec.mass_addition_id||']');
            end if;
        
            update fa_mass_additions fm
            set    asset_category_id             = pr_rec.asset_category_id      
                 , asset_key_ccid                = pr_rec.asset_key_ccid         
                 , book_type_code                = pr_rec.book_type_code         
                 , date_placed_in_service        = pr_rec.date_placed_in_service 
                 , description                   = pr_rec.description            
                 , fixed_assets_cost             = case 
                                                   when fm.parent_mass_addition_id is null then
                                                       fixed_assets_cost
                                                   else
                                                       pr_rec.fixed_assets_cost
                                                   end
                 , fixed_assets_units            = pr_rec.fixed_assets_units     
                 , location_id                   = case
                                                   when fm.location_id is null and
                                                        li_rowcount = 0 then
                                                       pr_rec.location_id
                                                   when fm.location_id is not null then
                                                       pr_rec.location_id
                                                   else
                                                       fm.location_id
                                                   end
                 , mass_addition_id              = pr_rec.mass_addition_id       
                 , payables_code_combination_id  = pr_rec.payables_ccid          
                 , project_id                    = pr_rec.project_id             
                 , queue_name                    = pr_rec.queue_name             
                 , posting_status                = pr_rec.posting_status         
                 , serial_number                 = pr_rec.serial_number          
                 , tag_number                    = pr_rec.tag_number             
                 , attribute15                   = pr_rec.skip_custom_processing 
                 , context                       = case 
                                                   when pr_rec.skip_custom_processing is null then
                                                       context
                                                   else
                                                       'PWC'
                                                   end
                 , task_id                       = pr_rec.task_id                
                 , add_to_asset_id               = pr_rec.add_to_asset_id        
                 , amortize_flag                 = case 
                                                   when pr_rec.add_to_asset_id is null then
                                                       null
                                                   else
                                                       'YES'
                                                   end
            where  mass_addition_id = pr_rec.mass_addition_id;
        
            -- flag the record as complete
            set_status(
                pr_rec       => pr_rec,
                pv_status    => gcv_sts_completed
            );
        
            update_record_status(
                pr_rec => pr_rec
            );
        end if;
        log.end_method;
    exception
        when others then
            log.log_error('Failed',sqlerrm);
            log.end_method;
    end update_mass_addition;
    --==========================================================================================================================
    -- Name :         validate_mass_additions
    -- Description :  This module validates all data in the xxfa_mass_additions_api table. 
    --
    --==========================================================================================================================
    function validate_mass_additions(
        pn_run_id in number
    ) return boolean 
    is
        lb_return_val  boolean := true;

        function validate_mass_addition(
            pr_rec in out nocopy run_records_typ) 
        return boolean is
            ln_cnt                         pls_integer;
            lb_return_val                  boolean := true;
        begin
            log.set_method('validate_mass_addition');
            log.log_number('mass_addition_id',pr_rec.mass_addition_id);
            
            -- KFF Validation will ensure these are valid
            --pr_rec.asset_category_id
            --pr_rec.asset_key_ccid
            --pr_rec.payables_ccid
            --pr_rec.deprn_expense_ccid
            --pr_rec.location_id
            
            -- JAVA validator will ensure this is valid
            --pr_rec.deprn_method_code      
            
            -- Not updateable
            --pr_rec.book_type_code         
            
            
            if pr_rec.mass_addition_id is null then
                set_status(
                    pr_rec       => pr_rec,
                    pv_status    => gcv_sts_error,
                    pv_error_msg => 'WebADI cannot be used to create a new Mass Additions record.'
                );
            end if;

            if pr_rec.project_number is not null then
                begin
                    select pp.project_id
                    into   pr_rec.project_id
                    from   pa_projects_all pp
                    where  pp.segment1 = pr_rec.project_number;
                exception
                    when no_data_found then 
                        -- shouldn't be possible - LOV on the field shouldn't allow it
                        null;
                end;
                log.log_char('pr_rec.project_number',pr_rec.project_number);
                log.log_number('pr_rec.project_id',pr_rec.project_id);
            end if;
            
            if pr_rec.task_number is not null then
                begin
                    select pt.task_id
                    into   pr_rec.task_id
                    from   pa_tasks pt
                    where  pt.task_number = pr_rec.task_number;
                exception
                    when no_data_found then 
                        -- shouldn't be possible - LOV on the field shouldn't allow it
                        null;
                end;
                log.log_char('pr_rec.task_number',pr_rec.task_number);
                log.log_number('pr_rec.task_id',pr_rec.task_id);
            end if;
            
            pr_rec.skip_custom_processing := upper(substr(pr_rec.skip_custom_processing,1,1));
            log.log_char('pr_rec.skip_custom_processing',pr_rec.skip_custom_processing);
            
            if pr_rec.queue_name in ('SPLIT','MERGED') then
              pr_rec.posting_status := pr_rec.queue_name;
              pr_rec.queue_name     := 'NEW';
            else
              pr_rec.posting_status := pr_rec.queue_name;
            end if;

            set_status(
                pr_rec       => pr_rec,
                pv_status    => gcv_sts_update
            );
          
            update xxfa_mass_additions_adi
            set    status = pr_rec.status
                 , error_description = pr_rec.error_description
                 , project_id = pr_rec.project_id
                 , task_id = pr_rec.task_id
                 , skip_custom_processing = pr_rec.skip_custom_processing
                 , queue_name = pr_rec.queue_name
                 , posting_status = pr_rec.posting_status
            where  rowid = pr_rec.myrow;
      
            if pr_rec.status = gcv_sts_error then
                lb_return_val := false;
            end if;  
      
            log.end_method;
            return lb_return_val;
        exception
            when others then
                log.log_error('Failed',sqlerrm);
                log.end_method;
        end validate_mass_addition;
    begin
        log.set_method('validate_mass_additions');
    
        -- Loop through all records in xxfa_mass_additions_adi
        for rec in run_records(
            pn_run_id => pn_run_id,
            pv_status => gcv_sts_processing
        ) loop
            if not validate_mass_addition(rec) then
                lb_return_val := false;
            end if;     
        end loop;
    
        if not lb_return_val then
            log.log_error('Errors have occurred. Please review the log file and ensure all data in the CSV is valid',sqlerrm);
        end if;
    
        log.end_method;
        return lb_return_val;
    exception
        when others then
            log.log_error('ERROR in validate_mass_additions: '||dbms_utility.format_error_backtrace,sqlerrm);
            log.end_method;
            return false;
    end validate_mass_additions;

    --==========================================================================================================================
    -- Name :         import_mass_additions
    -- Description :  This module loads all valid mass_additions details from the xxfa_mass_additions_adi staging table into the 
    --                fa_mass_additions table.
    --
    --==========================================================================================================================
    procedure import_mass_additions(
        pn_run_id in number
    ) is
    begin
        log.set_method('import_mass_additions');
        log.log_number('pn_run_id',pn_run_id);
    
        -- updated records  
        declare
            ln_updated  pls_integer := 0;
        begin
            for datarec in run_records(
                pn_run_id => pn_run_id,
                pv_status => gcv_sts_update
            ) loop
              update_mass_addition(
                  pr_rec => datarec
              );
              if datarec.status != gcv_sts_error then
                ln_updated := ln_updated + 1;
              end if;
            end loop;
            log.log_message(ln_updated || ' mass addditions updated.',gci_statement);
        end;
    
        log.end_method;
    exception
        when others then
            log.log_error('ERROR in import_mass_additions: '||sqlerrm || dbms_utility.format_error_backtrace,sqlerrm);
            log.end_method;
            raise;
    end import_mass_additions;
    --==========================================================================================================================
    -- Name :         MAIN_BANKS
    -- Description :  Called by the PWC Mass Additions ADI. Removes records associated with prior runs; performs validation 
    --                before finally importing. Error reporting is done via ADI.
    --
    --==========================================================================================================================
    procedure update_mass_additions(
        p_run_id in number
    ) is    
        cursor error_recs is
            select rowid myrow, error_description
            from   xxfa_mass_additions_adi
            where  run_id = p_run_id
            and    status = gcv_sts_error;
        subtype error_rec is error_recs%rowtype;
        type error_rec_table is table of error_rec;
        lar_errors error_rec_table := new error_rec_table();    
    begin
        log.set_method('update_mass_additions');
        log.log_number('p_run_id',p_run_id);
        -- Delete all records that aren't "New" from the ADI.
        delete from xxfa_mass_additions_adi 
        where  nvl(run_id, -1) != p_run_id
        or    (run_id = p_run_id and status != gcv_sts_new);
        log.log_message('Deleted '||sql%rowcount||' old records from xxfa_mass_additions_adi',gci_statement);
        /*
        || Flag all (N)ew records for this run id as in (P)rocess
        */
        update xxfa_mass_additions_adi
        set    status = gcv_sts_processing
        where  status = gcv_sts_new
        and    run_id = p_run_id;
        log.log_message('Flagged '||sql%rowcount||' records in xxfa_mass_additions_adi as In Process ['||p_run_id||'].',gci_statement);
        commit;
    
        -- Finally! Validate  
        if validate_mass_additions(
            pn_run_id => p_run_id
        ) then
            log.log_message('Records passed validation..',gci_statement);
            import_mass_additions (
                pn_run_id => p_run_id
            );
        else
            log.log_message('Records failed validation..',gci_statement);
        end if;

        open error_recs;
        fetch error_recs bulk collect into lar_errors;
        close error_recs;
    
        if lar_errors.count > 0 then 
            /*
            || Errors exist - rollback
            */
            log.log_message('Errors found - will need to roll back any API calls',gci_statement);
            rollback;
            /*
            || This has the net effect of undoing the error flags on our records - so re-apply them
            */
            forall i in indices of lar_errors 
                update xxfa_mass_additions_adi
                set    status = gcv_sts_error
                     , error_description = lar_errors(i).error_description
                where rowid = lar_errors(i).myrow; 
        end if;
        
        log.end_method;
    exception
        when others then
            log.log_error('Failed',sqlerrm);
            rollback;
            declare
                lv_err_msg varchar2(2000) := substr(sqlerrm||dbms_utility.format_error_backtrace,1,2000);
            begin
                update xxfa_mass_additions_adi s
                set    error_description = lv_err_msg 
                     , status = gcv_sts_error
                where  run_id = p_run_id;
            end;
            log.end_method;
    end update_mass_additions;
end xxfa_mass_additions_adis;
/


