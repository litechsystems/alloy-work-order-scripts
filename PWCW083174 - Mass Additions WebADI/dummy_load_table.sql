create table xxfa_mass_load(
    mass_addition_id               number
  , asset_id                       number
  , asset_number                   varchar2(15)
  , description                    varchar2(80)
  , major_category                 varchar2(30)
  , minor_category                 varchar2(30)
  , serial_number                  varchar2(35)
  , book_type_code                 varchar2(15)
  , date_placed_in_service         date
  , fixed_assets_cost              number
  , payables_cost                  number
  , fixed_assets_units             number
  , payables_units                 number
  , payables_code_combination_id   number
  , expense_code_combination       varchar2(250)
  , expense_code_combination_id    number
  , feeder_system_name             varchar2(40)
  , posting_status                 varchar2(15)
  , queue_name                     varchar2(15)
  , depreciate_flag                varchar2(3)
  , asset_key_ccid                 varchar2(15)
  , asset_type                     varchar2(11)
  , accounting_date                date
  , asset_category_id              number
  , asset_region                   varchar2(30)
  , asset_suburb                   varchar2(30)
  , site_location                  varchar2(30)
  , location_id                    number
  , category_life                  varchar2(30)
  , lot                            varchar2(30)
  , asset_key                      varchar2(30)
)
--  
--  
--  , 
--  
--fa_additions
--/
--select * from fa_mass_additions
--/
--select * from fa_categories
--/
--select * from fa_books
--/
--select * from fa_locations
/
select * from xxfa_mass_load
/
begin
    for datarec in (
        select fcb.asset_clearing_account_ccid payables_code_combination_id
             , (select gcc.code_combination_id
                from   gl_code_combinations_kfv gcc
                where  gcc.enabled_flag = 'Y'
                and    trunc(sysdate) between trunc(nvl(gcc.start_date_active, sysdate))
                                          and trunc(nvl(gcc.end_date_active,   sysdate))
                and    gcc.concatenated_segments = xm.expense_code_combination
               ) expense_code_combination_id
             , (select fak.code_combination_id
                from   fa_asset_keywords fak
                where  fak.enabled_flag = 'Y'
                and    trunc(sysdate) between trunc(nvl(fak.start_date_active, sysdate))
                                          and trunc(nvl(fak.end_date_active,   sysdate))
                and    fak.segment1 = xm.asset_key
               ) asset_key_ccid
             , sysdate accounting_date
             , fc.category_id asset_category_id
             , fl.location_id
             , xm.rowid
        from   xxfa_mass_load xm
             , fa_categories fc
             , fa_category_books fcb
             , fa_locations fl
        where  xm.major_category = fc.segment1
        and    xm.minor_category = fc.segment2
        and    xm.category_life = fc.segment3
        and    fc.category_id = fcb.category_id
        and    xm.book_type_code = fcb.book_type_code
        and    xm.asset_suburb = fl.segment1(+)
        and    xm.site_location = fl.segment2(+)
        and    xm.asset_region = fl.segment3(+)
    ) loop
        update xxfa_mass_load
        set    payables_code_combination_id = datarec.payables_code_combination_id
             , expense_code_combination_id = datarec.expense_code_combination_id
             , asset_key_ccid = datarec.asset_key_ccid
             , accounting_date = datarec.accounting_date
             , asset_category_id = datarec.asset_category_id
             , location_id = datarec.location_id
        where  rowid = datarec.rowid;
    end loop;
end;
/
insert into fa_mass_additions(
    mass_addition_id
  , asset_id
  , asset_number
  , description
  , serial_number
  , book_type_code
  , date_placed_in_service
  , fixed_assets_cost
  , payables_cost
  , fixed_assets_units
  , payables_units
  , payables_code_combination_id
  , expense_code_combination_id
  , feeder_system_name
  , posting_status
  , queue_name
  , depreciate_flag
  , asset_key_ccid
  , asset_type
  , accounting_date
  , asset_category_id
  , location_id
) 
select fa_mass_additions_s.nextval
     , asset_id
     , asset_number
     , description
     , serial_number
     , book_type_code
     , date_placed_in_service
     , fixed_assets_cost
     , payables_cost
     , fixed_assets_units
     , payables_units
     , payables_code_combination_id
     , expense_code_combination_id
     , feeder_system_name
     , posting_status
     , queue_name
     , depreciate_flag
     , asset_key_ccid
     , asset_type
     , accounting_date
     , asset_category_id
     , location_id
from   xxfa_mass_load
where  location_id is not null
and    expense_code_combination_id is not null
and    asset_Key_ccid is not null 
/

select * 
from   xxfa_mass_load
where  location_id is null
or     expense_code_combination_id is null
or     asset_Key_ccid is not null 
/
delete from fa_mass_additions where asset_number in (select asset_number from xxfa_mass_load)
/
update xxfa_mass_load set asset_key = 'REPLACEMENT' where asset_key = 'REPLACE'