set serveroutput on

declare
    lw_rowid rowid;
begin
    dbms_output.enable(null);
    dbms_output.put_line('Updating PROJECT_NUMBER where clause and field names');
    --
    -- Verify we can find the record to update
    --
    begin
        select bic.rowid
        into   lw_rowid
        from   bne_integrators_vl bi
             , bne_interfaces_b bf
             , bne_interface_cols_b bic
             , fnd_application fa
        where  bi.application_id = fa.application_id
        and    bf.integrator_app_id = bi.application_id
        and    bf.integrator_code = bi.integrator_code
        and    bic.application_id = bf.application_id
        and    bic.interface_code = bf.interface_code
        and    fa.application_short_name = 'XXFA'
        and    bi.user_name = 'PWC Mass Additions'
        and    bic.interface_col_name = 'PROJECT_NUMBER'
        for update of bic.val_addl_w_c;
    exception
        when no_data_found then
            raise_application_error(-20001,'Unable to locate the PROJECT_NUMBER field in the PWC Mass Additions WebAdI');
    end;
    --
    -- Update the where clause
    --
    begin
        update bne_interface_cols_b
        set    val_id_col = 'SEGMENT1'
             , val_mean_col = 'SEGMENT1'
             , val_desc_col = 'NAME'
             , val_addl_w_c = '1 = 1 ORDER BY 1'
        where  rowid = lw_rowid;
        if sql%rowcount = 0 then 
            -- shouldn't be possible
            raise no_data_found;
        end if;
    exception
        when no_data_found then
            rollback;
            raise_application_error(-20001,'Failed to find the PROJECT_NUMBER record to update in the PWC Mass Additions WebAdI');
        when others then
            rollback;
            raise_application_error(-20001,'Failed updating the where clause for the PROJECT_NUMBER field in the PWC Mass Additions WebAdI',true);
    end;
    --
    -- If we get here, we were successful
    --
    dbms_output.put_line('Success');
    commit;
end;
/
