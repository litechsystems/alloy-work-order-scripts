/*
|| Copy archived lines into interface table
*/
insert into ap_invoice_lines_interface
select 1249874 INVOICE_ID
     , INVOICE_LINE_ID
     , LINE_NUMBER
     , LINE_TYPE_LOOKUP_CODE
     , LINE_GROUP_NUMBER
     , AMOUNT
     , ACCOUNTING_DATE
     , DESCRIPTION
     , AMOUNT_INCLUDES_TAX_FLAG
     , PRORATE_ACROSS_FLAG
     , TAX_CODE
     , FINAL_MATCH_FLAG
     , PO_HEADER_ID
     , PO_NUMBER
     , PO_LINE_ID
     , PO_LINE_NUMBER
     , PO_LINE_LOCATION_ID
     , PO_SHIPMENT_NUM
     , PO_DISTRIBUTION_ID
     , PO_DISTRIBUTION_NUM
     , PO_UNIT_OF_MEASURE
     , INVENTORY_ITEM_ID
     , ITEM_DESCRIPTION
     , QUANTITY_INVOICED
     , SHIP_TO_LOCATION_CODE
     , UNIT_PRICE
     , DISTRIBUTION_SET_ID
     , DISTRIBUTION_SET_NAME
     , DIST_CODE_CONCATENATED
     , DIST_CODE_COMBINATION_ID
     , AWT_GROUP_ID
     , AWT_GROUP_NAME
     , LAST_UPDATED_BY
     , LAST_UPDATE_DATE
     , LAST_UPDATE_LOGIN
     , CREATED_BY
     , CREATION_DATE
     , ATTRIBUTE_CATEGORY
     , ATTRIBUTE1
     , ATTRIBUTE2
     , ATTRIBUTE3
     , ATTRIBUTE4
     , ATTRIBUTE5
     , ATTRIBUTE6
     , ATTRIBUTE7
     , ATTRIBUTE8
     , ATTRIBUTE9
     , ATTRIBUTE10
     , ATTRIBUTE11
     , ATTRIBUTE12
     , ATTRIBUTE13
     , ATTRIBUTE14
     , ATTRIBUTE15
     , GLOBAL_ATTRIBUTE_CATEGORY
     , GLOBAL_ATTRIBUTE1
     , GLOBAL_ATTRIBUTE2
     , GLOBAL_ATTRIBUTE3
     , GLOBAL_ATTRIBUTE4
     , GLOBAL_ATTRIBUTE5
     , GLOBAL_ATTRIBUTE6
     , GLOBAL_ATTRIBUTE7
     , GLOBAL_ATTRIBUTE8
     , GLOBAL_ATTRIBUTE9
     , GLOBAL_ATTRIBUTE10
     , GLOBAL_ATTRIBUTE11
     , GLOBAL_ATTRIBUTE12
     , GLOBAL_ATTRIBUTE13
     , GLOBAL_ATTRIBUTE14
     , GLOBAL_ATTRIBUTE15
     , GLOBAL_ATTRIBUTE16
     , GLOBAL_ATTRIBUTE17
     , GLOBAL_ATTRIBUTE18
     , GLOBAL_ATTRIBUTE19
     , GLOBAL_ATTRIBUTE20
     , PO_RELEASE_ID
     , RELEASE_NUM
     , ACCOUNT_SEGMENT
     , BALANCING_SEGMENT
     , COST_CENTER_SEGMENT
     , PROJECT_ID
     , TASK_ID
     , EXPENDITURE_TYPE
     , EXPENDITURE_ITEM_DATE
     , EXPENDITURE_ORGANIZATION_ID
     , PROJECT_ACCOUNTING_CONTEXT
     , PA_ADDITION_FLAG
     , PA_QUANTITY
     , USSGL_TRANSACTION_CODE
     , STAT_AMOUNT
     , TYPE_1099
     , INCOME_TAX_REGION
     , ASSETS_TRACKING_FLAG
     , PRICE_CORRECTION_FLAG
     , ORG_ID
     , RECEIPT_NUMBER
     , RECEIPT_LINE_NUMBER
     , MATCH_OPTION
     , PACKING_SLIP
     , RCV_TRANSACTION_ID
     , PA_CC_AR_INVOICE_ID
     , PA_CC_AR_INVOICE_LINE_NUM
     , REFERENCE_1
     , REFERENCE_2
     , PA_CC_PROCESSED_CODE
     , TAX_RECOVERY_RATE
     , TAX_RECOVERY_OVERRIDE_FLAG
     , TAX_RECOVERABLE_FLAG
     , TAX_CODE_OVERRIDE_FLAG
     , TAX_CODE_ID
     , CREDIT_CARD_TRX_ID
     , AWARD_ID
     , VENDOR_ITEM_NUM
     , TAXABLE_FLAG
     , PRICE_CORRECT_INV_NUM
     , EXTERNAL_DOC_LINE_REF
     , SERIAL_NUMBER
     , MANUFACTURER
     , MODEL_NUMBER
     , WARRANTY_NUMBER
     , DEFERRED_ACCTG_FLAG
     , DEF_ACCTG_START_DATE
     , DEF_ACCTG_END_DATE
     , DEF_ACCTG_NUMBER_OF_PERIODS
     , DEF_ACCTG_PERIOD_TYPE
     , UNIT_OF_MEAS_LOOKUP_CODE
     , PRICE_CORRECT_INV_LINE_NUM
     , ASSET_BOOK_TYPE_CODE
     , ASSET_CATEGORY_ID
     , REQUESTER_ID
     , REQUESTER_FIRST_NAME
     , REQUESTER_LAST_NAME
     , REQUESTER_EMPLOYEE_NUM
     , APPLICATION_ID
     , PRODUCT_TABLE
     , REFERENCE_KEY1
     , REFERENCE_KEY2
     , REFERENCE_KEY3
     , REFERENCE_KEY4
     , REFERENCE_KEY5
     , PURCHASING_CATEGORY
     , PURCHASING_CATEGORY_ID
     , COST_FACTOR_ID
     , COST_FACTOR_NAME
     , CONTROL_AMOUNT
     , ASSESSABLE_VALUE
     , DEFAULT_DIST_CCID
     , PRIMARY_INTENDED_USE
     , SHIP_TO_LOCATION_ID
     , PRODUCT_TYPE
     , PRODUCT_CATEGORY
     , PRODUCT_FISC_CLASSIFICATION
     , USER_DEFINED_FISC_CLASS
     , TRX_BUSINESS_CATEGORY
     , TAX_REGIME_CODE
     , TAX
     , TAX_JURISDICTION_CODE
     , TAX_STATUS_CODE
     , TAX_RATE_ID
     , TAX_RATE_CODE
     , TAX_RATE
     , INCL_IN_TAXABLE_LINE_FLAG
     , SOURCE_APPLICATION_ID
     , SOURCE_ENTITY_CODE
     , SOURCE_EVENT_CLASS_CODE
     , SOURCE_TRX_ID
     , SOURCE_LINE_ID
     , SOURCE_TRX_LEVEL_TYPE
     , TAX_CLASSIFICATION_CODE
     , CC_REVERSAL_FLAG
     , COMPANY_PREPAID_INVOICE_ID
     , EXPENSE_GROUP
     , JUSTIFICATION
     , MERCHANT_DOCUMENT_NUMBER
     , MERCHANT_NAME
     , MERCHANT_REFERENCE
     , MERCHANT_TAX_REG_NUMBER
     , MERCHANT_TAXPAYER_ID
     , RECEIPT_CURRENCY_CODE
     , RECEIPT_CONVERSION_RATE
     , RECEIPT_CURRENCY_AMOUNT
     , COUNTRY_OF_SUPPLY
     , PAY_AWT_GROUP_ID
     , PAY_AWT_GROUP_NAME
     , EXPENSE_START_DATE
     , EXPENSE_END_DATE
from   apps.pwcw083693 pw
where  not exists (
           select 'already copied'
           from   ap_invoice_lines_interface aili
           where  pw.invoice_line_id = aili.invoice_line_id)
/
/*
|| Clear processing status from parent invoice
|| Fix the vendor site id value and accts_pay_code_combination_id
|| Correct the payment terms id
*/
update ap_invoices_interface ai
set    ai.status = null 
     , ai.vendor_site_id = (
          select vendor_site_id
          from   apps.ap_supplier_sites_all pvs
          where  pvs.vendor_id = ai.vendor_id
          and    pvs.vendor_site_code = ai.vendor_site_code
          and    pvs.org_id = ai.org_id
       )
     , ai.accts_pay_code_combination_id = (
          select accts_pay_code_combination_id
          from   apps.ap_supplier_sites_all pvs
          where  pvs.vendor_id = ai.vendor_id
          and    pvs.vendor_site_code = ai.vendor_site_code
          and    pvs.org_id = ai.org_id
       )
     , ai.terms_id = (
          select tm.term_id
          from   ap_terms tm
          where  tm.name = ai.terms_name
       )
where  invoice_id = 1249874
/

commit
/