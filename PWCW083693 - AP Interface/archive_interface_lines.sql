-- Backup the records from the interface table
create table PWCW083693 as 
select * from ap_invoice_lines_interface
where invoice_line_id in (1404120,1404121)
/

-- remove them from the interface table
delete from ap_invoice_lines_interface
where invoice_line_id in (1404120,1404121)
/

--
commit
